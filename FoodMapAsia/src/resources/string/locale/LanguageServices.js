import I18n from 'react-native-i18n';
import en from './en';
import vi from './vi';
import moment from 'moment';
import 'moment/locale/vi';

I18n.fallbacks = true;
I18n.defaultLocale = 'vi';
I18n.translations = {
  en: {
    ...en,
  },
  vi: {
    ...vi,
  },
};

moment.locale('vi');

export const setUpLanguage = async (language) => {
  if (language) {
    I18n.locale = language;
    moment.locale(language);
  }
};

export const switchLanguage = async (
  language,
  component = null,
  screenProps = null,
) => {
  if (I18n.locale === language) {
    return;
  }
  screenProps && screenProps.setLocale(language);
  component && component.forceUpdate();
  I18n.locale = language;
  moment.locale(language);
};

export default I18n;
