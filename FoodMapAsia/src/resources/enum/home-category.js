import {} from '../string/strings';
import {
  ICON_HOME_VEGETABLE,
  ICON_HOME_COFFEE,
  ICON_HOME_DRINK,
  ICON_HOME_DRY_FOOD,
  ICON_HOME_MEAT,
  ICON_EGG,
  ICON_FISH,
  ICON_MEAT,
  ICON_MUSHROOMS,
  ICON_VEGETABLE,
  ICON_ALCOHOL,
  ICON_CACAO,
  ICON_CANNED_FOOD,
  ICON_CHEESE,
  ICON_FRUIT_JUICE,
  ICON_MILK,
  ICON_CUP,
  ICON_COFFEE,
  ICON_FRUIT,
  ICON_SPICE,
} from '@resources/images';
import {ICON_SEED} from '../images';

export const homeCategoryEnum = {
  'rau-cu-1': {
    svgIcon: ICON_HOME_VEGETABLE,
    title: 'Rau củ quả',
  },
  'thit-trung': {
    svgIcon: ICON_HOME_MEAT,
    title: 'Thịt trứng',
  },
  'tra-ca-phe': {
    svgIcon: ICON_HOME_COFFEE,
    title: 'Trà cà phê',
  },
  'do-kho': {
    svgIcon: ICON_HOME_DRY_FOOD,
    title: 'Đồ khô',
  },
  'do-uong-1': {
    svgIcon: ICON_HOME_DRINK,
    title: 'Đồ uống',
  },
  'na-m-da-u-hu': {
    svgIcon: ICON_MUSHROOMS,
    title: 'Nấm - Đậu hủ',
  },
  'tra-i-cay': {
    svgIcon: ICON_FRUIT,
    title: 'Trái cây',
  },
  'rau-cu': {
    svgIcon: ICON_VEGETABLE,
    title: 'Rau củ',
  },
  'thi-t': {
    svgIcon: ICON_MEAT,
    title: 'Thịt',
  },
  'thu-y-ha-i-sa-n': {
    svgIcon: ICON_FISH,
    title: 'Thuỷ hải sản',
  },
  'tru-ng': {
    svgIcon: ICON_EGG,
    title: 'Trứng',
  },
  tra: {
    svgIcon: ICON_CUP,
    title: 'Trà',
  },
  cafe: {
    svgIcon: ICON_COFFEE,
    title: 'Cà phê',
  },
  cacao: {
    svgIcon: ICON_CACAO,
    title: 'Ca cao',
  },
  'gia-vi-nguyen-lie-u-na-u-an': {
    svgIcon: ICON_SPICE,
    title: 'Gia vị, nguyên liệu nấu ăn',
  },
  'bo-pho-mai-kem': {
    svgIcon: ICON_CHEESE,
    title: 'Bơ, phô mai, kem',
  },
  'do-ho-p': {
    svgIcon: ICON_CANNED_FOOD,
    title: 'Đồ hộp',
  },
  'su-a-va-ca-c-sa-n-pha-m-tu-su-a': {
    svgIcon: ICON_MILK,
    title: 'Sữa và các sản phẩm từ sữa',
  },
  'bia-ruo-u': {
    svgIcon: ICON_ALCOHOL,
    title: 'Bia rượu',
  },
  'nuo-c-tra-i-cay': {
    svgIcon: ICON_FRUIT_JUICE,
    title: 'Nước trái cây',
  },

  'rau-cu-qua': {
    svgIcon: ICON_HOME_VEGETABLE,
    title: 'Rau củ quả',
  },
  'nuo-c-tra-i-cay': {
    svgIcon: ICON_FRUIT_JUICE,
    title: 'Nước trái cây',
  },
  'tra-ca-phe-cacao': {
    svgIcon: ICON_HOME_COFFEE,
    title: 'Trà - Cà Phê - Ca Cao',
  },
  'do-uong': {
    svgIcon: ICON_HOME_DRINK,
    title: 'Đồ uống',
  },
};

export const childCategoryEnum = {
  'rau-cu': {
    svgIcon: ICON_VEGETABLE,
    title: 'Rau củ',
  },

  'trai-cay': {
    svgIcon: ICON_FRUIT,
    title: 'Trái cây',
  },
  'nam-dau-hu': {
    svgIcon: ICON_MUSHROOMS,
    title: 'Nấm-Đậu hủ',
  },
  thit: {
    svgIcon: ICON_MEAT,
    title: 'Thịt',
  },
  'thuy-hai-san': {
    svgIcon: ICON_FISH,
    title: 'Thuỷ hải sản',
  },
  trung: {
    svgIcon: ICON_EGG,
    title: 'Trứng',
  },
  tra: {
    svgIcon: ICON_CUP,
    title: 'Trà',
  },
  'ca-phe': {
    svgIcon: ICON_COFFEE,
    title: 'Cà phê',
  },
  cacao: {
    svgIcon: ICON_CACAO,
    title: 'Cacao',
  },
  'gia-vi-nguyen-lieu-nau-an': {
    svgIcon: ICON_SPICE,
    title: 'Gia vị-Nguyên liệu nấu ăn',
  },
  'bo-pho-mai-kem': {
    svgIcon: ICON_CHEESE,
    title: 'Bơ-Phô mai-Kem',
  },
  'ngu-coc-dau-hat': {
    svgIcon: ICON_SEED,
    title: 'Ngũ cốc-Đậu-Hạt',
  },
  'do-hop': {
    svgIcon: ICON_CANNED_FOOD,
    title: 'Đồ hộp',
  },
  'sua-va-cac-san-pham-tu-sua': {
    svgIcon: ICON_MILK,
    title: 'Sữa và các sản phẩm từ sữa',
  },
  'bia-ruou': {
    svgIcon: ICON_ALCOHOL,
    title: 'Bia rượu',
  },
  'nuoc-trai-cay': {
    svgIcon: ICON_FRUIT_JUICE,
    title: 'Nước trái cây',
  },
};
