import {} from '../string/strings';
import {
  ICON_EGG,
  ICON_FISH,
  ICON_MEAT,
  ICON_MUSHROOMS,
  ICON_VEGETABLE,
  ICON_ALCOHOL,
  ICON_CACAO,
  ICON_CANNED_FOOD,
  ICON_CHEESE,
  ICON_FRUIT_JUICE,
  ICON_MILK,
  ICON_CUP,
  ICON_COFFEE,
  ICON_FRUIT,
  ICON_SPICE,
  ICON_HOME_VEGETABLE,
  ICON_HOME_COFFEE,
  ICON_HOME_DRINK,
  ICON_HOME_DRY_FOOD,
  ICON_HOME_MEAT,
} from '@resources/images';

export const deliveryStatusesData = {
  readytopick: 'chờ lấy hàng',
  picking: 'đang đi lấy',
  delivering: 'đang giao hàng',
  delivered: 'đã giao hàng',
  cancel: 'hủy giao hàng',
  return: 'chuyển hoàn',
  pending: 'chờ xử lý',
  notmeetcustomer: 'không gặp khách',
  waitingforreturn: 'chờ chuyển hoàn',
};

export const category = {
  'rau-cu-1': {
    svgIcon: ICON_HOME_VEGETABLE,
    title: 'Rau củ quả',
  },
  'thit-trung': {
    svgIcon: ICON_HOME_MEAT,
    title: 'Thịt trứng',
  },
  'tra-ca-phe': {
    svgIcon: ICON_HOME_COFFEE,
    title: 'Trà cà phê',
  },
  'do-kho': {
    svgIcon: ICON_HOME_DRY_FOOD,
    title: 'Đồ khô',
  },
  'do-uong-1': {
    svgIcon: ICON_HOME_DRINK,
    title: 'Đồ uống',
  },
  'na-m-da-u-hu': {
    svgIcon: ICON_MUSHROOMS,
    title: 'Nấm - Đậu hủ',
  },
  'tra-i-cay': {
    svgIcon: ICON_FRUIT,
    title: 'Trái cây',
  },
  'rau-cu': {
    svgIcon: ICON_VEGETABLE,
    title: 'Rau củ',
  },
  'thi-t': {
    svgIcon: ICON_MEAT,
    title: 'Thịt',
  },
  'thu-y-ha-i-sa-n': {
    svgIcon: ICON_FISH,
    title: 'Thuỷ hải sản',
  },
  'tru-ng': {
    svgIcon: ICON_EGG,
    title: 'Trứng',
  },
  tra: {
    svgIcon: ICON_CUP,
    title: 'Trà',
  },
  cafe: {
    svgIcon: ICON_COFFEE,
    title: 'Cà phê',
  },
  cacao: {
    svgIcon: ICON_CACAO,
    title: 'Ca cao',
  },
  'gia-vi-nguyen-lie-u-na-u-an': {
    svgIcon: ICON_SPICE,
    title: 'Gia vị, nguyên liệu nấu ăn',
  },
  'bo-pho-mai-kem': {
    svgIcon: ICON_CHEESE,
    title: 'Bơ, phô mai, kem',
  },
  'do-ho-p': {
    svgIcon: ICON_CANNED_FOOD,
    title: 'Đồ hộp',
  },
  'su-a-va-ca-c-sa-n-pha-m-tu-su-a': {
    svgIcon: ICON_MILK,
    title: 'Sữa và các sản phẩm từ sữa',
  },
  'bia-ruo-u': {
    svgIcon: ICON_ALCOHOL,
    title: 'Bia rượu',
  },
  'nuo-c-tra-i-cay': {
    svgIcon: ICON_FRUIT_JUICE,
    title: 'Nước trái cây',
  },
};
