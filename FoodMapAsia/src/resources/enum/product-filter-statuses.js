import {} from '../string/strings';

export const productFilterData = {
  'Sản phẩm nổi bật': {
    title: 'Sản phẩm nổi bật',
    key: 'manual',
  },
  'Giá tăng dần': {
    title: 'Giá tăng dần',
    key: 'price-ascending',
  },
  'Giá giảm dần': {
    title: 'Giá giảm dần',
    key: 'price-descending',
  },
  'Tên từ A-Z': {
    title: 'Tên từ A-Z',
    key: 'title-ascending',
  },
  'Tên từ Z-A': {
    title: 'Tên từ Z-A',
    key: 'title-descending',
  },
  'Cũ nhất': {
    title: 'Cũ nhất',
    key: 'created-ascending',
  },
  'Mới nhất': {
    title: 'Mới nhất',
    key: 'created-descending',
  },
  'Bán chạy nhất': {
    title: 'Bán chạy nhất',
    key: 'best-selling',
  },
};
