import {} from '../string/strings';

export const paymentStatusesData = {
  pending: 'Chưa thanh toán',
  partially_paid: 'Đã thanh toán một phần',
  paid: 'Đã thanh toán',
  partiallyrefunded: 'Đã thanh toán một phần',
  refunded: 'Đã hoàn tiền',
  voided: 'Đã huỷ',
};
