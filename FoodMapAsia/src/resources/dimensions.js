import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Dimensions} from 'react-native';

const {width, height} = Dimensions.get('window');

export const STATUS_BAR_HEIGHT = getStatusBarHeight();

export const BOTTOM_TABBAR_HEIGHT = 60;

export const MIN_SCREEN_HEIGHT = height - BOTTOM_TABBAR_HEIGHT;

export const WIDTH_SCREEN = width;
export const HEIGHT_SCREEN = height;
