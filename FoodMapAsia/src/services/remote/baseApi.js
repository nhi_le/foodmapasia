import axios from 'axios';

const api = axios.create({
  baseURL: 'https://choraucu.myharavan.com/',
  timeout: 10000,
  withCredentials: false,
});

const DEBUG = process.env.NODE_ENV === 'development';

axios.interceptors.request.use(
  (config) => {
    /** In dev, intercepts request and logs it into console for dev */
    if (DEBUG) {
      console.info('✉️ ', config);
    }
    return config;
  },
  (error) => {
    if (DEBUG) {
      console.error('✉️ ', error);
    }
    return Promise.reject(error);
  },
);

api.interceptors.response.use(
  (response) => {
    if (DEBUG) {
      console.info('✉️ ', response.data);
    }
    return response.data;
  },
  (error) => {
    if (DEBUG) {
    }
    if (error.response) {
      if (error.response.data) {
        return Promise.reject({
          code: error.response.data.Code,
          message: error.response.data.Message,
          ...error.response.data,
        });
      }
      return Promise.reject(error.response);
    }
    return Promise.reject(error);
  },
);

export function setAuthorizationToken(token) {
  api.defaults.headers.common.jwt = token;
}

export function getAuthorizationToken() {
  return api.defaults.headers.common.jwt;
}

export function removeAuthorizationToken() {
  delete api.defaults.headers.common.jwt;
}

export default api;
