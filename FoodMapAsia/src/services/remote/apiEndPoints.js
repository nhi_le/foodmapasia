//BASE API

const BASE_API = 'https://choraucu.myharavan.com/';
const BASE_API_WHIST_LIST = 'https://onapp.haravan.com/wishlist/frontend/api/';
const THEME_ID = '1000608813';

//PUT

//GET

const GET_USER_INFO =
  BASE_API + 'account?view=general_info_api&themeid=' + THEME_ID;
const LOGOUT_USER = BASE_API + 'account/logout';
const GET_SEARCH_PRODUCT =
  'search?q=filter=(title:product%20contains%20{0})&page={1}&view=search_api&themeid=' +
  THEME_ID;

const GET_CURRENT_CART =
  BASE_API + 'cart?view=current_cart_api&themeid=' + THEME_ID;
const GET_PRODUCT_DETAIL =
  'products/{Handle}?view=product_detail_api&themeid' + THEME_ID;
const GET_PRODUCT_RELEVANT =
  'products/{Handle}?view=related_products_api&themeid=' + THEME_ID;

const GET_PRODUCT_HTML =
  'products/{Handle}?view=product_description_api&themeid=' + THEME_ID;
const GET_PRODUCT_TAB_LIST =
  '/products/{Handle}?view=tablist_api&themeid' + THEME_ID;
const GET_ORDERS_HISTORY =
  BASE_API + 'account?view=order_list_api&themeid=' + THEME_ID;
const GET_LIST_ADDRESS =
  BASE_API + 'account/addresses?view=address_list_api&themeid=' + THEME_ID;
const GET_INFO_ACCOUNT =
  BASE_API + 'account?view=general_info_api&themeid=' + THEME_ID;

const GET_DETAIL_ORDER =
  'account/orders/{token}?view=order_detail_api&themeid' + THEME_ID;

const GET_LIST_CATEGORY_PRODUCT =
  'collections/{handle}?view=sub_collection_api&themeid=' + THEME_ID;

const GET_LIST_CATEGORY_HOME =
  'collections/all?view=root_collection_api&themeid=' + THEME_ID;

const GET_LIST_CATEGORY =
  'collections/all?view=main_collection_api&themeid=' + THEME_ID;

const GET_LIST_HOME_PRODUCT =
  'collections/{Handle}?view=collection_products_api&themeid=' +
  THEME_ID +
  '&page={PageNumber}';
const GET_LIST_PRODUCT =
  'collections/{handle}?view=collection_products_api&page={0}&sort_by={condition}&themeid=' +
  THEME_ID;

const GET_LIST_PRODUCT_FILTER =
  'collections/{collectionHandle}?view=collection_products_api&page={0}&sort_by={condition}&themeid=' +
  THEME_ID;

const GET_API_BLOG_LIST = 'blogs/all?view=blog_list_api';
const GET_API_ARTICLE_LIST = '/blogs/{Handle}?view=article_list_api';
const GET_DETAIL_ARTICLE =
  'blogs/{Handle1}/{Handle2}?view=article_detail_api&themeid=' + THEME_ID;
const GET_API_ARTICLE_HTML =
  'blogs/{Handle1}/{Handle2}?view=article_content_api&themeid=' + THEME_ID;
const GET_HOME_SLIDER = BASE_API + '?view=home_slider_api';

//POST

const ADD_TO_CART = BASE_API + 'cart/add.js';

const ADD_TO_FAVORITE = BASE_API_WHIST_LIST + 'likeproduct';
const GET_FAVORITE_LIST = BASE_API_WHIST_LIST + 'listproduct';
const REMOVE_FAVORITE_ITEM = BASE_API_WHIST_LIST + 'unlikeproduct';
const DELETE_ITEM_CART = BASE_API + 'cart/change.js';
const LOGIN = BASE_API + 'account/login?themeid=' + THEME_ID;
const FORGOT_PASSWORD = BASE_API + 'account/recover' + THEME_ID;
const REGISTER = BASE_API + 'account';
const UPDATE_CART = BASE_API + 'cart/update.js';
const ADD_NEW_ADDRESS = BASE_API + 'account/addresses?themeid=' + THEME_ID;
const UPDATE_ADDRESS = BASE_API + 'account/addresses/{id}';

const DELETE_ADDRESS = BASE_API + 'account/addresses/{AddressID}';

const GET_BROWSER_ID = BASE_API_WHIST_LIST + 'browserid';

//DELETE

export default {
  LOGIN,
  GET_USER_INFO,
  LOGOUT_USER,
  REGISTER,
  GET_LIST_CATEGORY,
  GET_PRODUCT_DETAIL,
  GET_SEARCH_PRODUCT,
  GET_PRODUCT_RELEVANT,
  GET_ORDERS_HISTORY,
  GET_LIST_ADDRESS,
  ADD_TO_CART,
  GET_CURRENT_CART,
  GET_PRODUCT_HTML,
  GET_INFO_ACCOUNT,
  GET_LIST_CATEGORY_PRODUCT,
  GET_LIST_PRODUCT,
  DELETE_ITEM_CART,
  GET_LIST_HOME_PRODUCT,
  UPDATE_CART,
  ADD_NEW_ADDRESS,
  DELETE_ADDRESS,
  GET_LIST_PRODUCT_FILTER,
  GET_LIST_CATEGORY_HOME,
  UPDATE_ADDRESS,
  GET_API_BLOG_LIST,
  GET_DETAIL_ARTICLE,
  GET_API_ARTICLE_LIST,
  GET_BROWSER_ID,
  ADD_TO_FAVORITE,
  GET_FAVORITE_LIST,
  REMOVE_FAVORITE_ITEM,
  GET_API_ARTICLE_HTML,
  GET_HOME_SLIDER,
  GET_PRODUCT_TAB_LIST,
  FORGOT_PASSWORD,
  GET_DETAIL_ORDER,
};
