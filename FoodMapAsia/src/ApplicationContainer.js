import React, {Component} from 'react';
import {Linking} from 'react-native';
import {connect} from 'react-redux';
import AuthorizedContainer from './areas/AuthorizedNavigator';
import LoadingPage from './areas/splash/LoadingPage';
import authTokens from './redux/auth-token';
import {StyleSheet, ActivityIndicator, View} from 'react-native';
import {ACCENT_COLOR} from './resources/palette';
import operations from '@redux/app/operations';
import authOperations from '@redux/auth/operations';
import YesNoModal from './common/components/modal/YesNoModal';
import {
  CANCEL,
  BUTTON_OK,
  TITLE_PERMISSION_MODAL,
  DESCRIPTION_PERMISSION_MODAL,
} from './resources/string/strings';
import {renderText} from './common/components/StringHelper';

const mapStateToProps = (state) => ({
  isAuthorized: state.auth.isAuthorized,
  isConnected: state.network.isConnected,
  isLoading: state.app.isLoading,
  isPermissionModalVisible: state.app.isPermissionModalVisible,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  actions: {
    setUser: (user) => dispatch(authOperations.setUser(user)),
    setAuthorized: (isAuthorized) =>
      dispatch(authOperations.setAuthorized(isAuthorized)),
  },
});

class ApplicationContainer extends Component {
  constructor() {
    super();
    this.state = {
      isConnected: true,
      isAuthorized: false,
    };
  }

  async UNSAFE_componentWillReceiveProps(props) {
    if (!props.isConnected && this.state.isConnected) {
      this.setState({isConnected: false, isAuthorized: false});
    } else if (props.isConnected && !this.state.isConnected) {
      this.setState({isConnected: true, isAuthorized: false});
    }
  }

  async UNSAFE_componentWillMount() {
    const user = await authTokens.getUser();
    if (user) {
      this.props.actions.setUser(user);
    } else {
      this.props.actions.setAuthorized(false);
    }
  }

  componentDidMount() {}

  componentWillUnmount() {}

  renderLoading = () => {
    return (
      <View style={styles.loadingContainer}>
        <View style={styles.indicatorContainer}>
          <ActivityIndicator
            style={styles.indicator}
            size="large"
            animating={true}
            color={ACCENT_COLOR}
          />
        </View>
      </View>
    );
  };

  renderPermissionModal = (isPermissionModalVisible) => {
    return (
      <YesNoModal
        title={renderText(TITLE_PERMISSION_MODAL)}
        description={renderText(DESCRIPTION_PERMISSION_MODAL)}
        isVisible={isPermissionModalVisible}
        noButton={{
          text: renderText(CANCEL),
          onPress: async () => {
            this.props.actions.setPermissionModalVisible(false);
          },
        }}
        yesButton={{
          text: renderText(BUTTON_OK),
          onPress: async () => {
            this.props.actions.setPermissionModalVisible(false);
            Linking.openSettings();
          },
          gradient: true,
        }}
        onBackButtonPress={() => {
          this.props.actions.setPermissionModalVisible(false);
        }}
        onBackdropPress={() => {
          this.props.actions.setPermissionModalVisible(false);
        }}
      />
    );
  };

  renderActiveView(state) {
    let status;
    if (state.user) {
      status = state.vendor.status;
    }

    if (!state.ready || state.isAuthorized === undefined) {
      return <LoadingPage />;
    } else {
      return (
        <AuthorizedContainer
          ref={(ref) => (this.navigator = ref)}
          key="activeView"
        />
      );
    }
  }

  render() {
    const {isLoading, isPermissionModalVisible} = this.props;
    return [
      this.renderActiveView(this.props),
      isLoading && this.renderLoading(),
      this.renderPermissionModal(isPermissionModalVisible),
    ];
  }
}

const styles = StyleSheet.create({
  container: {},
  loadingContainer: {
    ...StyleSheet.absoluteFill,
    alignItems: 'center',
    justifyContent: 'center',
  },
  indicatorContainer: {
    borderRadius: 10,
    minWidth: 80,
    minHeight: 80,
    backgroundColor: '#000D',
    alignItems: 'center',
    justifyContent: 'center',
  },
  indicator: {},
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ApplicationContainer);
