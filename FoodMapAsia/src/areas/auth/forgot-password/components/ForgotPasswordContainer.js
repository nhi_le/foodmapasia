import React from 'react';
import {View, StyleSheet} from 'react-native';
import ForgotPasswordHeader from './ForgotPasswordHeader';
import ForgotPasswordBody from './ForgotPasswordBody';

const ForgotPasswordContainer = ({header, body, footer}) => {
  return (
    <View style={styles.container}>
      <ForgotPasswordHeader style={styles.header} {...header} />
      <ForgotPasswordBody style={styles.body} {...body} />
      {/* <ForgotPasswordFooter style={styles.footer} {...footer} /> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {},
  body: {
    flex: 1,
    marginHorizontal: 20,
  },
  footer: {},
});

export default ForgotPasswordContainer;
