import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import Title from '@common/components/text/Title';
import SvgIcon from '@common/components/icon/SvgIcon';
import {TEXT_DARK_COLOR} from '@resources/palette';
import {ICON_CANCEL} from '@resources/images';
import {LIGHT_GRAY_COLOR} from '@resources/palette';

const ForgotPasswordHeader = ({style, title, backButton}) => {
  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <TouchableOpacity style={styles.icon} onPress={backButton.onPress}>
        <SvgIcon height={18} width={18} {...backButton} />
      </TouchableOpacity>
      <Title style={styles.titleToolbar} size="medium-title" text={title}>
        {title}
      </Title>
      <TouchableOpacity style={styles.icon} onPress={backButton.onPress}>
        <SvgIcon height={16} width={16} svgIcon={ICON_CANCEL} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 70,
    alignContent: 'center',
    flexDirection: 'row',
    paddingTop: 20,
    borderBottomColor: LIGHT_GRAY_COLOR,
    borderBottomWidth: 1,
    alignItems: 'center',
  },
  titleToolbar: {
    flex: 1,
    color: TEXT_DARK_COLOR,
    textAlign: 'center',
    fontWeight: 'normal',
  },
  icon: {
    width: 60,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    flex: 1,
  },
});

export default ForgotPasswordHeader;
