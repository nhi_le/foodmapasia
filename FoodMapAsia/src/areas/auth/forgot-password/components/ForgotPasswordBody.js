import React from 'react';
import {View, StyleSheet} from 'react-native';
import Button from '@common/components/button/Button';
import TransparentButton from '@common/components/button/Button';
import Input from '@common/components/input/Input';
import TextContent from '@common/components/text/TextContent';
import OutlineButton from '@common/components/button/OutlineButton';
import PasswordInput from '@common/components/input/PasswordInput';
import {WebView} from 'react-native-webview';

import {CONTENT_REGISTER} from '@resources/string/strings';

import {
  TEXT_DARK_COLOR,
  TEXT_GRAY_LIGHT_COLOR,
  GRAY_COLOR,
  BUTTON_LOGIN_COLOR,
  BUTTON_FACEBOOK_COLOR,
  BUTTON_GOOGLE_COLOR,
} from '@resources/palette';

const ForgotPasswordBody = ({
  style,
  form,
  buttonSubmit,
  contentForgot,
  forgotPasswordWebView,
  formSignInValidate,
}) => {
  let js = `
    var originalPostMessage = window.postMessage;
    var patchedPostMessage = function (message, targetOrigin, transfer) { originalPostMessage(message, targetOrigin, transfer);};
    patchedPostMessage.toString = function () {
    return String(Object.hasOwnProperty).replace('hasOwnProperty', 'postMessage'); };
    window.postMessage = patchedPostMessage; setTimeout(() => {
    var recaptcha = document.querySelectorAll('[name="g-recaptcha-response"]'); if (recaptcha && recaptcha.length > 0) {
    window.ReactNativeWebView.postMessage(recaptcha[0].value); }
    }, 2000);
    const patchPostMessageJsCode = '(' + String(patchPostMessageFunction) + ')();';
`;

  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <TextContent style={styles.conttentText}>{contentForgot}</TextContent>
      <Input
        {...form.email}
        inputStyle={styles.inputForm}
        errorMessage={formSignInValidate.email}
      />

      <Button
        buttonStyle={{backgroundColor: BUTTON_LOGIN_COLOR, borderRadius: 22}}
        textStyle={styles.btnLoginText}
        style={styles.buttonLogin}
        {...buttonSubmit}
      />

      <WebView
        style={{width: 0, height: 0}}
        injectedJavaScript={js}
        {...forgotPasswordWebView}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  titleWelcome: {
    color: TEXT_DARK_COLOR,
  },
  hintLoginEmail: {
    color: TEXT_GRAY_LIGHT_COLOR,
    marginTop: 20,
    marginBottom: 50,
  },
  password: {
    marginTop: 20,
  },
  inputEmptyStyle: {
    borderRadius: 10,
    backgroundColor: GRAY_COLOR,
  },
  buttonForgot: {
    justifyContent: 'flex-end',
    position: 'absolute',
    width: 200,
    height: 50,
    zIndex: 1,
    backgroundColor: 'white',
  },
  inputForm: {
    color: TEXT_GRAY_LIGHT_COLOR,
    borderRadius: 22,
    borderColor: GRAY_COLOR,
    backgroundColor: 'white',
    height: 40,
  },
  buttonLogin: {
    width: 200,
    marginTop: 50,
    justifyContent: 'center',
    alignSelf: 'center',
  },
  buttonRegister: {
    width: 200,
    marginTop: 30,
    justifyContent: 'center',
    alignSelf: 'center',
  },
  btnRegisterTextStyle: {
    color: BUTTON_LOGIN_COLOR,
    fontWeight: 'bold',
  },
  buttonFb: {
    width: 180,
    marginTop: 30,
    justifyContent: 'center',
    alignSelf: 'center',
  },
  btnFacebookTextStyle: {
    color: BUTTON_FACEBOOK_COLOR,
    fontWeight: 'bold',
  },
  btnGoogleTextStyle: {
    color: BUTTON_GOOGLE_COLOR,
    fontWeight: 'bold',
  },
  buttonGg: {
    width: 180,
    marginTop: 30,
    justifyContent: 'center',
    alignSelf: 'center',
  },
  conttentText: {
    alignSelf: 'center',
    marginTop: 50,
    textAlign: 'center',
    marginBottom: 20,
    color: TEXT_GRAY_LIGHT_COLOR,
  },
  outLineButton: {
    borderColor: BUTTON_LOGIN_COLOR,
    borderRadius: 22,
  },
  btnLoginText: {
    color: 'white',
    fontWeight: 'bold',
  },
  HoriView: {
    justifyContent: 'space-between',
  },
});

export default ForgotPasswordBody;
