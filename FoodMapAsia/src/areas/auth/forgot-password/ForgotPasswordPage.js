import React, {Component} from 'react';
import {connect} from 'react-redux';
import BackgroundTemplate from '@common/components/background/BackgroundTemplate';
import operations from '@redux/auth/operations';
import {ICON_CANCEL} from '@resources/images';
import ForgotPasswordContainer from './components/ForgotPasswordContainer';
import {HOLDER_EMAIL} from '../../../resources/string/strings';
import {ICON_BACK_SVG} from '../../../resources/images';

let wedViewRef = null;

const mapStateToProps = (state) => ({
  isAuthorized: state.auth.isAuthorized,
  formSignInValidate: state.auth.formSignInValidate,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  header: {
    title: 'Khôi Phục Lại Mật Khẩu',
    cancelButton: {
      svgIcon: ICON_CANCEL,
      onPress: async () => {},
    },
    backButton: {
      svgIcon: ICON_BACK_SVG,
      onPress: async () => {
        ownProps.navigation.goBack();
      },
    },
  },
  body: {
    form: {
      email: {
        placeholder: HOLDER_EMAIL,
        keyboardType: 'email-address',
        returnKeyType: 'next',
        autoCapitalize: 'none',
        onSubmitEditing: () => {},
        blurOnSubmit: false,
        onChangeText: (text) => {
          dispatch(operations.updateLoginForm('email', text));
        },
      },
    },
    contentForgot:
      ' Nhập email của bạn để chúng tôi hỗ trợ khôi phục lại mật khẩu',
    buttonSubmit: {
      text: 'Gửi',
      onPress: async () => {
        const a = await dispatch(operations.onForgotPassword());
      },
    },
    forgotPasswordWebView: {
      sharedCookiesEnabled: true,
      scrollEnabled: true,
      javaScriptEnabled: true,
      domStorageEnabled: true,
      source: {
        uri: 'https://choraucu.myharavan.com/account/login?themeid=1000608813',
      },
      ref: (ref) => {
        wedViewRef = ref;
      },
      onNavigationStateChange: async (navigationState) => {},
      onMessage: async (event) => {
        dispatch(
          operations.updateLoginForm('recaptcha', event.nativeEvent.data),
        );
      },
    },
  },
  footer: {},
  actions: {
    refreshLoginForm: () => dispatch(operations.refreshLoginForm()),
  },
});

class ForgotPasswordPage extends Component {
  constructor() {
    super();
  }

  componentDidMount() {
    this.walletListener = this.props.navigation.addListener('willFocus', () => {
      if (wedViewRef) {
        wedViewRef.reload();
      }
    });
  }

  render() {
    const {
      header,
      body,
      footer,
      login,
      actions,
      formSignInValidate,
    } = this.props;
    return (
      <BackgroundTemplate
        scrollable={false}
        fullscreen={true}
        onInitialized={() => {
          actions.refreshLoginForm();
        }}>
        <ForgotPasswordContainer
          header={{...header}}
          body={{
            ...body,
            formSignInValidate,
          }}
          footer={{
            ...footer,
          }}
        />
      </BackgroundTemplate>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordPage);
