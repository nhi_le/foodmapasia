import React from 'react';
import {View, StyleSheet} from 'react-native';
import LoginHeader from './LoginHeader';
import LoginBody from './LoginBody';
import LoginFooter from './LoginFooter';

const LoginContainer = ({header, body, footer}) => {
  return (
    <View style={styles.container}>
      <LoginHeader style={styles.header} {...header} />
      <LoginBody style={styles.body} {...body} />
      {/* <LoginFooter style={styles.footer} {...footer} /> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {},
  body: {
    flex: 1,
    marginHorizontal: 20,
  },
  footer: {},
});

export default LoginContainer;
