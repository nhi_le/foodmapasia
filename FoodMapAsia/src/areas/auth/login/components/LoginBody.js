import React from 'react';
import {View, StyleSheet} from 'react-native';
import Button from '@common/components/button/Button';
import FieldInput from '@common/components/input/FieldInput';
import TextContent from '@common/components/text/TextContent';
import OutlineButton from '@common/components/button/OutlineButton';
import PasswordInput from '@common/components/input/PasswordInput';
import {WebView} from 'react-native-webview';
import {
  TEXT_DARK_COLOR,
  TEXT_GRAY_LIGHT_COLOR,
  GRAY_COLOR,
  BUTTON_LOGIN_COLOR,
  BUTTON_FACEBOOK_COLOR,
  BUTTON_GOOGLE_COLOR,
} from '@resources/palette';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {
  CONTENT_TERM_CONDITION,
  CONTENT_TERM_CONDITION_1,
  CONTENT_TERM_CONDITION_2,
  CONTENT_TERM_CONDITION_3,
  CONTENT_TERM_CONDITION_4,
  CONTENT_LOGIN_SNS,
  CONTENT_REGISTER,
} from '@resources/string/strings';
import {renderText} from '@common/components/StringHelper';
import {ACCENT_COLOR} from '@resources/palette';
import HorizontalView from '../../../../common/components/layout/HorizontalView';

const LoginBody = ({
  style,
  form,
  buttonForgotPassword,
  buttonLogin,
  buttonRegister,
  buttonFacebook,
  buttonGoogle,
  loginWebView,
  formSignInValidate,
}) => {
  let js = `
    var originalPostMessage = window.postMessage;
    var patchedPostMessage = function (message, targetOrigin, transfer) { originalPostMessage(message, targetOrigin, transfer);};
    patchedPostMessage.toString = function () {
    return String(Object.hasOwnProperty).replace('hasOwnProperty', 'postMessage'); };
    window.postMessage = patchedPostMessage; setTimeout(() => {
    var recaptcha = document.querySelectorAll('[name="g-recaptcha-response"]'); if (recaptcha && recaptcha.length > 0) {
    window.ReactNativeWebView.postMessage(recaptcha[0].value); }
    }, 2000);
    const patchPostMessageJsCode = '(' + String(patchPostMessageFunction) + ')();';
`;

  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <FieldInput
        {...form.email}
        inputStyle={styles.inputForm}
        errorMessage={formSignInValidate.email}
      />
      <PasswordInput
        {...form.password}
        style={{marginTop: 22}}
        inputStyle={styles.inputForm}
        errorMessage={formSignInValidate.password}
      />

      <View style={styles.forgotContainer}>
        <TouchableOpacity
          style={styles.buttonForgot}
          onPress={buttonForgotPassword.onPress}>
          <TextContent style={styles.forgotButtonText}>
            {buttonForgotPassword.text}
          </TextContent>
        </TouchableOpacity>
      </View>
      <Button
        buttonStyle={styles.buttonLoginStyle}
        text={buttonLogin.text}
        textStyle={styles.btnLoginText}
        style={styles.buttonLogin}
        onPress={buttonLogin.onPress}
      />
      <TextContent style={styles.conttentText}>{CONTENT_REGISTER}</TextContent>
      <OutlineButton
        buttonStyle={styles.outLineButton}
        text={buttonRegister.text}
        textStyle={styles.btnRegisterTextStyle}
        style={styles.buttonRegister}
        onPress={buttonRegister.onPress}
      />
      <TextContent style={styles.conttentText}>{CONTENT_LOGIN_SNS}</TextContent>
      <HorizontalView style={styles.horizontalSocialBtnStyle}>
        <OutlineButton
          buttonStyle={styles.buttonFb}
          text={buttonFacebook.text}
          textStyle={styles.btnFacebookTextStyle}
          style={styles.socialBtnStyle}
          onPress={buttonFacebook.onPress}
        />
        <OutlineButton
          buttonStyle={styles.buttonGg}
          text={buttonGoogle.text}
          textStyle={styles.btnGoogleTextStyle}
          style={styles.socialBtnStyle}
          onPress={buttonGoogle.onPress}
        />
      </HorizontalView>
      <WebView
        style={{width: 0, height: 0}}
        injectedJavaScript={js}
        {...loginWebView}
      />
      <View style={styles.containerFooter}>
        <TextContent style={styles.contentText}>
          {renderText(CONTENT_TERM_CONDITION)}
          <TextContent style={styles.textRed}>
            {renderText(CONTENT_TERM_CONDITION_1)}
          </TextContent>
          {renderText(CONTENT_TERM_CONDITION_2)}
          <TextContent style={styles.textRed}>
            {renderText(CONTENT_TERM_CONDITION_3)}
          </TextContent>
          {renderText(CONTENT_TERM_CONDITION_4)}
        </TextContent>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
  },
  titleWelcome: {
    color: TEXT_DARK_COLOR,
  },
  hintLoginEmail: {
    color: TEXT_GRAY_LIGHT_COLOR,
    marginTop: 20,
    marginBottom: 50,
  },
  password: {
    marginTop: 20,
  },
  inputEmptyStyle: {
    borderRadius: 10,
    backgroundColor: GRAY_COLOR,
  },
  buttonForgot: {
    marginTop: 10,
    width: 120,
    height: 45,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  inputForm: {
    color: TEXT_GRAY_LIGHT_COLOR,
    borderRadius: 22,
    borderColor: GRAY_COLOR,
    backgroundColor: 'white',
    height: 40,
  },
  buttonLoginStyle: {
    backgroundColor: BUTTON_LOGIN_COLOR,
    borderRadius: 22,
  },
  buttonLogin: {
    width: 150,
    marginTop: 10,
    justifyContent: 'center',
    alignSelf: 'center',
    flex: 1,
  },
  buttonRegister: {
    width: 150,
    marginTop: 10,
    justifyContent: 'center',
    alignSelf: 'center',
    flex: 1,
  },
  btnRegisterTextStyle: {
    color: BUTTON_LOGIN_COLOR,
    fontWeight: 'bold',
  },
  buttonFb: {
    borderColor: BUTTON_FACEBOOK_COLOR,
    borderRadius: 22,
  },
  btnFacebookTextStyle: {
    color: BUTTON_FACEBOOK_COLOR,
    fontWeight: 'bold',
  },
  btnGoogleTextStyle: {
    color: BUTTON_GOOGLE_COLOR,
    fontWeight: 'bold',
  },
  buttonGg: {
    borderColor: BUTTON_GOOGLE_COLOR,
    borderRadius: 22,
  },
  conttentText: {
    alignSelf: 'center',
    marginTop: 30,
    color: TEXT_GRAY_LIGHT_COLOR,
  },
  outLineButton: {
    borderColor: BUTTON_LOGIN_COLOR,
    borderRadius: 22,
  },
  btnLoginText: {
    color: 'white',
    fontWeight: 'bold',
  },
  HoriView: {
    justifyContent: 'space-between',
  },
  forgotButtonText: {
    color: ACCENT_COLOR,
  },
  forgotContainer: {
    alignItems: 'flex-end',
  },
  containerFooter: {
    paddingBottom: 12,
  },
  contentText: {
    alignSelf: 'center',
    textAlign: 'center',
    color: TEXT_GRAY_LIGHT_COLOR,
  },
  textRed: {
    color: ACCENT_COLOR,
  },
  horizontalSocialBtnStyle: {
    marginHorizontal: 5,
    marginVertical: 10,
    flex: 1,
  },
  socialBtnStyle: {
    flex: 1,
    marginHorizontal: 10,
  },
});

export default LoginBody;
