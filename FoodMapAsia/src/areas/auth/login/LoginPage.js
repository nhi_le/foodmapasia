import React, {Component} from 'react';
import {connect} from 'react-redux';
import BackgroundTemplate from '@common/components/background/BackgroundTemplate';
import operations from '@redux/auth/operations';
import {ICON_CANCEL} from '@resources/images';
import LoginContainer from './components/LoginContainer';
import {
  BUTTON_LOGIN,
  BUTTON_REGISTER,
  BUTTON_FORGOT_PASSWORD,
} from '@resources/string/strings';
import {renderText} from '@common/components/StringHelper';
import {HOLDER_EMAIL, HOLDER_PASSWORD} from '../../../resources/string/strings';

let passwordRef = null;
let wedViewRef = null;

const mapStateToProps = (state) => ({
  isAuthorized: state.auth.isAuthorized,
  formSignInValidate: state.auth.formSignInValidate,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  header: {
    title: 'TITLE_LOGIN',
    cancelButton: {
      svgIcon: ICON_CANCEL,
      onPress: async () => {},
    },
    backButton: {
      svgIcon: ICON_CANCEL,
      onPress: async () => {},
    },
  },
  body: {
    form: {
      email: {
        placeholder: HOLDER_EMAIL,
        keyboardType: 'email-address',
        returnKeyType: 'next',
        autoCapitalize: 'none',
        errorMessage: '',
        onSubmitEditing: () => {
          if (passwordRef) {
            passwordRef.focus();
          } else {
            dispatch(operations.loginEmailValidation());
          }
        },
        blurOnSubmit: false,
        onChangeText: (text) => {
          dispatch(operations.updateLoginForm('email', text.toLowerCase()));
        },
      },
      password: {
        placeholder: HOLDER_PASSWORD,
        secureTextEntry: true,
        returnKeyType: 'done',
        onChangeText: (text) => {
          dispatch(operations.updateLoginForm('password', text));
        },
        inputRef: (ref) => {
          passwordRef = ref;
        },
        onSubmitEditing: async () => {
          await dispatch(operations.login());
        },
        autoCapitalize: 'none',
      },
    },
    buttonForgotPassword: {
      text: renderText(BUTTON_FORGOT_PASSWORD),
      onPress: () => {
        ownProps.navigation.navigate('ForgotPasswordPage');
      },
    },
    buttonLogin: {
      text: renderText(BUTTON_LOGIN),
      onPress: async () => {
        let isSuccess = await dispatch(operations.login());
        if (isSuccess) {
          ownProps.navigation.navigate('ManagerAccountPage');
        }
      },
    },
    buttonRegister: {
      text: renderText(BUTTON_REGISTER),
      onPress: () => {
        ownProps.navigation.navigate('RegisterPage');
      },
    },
    buttonFacebook: {
      text: 'Facebook',
      onPress: () => {},
    },
    buttonGoogle: {
      text: 'Google',
      onPress: () => {},
    },
    loginWebView: {
      sharedCookiesEnabled: true,
      scrollEnabled: true,
      javaScriptEnabled: true,
      domStorageEnabled: true,
      source: {
        uri: 'https://choraucu.myharavan.com/account/login?themeid=1000608813',
      },
      ref: (ref) => {
        wedViewRef = ref;
      },
      onNavigationStateChange: async (navigationState) => {},
      onMessage: async (event) => {
        dispatch(
          operations.updateLoginForm('recaptcha', event.nativeEvent.data),
        );
      },
    },
  },
  footer: {},
  actions: {
    refreshLoginForm: () => dispatch(operations.refreshLoginForm()),
  },
});

class LoginPage extends Component {
  constructor() {
    super();
  }

  componentDidMount() {
    this.walletListener = this.props.navigation.addListener('willFocus', () => {
      if (wedViewRef) {
        wedViewRef.reload();
      }
    });
  }

  render() {
    const {
      header,
      body,
      footer,
      login,
      actions,
      formSignInValidate,
    } = this.props;
    return (
      <BackgroundTemplate
        scrollable={false}
        fullscreen={true}
        onInitialized={() => {
          actions.refreshLoginForm();
        }}>
        <LoginContainer
          header={{...header}}
          body={{
            ...body,
            formSignInValidate,
          }}
          footer={{
            ...footer,
          }}
        />
      </BackgroundTemplate>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
