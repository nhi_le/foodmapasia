import React from 'react';
import {View, StyleSheet, KeyboardAvoidingView, Platform} from 'react-native';
import RegisterHeader from './RegisterHeader';
import RegisterBody from './RegisterBody';
import RegisterFooter from './RegisterFooter';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const RegisterContainer = ({header, body, footer}) => {
  return (
    <View style={styles.container}>
      <RegisterHeader style={styles.header} {...header} />
      <KeyboardAvoidingView
        style={{flex: 1}}
        {...(Platform.OS === 'ios' && {
          behavior: 'height',
          ...ifIphoneX(
            {
              keyboardVerticalOffset: 140,
            },
            {
              keyboardVerticalOffset: 60,
            },
          ),
        })}>
        <View style={styles.contentContainer}>
          <RegisterBody style={styles.body} {...body} />
        </View>
      </KeyboardAvoidingView>
      {/* <RegisterFooter style={styles.footer} {...footer} /> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {},
  contentContainer: {
    height: '100%',
  },
  body: {
    flex: 1,
    marginHorizontal: 20,
  },
  footer: {},
});

export default RegisterContainer;
