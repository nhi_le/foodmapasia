import React from 'react';
import {StyleSheet, View} from 'react-native';
import TextContent from '@common/components/text/TextContent';
import {TEXT_GRAY_LIGHT_COLOR} from '@resources/palette';
import {renderText} from '@common/components/StringHelper';
import {
  CONTENT_TERM_CONDITION,
  CONTENT_TERM_CONDITION_1,
  CONTENT_TERM_CONDITION_2,
  CONTENT_TERM_CONDITION_3,
  CONTENT_TERM_CONDITION_4,
} from '@resources/string/strings';
import {ACCENT_COLOR} from '@resources/palette';
const RegisterFooter = ({style}) => {
  return (
    <View style={styles.container}>
      <TextContent style={styles.contentText}>
        {renderText(CONTENT_TERM_CONDITION)}
        <TextContent style={styles.textRed}>
          {renderText(CONTENT_TERM_CONDITION_1)}
        </TextContent>
        {renderText(CONTENT_TERM_CONDITION_2)}
        <TextContent style={styles.textRed}>
          {renderText(CONTENT_TERM_CONDITION_3)}
          {renderText(CONTENT_TERM_CONDITION_4)}
        </TextContent>
      </TextContent>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingBottom: 10,
  },
  contentText: {
    alignSelf: 'center',
    textAlign: 'center',
    padding: 14,
    color: TEXT_GRAY_LIGHT_COLOR,
  },
  textRed: {
    color: ACCENT_COLOR,
  },
});

export default RegisterFooter;
