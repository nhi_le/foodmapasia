import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import Title from '@common/components/text/Title';

import SvgIcon from '@common/components/icon/SvgIcon';
import {TEXT_DARK_COLOR} from '@resources/palette';
import {ICON_PRE} from '@resources/images';
import {LIGHT_GRAY_COLOR} from '../../../../resources/palette';

const RegisterHeader = ({style, title, backButton}) => {
  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <TouchableOpacity style={styles.icon}>
        <SvgIcon height={18} width={18} {...backButton} svgIcon={ICON_PRE} />
      </TouchableOpacity>

      <Title style={styles.titleToolbar} size="medium-title">
        {title}
      </Title>

      <View style={styles.icon} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 70,
    alignContent: 'center',
    flexDirection: 'row',
    paddingTop: 20,
    borderBottomColor: LIGHT_GRAY_COLOR,
    borderBottomWidth: 1,
    alignItems: 'center',
  },
  titleToolbar: {
    flex: 1,
    color: TEXT_DARK_COLOR,
    textAlign: 'center',
    fontWeight: 'normal',
  },
  icon: {
    width: 60,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default RegisterHeader;
