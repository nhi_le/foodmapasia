import React from 'react';
import {View, StyleSheet} from 'react-native';
import FieldInput from '@common/components/input/FieldInput';
import Button from '@common/components/button/Button';
import TextError from '@common/components/text/TextError';
import {ICON_ALERT_ERROR} from '@resources/images';
import SvgIcon from '@common/components/icon/SvgIcon';
import {ScrollView} from 'react-native-gesture-handler';
import {ACCENT_COLOR, GRAY_COLOR} from '@resources/palette';
import {WebView} from 'react-native-webview';
import PasswordInput from '@common/components/input/PasswordInput';

const LoadViewValidate = ({checkError = false, listErrorMessage}) => {
  if (checkError) {
    return listErrorMessage.map((errorM) => {
      return (
        <View style={styles.styleContainerError}>
          <SvgIcon
            height={10}
            width={10}
            svgIcon={ICON_ALERT_ERROR}
            style={styles.styleIconError}
          />
          <TextError errorMessage={errorM} />
        </View>
      );
    });
  } else {
    return null;
  }
};
const RegisterBody = ({
  style,
  citySelection,
  districtSelection,
  wardSelection,
  registerWebView,
  buttonRegister,
  form,
  formValidate,
}) => {
  let js = `
    var originalPostMessage = window.postMessage;
    var patchedPostMessage = function (message, targetOrigin, transfer) { originalPostMessage(message, targetOrigin, transfer);};
    patchedPostMessage.toString = function () {
    return String(Object.hasOwnProperty).replace('hasOwnProperty', 'postMessage'); };
    window.postMessage = patchedPostMessage; setTimeout(() => {
    var recaptcha = document.querySelectorAll('[name="g-recaptcha-response"]'); if (recaptcha && recaptcha.length > 0) {
    window.ReactNativeWebView.postMessage(recaptcha[0].value); }
    }, 2000);
    const patchPostMessageJsCode = '(' + String(patchPostMessageFunction) + ')();';
`;

  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <ScrollView>
        <View>
          <FieldInput
            errorMessage={formValidate.company}
            {...form.company}
            inputStyle={styles.inputStyle}
          />
          <FieldInput
            errorMessage={formValidate.fullName}
            {...form.fullName}
            inputStyle={styles.inputStyle}
          />
          <FieldInput
            errorMessage={formValidate.email}
            {...form.email}
            inputStyle={styles.inputStyle}
          />
          <PasswordInput
            errorMessage={formValidate.password}
            {...form.password}
            inputStyle={styles.inputStyle}
          />
          <FieldInput
            errorMessage={formValidate.phoneNumber}
            {...form.phone}
            inputStyle={styles.inputStyle}
          />
          <FieldInput
            errorMessage={formValidate.address}
            {...form.address}
            inputStyle={styles.inputStyle}
          />
        </View>
        <LoadViewValidate
          checkError={false} // !formValidate.isValid
          listErrorMessage={[
            'Vui lòng chọn tỉnh / thành.',
            'Vui lòng chọn quận / huyện.',
            'Vui lòng chọn xã / phường.',
          ]}
        />
        <Button
          {...buttonRegister}
          buttonStyle={styles.buttonRegister}
          textStyle={styles.textStyle}
        />
      </ScrollView>
      <WebView
        style={{width: 0, height: 0}}
        injectedJavaScript={js}
        {...registerWebView}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
  },
  styleContainerError: {
    flexDirection: 'row',
    marginTop: 10,
  },
  styleIconError: {
    marginEnd: 5,
    paddingTop: 10,
  },
  buttonRegister: {
    backgroundColor: ACCENT_COLOR,
    borderRadius: 22,
    width: 200,
    marginTop: 30,
    justifyContent: 'center',
    alignSelf: 'center',
  },
  inputStyle: {
    marginTop: 5,
    borderRadius: 20,
    borderColor: '#BDBDBD',
    backgroundColor: 'white',
    height: 40,
  },
  selectedStyle: {
    marginTop: 10,
    borderRadius: 20,
    borderColor: '#BDBDBD',
    backgroundColor: 'white',
    height: 40,
  },
  textSelected: {
    flex: 1,
    color: GRAY_COLOR,
  },
});

export default RegisterBody;
