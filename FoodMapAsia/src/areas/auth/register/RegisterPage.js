import React from 'react';
import {connect} from 'react-redux';
import BackgroundTemplate from '@common/components/background/BackgroundTemplate';
import operations from '@redux/auth/operations';
import {ICON_PRE} from '@resources/images';
import RegisterContainer from './components/RegisterContainer';
import {TITLE_REGISTER} from '@resources/string/strings';
import {renderText} from '@common/components/StringHelper';
import {
  HOLDER_COMPANYNAME,
  HOLDER_FULLNAME,
  HOLDER_EMAIL,
  HOLDER_PHONENUMBER,
  HOLDER_PASSWORD,
  HOLDER_ADDRESS,
  BUTTON_REGISTER,
} from '@resources/string/strings';

let emailRef = null;
let fullNameRef = null;
let passwordRef = null;
let phoneRef = null;
let addressRef = null;

const mapStateToProps = (state) => ({
  formValidate: state.auth.formValidate,
  selection: state.auth.selection,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  header: {
    title: renderText(TITLE_REGISTER),
    backButton: {
      svgIcon: ICON_PRE,
      onPress: async () => {
        ownProps.navigation.goBack();
      },
    },
  },
  body: {
    citySelection: {
      isShow: true,
      onItemPress: (index) => {
        dispatch(operations.setSelectCity(index));
      },
    },
    districtSelection: {
      isShow: true,
      onItemPress: (index) => {
        dispatch(operations.setSelectDistrict(index));
      },
    },
    wardSelection: {
      isShow: true,
      onItemPress: (index) => {
        dispatch(operations.setSelectWard(index));
      },
    },

    form: {
      company: {
        label: '',
        placeholder: HOLDER_COMPANYNAME,
        isAlwayShowingLabel: true,
        autoCapitalize: 'none',
        returnKeyType: 'next',
        onChangeText: (text) => {
          dispatch(operations.setSignUpForm({company: text}));
        },
        onSubmitEditing: async () => {
          fullNameRef.focus();
        },
      },
      fullName: {
        label: '',
        placeholder: HOLDER_FULLNAME,
        isAlwayShowingLabel: true,
        autoCapitalize: 'none',
        returnKeyType: 'next',
        onChangeText: (text) => {
          dispatch(operations.setSignUpForm({fullName: text}));
        },
        inputRef: (ref) => {
          fullNameRef = ref;
        },
        onSubmitEditing: async () => {
          emailRef.focus();
        },
      },
      email: {
        label: '',
        placeholder: HOLDER_EMAIL,
        isAlwayShowingLabel: true,
        autoCapitalize: 'none',
        returnKeyType: 'next',
        onChangeText: (text) => {
          dispatch(operations.setSignUpForm({email: text}));
        },
        inputRef: (ref) => {
          emailRef = ref;
        },
        onSubmitEditing: async () => {
          passwordRef.focus();
        },
      },
      password: {
        label: '',
        placeholder: HOLDER_PASSWORD,
        secureTextEntry: true,
        isAlwayShowingLabel: true,
        returnKeyType: 'next',
        onChangeText: (text) => {
          dispatch(operations.setSignUpForm({password: text}));
        },
        inputRef: (ref) => {
          passwordRef = ref;
        },
        blurOnSubmit: false,
        onSubmitEditing: async () => {
          phoneRef.focus();
        },
        autoCapitalize: 'none',
      },
      phone: {
        label: '',
        placeholder: HOLDER_PHONENUMBER,
        isAlwayShowingLabel: true,
        returnKeyType: 'next',
        keyboardType: 'numeric',
        onChangeText: (text) => {
          dispatch(operations.setSignUpForm({phoneNumber: text}));
        },
        inputRef: (ref) => {
          phoneRef = ref;
        },
        blurOnSubmit: false,
        onSubmitEditing: async () => {
          addressRef.focus();
        },
        autoCapitalize: 'none',
      },
      address: {
        label: '',
        placeholder: HOLDER_ADDRESS,
        isAlwayShowingLabel: true,
        returnKeyType: 'next',
        onChangeText: (text) => {
          dispatch(operations.setSignUpForm({address: text}));
        },
        inputRef: (ref) => {
          addressRef = ref;
        },
        blurOnSubmit: false,
        onSubmitEditing: async () => {},
        autoCapitalize: 'none',
      },
    },
    buttonRegister: {
      text: BUTTON_REGISTER,
      onPress: async () => {
        let isSuccess = await dispatch(operations.signUp());
        if (isSuccess) {
          ownProps.navigation.navigate('LoginPage');
        }
      },
    },
    registerWebView: {
      sharedCookiesEnabled: true,
      scrollEnabled: true,
      javaScriptEnabled: true,
      domStorageEnabled: true,
      source: {
        uri:
          'https://choraucu.myharavan.com/account/register?themeid=1000608813',
      },
      onNavigationStateChange: async (navigationState) => {},
      onMessage: async (event) => {
        dispatch(operations.setSignUpForm({recaptcha: event.nativeEvent.data}));
      },
    },
  },
  footer: {},
  actions: {
    refreshLoginForm: () => dispatch(operations.refreshLoginForm()),
  },
});

const RegisterPage = ({
  header,
  body,
  footer,
  login,
  actions,
  formValidate,
  selection,
  ...props
}) => {
  return (
    <BackgroundTemplate
      scrollable={false}
      fullscreen={true}
      onInitialized={() => {
        actions.refreshLoginForm();
      }}>
      <RegisterContainer
        header={{...header}}
        body={{
          ...body,
          formValidate,
          citySelection: {
            ...body.citySelection,
            ...selection.citySelection,
          },
          districtSelection: {
            ...body.districtSelection,
            ...selection.districtSelection,
          },
          wardSelection: {
            ...body.wardSelection,
            ...selection.wardSelection,
          },
        }}
        footer={{...footer}}
      />
    </BackgroundTemplate>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(RegisterPage);
