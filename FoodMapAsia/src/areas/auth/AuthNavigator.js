import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import LoginPage from './login/LoginPage';
import RegisterPage from './register/RegisterPage';

const AuthNavigator = createStackNavigator(
  {
    LoginPage: LoginPage,
    RegisterPage,
  },
  {
    initialRouteName: 'LoginPage',
    defaultNavigationOptions: {
      header: null,
    },
  },
);

const AuthContainer = createAppContainer(AuthNavigator);

export default AuthContainer;
