import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import AppNavigator from './app/AppNavigator';
import {WHITE_COLOR} from '../resources/palette';
import {StatusBar, Platform} from 'react-native';

const StatusBarStyle = {
  HomePage: 'light-content',
  RecognitionPage: 'light-content',
  ProductDetailPage: 'light-content',
  FlexibleBenefitsTotalPage: 'light-content',
  FlexibleBenefitsPage: 'light-content',
  default: 'dark-content',
};
const StatusBarBackgroundColor = {
  HomePage: WHITE_COLOR,
  RecognitionPage: WHITE_COLOR,
  FlexibleBenefitsTotalPage: WHITE_COLOR,
  FlexibleBenefitsPage: WHITE_COLOR,
  default: WHITE_COLOR,
  defaultAndroidApiLowerThan23: WHITE_COLOR,
};
AppNavigator.navigationOptions = ({navigation}) => {
  let childRoutes = navigation.state.routes[navigation.state.index];
  let routeName = 'default';
  if (childRoutes.routes) {
    routeName = childRoutes.routes[childRoutes.index].routeName;
  }
  StatusBar.setBarStyle(
    StatusBarStyle[routeName]
      ? StatusBarStyle[routeName]
      : StatusBarStyle.default,
  );
  if (Platform.OS === 'android') {
    StatusBar.setBackgroundColor(
      StatusBarBackgroundColor[routeName]
        ? StatusBarBackgroundColor[routeName]
        : Platform.Version >= 23
        ? StatusBarBackgroundColor.default
        : StatusBarBackgroundColor.defaultAndroidApiLowerThan23,
    );
  }
};

AppNavigator.navigationOptions = ({navigation}) => {
  let childRoutes = navigation.state.routes[navigation.state.index];
  let routeName = 'default';
  if (childRoutes.routes) {
    routeName = childRoutes.routes[childRoutes.index].routeName;
  }
};

const AuthorizedNavigator = createStackNavigator(
  {
    AppNavigator: AppNavigator,
  },
  {
    initialRouteName: 'AppNavigator',
    defaultNavigationOptions: {
      header: null,
    },
  },
);

const AuthorizedContainer = createAppContainer(AuthorizedNavigator);

export default AuthorizedContainer;
