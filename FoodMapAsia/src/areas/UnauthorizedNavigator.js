import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import AuthNavigator from './auth/AuthNavigator';

import {StatusBar, Platform} from 'react-native';
import {WHITE_COLOR} from '../resources/palette';

const StatusBarStyle = {
  default: 'dark-content',
};
const StatusBarBackgroundColor = {
  default: WHITE_COLOR,
};
AuthNavigator.navigationOptions = ({navigation}) => {
  let childRoutes = navigation.state.routes[navigation.state.index];
  let routeName = 'default';
  if (childRoutes.routes) {
    routeName = childRoutes.routes[childRoutes.index].routeName;
  }
  StatusBar.setBarStyle(
    StatusBarStyle[routeName]
      ? StatusBarStyle[routeName]
      : StatusBarStyle.default,
  );
  if (Platform.OS === 'android') {
    StatusBar.setBackgroundColor(
      StatusBarBackgroundColor[routeName]
        ? StatusBarBackgroundColor[routeName]
        : StatusBarBackgroundColor.default,
    );
  }
};

const UnauthorizedNavigator = createStackNavigator(
  {
    Auth: AuthNavigator,
  },
  {
    initialRouteName: 'Auth',
    defaultNavigationOptions: {
      header: null,
    },
  },
);

const UnauthorizedContainer = createAppContainer(UnauthorizedNavigator);

export default UnauthorizedContainer;
