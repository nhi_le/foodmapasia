import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import SvgIcon from '@common/components/icon/SvgIcon';
import SearchFieldInput from '@common/components/input/SearchFieldInput';

const HomeHeader = ({style, locateButton, notiButton, searchInput}) => {
  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <TouchableOpacity style={styles.icon} onPress={locateButton.onPress}>
        <SvgIcon height={30} width={30} {...locateButton} />
      </TouchableOpacity>

      <SearchFieldInput style={styles.search} {...searchInput} />
      <View style={styles.iconNoti} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 80,
    alignContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingTop: 20,
  },
  icon: {
    width: 60,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconNoti: {
    width: 40,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  search: {
    flex: 1,
    borderRadius: 60,
  },
});

export default HomeHeader;
