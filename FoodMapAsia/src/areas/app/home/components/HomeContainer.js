import React from 'react';
import {View, StyleSheet} from 'react-native';
import HomeHeader from './HomeHeader';
import HomeBody from './HomeBody';

const HomeContainer = ({header, body, footer}) => {
  return (
    <View style={styles.container}>
      <HomeHeader style={styles.header} {...header} />
      <HomeBody style={styles.body} {...body} />
      {/* <HomeFooter style={styles.footer} {...footer} /> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {},
  body: {
    flex: 1,
  },
  footer: {},
});

export default HomeContainer;
