import React from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';
import ListContent from '@common/components/list/ListContent';
import {
  BACKGROUND_PROFILE,
  BUTTON_LOGIN_COLOR,
  ACCENT_COLOR,
} from '@resources/palette';
import {WHITE_COLOR, GRAY_COLOR} from '@resources/palette';
import YesNoModal from '@common/components/modal/YesNoModal';
import TextContent from '@common/components/text/TextContent';
import Image from '@common/components/image/Image';
import Button from '@common/components/button/Button';
import {COLOR_BUTTON_MENU_HOME} from '@resources/palette';
import Swiper from '@common/components/swiper/Swiper';

export const createNewListSwipe = (dataList) => {
  let listNew = dataList.data.map((item) => {
    return item;
  });

  var tempList = listNew,
    childList;
  var newList = [];

  while (tempList.length > 0) {
    childList = tempList.splice(0, 6);
    newList.push(childList);
  }
  return newList;
};

const renderImageItems = (images) => {
  return images.map((item, index) => {
    return (
      <View style={styles.imageWrapper} key={index}>
        <Image
          style={styles.bannerImage}
          source={{
            uri: item.img_url,
          }}
          resizeMode="cover"
        />
      </View>
    );
  });
};

const renderListItems = (dataList, type) => {
  let newList = createNewListSwipe(dataList);
  return newList.map((item, index) => {
    return (
      <View style={styles.mainColorContainer} key={index}>
        <ListContent
          numColumns={2}
          {...dataList}
          data={item}
          type={type}
          onEndReached={() => {
            dataList.onEndReached && dataList.onEndReached();
          }}
          scrollEnabled={false}
          onEndReachedThreshold={0.5}
          style={styles.listContent}
        />
      </View>
    );
  });
};

const renderListImageHeader = (swiperImages) => {
  return (
    <View style={{backgroundColor: WHITE_COLOR}}>
      <View style={styles.viewImage}>
        <Swiper
          renderItems={() => renderImageItems(swiperImages.data)}
          showsButtons={false}
          dotColor={GRAY_COLOR}
          activeDotColor={WHITE_COLOR}
          activeDotStyle={styles.activeDotStyle}
        />
      </View>
    </View>
  );
};

const renderList = (dataList, type) => {
  return (
    <View style={styles.containerList}>
      <Swiper
        renderItems={() => renderListItems(dataList, type)}
        showsButtons={false}
        dotColor={BACKGROUND_PROFILE}
        activeDotColor={ACCENT_COLOR}
        activeDotStyle={styles.activeDotStyle}
        style={styles.wrapper}
      />
    </View>
  );
};

export default HomeBody = ({
  style,
  listCate,
  listHomeItem,
  listHomeMarketOnlineItem,
  listHomeSpecialItem,
  locationModal,
  swiperImages,
}) => {
  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <View style={styles.categoryStyle}>
        <ListContent horizontal={true} {...listCate} type={'homeCategorie'} />
      </View>
      <ScrollView style={styles.mainColorContainer}>
        {renderListImageHeader(swiperImages)}
        <Button
          {...listHomeItem.menuButton}
          style={styles.buttonContainerStyle}
          buttonStyle={styles.buttonStyle}
          textStyle={styles.textStyle}
          iconStyle={styles.iconButtonStyle}
        />
        {listHomeItem && listHomeItem.data && listHomeItem.data.length > 0 ? (
          renderList(listHomeItem, 'home')
        ) : (
          <View style={styles.mainColorContainer}>
            <TextContent style={styles.empty}>
              {'Không có sản phẩm'}
            </TextContent>
          </View>
        )}
        <Button
          {...listHomeMarketOnlineItem.menuButton}
          style={styles.buttonContainerStyle}
          buttonStyle={styles.buttonStyle}
          textStyle={styles.textLeftStyle}
          iconStyle={styles.iconButtonStyle}
          iconLeftStyle={styles.iconLeftStyle}
        />
        {listHomeMarketOnlineItem &&
        listHomeMarketOnlineItem.data &&
        listHomeMarketOnlineItem.data.length > 0 ? (
          renderList(listHomeMarketOnlineItem, 'homeChild')
        ) : (
          <View style={styles.mainColorContainer}>
            <TextContent style={styles.empty}>
              {'Không có sản phẩm'}
            </TextContent>
          </View>
        )}
        <Button
          {...listHomeSpecialItem.menuButton}
          style={styles.buttonContainerStyle}
          buttonStyle={styles.buttonStyle}
          textStyle={styles.textLeftStyle}
          iconStyle={styles.iconButtonStyle}
          iconLeftStyle={styles.iconLeftStyle}
        />
        {listHomeSpecialItem &&
        listHomeSpecialItem.data &&
        listHomeSpecialItem.data.length > 0 ? (
          renderList(listHomeSpecialItem, 'homeChild')
        ) : (
          <View style={styles.mainColorContainer}>
            <TextContent style={styles.empty}>
              {'Không có sản phẩm'}
            </TextContent>
          </View>
        )}
      </ScrollView>
      <YesNoModal {...locationModal} />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: BACKGROUND_PROFILE,
  },
  containerList: {
    alignContent: 'center',
    alignItems: 'center',
    height: 760,
  },
  wrapper: {
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  mainColorContainer: {
    flex: 1,
  },
  listItem: {
    padding: 5,
    marginLeft: 3,
  },
  textButtonDetail: {
    color: BUTTON_LOGIN_COLOR,
  },
  itemArena: {
    height: 100,
    width: 100,
  },
  horiView: {
    flex: 1,
  },
  categoryList: {},
  bannerImage: {
    width: '100%',
    borderRadius: 20,
    height: 136,
    backgroundColor: 'white',
  },
  viewImage: {
    marginHorizontal: 10,
    height: 150,
  },
  horiMenu: {
    alignItems: 'center',
    marginBottom: 10,
  },
  categoryStyle: {
    height: 110,
    paddingHorizontal: 10,
    backgroundColor: WHITE_COLOR,
  },
  activeDotStyle: {
    width: 30,
    height: 2,
  },
  empty: {
    marginVertical: 50,
    flex: 1,
    color: GRAY_COLOR,
    textAlign: 'center',
  },
  listContent: {
    marginTop: 20,
  },
  buttonContainerStyle: {
    height: 40,
  },
  buttonStyle: {
    backgroundColor: COLOR_BUTTON_MENU_HOME,
    borderRadius: 50,
    margin: 10,
  },
  textStyle: {
    textAlign: 'center',
    color: WHITE_COLOR,
    fontSize: 16,
  },
  iconButtonStyle: {
    marginEnd: 5,
    marginBottom: 2,
  },
  iconLeftStyle: {
    borderRadius: 50,
    backgroundColor: ACCENT_COLOR,
  },
  textLeftStyle: {
    color: WHITE_COLOR,
    fontSize: 16,
    marginStart: 8,
  },
});
