import React from 'react';
import {connect} from 'react-redux';
import BackgroundTemplate from '@common/components/background/BackgroundTemplate';
import operations from '@redux/home/operations';
import HomeContainer from './components/HomeContainer';
import {
  ICON_LOCATE,
  ICON_NOTIFICATION,
  ICON_DOWN_MENU,
  ICON_MARKET_ONLINE,
  ICON_SPECIAL_ITEM,
} from '@resources/images';
import homeBannerImg from '../../../../assets/image/home_banner.png';
import {WHITE_COLOR} from '@resources/palette';
const mapStateToProps = (state) => ({
  tabSelected: state.profile.tabSelected,
  listCategories: state.home.listCategories,
  listHomeItem: state.home.listHomeItem,
  listHomeMarketOnlineItem: state.home.listHomeItem,
  listHomeSpecialItem: state.home.listHomeItem,
  locationList: state.home.listLocation,
  isShowModal: state.home.isVisible,
  CategoryHandle: state.home.CategoryHandle,
  swiperImagesRedux: state.home.swiperImages,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  header: {
    title: 'home',
    locateButton: {
      svgIcon: ICON_LOCATE,
      onPress: () => {
        dispatch(operations.setShowModal());
      },
    },
    searchInput: {
      placeholder: 'Nhập tên sản phẩm',
      onChangeText: async (text) => {
        await dispatch(operations.clearListProduct());
        if (!text.isEmpty()) {
          await dispatch(operations.searchProduct(text));
        } else {
          await dispatch(operations.getListHomeProduct());
        }
      },
    },
    notiButton: {
      svgIcon: ICON_NOTIFICATION,
      onPress: async () => {
        ownProps.navigation.navigate('NotificationPage');
      },
    },
  },
  body: {
    swiperImages: {
      data: [],
    },
    locationModal: {
      title: 'Chọn Khu Vực Giao Hàng',
      description: 'Chọn Khu Vực Giao Hàng',
      icon: {
        source: '',
      },
      yesButton: {
        text: 'Đến Chợ Rau Củ',
        onPress: () => {
          dispatch(operations.setHideModal());
        },
        gradient: true,
      },
      listLocation: {
        data: [],
        onItemPress: (item) => {
          dispatch(operations.updateSelectModal(item));
        },
      },
      onBackButtonPress: () => {},
      onBackdropPress: () => {
        dispatch(operations.setHideModal());
      },
    },

    listBannerImg: {
      data: homeBannerImg,
    },
    listHomeItem: {
      data: [],
      onItemPress: async (data) => {
        await dispatch(operations.updateProduct(data));
        ownProps.navigation.navigate('ProductDetailPage');
      },
      onActionPress: async (id) => {
        await dispatch(operations.updateProductId(id));
        ownProps.navigation.navigate('ProductDetailPage');
      },
      onEndReached: async () => {
        await dispatch(operations.loadMoreProduct());
      },
      menuButton: {
        text: 'Tất Cả Chiến Dịch',
        icon: {
          svgIcon: ICON_DOWN_MENU,
          tintColor: WHITE_COLOR,
        },
        onPress: () => {},
      },
    },
    listHomeMarketOnlineItem: {
      data: [],
      onItemPress: async (data) => {
        await dispatch(operations.updateProduct(data));
        ownProps.navigation.navigate('ProductDetailPage');
      },
      onActionPress: () => {},
      onEndReached: async () => {
        await dispatch(operations.loadMoreProduct());
      },
      menuButton: {
        text: 'Đi chợ online',
        icon: {
          svgIcon: ICON_DOWN_MENU,
          tintColor: WHITE_COLOR,
        },
        iconLeft: {
          svgIcon: ICON_MARKET_ONLINE,
          tintColor: WHITE_COLOR,
        },
        onPress: () => {},
      },
    },
    listHomeSpecialItem: {
      data: [],
      onItemPress: async (data) => {
        await dispatch(operations.updateProduct(data));
        ownProps.navigation.navigate('ProductDetailPage');
      },
      onActionPress: () => {},
      onEndReached: async () => {
        await dispatch(operations.loadMoreProduct());
      },
      menuButton: {
        text: 'Đặc sản vùng miền',
        icon: {
          svgIcon: ICON_DOWN_MENU,
          tintColor: WHITE_COLOR,
        },
        iconLeft: {
          svgIcon: ICON_SPECIAL_ITEM,
          tintColor: WHITE_COLOR,
        },
        onPress: () => {},
      },
    },
    listCate: {
      data: [],
      onItemPress: async (item) => {
        await dispatch(operations.updateHandle(item));
        ownProps.navigation.navigate('Menu');
      },
      onActionPress: () => {},
    },
  },
  actions: {
    getListCategory: () => dispatch(operations.getListCategory()),
    getListHomeProduct: () => dispatch(operations.getListHomeProduct()),
    getHomeSlider: () => dispatch(operations.getHomeSlider()),
  },
});

const HomePage = ({
  header,
  body,
  footer,
  login,
  actions,
  tabSelected,
  listHomeItem,
  listHomeMarketOnlineItem,
  listHomeSpecialItem,
  listCategories,
  locationList,
  isShowModal,
  CategoryHandle,
  swiperImagesRedux,
  ...props
}) => {
  return (
    <BackgroundTemplate
      scrollable={false}
      fullscreen={false}
      onInitialized={async () => {
        await actions.getHomeSlider();
        await actions.getListCategory();
        await actions.getListHomeProduct();
      }}>
      <HomeContainer
        header={{...header}}
        body={{
          ...body,
          swiperImages: {
            ...body.swiperImages,
            data: swiperImagesRedux.data,
          },
          listHomeItem: {
            ...body.listHomeItem,
            data: listHomeItem.data,
          },
          listHomeMarketOnlineItem: {
            ...body.listHomeMarketOnlineItem,
            data: listHomeMarketOnlineItem.data,
          },
          listHomeSpecialItem: {
            ...body.listHomeSpecialItem,
            data: listHomeSpecialItem.data,
          },
          listCate: {
            ...body.listCate,
            data: listCategories.data,
          },
          tabSelected: tabSelected,
          locationModal: {
            ...body.locationModal,
            isVisible: isShowModal,
            listLocation: {
              ...body.locationModal.listLocation,
              data: locationList.data,
            },
          },
        }}
        footer={{
          ...footer,
          tabSelected: tabSelected,
        }}
      />
    </BackgroundTemplate>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
