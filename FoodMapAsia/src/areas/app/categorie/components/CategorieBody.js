import React from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';
import ListContent from '@common/components/list/ListContent';
import {
  TEXT_GRAY_LIGHT_COLOR,
  BACKGROUND_PROFILE,
  BUTTON_LOGIN_COLOR,
  LIGHT_GRAY_COLOR,
  COLOR_READ,
} from '@resources/palette';
import HorizontalView from '@common/components/layout/HorizontalView';
import {ICON_FILTER} from '@resources/images';
import SvgIcon from '@common/components/icon/SvgIcon';
import TextContent from '@common/components/text/TextContent';
import SelectionPicker from '@common/components/select/SelectionPicker';
import YesNoModal from '@common/components/modal/YesNoModal';

const CategorieBody = ({
  style,
  listProduct,
  tabSelected,
  listCategoryProduct,
  listCategory,
  locationModal,
  listFilter,
}) => {
  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <HorizontalView style={styles.HoriView}>
        <View style={styles.categoryList}>
          <ListContent {...listCategory} type={'categoriesRoot'} />
        </View>
        <View style={styles.itemArena}>
          <ScrollView
            style={styles.scrollCategory}
            horizontal={true}
            showsHorizontalScrollIndicator={false}>
            {listCategoryProduct &&
              listCategoryProduct.data &&
              listCategoryProduct.data.length > 0 && (
                <ListContent
                  {...listCategoryProduct}
                  horizontal={true}
                  type="account-category"
                  style={styles.categoryList}
                />
              )}
          </ScrollView>

          <View style={styles.containerList}>
            <View style={styles.combobox}>
              <HorizontalView>
                <View style={styles.redContainer}>
                  <SvgIcon
                    style={{alignSelf: 'center'}}
                    height={14}
                    width={14}
                    svgIcon={ICON_FILTER}
                  />
                </View>

                <SelectionPicker
                  containerStyle={styles.selecttionStyle}
                  textStyle={styles.selecttionText}
                  {...listFilter}
                />
              </HorizontalView>
            </View>

            {listProduct && listProduct.data && listProduct.data.length > 0 ? (
              <ListContent
                {...listProduct}
                type={'item'}
                style={styles.categoryList}
                onEndReached={() => {
                  listProduct.onEndReached && listProduct.onEndReached();
                }}
                onEndReachedThreshold={0.5}
              />
            ) : (
              <TextContent style={styles.noDataTextContainer}>
                {' '}
                {'Chưa có sản phẩm'}
              </TextContent>
            )}
          </View>
        </View>
      </HorizontalView>
      <YesNoModal {...locationModal} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollCategory: {
    flexGrow: 0,
    borderBottomColor: LIGHT_GRAY_COLOR,
    borderBottomWidth: 1,
  },
  containerList: {
    flex: 1,
    backgroundColor: BACKGROUND_PROFILE,
  },
  titleInfo: {
    color: BUTTON_LOGIN_COLOR,
    opacity: 1,
    marginHorizontal: 15,
    marginVertical: 15,
  },
  titleName: {
    color: TEXT_GRAY_LIGHT_COLOR,
  },
  address: {
    color: TEXT_GRAY_LIGHT_COLOR,
    marginTop: 12,
  },
  noDataTextContainer: {
    alignSelf: 'center',
    marginTop: 30,
    color: TEXT_GRAY_LIGHT_COLOR,
  },

  textButtonDetail: {
    color: BUTTON_LOGIN_COLOR,
  },
  itemArena: {
    flex: 7,
  },
  HoriView: {
    justifyContent: 'space-between',
    flex: 1,
  },
  categoryList: {
    flex: 2.5,
    marginTop: 10,
  },
  combobox: {
    height: 30,
    marginLeft: 8,
    marginTop: 5,
    justifyContent: 'center',
  },
  redContainer: {
    backgroundColor: BUTTON_LOGIN_COLOR,
    width: 30,
    height: 30,
    borderRadius: 30,
    alignItems: 'flex-start',
    justifyContent: 'center',
    alignSelf: 'center',
    position: 'absolute',
    zIndex: 1,
  },
  text: {
    fontSize: 12,
    color: COLOR_READ,
    marginHorizontal: 5,
    alignSelf: 'center',
  },
  selecttionStyle: {
    borderRadius: 15,
    alignItems: 'center',
    height: 30,
    width: 180,
  },
  selecttionText: {
    flex: 1,
    color: COLOR_READ,
    fontSize: 12,
    fontWeight: 'bold',
    paddingLeft: 14,
    textAlign: 'center',
    marginLeft: 14,
  },
});

export default CategorieBody;
