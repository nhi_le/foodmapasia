import React from 'react';
import {StyleSheet, View} from 'react-native';
import {TEXT_GRAY_LIGHT_COLOR} from '@resources/palette';
import {ACCENT_COLOR, TEXT_DARK_COLOR, BORDER_COLOR} from '@resources/palette';
import Button from '@common/components/button/Button';
import {BACKGROUND_PROFILE, BUTTON_LOGIN_COLOR} from '@resources/palette';

const CategorieFooter = ({style, tabSelected, buttonAddAddress}) => {
  return (
    <View style={tabSelected === 1 ? styles.container : styles.containerNon}>
      <View style={styles.containerButtonAddAddress}>
        <Button
          style={styles.button}
          buttonStyle={styles.buttonAdd}
          textStyle={styles.texButton}
          {...buttonAddAddress}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 90,
    backgroundColor: BACKGROUND_PROFILE,
  },
  containerNon: {
    height: 0,
  },
  containerButtonAddAddress: {
    height: 70,
    width: '100%',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  buttonAdd: {
    backgroundColor: BUTTON_LOGIN_COLOR,
    borderRadius: 40,
    width: 160,
  },
  texButton: {
    color: 'white',
  },
});

export default CategorieFooter;
