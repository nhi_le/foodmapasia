import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import SvgIcon from '@common/components/icon/SvgIcon';
import BadgeTabIcon from '@common/components/icon/BadgeTabIcon';
import {
  DANGEROUS_COLOR,
  WHITE_COLOR,
  TEXT_DARK_COLOR,
} from '@resources/palette';
import SearchFieldInput from '@common/components/input/SearchFieldInput';
import {LIGHT_GRAY_COLOR} from '@resources/palette';

const CategorieHeader = ({style, searchInput, locateButton, notiButton}) => {
  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <TouchableOpacity style={styles.icon} onPress={locateButton.onPress}>
        <SvgIcon
          style={styles.icon}
          height={30}
          width={30}
          {...locateButton}
          onPress={locateButton.onPress}
        />
      </TouchableOpacity>
      <SearchFieldInput style={styles.search} {...searchInput} />

      <View style={styles.iconNoti} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 80,
    alignContent: 'center',
    borderBottomColor: LIGHT_GRAY_COLOR,
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  search: {
    marginBottom: 5,
    flex: 1,
    borderRadius: 60,
  },
  icon: {
    width: 60,
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
  iconNoti: {
    width: 40,
    height: '100%',
    justifyContent: 'center',
  },
  titleToolbar: {
    flex: 1,
    color: TEXT_DARK_COLOR,
    textAlign: 'center',
    fontWeight: 'normal',
  },
});

export default CategorieHeader;
