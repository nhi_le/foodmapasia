import React from 'react';
import {View, StyleSheet} from 'react-native';
import CategorieHeader from './CategorieHeader';
import CategorieBody from './CategorieBody';

const CategorieContainer = ({header, body, footer}) => {
  return (
    <View style={styles.container}>
      <CategorieHeader style={styles.header} {...header} />
      <CategorieBody style={styles.body} {...body} />
      {/* <CategorieFooter style={styles.footer} {...footer} /> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {},
  body: {
    flex: 1,
  },
  footer: {},
});

export default CategorieContainer;
