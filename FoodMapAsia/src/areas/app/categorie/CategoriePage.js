import React, {Component} from 'react';
import {connect} from 'react-redux';
import BackgroundTemplate from '@common/components/background/BackgroundTemplate';
import operations from '@redux/categories/operations';
import homeOperation from '@redux/home/operations';
import CategorieContainer from './components/CategorieContainer';
import {ICON_LOCATE, ICON_NOTIFICATION} from '@resources/images';
import homeOperations from '@redux/home/operations';

const mapStateToProps = (state) => ({
  tabSelected: state.profile.tabSelected,
  listCategory: state.categories.listCategory,
  listCategoryProduct: state.categories.listCategoryProduct,
  listProduct: state.categories.listProduct,
  locationList: state.home.listLocation,
  isShowModal: state.home.isVisible,
  listFilter: state.categories.listFilter,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  header: {
    title: 'categorie',
    locateButton: {
      svgIcon: ICON_LOCATE,
      onPress: () => {
        dispatch(homeOperation.setShowModal());
      },
    },
    searchInput: {
      placeholder: 'Nhập tên sẩn phẩm',
      onChangeText: async (text) => {
        await dispatch(operations.clearListProduct());
        if (text.isEmpty()) {
          await dispatch(operations.getListProduct());
        } else {
          dispatch(operations.searchProduct(text));
        }
      },
    },
    notiButton: {
      svgIcon: ICON_NOTIFICATION,
      onPress: async () => {
        ownProps.navigation.navigate('NotificationPage');
      },
    },
  },
  body: {
    listFilter: {
      isShow: true,
      options: [],
      onItemPress: async (index) => {
        await dispatch(operations.clearListProduct());
        await dispatch(operations.setSelectFilter(index));
      },
      placeholder: '',
    },
    listCategoryProduct: {
      data: [],
      onItemPress: (item) => {
        dispatch(operations.setCategoryProductSelected(item));
        dispatch(operations.getListProduct(item.handle));
        dispatch(operations.setSelectFilter(item.handle));
      },
    },
    listProduct: {
      data: [],
      onItemPress: async (data) => {
        await dispatch(homeOperations.updateProduct(data));
        ownProps.navigation.navigate('ProductDetailPage');
      },
      onEndReached: async () => {
        await dispatch(operations.loadMoreProduct());
      },
      onActionPress: () => {},
    },
    locationModal: {
      title: 'Chọn Khu Vực Giao Hàng',
      description: 'Chọn Khu Vực Giao Hàng',
      icon: {
        source: '',
      },
      yesButton: {
        text: 'Đến Chợ Rau Củ',
        onPress: () => {
          dispatch(homeOperation.setHideModal());
        },
        gradient: true,
      },
      listLocation: {
        data: [],
        onItemPress: (item) => {
          dispatch(homeOperation.updateSelectModal(item));
        },
      },
      onBackButtonPress: () => {},
      onBackdropPress: () => {
        dispatch(homeOperation.setHideModal());
      },
    },

    listCategory: {
      data: [],
      onItemPress: async (item, index) => {
        await dispatch(operations.clearListProduct());
        await dispatch(homeOperations.updateHandle(item));
        await dispatch(operations.getListCategory());
      },
      onActionPress: async (data) => {
        await dispatch(operations.clearListProduct());
        await dispatch(operations.setCategoryItemSelected(data.id));
        await dispatch(operations.getListCategoryProduct(data.handle));
      },
    },
  },
  citySelection: {
    isShow: true,
    options: ['Hồ Chí Minh', 'Hà Nội'],
    onItemPress: (index) => {},
    placeholder: 'Chọn tỉnh / thành',
  },
  footer: {
    buttonAddAddress: {
      text: 'Nhập Địa Chỉ Mới',
      onPress: () => {},
    },
  },
  actions: {
    getListCategory: () => dispatch(operations.getListCategory()),
  },
});

class CategoriePage extends Component {
  constructor() {
    super();
  }

  componentDidMount() {
    this.walletListener = this.props.navigation.addListener(
      'willFocus',
      async () => {
        await this.props.actions.getListCategory();
      },
    );
  }

  render() {
    const {
      header,
      body,
      footer,
      login,
      actions,
      tabSelected,
      listCategory,
      listCategoryProduct,
      listProduct,
      locationList,
      isShowModal,
      listFilter,
    } = this.props;
    return (
      <BackgroundTemplate scrollable={false} fullscreen={true}>
        <CategorieContainer
          header={{...header}}
          body={{
            ...body,
            listCategory: {
              ...body.listCategory,
              data: listCategory,
            },
            listCategoryProduct: {
              ...body.listCategoryProduct,
              data: listCategoryProduct,
            },
            listProduct: {
              ...body.listProduct,
              data: listProduct.data,
            },
            listFilter: {
              ...body.listFilter,
              options: listFilter.options,
              placeholder: listFilter.placeholder,
            },
            tabSelected: tabSelected,
            locationModal: {
              ...body.locationModal,
              isVisible: isShowModal,
              listLocation: {
                ...body.locationModal.listLocation,
                data: locationList.data,
              },
            },
          }}
          footer={{}}
        />
      </BackgroundTemplate>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(CategoriePage);
