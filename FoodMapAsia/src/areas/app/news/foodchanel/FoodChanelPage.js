import React from 'react';
import {connect} from 'react-redux';
import BackgroundTemplate from '@common/components/background/BackgroundTemplate';
import {ICON_PRE} from '@resources/images';
import FoodChanelContainer from './components/FoodChanelContainer';
import operations from '@redux/news/operations';
import {TITLE_FOODCHANEL} from '@resources/string/strings';
import {renderText} from '@common/components/StringHelper';

const mapStateToProps = (state) => ({
  tabSelected: state.news.tabSelected,
  listFoodChannel: state.news.listFoodChannel,
  listArticle: state.news.listArticle,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  header: {
    title: renderText(TITLE_FOODCHANEL),
  },
  body: {
    foodChannel: {
      onItemPress: (data) => {
        dispatch(operations.setCategorySelected(data.id));
      },
    },
    listArticle: {
      data: [''],
      onItemPress: async (data) => {
        await dispatch(operations.getArticleDetail(data));
        ownProps.navigation.navigate('ArticleDetailPage');
      },
    },
  },
  footer: {},
  actions: {
    getAPIBlogList: () => dispatch(operations.getAPIBlogList()),
  },
});

const FoodChanelPage = ({
  header,
  body,
  footer,
  login,
  actions,
  tabSelected,
  listFoodChannel,
  listArticle,
  ...props
}) => {
  return (
    <BackgroundTemplate
      scrollable={false}
      fullscreen={true}
      onInitialized={() => {
        actions.getAPIBlogList();
      }}>
      <FoodChanelContainer
        header={{...header}}
        body={{
          ...body,
          foodChannel: {
            ...body.foodChannel,
            data: listFoodChannel.data,
          },
          listArticle: {
            ...body.listArticle,
            data: listArticle.data,
          },
          tabSelected: tabSelected,
        }}
      />
    </BackgroundTemplate>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(FoodChanelPage);
