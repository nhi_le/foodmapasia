import React from 'react';
import {StyleSheet, ImageBackground} from 'react-native';
import {BACKGROUND_FOOTER} from '@resources/images';
import {TEXT_GRAY_LIGHT_COLOR} from '@resources/palette';
import {
  ACCENT_COLOR,
  TEXT_DARK_COLOR,
  BORDER_COLOR,
} from '@resources/palette';

const FoodChanelFooter = ({style}) => {
  return (
    <ImageBackground
      source={BACKGROUND_FOOTER}
      style={{
        ...styles.container,
        ...style,
      }}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    height: 120,
    paddingLeft: 20,
    paddingRight: 20,
  },
  signInButtonActive: {
    borderRadius: 10,
    backgroundColor: ACCENT_COLOR,
  },
  signInButton: {
    borderRadius: 10,
    backgroundColor: BORDER_COLOR,
  },
  textButtonActive: {
    color: TEXT_DARK_COLOR,
    width: null,
  },
  textButton: {
    color: TEXT_GRAY_LIGHT_COLOR,
    width: null,
  },
  forgotPassword: {
    alignItems: 'center',
  },
  forgotPasswordButton: {
    minHeight: 0,
    marginBottom: 20,
    borderBottomWidth: 2,
    borderColor: ACCENT_COLOR,
    borderRadius: 0,
    width: 115,
    paddingBottom: 2,
  },
  forgotPasswordButtonText: {
    color: TEXT_DARK_COLOR,
  },
});

export default FoodChanelFooter;
