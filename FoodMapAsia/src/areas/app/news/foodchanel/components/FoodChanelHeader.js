import React from 'react';
import {View, StyleSheet} from 'react-native';
import Title from '@common/components/text/Title';

import SvgIcon from '@common/components/icon/SvgIcon';
import {TEXT_DARK_COLOR} from '@resources/palette';
import {TouchableOpacity} from 'react-native-gesture-handler';

const FoodChanelHeader = ({style, title, backButton}) => {
  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <Title style={styles.titleToolbar} size="medium-title">
        {title}
      </Title>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 60,
    alignContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 10,
  },
  titleToolbar: {
    flex: 1,
    color: TEXT_DARK_COLOR,
    textAlign: 'center',
    fontWeight: 'normal',
  },
  icon: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 60,
    height: '100%',
  },
  welcome: {
    flex: 1,
  },
});

export default FoodChanelHeader;
