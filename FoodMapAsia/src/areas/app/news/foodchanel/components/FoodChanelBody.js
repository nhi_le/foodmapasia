import React from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';
import {WebView} from 'react-native-webview';
import ListContent from '@common/components/list/ListContent';
import DetailArticle from './DetailArticle';
const FoodChanelBody = ({
  style,
  foodChannel,
  tabSelected,
  listArticle,
  articleDetail,
}) => {
  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <ScrollView
        style={styles.scrollCategory}
        horizontal={true}
        showsHorizontalScrollIndicator={false}>
        <ListContent
          {...foodChannel}
          horizontal={true}
          type="account-category"
          style={styles.categoryList}
        />
      </ScrollView>
      <ScrollView style={{padding: 10}}>
        <ListContent {...listArticle} type="listArticle" />
        {/* <DetailArticle /> */}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  password: {
    marginTop: 20,
  },
  buttonRegister: {
    width: 200,
    marginTop: 30,
    justifyContent: 'center',
    alignSelf: 'center',
  },
  categoryList: {},
  scrollCategory: {
    flexGrow: 0,
  },
});

export default FoodChanelBody;
