import React from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';
import {WebView} from 'react-native-webview';
import ListContent from '@common/components/list/ListContent';
import TextContent from '@common/components/text/TextContent';
import Image from '@common/components/image/Image';
import Title from '@common/components/text/Title';
import {ACCENT_COLOR, COLOR_READ, COLOR_UNREAD} from '@resources/palette';
import HorizontalView from '@common/components/layout/HorizontalView';
import Button from '@common/components/button/Button';

const DetailArticle = ({
  style,
  listArticle,
  image = '',
  title = 'kasjfhksjadhf',
  published_at,
  content_preview = 'preview',
  author,
}) => {
  let text1 = 'Đăng bởi';
  let text3 = 'lúc';
  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <ScrollView>
        <View style={styles.firstItem}>
          <Image style={styles.ImageFirst} source={{uri: image}} />
          <Title size="large" style={styles.title}>
            {title.toUpperCase()}
          </Title>
          <HorizontalView style={styles.public}>
            <TextContent size="small" style={styles.text}>
              {text1}{' '}
            </TextContent>
            <TextContent size="small" style={styles.textUser}>
              {author}{' '}
            </TextContent>
            <TextContent size="small" style={styles.text}>
              {text3}{' '}
            </TextContent>
            <TextContent size="small" style={styles.text}>
              {published_at}{' '}
            </TextContent>
          </HorizontalView>
          <HorizontalView style={{flexDirection: 'row'}}>
            <Button
              text="Thich"
              style={{width: 50, height: 10, backgroundColor: 'blue'}}
              textStyle={{fontSize: 10}}
            />
            <Button
              text="Chia sẻ"
              style={{width: 50, height: 10, backgroundColor: 'blue'}}
            />
          </HorizontalView>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  //firt
  container: {
    flex: 1,
  },
  firstItem: {
    height: 350,
  },
  ImageFirst: {
    width: '100%',
    height: 230,
    marginBottom: 10,
  },
  title: {
    color: ACCENT_COLOR,
    marginBottom: 10,
  },
  public: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  text: {
    color: COLOR_READ,
  },
  textUser: {
    color: COLOR_READ,
    fontWeight: 'bold',
  },
  textPreview: {
    flexShrink: 1,
    height: 40,
    color: COLOR_UNREAD,
  },
});

export default DetailArticle;
