import React from 'react';
import {View, StyleSheet} from 'react-native';
import FoodChanelHeader from './FoodChanelHeader';
import FoodChanelBody from './FoodChanelBody';
import FoodChanelFooter from './FoodChanelFooter';

const FoodChanelContainer = ({header, body, footer}) => {
  return (
    <View style={styles.container}>
      <FoodChanelHeader style={styles.header} {...header} />
      <FoodChanelBody style={styles.body} {...body} />
      {/* <FoodChanelFooter style={styles.footer} {...footer} /> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {},
  body: {
    flex: 1,
  },
  footer: {},
});

export default FoodChanelContainer;
