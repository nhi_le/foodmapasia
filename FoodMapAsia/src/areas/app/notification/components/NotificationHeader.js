import React from 'react';
import {View, StyleSheet} from 'react-native';
import Title from '@common/components/text/Title';

import SvgIcon from '@common/components/icon/SvgIcon';
import {TEXT_DARK_COLOR} from '@resources/palette';
import {ICON_PRE} from '@resources/images';
import {LIGHT_GRAY_COLOR} from '@resources/palette';

const NotificationHeader = ({style, title, backButton}) => {
  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <SvgIcon height={18} width={18} {...backButton} svgIcon={ICON_PRE} />
      <Title style={styles.titleToolbar} size="medium-title">
        {title}
      </Title>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 70,
    alignContent: 'center',
    flexDirection: 'row',
    paddingStart: 15,
    paddingEnd: 20,
    paddingTop: 20,
    borderBottomColor: LIGHT_GRAY_COLOR,
    borderBottomWidth: 1,
    alignItems: 'center',
  },
  titleToolbar: {
    lineHeight: 21,
    fontWeight: 500,
    flex: 1,
    color: TEXT_DARK_COLOR,
    textAlign: 'center',
    fontWeight: 'normal',
  },
  iconToolbar: {
    width: 24,
    height: 24,
  },
  welcome: {
    flex: 1,
  },
});

export default NotificationHeader;
