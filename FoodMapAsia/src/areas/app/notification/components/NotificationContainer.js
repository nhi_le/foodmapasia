import React from 'react';
import {View, StyleSheet} from 'react-native';
import NotificationHeader from './NotificationHeader';
import NotificationBody from './NotificationBody';
import {BACKGROUND_COLOR_TRANS} from '../../../../resources/palette';

const NotificationContainer = ({header, body}) => {
  return (
    <View style={styles.container}>
      <NotificationHeader style={styles.header} {...header} />
      <View style={styles.distance} />
      <NotificationBody style={styles.body} {...body} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {},
  body: {
    flex: 1,
  },
  footer: {},
  distance: {
    // backgroundColor: BACKGROUND_COLOR_TRANS,
    height: 12,
  },
});

export default NotificationContainer;
