import React from 'react';
import {View, StyleSheet, Text, ScrollView} from 'react-native';
import ListContent from '@common/components/list/ListContent';
import {
  LIGHT_GRAY_COLOR,
  COLOR_READ,
  BACKGROUND_COLOR_TRANS,
} from '@resources/palette';
import {CONTENT_NUL_NOTIFICATION} from '@resources/string/strings';
import {renderText} from '@common/components/StringHelper';

const NotificationBody = ({style, listNotification}) => {
  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      {listNotification && listNotification.data.length > 0 ? (
        <ScrollView
          style={styles.scrollCategory}
          showsHorizontalScrollIndicator={false}>
          <ListContent
            {...listNotification}
            type="notification"
            style={styles.notificationList}
          />
        </ScrollView>
      ) : (
        <Text style={styles.textNullNotification}>
          {renderText(CONTENT_NUL_NOTIFICATION)}
        </Text>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: BACKGROUND_COLOR_TRANS,
  },
  notificationList: {
    flex: 1,
  },
  scrollCategory: {
    flexGrow: 0,
    borderBottomColor: LIGHT_GRAY_COLOR,
    borderBottomWidth: 1,
  },
  textNullNotification: {
    fontSize: 12,
    color: COLOR_READ,
    textAlign: 'center',
    marginTop: 58,
  },
});

export default NotificationBody;
