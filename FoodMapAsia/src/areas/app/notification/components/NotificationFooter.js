import React from 'react';
import {StyleSheet, View} from 'react-native';
import Button from '@common/components/button/Button';
import {BUTTON_LOGIN_COLOR} from '@resources/palette';
import {BUTTON_SEEN_NOTIFICATION} from '@resources/string/strings';
import {GRAY_COLOR} from '../../../../resources/palette';

const NotificationFooter = ({style, ListIsNull}) => {
  return (
    <View style={styles.container}>
      <Button
        buttonStyle={ListIsNull ? styles.styleButton : styles.styleButtonIsNull}
        text={BUTTON_SEEN_NOTIFICATION}
        textStyle={styles.textStyle}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderTopColor: GRAY_COLOR,
    zIndex: 1,
    borderTopWidth: 1,
    height: 80,
    paddingLeft: 20,
    paddingRight: 20,
    justifyContent: 'center',
  },
  styleButton: {
    backgroundColor: BUTTON_LOGIN_COLOR,
    borderRadius: 22,
  },
  styleButtonIsNull: {
    backgroundColor: '#E0E0E0',
    borderRadius: 22,
  },
  textStyle: {
    color: '#ffffff',
  },
});

export default NotificationFooter;
