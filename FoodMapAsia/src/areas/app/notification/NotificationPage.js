import React from 'react';
import {connect} from 'react-redux';
import BackgroundTemplate from '@common/components/background/BackgroundTemplate';
import {ICON_CANCEL} from '@resources/images';
import NotificationContainer from './components/NotificationContainer';
import {TITLE_NOTIFICATION} from '@resources/string/strings';
import {renderText} from '@common/components/StringHelper';
import operations from '@redux/notifications/operations';

const mapStateToProps = (state) => ({
  listNotification: state.notifications.listNotification,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  header: {
    title: renderText(TITLE_NOTIFICATION),
    backButton: {
      svgIcon: ICON_CANCEL,
      onPress: async () => {
        ownProps.navigation.goBack();
      },
    },
  },
  body: {
    listNotification: {
      data: [],
      onItemPress: (item) => {
        dispatch(operations.updateReadNotification(item.id));
      },
      onActionPress: () => {},
    },
  },
  actions: {
    getListNotification: () => {},
  },
});

const NotificationPage = ({
  header,
  body,
  login,
  actions,
  listNotification,
  ...props
}) => {
  return (
    <BackgroundTemplate
      scrollable={false}
      fullscreen={true}
      onInitialized={() => {
        actions.getListNotification();
      }}>
      <NotificationContainer
        header={{...header}}
        body={{
          ...body,
          listNotification: {
            ...body.listNotification,
            data: listNotification,
          },
        }}
      />
    </BackgroundTemplate>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(NotificationPage);
