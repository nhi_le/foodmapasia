import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import FoodChanelPage from './news/foodchanel/FoodChanelPage';
import NotificationPage from './notification/NotificationPage';
import ManagerAccountPage from './profile/manager-account/ManagerAccountPage';
import CartDetailPage from './profile/cart_detail/CartDetailPage';
import CartListPage from './cart/cart-list/CartListPage';
import CategoriePage from './categorie/CategoriePage';
import FavoritePage from './favorite/FavoritePage';
import HomePage from './home/HomePage';
import ProductDetailPage from './product-detail/ProductDetailPage';
import ArticleDetailPage from './article-detail/ArticleDetailPage';
import CartTransportPage from './cart/cart-transport/CartTransportPage';
import CartPaymentPage from './cart/cart-payment/CartPaymentPage';
import CartNotiOrderPage from './cart/cart-noti-order/CartNotiOrderPage';
import LoginPage from '../auth/login/LoginPage';
import RegisterPage from '../auth/register/RegisterPage';
import ForgotPasswordPage from '../auth/forgot-password/ForgotPasswordPage';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {
  ICON_TABBAR_HOME,
  ICON_HOME,
  ICON_MENU,
  ICON_MENU_OUTLINE,
  ICON_NEW,
  ICON_HEART,
  ICON_CART,
  ICON_PROFILE,
  LOGO_FOODMAP,
  ICON_NEW_OUTLINE,
  ICON_HEART_OUTLINE,
  ICON_PROFILE_OUTLINE,
  ICON_CART_OUTLINE,
} from '../../resources/images';
import {
  PLACEHOLDER_COLOR,
  WHITE_COLOR,
  DANGEROUS_COLOR,
  BUTTON_LOGIN_COLOR,
} from '../../resources/palette';
import {BOTTOM_TABBAR_HEIGHT} from '../../resources/dimensions';
import BadgeTabIcon from '../../common/components/icon/BadgeTabIcon';

const hideTabBarScreen = ['ProductDetailPage', 'NotificationPage'];

const FoodChanelNavigator = createStackNavigator(
  {
    FoodChanelPage: FoodChanelPage,
    ArticleDetailPage: ArticleDetailPage,
  },
  {
    initialRouteName: 'FoodChanelPage',
    uriPrefix: 'taskkap://vendor/',
    defaultNavigationOptions: {
      header: null,
    },
  },
);

FoodChanelNavigator.navigationOptions = ({navigation}) => {
  return {
    tabBarVisible: !hideTabBarScreen.includes(
      navigation.state.routes[navigation.state.index].routeName,
    ),
  };
};

const FavoriteNavigator = createStackNavigator(
  {
    FavoritePage: FavoritePage,
  },
  {
    initialRouteName: 'FavoritePage',
    uriPrefix: 'taskkap://vendor/',
    defaultNavigationOptions: {
      header: null,
    },
  },
);

FavoriteNavigator.navigationOptions = ({navigation}) => {
  return {
    tabBarVisible: !hideTabBarScreen.includes(
      navigation.state.routes[navigation.state.index].routeName,
    ),
  };
};

const ProfileNavigator = createStackNavigator(
  {
    LoginPage: LoginPage,
    RegisterPage: RegisterPage,
    ForgotPasswordPage: ForgotPasswordPage,
    ManagerAccountPage: ManagerAccountPage,
    CartDetailPage: CartDetailPage,
  },
  {
    initialRouteName: 'ManagerAccountPage',
    uriPrefix: 'taskkap://vendor/',
    defaultNavigationOptions: {
      header: null,
    },
  },
);

ProfileNavigator.navigationOptions = ({navigation}) => {
  return {
    tabBarVisible: !hideTabBarScreen.includes(
      navigation.state.routes[navigation.state.index].routeName,
    ),
  };
};

const CartNavigator = createStackNavigator(
  {
    CartListPage: CartListPage,
    CartTransportPage: CartTransportPage,
    CartPaymentPage: CartPaymentPage,
    CartNotiOrderPage: CartNotiOrderPage,
  },
  {
    initialRouteName: 'CartListPage',
    uriPrefix: 'taskkap://vendor/',
    defaultNavigationOptions: {
      header: null,
    },
  },
);

CartNavigator.navigationOptions = ({navigation}) => {
  return {
    tabBarVisible: !hideTabBarScreen.includes(
      navigation.state.routes[navigation.state.index].routeName,
    ),
  };
};

const CategorieNavigator = createStackNavigator(
  {
    CategoriePage: CategoriePage,
    NotificationPage: NotificationPage,
    ProductDetailPage: ProductDetailPage,
  },
  {
    initialRouteName: 'CategoriePage',
    uriPrefix: 'taskkap://vendor/',
    defaultNavigationOptions: {
      header: null,
    },
  },
);

CategorieNavigator.navigationOptions = ({navigation}) => {
  return {
    tabBarVisible: !hideTabBarScreen.includes(
      navigation.state.routes[navigation.state.index].routeName,
    ),
  };
};

const HomeNavigator = createStackNavigator(
  {
    HomePage: HomePage,
    NotificationPage: NotificationPage,
    ProductDetailPage: ProductDetailPage,
    CartTransportPage: CartTransportPage,
    CartPaymentPage: CartPaymentPage,
    CartNotiOrderPage: CartNotiOrderPage,
    CartListPage: CartListPage,
  },
  {
    initialRouteName: 'HomePage',
    uriPrefix: 'taskkap://vendor/',
    defaultNavigationOptions: {
      header: null,
    },
  },
);

HomeNavigator.navigationOptions = ({navigation}) => {
  return {
    tabBarVisible: !hideTabBarScreen.includes(
      navigation.state.routes[navigation.state.index].routeName,
    ),
  };
};

const AppNavigator = createBottomTabNavigator(
  {
    Home: HomeNavigator,
    Menu: CategorieNavigator,
    News: FoodChanelNavigator,
    LoveList: FavoriteNavigator,
    Cart: CartNavigator,
    Profile: ProfileNavigator,
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, horizontal, tintColor}) => {
        const {routeName} = navigation.state;
        let svgIcon = ICON_TABBAR_HOME;
        switch (routeName) {
          case 'Home':
            if (focused) {
              return <LOGO_FOODMAP fill={tintColor} />;
            } else {
              return <ICON_HOME fill={tintColor} />;
            }
          case 'Menu':
            if (focused) {
              return <ICON_MENU fill={tintColor} />;
            } else {
              return <ICON_MENU_OUTLINE />;
            }
          case 'News':
            if (focused) {
              return <ICON_NEW fill={tintColor} />;
            } else {
              return <ICON_NEW_OUTLINE />;
            }

          case 'LoveList':
            if (focused) {
              return <ICON_HEART fill={tintColor} />;
            } else {
              return <ICON_HEART_OUTLINE />;
            }
          case 'Cart':
            if (focused) {
              svgIcon = ICON_CART;
            } else {
              svgIcon = ICON_CART_OUTLINE;
            }
            break;
          case 'Profile':
            if (focused) {
              return <ICON_PROFILE fill={tintColor} />;
            } else {
              return <ICON_PROFILE_OUTLINE />;
            }
        }
        if (routeName === 'Cart') {
          return (
            <BadgeTabIcon
              size={25}
              svgIcon={svgIcon}
              tintColor={tintColor}
              badgeColor={DANGEROUS_COLOR}
              textColor={WHITE_COLOR}
            />
          );
        }
      },
    }),
    tabBarOptions: {
      activeTintColor: BUTTON_LOGIN_COLOR,
      inactiveTintColor: PLACEHOLDER_COLOR,
      style: {
        height: BOTTOM_TABBAR_HEIGHT,
      },
      showLabel: false,
    },
  },
);
const AppContainer = createAppContainer(AppNavigator);
export default AppContainer;
