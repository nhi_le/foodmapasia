import React from 'react';
import {StyleSheet, View, TouchableOpacity} from 'react-native';
import Button from '@common/components/button/Button';
import {
  BACKGROUND_PROFILE,
  BUTTON_LOGIN_COLOR,
  GRAY_COLOR,
  WHITE_COLOR,
  LIGHT_GRAY_COLOR,
  ACCENT_COLOR,
  COLOR_READ,
} from '@resources/palette';
import TextContent from '@common/components/text/TextContent';
import Input from '@common/components/input/Input';
import HorizontalView from '@common/components/layout/HorizontalView';
import OutlineButton from '@common/components/button/OutlineButton';
import SvgIcon from '@common/components/icon/SvgIcon';
import {ICON_RADIO_UNCHECKED, ICON_RADIO_CHECKED} from '@resources/images';
import {SET_DEFAULT_ADDRESS} from '@resources/string/strings';

const ManagerAccountFooter = ({
  style,
  tabSelected,
  buttonAddAddress,
  isInputAddress,
  buttonDiscard,
  buttonNewAddress,
  buttonUpdateAddress,
  formInputAddress,
  listCity,
  formValidate,
  form,
}) => {
  if (!isInputAddress) {
    return (
      <View style={tabSelected === 1 ? styles.container : styles.containerNon}>
        <View style={styles.containerButtonAddAddress}>
          <Button
            style={styles.buttonAddAddress}
            buttonStyle={styles.buttonAdd}
            textStyle={styles.texButton}
            {...buttonAddAddress}
          />
        </View>
      </View>
    );
  } else {
    return (
      <View style={styles.containerMain}>
        <View>
          <Input
            inputStyle={styles.inputStyle}
            {...form.firstName}
            value={formInputAddress.firstName}
            errorMessage={formValidate.lastName}
            errorMessageStyle={styles.errorStyle}
          />
          <Input
            inputStyle={styles.inputStyle}
            {...form.lastName}
            value={formInputAddress.lastName}
            errorMessage={formValidate.lastName}
            errorMessageStyle={styles.errorStyle}
          />

          <Input
            inputStyle={styles.inputStyle}
            {...form.address}
            value={formInputAddress.address1}
            errorMessage={formValidate.address1}
            errorMessageStyle={styles.errorStyle}
          />

          <Input
            inputStyle={styles.inputStyle}
            {...form.phone}
            value={formInputAddress.phoneNumber}
            errorMessage={formValidate.phoneNumber}
            errorMessageStyle={styles.errorStyle}
          />
          <TouchableOpacity
            onPress={() => {
              form.radioButton.onPress &&
                form.radioButton.onPress(formInputAddress.isDefault);
            }}>
            <HorizontalView style={styles.defaultView}>
              <SvgIcon
                width={18}
                height={18}
                svgIcon={
                  formInputAddress.isDefault === 1
                    ? ICON_RADIO_CHECKED
                    : ICON_RADIO_UNCHECKED
                }
              />
              <TextContent style={styles.defaultText}>
                {SET_DEFAULT_ADDRESS}
              </TextContent>
            </HorizontalView>
          </TouchableOpacity>
          <HorizontalView style={styles.horizontalButtonAddAddress}>
            {formInputAddress.isUpdate ? (
              <Button
                style={styles.button}
                buttonStyle={styles.buttonAdd}
                textStyle={styles.texButton}
                {...buttonUpdateAddress}
              />
            ) : (
              <Button
                style={styles.button}
                buttonStyle={styles.buttonAdd}
                textStyle={styles.texButton}
                {...buttonNewAddress}
              />
            )}

            <OutlineButton
              style={styles.button}
              textStyle={styles.textStyle}
              buttonStyle={styles.buttonDiscard}
              {...buttonDiscard}
            />
          </HorizontalView>
        </View>
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    height: 70,
    backgroundColor: BACKGROUND_PROFILE,
    borderTopColor: LIGHT_GRAY_COLOR,
    borderTopWidth: 1,
  },
  containerNon: {
    height: 0,
  },
  containerMain: {
    borderTopColor: LIGHT_GRAY_COLOR,
    borderTopWidth: 1,
  },
  textStyle: {
    color: ACCENT_COLOR,
  },
  containerButtonAddAddress: {
    height: 60,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: 'white',
    borderTopColor: GRAY_COLOR,
  },
  horizontalButtonAddAddress: {
    paddingBottom: 15,
    alignItems: 'center',
    backgroundColor: 'white',
    paddingHorizontal: 8,
    borderTopColor: GRAY_COLOR,
  },
  normalContainer: {
    height: 30,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: 'white',
    borderTopColor: GRAY_COLOR,
  },
  button: {
    flex: 1,
    paddingHorizontal: 8,
  },
  buttonAddAddress: {
    width: 200,
  },
  buttonAdd: {
    backgroundColor: BUTTON_LOGIN_COLOR,
    borderRadius: 40,
  },
  buttonDiscard: {
    backgroundColor: WHITE_COLOR,
    borderRadius: 40,
  },
  texButton: {
    color: WHITE_COLOR,
  },
  inputStyle: {
    fontSize: 14,
    alignSelf: 'center',
    borderRadius: 20,
    borderColor: GRAY_COLOR,
    backgroundColor: 'white',
    marginTop: 10,
    width: 352,
    height: 40,
  },
  errorStyle: {
    fontSize: 12,
    alignSelf: 'center',
    marginTop: 10,
    marginLeft: 10,
    width: 352,
  },
  defaultText: {
    marginLeft: 10,
    textAlign: 'center',
    alignSelf: 'center',
    color: COLOR_READ,
  },
  defaultView: {
    padding: 15,
  },
  selecttionStyle: {
    borderRadius: 15,
    backgroundColor: 'white',
    height: 30,
    width: 170,
  },
  selecttionText: {
    flex: 1,
    color: COLOR_READ,
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'left',
    marginLeft: 10,
  },
});

export default ManagerAccountFooter;
