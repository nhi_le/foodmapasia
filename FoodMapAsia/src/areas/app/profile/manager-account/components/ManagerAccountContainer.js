import React from 'react';
import {View, StyleSheet} from 'react-native';
import ManagerAccountHeader from './ManagerAccountHeader';
import ManagerAccountBody from './ManagerAccountBody';
import ManagerAccountFooter from './ManagerAccountFooter';

const LoginContainer = ({header, body, footer}) => {
  return (
    <View style={styles.container}>
      <ManagerAccountHeader style={styles.header} {...header} />
      <ManagerAccountBody style={styles.body} {...body} />
      <ManagerAccountFooter style={styles.footer} {...footer} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {},
  body: {
    flex: 1,
  },
  footer: {},
});

export default LoginContainer;
