import React from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';
import ListContent from '@common/components/list/ListContent';
import {
  TEXT_GRAY_LIGHT_COLOR,
  BACKGROUND_PROFILE,
  BUTTON_LOGIN_COLOR,
  LIGHT_GRAY_COLOR,
  ACCENT_COLOR,
  COLOR_READ,
} from '@resources/palette';
import Title from '@common/components/text/Title';
import TextContent from '@common/components/text/TextContent';
import TransparentButton from '@common/components/button/Button';
import WebView from 'react-native-webview';

const ManagerAccountBody = ({
  style,
  categoryList,
  infoAccount,
  listInfoOrder,
  listAddress,
  listOrder,
  tabSelected,
  user,
  logoutWebview,
  isAuth,
}) => {
  const userJson = user ? user : '';
  let title = '';
  let type = '';
  let listData = {};

  switch (tabSelected) {
    case 0:
      title = listInfoOrder.title;
      type = 'order';
      listData = listInfoOrder;
      break;
    case 1:
      title = listAddress.title;
      type = 'address';
      listData = listAddress;
      break;
    case 2:
      title = listOrder.title;
      type = 'order';
      listData = listOrder.tabs.orderAll.listOrderHistory;
  }
  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <ScrollView
        style={styles.scrollCategory}
        horizontal={true}
        showsHorizontalScrollIndicator={false}>
        <ListContent
          {...categoryList}
          horizontal={true}
          type="account-category"
          style={styles.categoryList}
        />
      </ScrollView>
      <View style={styles.containerList}>
        {tabSelected === 0 && (
          <Title style={styles.titleInfo} size={'medium'}>
            {infoAccount.title}
          </Title>
        )}

        {tabSelected === 0 && (
          <View style={styles.containerInfo}>
            <Title size={'normal'} style={styles.titleName}>
              {user && userJson.email}
            </Title>
            <TextContent size={'normal'} style={styles.address}>
              {infoAccount.infoAccount.address}
            </TextContent>
            <TextContent size={'normal'} style={styles.address}>
              {infoAccount.infoAccount.numberPhone}
            </TextContent>
            <View style={styles.buttonDetailContainer}>
              <TransparentButton
                buttonStyle={styles.buttonDetail}
                textStyle={styles.textButtonDetail}
                text={infoAccount.info.buttonDetail.text}
                onPress={infoAccount.info.buttonDetail.onPress}
              />
            </View>
          </View>
        )}

        <Title style={styles.titleInfo} size={'medium'}>
          {title}
        </Title>

        {listData && listData.data && listData.data.length > 0 ? (
          <ListContent {...listData} type={type} style={styles.categoryList} />
        ) : (
          <TextContent style={styles.noData}>{'Không có dữ liệu'}</TextContent>
        )}
      </View>
      {!isAuth && <WebView style={styles.webViewStyle} {...logoutWebview} />}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tab: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  tabButton: {
    backgroundColor: 'transparent',
  },
  scrollCategory: {
    flexGrow: 0,
    borderBottomColor: LIGHT_GRAY_COLOR,
    borderBottomWidth: 1,
  },
  categoryList: {},
  containerList: {
    flex: 1,
    backgroundColor: BACKGROUND_PROFILE,
  },
  titleInfo: {
    color: BUTTON_LOGIN_COLOR,
    opacity: 1,
    marginHorizontal: 15,
    marginVertical: 15,
  },
  containerInfo: {
    backgroundColor: 'white',
    height: 155,
    padding: 12,
    marginBottom: 10,
  },
  titleName: {
    color: TEXT_GRAY_LIGHT_COLOR,
  },
  address: {
    color: TEXT_GRAY_LIGHT_COLOR,
    marginTop: 12,
  },
  buttonDetailContainer: {
    alignSelf: 'flex-start',
    position: 'absolute',
    bottom: 1,
    marginLeft: 10,
  },
  buttonDetail: {
    backgroundColor: 'transparent',
    width: 100,
  },
  textButtonDetail: {
    color: BUTTON_LOGIN_COLOR,
    textAlign: 'justify',
  },
  noData: {
    color: ACCENT_COLOR,
    textAlign: 'center',
    marginTop: 100,
  },
  line: {
    height: 20,
    width: 1,
    alignSelf: 'center',
    backgroundColor: COLOR_READ,
    paddingVertical: 8,
  },
  textSelected: {
    color: ACCENT_COLOR,
  },
  textUnSelected: {
    color: COLOR_READ,
  },
  webViewStyle: {
    width: 0,
    height: 0,
  },
});

export default ManagerAccountBody;
