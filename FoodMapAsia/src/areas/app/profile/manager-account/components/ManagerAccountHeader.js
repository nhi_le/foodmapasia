import React from 'react';
import {View, StyleSheet} from 'react-native';
import Title from '@common/components/text/Title';
import SvgIcon from '@common/components/icon/SvgIcon';
import {TEXT_DARK_COLOR} from '@resources/palette';

const ManagerAccountHeader = ({style, title, logoutButton}) => {
  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <Title style={styles.titleToolbar} size="medium-title" text={title}>
        {title}
      </Title>
      <SvgIcon height={24} width={24} {...logoutButton} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 60,
    alignContent: 'center',
    flexDirection: 'row',
    paddingStart: 20,
    paddingEnd: 20,
    paddingTop: 20,
  },
  titleToolbar: {
    flex: 1,
    color: TEXT_DARK_COLOR,
    textAlign: 'center',
    fontWeight: 'normal',
  },
  iconToolbar: {
    width: 24,
    height: 24,
  },
  welcome: {
    flex: 1,
  },
});

export default ManagerAccountHeader;
