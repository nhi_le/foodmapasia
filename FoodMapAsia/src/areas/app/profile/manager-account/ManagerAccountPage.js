import React, {Component} from 'react';
import {connect} from 'react-redux';
import BackgroundTemplate from '@common/components/background/BackgroundTemplate';
import operations from '@redux/profile/operations';
import authOperations from '@redux/auth/operations';
import ManagerAccountContainer from './components/ManagerAccountContainer';
import {ICON_LOGOUT} from '../../../../resources/images';

const mapStateToProps = (state) => ({
  tabSelected: state.profile.tabSelected,
  listCategory: state.profile.listCategory,
  login: state.auth.login,
  isAuthorized: state.auth.isAuthorized,
  userInfo: state.auth.user,
  listAddress: state.profile.listAddress,
  listOrderHistory: state.profile.listOrderHistory,
  infoAccount: state.profile.infoAccount,
  listOrderToday: state.profile.listOrderToday,
  isInputAddress: state.profile.isAddressInput,
  formInputAddress: state.profile.formInputAddress,
  listCity: state.profile.listCity,
  formValidate: state.profile.formValidate,
  tabOrderSelected: state.profile.tabOrderSelected,
});
import {HOLDER_ADDRESS, HOLDER_PHONENUMBER} from '@resources/string/strings';

let fullNameRef = null;
let passwordRef = null;
let phoneRef = null;
let addressRef = null;

const mapDispatchToProps = (dispatch, ownProps) => ({
  header: {
    title: 'Quản Lý Tài Khoản',
    logoutButton: {
      svgIcon: ICON_LOGOUT,
      onPress: async () => {
        await dispatch(authOperations.logout());
        ownProps.navigation.navigate('LoginPage');
      },
    },
  },
  body: {
    categoryList: {
      data: [],
      onItemPress: (data) => {
        dispatch(operations.setCategorySelected(data.id));
      },
    },
    infoAccount: {
      title: 'Thông Tin Tài Khoản',
      info: {
        buttonDetail: {
          text: 'Xem địa chỉ >',
          onPress: () => {},
        },
      },
    },
    listInfoOrder: {
      title: 'Các Đơn Hàng Đã Đặt',
      data: [],
      onItemPress: (data) => {
        dispatch(operations.setOrderForm(data));
        ownProps.navigation.navigate('CartDetailPage');
      },
      onActionPress: (type, id) => {},
    },
    listAddress: {
      title: 'Danh Sách Địa Chỉ',
      data: [],
      onItemPress: (data) => {},
      onActionPress: (type, id) => {
        switch (type) {
          case 'cancel':
            dispatch(operations.removeAddress(id));
            break;
          case 'edit':
            dispatch(operations.setAddressInputUpdate(id));
            break;
        }
      },
    },
    listOrder: {
      title: 'Danh Sách Đơn Hàng',
      tabs: {
        orderToday: {
          text: 'Đơn Giao Hôm Nay',
          listOrderToday: {
            data: [],
            onItemPress: () => {
              ownProps.navigation.navigate('CartDetailPage');
            },
          },
          onPress: () => {
            dispatch(operations.setTabOrderSelected(0));
          },
        },
        orderAll: {
          text: 'Đơn Hàng Đã Đặt',
          listOrderHistory: {
            data: [],
            onItemPress: (data) => {
              dispatch(operations.setOrderForm(data));
              ownProps.navigation.navigate('CartDetailPage');
            },
          },
          onPress: () => {
            dispatch(operations.setTabOrderSelected(1));
          },
        },
      },
    },
    logoutWebView: {
      sharedCookiesEnabled: true,
      scrollEnabled: true,
      javaScriptEnabled: true,
      domStorageEnabled: true,
      source: {
        uri: 'https://choraucu.myharavan.com/account/logout',
      },
    },
  },
  footer: {
    buttonAddAddress: {
      text: 'Nhập Địa Chỉ Mới',
      onPress: () => {
        dispatch(operations.setAddressInput());
      },
    },
    buttonNewAddress: {
      text: 'Thêm mới',
      onPress: async () => {
        await dispatch(operations.addNewAddress());
        await dispatch(operations.getListAddress());
      },
    },
    buttonUpdateAddress: {
      text: 'Cập nhật',
      onPress: async () => {
        await dispatch(operations.updateAddress());
        await dispatch(operations.getListAddress());
      },
    },
    buttonDiscard: {
      text: 'Hủy',
      onPress: () => {
        dispatch(operations.setAddressInput());
      },
    },
    listCity: {
      isShow: true,
      options: [],
      onItemPress: (index) => {
        dispatch(operations.setSelectCity(index));
      },
      placeholder: '',
    },
    form: {
      lastName: {
        label: '',
        placeholder: 'Last Name',
        isAlwayShowingLabel: true,
        autoCapitalize: 'none',
        returnKeyType: 'next',
        errorMessage: '',
        onChangeText: async (text) => {
          dispatch(operations.setFormInput({lastName: text}));
        },
        inputRef: (ref) => {
          fullNameRef = ref;
        },
        onSubmitEditing: async () => {
          fullNameRef.focus();
        },
      },
      firstName: {
        label: '',
        placeholder: 'First Name',
        isAlwayShowingLabel: true,
        autoCapitalize: 'none',
        returnKeyType: 'next',
        errorMessage: '',
        onChangeText: async (text) => {
          dispatch(operations.setFormInput({firstName: text}));
        },
        inputRef: (ref) => {
          fullNameRef = ref;
        },
        onSubmitEditing: async () => {
          fullNameRef.focus();
        },
      },
      phone: {
        label: '',
        placeholder: HOLDER_PHONENUMBER,
        isAlwayShowingLabel: true,
        returnKeyType: 'next',
        keyboardType: 'numeric',
        onChangeText: (text) => {
          dispatch(operations.setFormInput({phoneNumber: text}));
        },
        inputRef: (ref) => {
          phoneRef = ref;
        },
        blurOnSubmit: false,
        onSubmitEditing: async () => {
          addressRef.focus();
        },
        autoCapitalize: 'none',
      },
      address: {
        label: '',
        placeholder: HOLDER_ADDRESS,
        isAlwayShowingLabel: true,
        returnKeyType: 'next',
        onChangeText: (text) => {
          dispatch(operations.setFormInput({address1: text}));
        },
        inputRef: (ref) => {
          addressRef = ref;
        },
        blurOnSubmit: false,
        onSubmitEditing: async () => {},
        autoCapitalize: 'none',
      },
      radioButton: {
        onPress: (text) => {
          switch (text) {
            case 0:
              dispatch(operations.setFormInput({isDefault: 1}));
              break;
            case 1:
              dispatch(operations.setFormInput({isDefault: 0}));
              break;
          }
        },
      },
    },
  },
  actions: {
    checkAuth: (isAuth) => {
      if (!isAuth) {
        ownProps.navigation.navigate('LoginPage');
      }
    },
    getListOrderHistory: () => dispatch(operations.getListOrderHistory()),
    getListAddress: () => dispatch(operations.getListAddress()),
    getInfoAccount: () => dispatch(operations.getInfoAccount()),
  },
});

class ManagerAccountPage extends Component {
  constructor() {
    super();
  }

  componentDidMount() {
    this.walletListener = this.props.navigation.addListener('willFocus', () => {
      this.props.actions.getListOrderHistory();
      this.props.actions.getListAddress();
      this.props.actions.getInfoAccount();
    });
  }

  render() {
    const {
      header,
      body,
      footer,
      actions,
      tabSelected,
      listCategory,
      isAuthorized,
      userInfo,
      infoAccount,
      listAddress,
      listOrderHistory,
      isInputAddress,
      formInputAddress,
      listCity,
      formValidate,
      tabOrderSelected,
      listOrderToday,
    } = this.props;
    return (
      <BackgroundTemplate
        scrollable={false}
        fullscreen={true}
        onInitialized={() => {
          actions.checkAuth(isAuthorized);
        }}>
        <ManagerAccountContainer
          header={{...header}}
          body={{
            ...body,
            isAuth: isAuthorized,
            categoryList: {
              ...body.categoryList,
              data: listCategory,
            },
            user: userInfo,
            tabSelected: tabSelected,
            listAddress: {
              ...body.listAddress,
              data: listAddress,
            },
            listOrderHistory: {
              ...body.listOrderHistory,
              data: listOrderHistory,
            },
            infoAccount: {
              ...body.infoAccount,
              infoAccount,
            },
            listInfoOrder: {
              ...body.listInfoOrder,
              data: listOrderHistory,
            },
            listOrder: {
              ...body.listOrder,
              tabs: {
                ...body.listOrder.tabs,
                orderToday: {
                  ...body.listOrder.tabs.orderToday,
                  listOrderToday: {
                    ...body.listOrder.tabs.orderToday.listOrderToday,
                    data: listOrderToday,
                  },
                },
                orderAll: {
                  ...body.listOrder.tabs.orderAll,
                  listOrderHistory: {
                    ...body.listOrder.tabs.orderAll.listOrderHistory,
                    data: listOrderHistory,
                  },
                },
              },
            },
            tabOrderSelected: tabOrderSelected,
          }}
          footer={{
            ...footer,
            ...body,
            tabSelected: tabSelected,
            isInputAddress: isInputAddress,
            formInputAddress: formInputAddress,
            listCity: {
              ...footer.listCity,
              ...(footer.listCity.options = listCity.options),
              ...(footer.listCity.placeholder = listCity.placeholder),
            },
            formValidate: formValidate,
          }}
        />
      </BackgroundTemplate>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManagerAccountPage);
