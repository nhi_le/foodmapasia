import React from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';
import ListContent from '@common/components/list/ListContent';
import {TEXT_GRAY_LIGHT_COLOR, BUTTON_LOGIN_COLOR} from '@resources/palette';
import Title from '@common/components/text/Title';
import TextContent from '@common/components/text/TextContent';
import HorizontalView from '@common/components/layout/HorizontalView';
import {currencyConvert} from '@common/components/StringHelper';

const CartDetailBody = ({
  style,
  recivieAddress,
  shippingAddress,
  oderInfo,
  cartDetail,
}) => {
  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <View style={styles.containerInfo}>
        <ScrollView>
          <HorizontalView>
            <Title size={'normal'} style={styles.parentTitle}>
              {cartDetail.oderInfo.oderName.title}
            </Title>
            <Title size={'normal'} style={styles.parentTitle}>
              {cartDetail.oderInfo.oderName.value}
            </Title>
          </HorizontalView>
          <HorizontalView>
            <TextContent size={'small'} style={styles.address}>
              {cartDetail.oderInfo.info.oderTime.title}
            </TextContent>
            <TextContent size={'small'} style={styles.address}>
              {cartDetail.oderInfo.info.oderTime.value.date}
            </TextContent>
            <TextContent size={'small'} style={styles.address}>
              {cartDetail.oderInfo.info.oderTime.value.time}
            </TextContent>
          </HorizontalView>
          <TextContent size={'small'} style={styles.address}>
            {cartDetail.oderInfo.info.contact}
          </TextContent>
          <HorizontalView>
            <TextContent size={'small'} style={styles.address}>
              {cartDetail.oderInfo.info.totalMoney.title}
            </TextContent>
            <TextContent size={'small'} style={styles.address}>
              {currencyConvert(cartDetail.oderInfo.info.totalMoney.value)}
            </TextContent>
          </HorizontalView>
          <HorizontalView>
            <TextContent size={'small'} style={styles.address}>
              {cartDetail.oderInfo.info.paymentStatus.title}
            </TextContent>
            <TextContent size={'small'} style={styles.address}>
              {cartDetail.oderInfo.info.paymentStatus.value}
            </TextContent>
          </HorizontalView>
          <HorizontalView>
            <TextContent size={'small'} style={styles.address}>
              {cartDetail.oderInfo.info.shipStatus.title}
            </TextContent>
            <TextContent size={'small'} style={styles.address}>
              {cartDetail.oderInfo.info.shipStatus.value}
            </TextContent>
          </HorizontalView>
          <View style={styles.rule} />
          {/* chi tiet don hang */}
          <View>
            <Title size={'normal'} style={styles.parentTitle}>
              {cartDetail.title}
            </Title>
            <ScrollView showsHorizontalScrollIndicator={true}>
              <ListContent {...cartDetail} type="detailOrderItem" />
            </ScrollView>
            <View style={styles.rule} />
            {/* Address payment */}
            <View style={{paddingLeft: 10}}>
              <Title size={'normal'} style={styles.titleName}>
                {'Địa chỉ nhận thanh toán'}
              </Title>
              <View style={{paddingLeft: 10}}>
                <HorizontalView>
                  <TextContent size={'small'} style={styles.parentTitle}>
                    {'Tình trạng thanh toán : '}
                  </TextContent>
                  <TextContent size={'small'} style={styles.parentTitle}>
                    {cartDetail.recivieAddress.paymentStatus}
                  </TextContent>
                </HorizontalView>
                {cartDetail &&
                cartDetail.recivieAddress &&
                cartDetail.recivieAddress.storeName ? (
                  <View>
                    <Title size={'small'} style={styles.address}>
                      {cartDetail.recivieAddress.storeName}
                    </Title>
                    <TextContent size={'small'} style={styles.address}>
                      {cartDetail.recivieAddress.address.street}
                    </TextContent>
                  </View>
                ) : (
                  <Title size={'small'} style={styles.address}>
                    {cartDetail.recivieAddress.address.street}
                  </Title>
                )}

                <TextContent size={'small'} style={styles.address}>
                  {cartDetail.recivieAddress.address.city}
                </TextContent>
                <TextContent size={'small'} style={styles.address}>
                  {cartDetail.recivieAddress.phoneNumber}
                </TextContent>
              </View>
            </View>

            <View style={styles.rule} />
            {/* Address payment */}
            <View style={{paddingLeft: 10}}>
              <Title size={'normal'} style={styles.titleName}>
                {'Địa chỉ gửi hàng'}
              </Title>
              <View style={{paddingLeft: 10}}>
                <HorizontalView>
                  <TextContent size={'small'} style={styles.parentTitle}>
                    {'Tình trạng thanh toán : '}
                  </TextContent>
                  <TextContent size={'small'} style={styles.parentTitle}>
                    {cartDetail.recivieAddress.paymentStatus}
                  </TextContent>
                </HorizontalView>
                {cartDetail &&
                cartDetail.shippingAddress &&
                cartDetail.shippingAddress.storeName ? (
                  <View>
                    <Title size={'small'} style={styles.address}>
                      {cartDetail.shippingAddress.storeName}
                    </Title>
                    <TextContent size={'small'} style={styles.address}>
                      {cartDetail.shippingAddress.address.street}
                    </TextContent>
                  </View>
                ) : (
                  <Title size={'small'} style={styles.address}>
                    {cartDetail.shippingAddress.address.street}
                  </Title>
                )}

                <TextContent size={'small'} style={styles.address}>
                  {cartDetail.shippingAddress.address.city}
                </TextContent>
                <TextContent size={'small'} style={styles.address}>
                  {cartDetail.shippingAddress.phoneNumber}
                </TextContent>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerInfo: {
    flex: 1,
    padding: 12,
    marginBottom: 10,
  },
  titleName: {
    marginTop: 12,
    color: TEXT_GRAY_LIGHT_COLOR,
  },
  address: {
    color: TEXT_GRAY_LIGHT_COLOR,
    marginTop: 12,
  },
  rule: {
    padding: 10,
    borderBottomColor: '#E0E0E0',
    borderBottomWidth: 1,
  },
  parentTitle: {
    color: BUTTON_LOGIN_COLOR,
    marginTop: 12,
  },
  HoriView: {
    justifyContent: 'space-between',
  },
});

export default CartDetailBody;
