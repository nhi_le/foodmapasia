import React from 'react';
import {View, StyleSheet} from 'react-native';
import Title from '@common/components/text/Title';
import SvgIcon from '@common/components/icon/SvgIcon';
import {TEXT_DARK_COLOR, TEXT_GRAY_LIGHT_COLOR} from '@resources/palette';
import {LIGHT_GRAY_COLOR} from '../../../../../resources/palette';

const CartDetailHeader = ({style, title, menuButton}) => {
  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <SvgIcon height={20} width={20} {...menuButton} />
      <Title style={styles.titleToolbar} size="medium-title" text={title}>
        {title}
      </Title>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 70,
    alignContent: 'center',
    flexDirection: 'row',
    paddingStart: 20,
    paddingEnd: 20,
    paddingTop: 20,
    borderBottomColor: LIGHT_GRAY_COLOR,
    borderBottomWidth: 1,
    alignItems: 'center',
  },
  titleToolbar: {
    flex: 1,
    color: TEXT_DARK_COLOR,
    textAlign: 'center',
  },
  iconToolbar: {
    width: 24,
    height: 24,
  },
  welcome: {
    flex: 1,
  },
});

export default CartDetailHeader;
