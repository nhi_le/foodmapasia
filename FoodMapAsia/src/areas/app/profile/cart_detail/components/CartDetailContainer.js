import React from 'react';
import {View, StyleSheet} from 'react-native';
import CartDetailHeader from './CartDetailHeader';
import CartDetailBody from './CartDetailBody';
import CartDetailFooter from './CartDetailFooter';

const CartDetailContainer = ({header, body, footer}) => {
  return (
    <View style={styles.container}>
      <CartDetailHeader style={styles.header} {...header} />
      <CartDetailBody style={styles.body} {...body} />
      {/* <CartDetailFooter style={styles.footer} {...footer} />  */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
  },
  body: {
    flex: 1,
  },
});

export default CartDetailContainer;
