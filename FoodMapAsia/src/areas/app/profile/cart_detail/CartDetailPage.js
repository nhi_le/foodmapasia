import React from 'react';
import {connect} from 'react-redux';
import BackgroundTemplate from '@common/components/background/BackgroundTemplate';

import operations from '@redux/profile/operations';
import authOperations from '@redux/auth/operations';
import CartDetailContainer from './components/CartDetailContainer';
import {ICON_BACK_SVG} from '@resources/images';
import {
  TITLE_CART_DETAIL,
  CONTENT_ODER,
  CONTENT_ODER_TIME,
  CONTENT_CONTACT,
  CONTENT_TOTAL_MONEY,
  CONTENT_PAYMENT_STATUTS,
  CONTENT_SHIP_STATUS,
  CONTENT_ADDRESS_RECIVIE,
  CONTENT_ADDRESS_SEND,
} from '@resources/string/strings';

const mapStateToProps = (state) => ({
  cartDetail: state.profile.cartDetail,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  header: {
    title: TITLE_CART_DETAIL,
    menuButton: {
      svgIcon: ICON_BACK_SVG,
      onPress: async () => {
        ownProps.navigation.goBack();
      },
    },
  },
  body: {
    oderInfo: {
      oderName: {
        title: CONTENT_ODER,
        value: '#103456',
      },
      info: {
        oderTime: {
          title: CONTENT_ODER_TIME,
          value: {
            date: '21/05/2020, ',
            time: '11:16CH',
          },
        },
        contact: CONTENT_CONTACT,
        totalMoney: {
          title: CONTENT_TOTAL_MONEY,
          value: '58,525,000đ',
        },
        paymentStatus: {
          title: CONTENT_PAYMENT_STATUTS,
          value: 'Đã thanh toán',
        },
        shipStatus: {
          title: CONTENT_SHIP_STATUS,
          value: 'Đã giao hàng',
        },
      },
    },
    cartDetail: {
      title: TITLE_CART_DETAIL,
      data: [],
      onItemPress: async (data) => {
        // await dispatch(homeOperations.updateProduct(data));
        // await ownProps.navigation.navigate('ProductDetailPage');
      },
    },
    recivieAddress: {
      title: CONTENT_ADDRESS_RECIVIE,
      paymentStatus: 'Tình trạng thanh toán: Đã thanh toán',
      storeName: 'Bách Hoá 711 - CN Q1',
      address: {
        street: '114 đường 9A, Bình Hưng, Bình Chánh ',
        city: 'Hồ Chí Minh, Việt Nam',
      },
      phoneNumber: '',
    },
    shippingAddress: {
      title: CONTENT_ADDRESS_SEND,
      paymentStatus: 'Tình trạng thanh toán: Đã thanh toán',
      storeName: 'Bách Hoá 711 - CN Q1',
      address: {
        street: '114 đường 9A, Bình Hưng, Bình Chánh ',
        city: 'Hồ Chí Minh, Việt Nam',
      },
      phoneNumber: '',
    },
  },
  footer: {},
  actions: {
    refreshLoginForm: () => dispatch(authOperations.refreshLoginForm()),
    getDetailOrder: () => dispatch(operations.getDetailOrder()),
  },
});

const LoginPage = ({
  header,
  body,
  footer,
  login,
  actions,
  cartDetail,
  ...props
}) => {
  return (
    <BackgroundTemplate
      scrollable={false}
      fullscreen={true}
      onInitialized={async () => {
        await actions.getDetailOrder();
        await actions.refreshLoginForm();
      }}>
      <CartDetailContainer
        header={{...header}}
        body={{
          ...body,
          cartDetail: {
            ...body.cartDetail,
            ...cartDetail,
          },
        }}
      />
    </BackgroundTemplate>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
