import React, {Component} from 'react';
import {connect} from 'react-redux';
import BackgroundTemplate from '@common/components/background/BackgroundTemplate';
import {ICON_CANCEL} from '@resources/images';
import FavoriteContainer from './components/FavoriteContainer';
import {renderText} from '@common/components/StringHelper';
import operations from '@redux/favorite/operations';
import homeOperations from '@redux/home/operations';
const mapStateToProps = (state) => ({
  stepFavorite: state.favorite.stepFavorite,
  listFavorite: state.favorite.listFavorite,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  header: {
    title: renderText('TITLE_FAVORITE_LIST'),
    backButton: {
      svgIcon: ICON_CANCEL,
      onPress: async () => {
        ownProps.navigation.goBack();
      },
    },
  },
  body: {
    onItemPress: async (data) => {
      await dispatch(homeOperations.updateProduct(data));
      await ownProps.navigation.navigate('ProductDetailPage');
    },
    onActionPress: (type, id) => {
      switch (type) {
        case 'increase':
          dispatch(operations.increaseAmount(type, id));
          break;
        case 'decrease':
          dispatch(operations.decreaseAmount(type, id));
          break;
        case 'cancel':
          dispatch(operations.removeFavoriteItem(type, id));
          break;
        default:
          dispatch(operations.updateAmount(type, id));
          break;
      }
    },
  },
  footer: {
    buttonPayment: {
      text: 'Mua Hàng',
      onPress: () => {
        ownProps.navigation.navigate('CartTransportPage');
      },
    },
    buttonCart: {
      text: 'Thêm vào giỏ hàng',
      onPress: async () => {
        await dispatch(operations.addToCart());
        ownProps.navigation.navigate('Cart');
      },
    },
  },
  actions: {
    getListFavorite: () => dispatch(operations.getListFavorite()),
  },
});
class FavoritePage extends Component {
  constructor() {
    super();
  }

  componentDidMount() {
    this.walletListener = this.props.navigation.addListener(
      'willFocus',
      async () => {
        await this.props.actions.getListFavorite();
      },
    );
  }

  render() {
    const {
      header,
      body,
      footer,
      login,
      actions,
      stepFavorite,
      listFavorite,
    } = this.props;

    return (
      <BackgroundTemplate
        scrollable={false}
        fullscreen={true}
        onInitialized={async () => {
          await actions.getListFavorite();
        }}>
        <FavoriteContainer
          header={{...header, stepFavorite}}
          body={{
            body,
            stepFavorite,
            listFavorite,
          }}
          footer={{...footer, stepFavorite, listFavorite}}
        />
      </BackgroundTemplate>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(FavoritePage);
