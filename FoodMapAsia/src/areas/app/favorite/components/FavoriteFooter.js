import React from 'react';
import {StyleSheet, View} from 'react-native';
import OutlineButton from '@common/components/button/OutlineButton';
import Button from '@common/components/button/Button';
import {BUTTON_LOGIN_COLOR} from '@resources/palette';
import {
  WHITE_COLOR,
  LIGHT_GRAY_COLOR,
  ACCENT_COLOR,
} from '../../../../resources/palette';
import HorizontalView from '../../../../common/components/layout/HorizontalView';

const FavoriteFooter = ({listFavorite, buttonPayment, buttonCart}) => {
  let listEmpty = listFavorite && listFavorite.data.length === 0;
  return (
    <View style={styles.container}>
      <HorizontalView style={styles.horiView}>
        <OutlineButton
          buttonStyle={
            listEmpty ? styles.buttonAddCartDisable : styles.buttonAddCart
          }
          textStyle={listEmpty ? styles.textAddCartDisable : styles.textAddCart}
          style={styles.styleButton}
          {...buttonCart}
          disabled={listEmpty}
        />
        <Button
          textStyle={styles.textStyle}
          buttonStyle={
            listEmpty ? styles.buttonPaymentDisable : styles.buttonPayment
          }
          style={styles.styleButton}
          {...buttonPayment}
          disabled={listEmpty}
        />
      </HorizontalView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderTopColor: LIGHT_GRAY_COLOR,
    borderTopWidth: 1,
    height: 67,
    borderColor: LIGHT_GRAY_COLOR,
    justifyContent: 'center',
    paddingHorizontal: 8,
  },
  styleButton: {
    flex: 1,
    paddingHorizontal: 8,
  },
  disableStyleButton: {
    backgroundColor: LIGHT_GRAY_COLOR,
    borderRadius: 22,
  },
  styleOutlineButton: {
    backgroundColor: WHITE_COLOR,
    borderRadius: 22,
  },
  textStyle: {
    color: WHITE_COLOR,
  },
  horiView: {
    justifyContent: 'space-around',
  },
  textAddCart: {
    color: ACCENT_COLOR,
  },
  textAddCartDisable: {
    color: LIGHT_GRAY_COLOR,
  },
  buttonAddCart: {
    borderColor: BUTTON_LOGIN_COLOR,
    borderRadius: 22,
  },
  buttonAddCartDisable: {
    borderColor: LIGHT_GRAY_COLOR,
    borderRadius: 22,
  },
  buttonPayment: {
    borderRadius: 22,
  },
  buttonPaymentDisable: {
    borderRadius: 22,
    backgroundColor: LIGHT_GRAY_COLOR,
  },
});

export default FavoriteFooter;
