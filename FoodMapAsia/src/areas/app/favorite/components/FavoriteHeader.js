import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import Title from '@common/components/text/Title';

import SvgIcon from '@common/components/icon/SvgIcon';
import {TEXT_DARK_COLOR} from '@resources/palette';
import {ICON_PRE} from '@resources/images';
import {LIGHT_GRAY_COLOR} from '@resources/palette';

const FavoriteHeader = ({style, title}) => {
  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <Title style={styles.titleToolbar} size="medium-title">
        {title}
      </Title>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 70,
    alignContent: 'center',
    flexDirection: 'row',
    borderBottomColor: LIGHT_GRAY_COLOR,
    borderBottomWidth: 1,
    alignItems: 'center',
  },
  titleToolbar: {
    flex: 1,
    color: TEXT_DARK_COLOR,
    textAlign: 'center',
    fontWeight: 'normal',
  },


});

export default FavoriteHeader;
