import React from 'react';
import {View, StyleSheet, KeyboardAvoidingView, Platform} from 'react-native';
import FavoriteHeader from './FavoriteHeader';
import FavoriteBody from './FavoriteBody';
import FavoriteFooter from './FavoriteFooter';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const FavoriteContainer = ({header, body, footer}) => {
  return (
    <View style={styles.container}>
      <FavoriteHeader style={styles.header} {...header} />
      <KeyboardAvoidingView
        style={{flex: 1}}
        {...(Platform.OS === 'ios' && {
          behavior: 'height',
          ...ifIphoneX(
            {
              keyboardVerticalOffset: 130,
            },
            {
              keyboardVerticalOffset: 50,
            },
          ),
        })}>
        <View style={styles.contentContainer}>
          <FavoriteBody style={styles.body} {...body} />
          <FavoriteFooter style={styles.footer} {...footer} />
        </View>
      </KeyboardAvoidingView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {},
  body: {
    flex: 1,
  },
  contentContainer: {
    height: '100%',
  },
  footer: {},
});

export default FavoriteContainer;
