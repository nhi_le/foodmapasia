import React from 'react';
import {View, StyleSheet, Text, ScrollView} from 'react-native';

import ListContent from '@common/components/list/ListContent';
import {
  LIGHT_GRAY_COLOR,
  BACKGROUND_COLOR_TRANS,
  COLOR_READ,
} from '@resources/palette';
const FavoriteBody = ({body, style, listFavorite, stepFavorite}) => {
  if (listFavorite && listFavorite.data.length >= 1) {
    return (
      <View
        style={{
          ...styles.container,
          ...style,
        }}>
        <ScrollView
          style={styles.scrollCategory}
          showsHorizontalScrollIndicator={true}>
          <ListContent
            {...listFavorite}
            {...body}
            type="favorite"
            style={styles.notificationList}
          />
        </ScrollView>
      </View>
    );
  } else {
    return (
      <View
        style={{
          ...styles.container,
          ...style,
        }}>
        <Text style={styles.empty}>{'Bạn chưa có sản phẩm yêu thích'}</Text>
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: BACKGROUND_COLOR_TRANS,
  },
  notificationList: {
    flex: 1,
  },
  scrollCategory: {
    flexGrow: 0,
    borderBottomColor: LIGHT_GRAY_COLOR,
  },
  empty: {
    color: COLOR_READ,
    textAlign: 'center',
    marginTop: 100,
  },
});

export default FavoriteBody;
