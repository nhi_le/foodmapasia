import React, {Component} from 'react';
import {connect} from 'react-redux';
import BackgroundTemplate from '@common/components/background/BackgroundTemplate';
import CartListContainer from './components/CartListContainer';
import operations from '@redux/cart/operations';
import homeOperations from '@redux/home/operations';
const mapStateToProps = (state) => ({
  listShoppingCart: state.cart.listShoppingCart,
  step: state.cart.stepFavorite,
  total: state.cart.total,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  header: {
    title: 'Giỏ Hàng Của Bạn',
  },
  body: {
    listShoppingCart: {
      onItemPress: async (data) => {
        await dispatch(homeOperations.updateProduct(data));
        await ownProps.navigation.navigate('ProductDetailPage');
      },
      onActionPress: async (type, id) => {
        switch (type) {
          case 'increase':
            dispatch(operations.increaseAmount(type, id));
            dispatch(operations.updatePrice());
            break;
          case 'decrease':
            dispatch(operations.decreaseAmount(type, id));
            dispatch(operations.updatePrice());
            break;
          case 'remove':
            await dispatch(operations.removeCartItem(type, id));
            dispatch(operations.updatePrice());
            break;
          default:
            dispatch(operations.updateAmount(type, id));
            dispatch(operations.updatePrice());
            break;
        }
      },
    },
  },
  footer: {
    buttonPayment: {
      text: 'Thanh Toán',
      onPress: () => {
        ownProps.navigation.navigate('CartTransportPage');
      },
    },
  },
  actions: {
    getListCart: () => dispatch(operations.getListCart()),
    updatePrice: () => dispatch(operations.updatePrice()),
  },
});

class CartListPage extends Component {
  constructor() {
    super();
  }

  componentDidMount() {
    this.walletListener = this.props.navigation.addListener(
      'willFocus',
      async () => {
        await this.props.actions.getListCart();
        this.props.actions.updatePrice();
      },
    );
  }

  render() {
    const {header, body, footer, total, listShoppingCart} = this.props;

    return (
      <BackgroundTemplate scrollable={false} fullscreen={true}>
        <CartListContainer
          header={{...header}}
          body={{
            ...body,
            listShoppingCart: {
              ...body.listShoppingCart,
              data: listShoppingCart.data,
            },
          }}
          footer={{
            ...footer,
            listShoppingCart,
            total,
          }}
        />
      </BackgroundTemplate>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CartListPage);
