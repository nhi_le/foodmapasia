import React from 'react';
import {View, StyleSheet} from 'react-native';
import Title from '@common/components/text/Title';
import {TEXT_DARK_COLOR} from '@resources/palette';

const CartListHeader = ({style, title, menuButton}) => {
  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <Title style={styles.titleToolbar} size="medium-title" text={title}>
        {title}
      </Title>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 60,
    alignContent: 'center',
    flexDirection: 'row',
    paddingStart: 20,
    paddingEnd: 20,
    paddingTop: 20,
  },
  titleToolbar: {
    flex: 1,
    color: TEXT_DARK_COLOR,
    textAlign: 'center',
    fontWeight: 'normal',
  },
  iconToolbar: {
    width: 24,
    height: 24,
  },
  welcome: {
    flex: 1,
  },
});

export default CartListHeader;
