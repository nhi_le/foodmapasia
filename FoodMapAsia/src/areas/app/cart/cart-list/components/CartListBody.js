import React from 'react';
import {View, StyleSheet, Text, ScrollView} from 'react-native';

import ListContent from '@common/components/list/ListContent';
import {LIGHT_GRAY_COLOR, BACKGROUND_PROFILE} from '@resources/palette';
import {COLOR_READ} from '../../../../../resources/palette';

const CartListBody = ({style, body, listShoppingCart}) => {
  if (listShoppingCart.data && listShoppingCart.data.length >= 1) {
    return (
      <View
        style={{
          ...styles.container,
          ...style,
        }}>
        <ScrollView
          style={styles.scrollCategory}
          showsHorizontalScrollIndicator={true}>
          <ListContent
            {...listShoppingCart}
            type="cart"
            style={styles.notificationList}
          />
        </ScrollView>
      </View>
    );
  } else {
    return (
      <View
        style={{
          ...styles.container,
          ...style,
        }}>
        <Text style={styles.empty}>
          {'Bạn chưa có sản phẩm trong giỏ hàng'}
        </Text>
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: BACKGROUND_PROFILE,
  },
  notificationList: {
    flex: 1,
  },
  scrollCategory: {
    flex: 1,
    borderBottomColor: LIGHT_GRAY_COLOR,
  },
  empty: {
    color: COLOR_READ,
    textAlign: 'center',
    marginTop: 100,
  },
});

export default CartListBody;
