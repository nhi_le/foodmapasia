import React from 'react';
import {View, StyleSheet, KeyboardAvoidingView, Platform} from 'react-native';
import CartListHeader from './CartListHeader';
import CartListBody from './CartListBody';
import CartListFooter from './CartListFooter';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const CartContainer = ({header, body, footer}) => {
  return (
    <View style={styles.container}>
      <CartListHeader style={styles.header} {...header} />
      <KeyboardAvoidingView
        style={{flex: 1}}
        {...(Platform.OS === 'ios' && {
          behavior: 'height',
          ...ifIphoneX(
            {
              keyboardVerticalOffset: 130,
            },
            {
              keyboardVerticalOffset: 50,
            },
          ),
        })}>
        <View style={styles.contentContainer}>
          <CartListBody style={styles.body} {...body} />
          <CartListFooter style={styles.footer} {...footer} />
        </View>
      </KeyboardAvoidingView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {},
  body: {
    flex: 1,
  },
  contentContainer: {
    height: '100%',
  },
  footer: {},
});

export default CartContainer;
