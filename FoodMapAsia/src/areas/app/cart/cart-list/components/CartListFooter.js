/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {StyleSheet, View} from 'react-native';
import OutlineButton from '@common/components/button/OutlineButton';
import {TEXT_GRAY_LIGHT_COLOR} from '@resources/palette';
import {BUTTON_LOGIN_COLOR} from '@resources/palette';
import Button from '@common/components/button/Button';
import HorizontalView from '@common/components/layout/HorizontalView';
import TextContent from '@common/components/text/TextContent';
import Title from '@common/components/text/Title';
import {
  LIGHT_GRAY_COLOR,
  WHITE_COLOR,
  BACKGROUND_COLOR,
  ACCENT_COLOR,
} from '@resources/palette';
import {currencyConvert} from '@common/components/StringHelper';
const CartListFooter = ({buttonPayment, listShoppingCart, total}) => {
  if (listShoppingCart && listShoppingCart.data.length > 0) {
    return (
      <View style={styles.container}>
        <View style={styles.containerButtonAddAddress}>
          <HorizontalView>
            <View>
              <TextContent size={'small'} style={styles.total}>
                {'Thành tiền'}
              </TextContent>
            </View>
            <View style={{marginLeft: 10}}>
              <HorizontalView>
                <Title size={'normal'} style={styles.parentTitle}>
                  {currencyConvert(total)}
                </Title>
              </HorizontalView>
              <TextContent style={styles.address}>
                {'Đã bao gồm VAT'}
              </TextContent>
            </View>
            <View style={{flex: 1}}>
              <Button
                buttonStyle={styles.buttonAdd}
                textStyle={styles.texButton}
                {...buttonPayment}
              />
            </View>
          </HorizontalView>
        </View>
      </View>
    );
  } else {
    return (
      <View style={styles.container}>
        <View style={styles.containerButtonAddAddress}>
          <HorizontalView>
            <View>
              <TextContent size={'small'} style={styles.total}>
                {'Thành tiền'}
              </TextContent>
            </View>
            <View style={{marginLeft: 10}}>
              <Title size={'normal'} style={styles.parentTitle}>
                {'0đ'}
              </Title>
              <TextContent style={styles.address}>
                {'Đã bao gồm VAT'}
              </TextContent>
            </View>
            <View style={{flex: 1}}>
              <OutlineButton
                textStyle={styles.textStyle}
                buttonStyle={{borderColor: LIGHT_GRAY_COLOR, borderRadius: 22}}
                style={styles.disableStyleButton}
                text={'Thanh toán'}
                disabled={true}
              />
            </View>
          </HorizontalView>
        </View>
      </View>
    );
  }
};

const styles = StyleSheet.create({
  textStyle: {
    color: WHITE_COLOR,
  },
  container: {
    backgroundColor: BACKGROUND_COLOR,
    borderTopWidth: 1,
    borderColor: LIGHT_GRAY_COLOR,
  },
  disableStyleButton: {
    width: 170,
    backgroundColor: LIGHT_GRAY_COLOR,
    borderRadius: 22,
    marginLeft: 10,
  },
  containerButtonAddAddress: {
    height: 70,
    width: '100%',
    alignContent: 'center',
    justifyContent: 'center',
    backgroundColor: WHITE_COLOR,
  },
  buttonAdd: {
    backgroundColor: ACCENT_COLOR,
    borderRadius: 40,
    width: 160,
    marginLeft: 20,
    marginRight: 20,
  },
  texButton: {
    color: WHITE_COLOR,
    fontSize: 16,
  },
  buttonPaymentDisable: {
    backgroundColor: LIGHT_GRAY_COLOR,
    borderRadius: 40,
    width: 160,
    marginLeft: 20,
    marginRight: 20,
  },
  total: {
    color: TEXT_GRAY_LIGHT_COLOR,
    marginLeft: 15,
    marginTop: 4,
    fontSize: 12,
  },
  address: {
    color: TEXT_GRAY_LIGHT_COLOR,
    margin: 5,
    fontSize: 12,
  },
  parentTitle: {
    color: BUTTON_LOGIN_COLOR,
    fontSize: 16,
    marginLeft: 5,
  },
});

export default CartListFooter;
