import React from 'react';
import {View, StyleSheet} from 'react-native';
import WebView from 'react-native-webview';

const CartDeliveryBody = ({style, webCart}) => {
  let js = `
      if(document.cookie.includes('access_token')){
        var cookies = document.cookie.split(";");
        for (var i = 0; i < cookies.length; i++) {
          var cookie = cookies[i];
          var eqPos = cookie.indexOf("=");
          var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
          document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
        }
      }else{
        document.cookie='access_token=''';
        if( window.localStorage )
        {
          if( !localStorage.getItem('firstLoad') )
          {
            localStorage['firstLoad'] = true;
            window.location.reload();
          }  
          else
            localStorage.removeItem('firstLoad');
        }
      }
      (function() {
        function wrap(fn) {
          return function wrapper() {
            var res = fn.apply(this, arguments);
            window.ReactNativeWebView.postMessage(window.location.href);
            return res;
          }
        }
        history.pushState = wrap(history.pushState);
        history.replaceState = wrap(history.replaceState);
        window.addEventListener('popstate', function() {
          window.ReactNativeWebView.postMessage(window.location.href);
        });
      })();
      true;
  `;
  return (
    <View style={styles.container}>
      <WebView
        style={styles.webView}
        source={{
          uri: webCart.uri,
        }}
        {...webCart}
        injectedJavaScript={js}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  webView: {},
});

export default CartDeliveryBody;
