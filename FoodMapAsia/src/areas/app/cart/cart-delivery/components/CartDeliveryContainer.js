import React from 'react';
import {View, StyleSheet} from 'react-native';
import CartDeliveryHeader from './CartDeliveryHeader';
import CartDeliveryBody from './CartDeliveryBody';
import CartDeliveryFooter from './CartDeliveryFooter';

const CartDeliveryContainer = ({header, body, footer}) => {
  return (
    <View style={styles.container}>
      <CartDeliveryHeader style={styles.header} {...header} />
      <CartDeliveryBody style={styles.body} {...body} />
      {/* <CartDeliveryFooter style={styles.footer} {...footer} /> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {},
  body: {
    flex: 1,
  },
  footer: {},
});

export default CartDeliveryContainer;
