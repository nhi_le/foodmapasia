import React from 'react';
import {StyleSheet, View} from 'react-native';
import {TEXT_GRAY_LIGHT_COLOR} from '@resources/palette';
import {BUTTON_LOGIN_COLOR} from '@resources/palette';
import Button from '@common/components/button/Button';

const CartDeliveryFooter = ({buttonPayment}) => {
  return (
    <View style={styles.container}>
      <View style={styles.containerButtonAddAddress}>
        <Button
          buttonStyle={styles.buttonAdd}
          textStyle={styles.texButton}
          {...buttonPayment}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fce2de',
  },
  containerButtonAddAddress: {
    height: 70,
    width: '100%',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  buttonAdd: {
    backgroundColor: '#D8412C',
    borderRadius: 40,
    width: 350,
    paddingLeft: 20,
  },
  texButton: {
    color: 'white',
    fontWeight: 'bold',
  },
  address: {
    color: TEXT_GRAY_LIGHT_COLOR,
    marginTop: 10,
  },
  parentTitle: {
    color: BUTTON_LOGIN_COLOR,
  },
  HoriView: {
    justifyContent: 'flex-start',
  },
});

export default CartDeliveryFooter;
