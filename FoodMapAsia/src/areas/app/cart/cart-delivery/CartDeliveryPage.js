import React from 'react';
import {connect} from 'react-redux';
import BackgroundTemplate from '@common/components/background/BackgroundTemplate';
import CartDeliveryContainer from './components/CartDeliveryContainer';
import {ICON_BACK_SVG} from '../../../../resources/images';

const mapStateToProps = (state) => ({
  tabSelected: state.profile.tabSelected,
  listCategory: state.profile.listCategory,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  header: {
    title: 'Giỏ Hàng Của Bạn',
    menuButton: {
      svgIcon: ICON_BACK_SVG,
      onPress: async () => {
        ownProps.navigation.navigate('LoginPage');
      },
    },
  },
  body: {
    webCart: {
      uri: 'https://choraucu.com/cart/',
      sharedCookiesEnabled: true,
      scrollEnabled: true,
      javaScriptEnabled: true,
      domStorageEnabled: true,
      onNavigationStateChange: async (navigationState) => {
        console.log(navigationState);
      },
      onMessage: async (event) => {
        console.log('event', event.nativeEvent.data);
      },
    },
  },
  footer: {
    buttonPayment: {
      text: 'Tiếp Tục Đến Phương Thức Thanh Toán',
      onPress: () => {},
    },
  },
  actions: {},
});

const LoginPage = ({
  header,
  body,
  footer,
  login,
  actions,
  tabSelected,
  listCategory,
  ...props
}) => {
  return (
    <BackgroundTemplate scrollable={false} fullscreen={true}>
      <CartDeliveryContainer
        header={{...header}}
        body={{
          ...body,
        }}
        footer={{
          ...footer,
        }}
      />
    </BackgroundTemplate>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
