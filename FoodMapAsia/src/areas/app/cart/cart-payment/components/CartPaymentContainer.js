import React from 'react';
import {View, StyleSheet, KeyboardAvoidingView, Platform} from 'react-native';
import CartPaymentHeader from './CartPaymentHeader';
import CartPaymentBody from './CartPaymentBody';
import CartPaymentFooter from './CartPaymentFooter';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const CartPaymentContainer = ({header, body, footer}) => {
  return (
    <View style={styles.container}>
      <CartPaymentHeader style={styles.header} {...header} />
      <KeyboardAvoidingView
        style={{flex: 1}}
        {...(Platform.OS === 'ios' && {
          behavior: 'height',
          ...ifIphoneX(
            {
              keyboardVerticalOffset: 130,
            },
            {
              keyboardVerticalOffset: 50,
            },
          ),
        })}>
        <View style={styles.contentContainer}>
          <CartPaymentBody style={styles.body} {...body} />
          <CartPaymentFooter style={styles.footer} {...footer} />
        </View>
      </KeyboardAvoidingView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {},
  body: {
    flex: 1,
  },
  contentContainer: {
    height: '100%',
  },
  footer: {},
});

export default CartPaymentContainer;
