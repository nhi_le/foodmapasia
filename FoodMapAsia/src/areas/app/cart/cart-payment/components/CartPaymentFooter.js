import React from 'react';
import {StyleSheet, View} from 'react-native';
import Button from '@common/components/button/Button';
import {LIGHT_GRAY_COLOR, WHITE_COLOR} from '../../../../../resources/palette';

const CartPaymentFooter = ({buttonPayment}) => {
  return (
    <View style={styles.container}>
      <Button
        {...buttonPayment}
        text={buttonPayment.text}
        buttonStyle={styles.buttonStyle}
        textStyle={styles.textStyle}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 70,
    borderTopColor: LIGHT_GRAY_COLOR,
    borderTopWidth: 1,
    backgroundColor: WHITE_COLOR,
    justifyContent: 'center',
  },
  buttonStyle: {
    borderRadius: 22,
    width: 350,
  },
  textStyle: {
    fontSize: 16,
    color: WHITE_COLOR,
    fontWeight: 'bold',
  },
});

export default CartPaymentFooter;
