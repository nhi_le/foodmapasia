import React from 'react';
import {View, StyleSheet, TouchableOpacity, ScrollView} from 'react-native';
import Title from '@common/components/text/Title';
import SvgIcon from '@common/components/icon/SvgIcon';
import Input from '@common/components/input/Input';
import Button from '@common/components/button/Button';
import {ICON_CART_PAYMENT, ICON_SELECTBOX} from '@resources/images';
import {
  LIGHT_GRAY_COLOR,
  ACCENT_COLOR,
  WHITE_COLOR,
  GRAY_COLOR,
} from '@resources/palette';
import TextContent from '@common/components/text/TextContent';
import ListContent from '@common/components/list/ListContent';
import HorizontalView from '@common/components/layout/HorizontalView';
import {
  BUTTON_DISCOUNT,
  TITLE_SHOW_CART,
  HOLDER_DISCOUNT,
} from '@resources/string/strings';
import {currencyConvert} from '@common/components/StringHelper';
const CartPaymentBody = ({
  price,
  inputDiscount,
  listTransport,
  listPayment,
  codeDiscount,
  total,
}) => {
  return (
    <View style={styles.container}>
      <View style={styles.fisrtInfoOrder}>
        <TouchableOpacity style={styles.showInfoCart}>
          <SvgIcon
            width={16}
            height={16}
            svgIcon={ICON_CART_PAYMENT}
            style={styles.iconCart}
          />
          <TextContent style={styles.textTittle}>{TITLE_SHOW_CART}</TextContent>
          <SvgIcon
            width={14}
            height={14}
            svgIcon={ICON_SELECTBOX}
            tintColor={ACCENT_COLOR}
          />
        </TouchableOpacity>
        <TextContent size="large" style={styles.totalPrice}>
          {currencyConvert(total)}
        </TextContent>
      </View>
      <HorizontalView style={styles.flexInputDiscount}>
        <View style={{flex: 2, marginRight: 10}}>
          <Input
            {...inputDiscount}
            inputStyle={styles.inputDiscount}
            size="medium"
            textValue={codeDiscount}
            placeholder={HOLDER_DISCOUNT}
          />
        </View>
        <View style={{flex: 1}}>
          <Button
            text={BUTTON_DISCOUNT}
            buttonStyle={
              codeDiscount ? styles.buttonActive : styles.buttonNoneActive
            }
            disabled={codeDiscount ? false : true}
            textStyle={styles.textButton}
          />
        </View>
      </HorizontalView>

      <ScrollView style={styles.thirdContent}>
        <View>
          <Title size="large" style={styles.textTitle}>
            {listTransport.title}
          </Title>
          <ListContent
            {...listTransport}
            type="cartPayment"
            style={styles.listView}
          />
        </View>
        <View>
          <Title size="large" style={styles.textTitle}>
            {listPayment.title}
          </Title>
          <ListContent
            {...listPayment}
            type="cartPayment"
            style={styles.listView}
          />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingStart: 10,
    paddingEnd: 10,
    marginTop: 10,
  },
  //first
  fisrtInfoOrder: {
    height: 40,
    paddingBottom: 10,
    paddingTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  iconCart: {
    marginEnd: 10,
  },
  showInfoCart: {
    flexDirection: 'row',
  },
  textTittle: {
    textAlign: 'left',
    lineHeight: 16,
    color: ACCENT_COLOR,
    marginEnd: 5,
  },
  totalPrice: {
    letterSpacing: 1,
    color: ACCENT_COLOR,
    fontWeight: 'bold',
    lineHeight: 19,
  },
  //second
  flexInputDiscount: {
    paddingTop: 10,
    paddingBottom: 15,
    flexDirection: 'row',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderTopColor: LIGHT_GRAY_COLOR,
    borderBottomColor: LIGHT_GRAY_COLOR,
  },
  inputDiscount: {
    borderRadius: 30,
    borderColor: GRAY_COLOR,
    backgroundColor: WHITE_COLOR,
    height: 40,
  },
  buttonNoneActive: {
    borderRadius: 30,
    backgroundColor: LIGHT_GRAY_COLOR,
  },
  buttonActive: {
    borderRadius: 30,
    backgroundColor: ACCENT_COLOR,
  },
  textButton: {
    color: WHITE_COLOR,
    fontWeight: 'bold',
  },
  thirdContent: {
    marginTop: 10,
    flex: 1,
  },
  textTitle: {
    color: ACCENT_COLOR,
  },
  listView: {
    marginTop: 20,
    marginBottom: 20,
  },
});

export default CartPaymentBody;
