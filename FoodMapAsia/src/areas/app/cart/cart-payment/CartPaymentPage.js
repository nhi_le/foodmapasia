import React from 'react';
import {connect} from 'react-redux';
import BackgroundTemplate from '@common/components/background/BackgroundTemplate';
import CartPaymentContainer from './components/CartPaymentContainer';
import {ICON_PRE} from '@resources/images';
import operations from '@redux/cart/operations';
import {
  TITLE_METHOD_PAYMENT,
  TITLE_METHOD_TRANSPORT,
} from '@resources/string/strings';

const mapStateToProps = (state) => ({
  codeDiscount: state.cart.codeDiscount,
  price: state.cart.price,
  listTransport: state.cart.listTransport,
  listPayment: state.cart.listPayment,
  total: state.cart.total,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  header: {
    title: TITLE_METHOD_PAYMENT,
    backButton: {
      svgIcon: ICON_PRE,
      onPress: async () => {
        ownProps.navigation.goBack();
        dispatch(operations.setRootPrice());
      },
    },
  },
  body: {
    inputDiscount: {
      onChangeText: (text) => {
        dispatch(operations.setDiscount(text));
      },
    },
    listTransport: {
      title: TITLE_METHOD_TRANSPORT,
      data: [],
      onItemPress: (itemTransport) => {
        dispatch(operations.updateMethodTransport(itemTransport));
      },
    },
    listPayment: {
      title: TITLE_METHOD_PAYMENT,
      data: [],
      onItemPress: (itemPayment) => {
        dispatch(operations.updateMethodPayment(itemPayment));
      },
    },
  },
  footer: {
    buttonPayment: {
      text: 'Hoàn Tất Đơn Hàng',
      onPress: () => {
        ownProps.navigation.navigate('CartNotiOrderPage');
      },
    },
  },
  actions: {},
});

const CartPaymentPage = ({
  header,
  body,
  footer,
  codeDiscount,
  actions,
  listTransport,
  listPayment,
  price,
  total,
  ...props
}) => {
  return (
    <BackgroundTemplate scrollable={false} fullscreen={true}>
      <CartPaymentContainer
        header={{
          ...header,
        }}
        body={{
          ...body,
          listTransport: {
            ...body.listTransport,
            data: listTransport.data,
          },
          listPayment: {
            ...body.listPayment,
            data: listPayment.data,
          },
          codeDiscount,
          price,
          total,
        }}
        footer={{
          ...footer,
        }}
      />
    </BackgroundTemplate>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(CartPaymentPage);
