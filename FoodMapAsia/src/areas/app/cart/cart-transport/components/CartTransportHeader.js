import React from 'react';
import {View, StyleSheet} from 'react-native';
import Title from '@common/components/text/Title';
import {TEXT_DARK_COLOR} from '@resources/palette';
import SvgIcon from '@common/components/icon/SvgIcon';
import {LIGHT_GRAY_COLOR} from '@resources/palette';

const CartTransportHeader = ({style, backButton, title}) => {
  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <SvgIcon width={16} height={16} {...backButton} />
      <Title style={styles.titleToolbar} size="medium-title" text={title}>
        {title}
      </Title>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 60,
    alignContent: 'center',
    flexDirection: 'row',
    paddingStart: 20,
    paddingEnd: 20,
    paddingTop: 20,
    borderBottomColor: LIGHT_GRAY_COLOR,
    borderBottomWidth: 1,
    alignItems: 'center',
  },
  titleToolbar: {
    flex: 1,
    color: TEXT_DARK_COLOR,
    textAlign: 'center',
    fontWeight: 'normal',
  },
});

export default CartTransportHeader;
