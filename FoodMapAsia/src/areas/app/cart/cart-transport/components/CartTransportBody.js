import React from 'react';
import {View, StyleSheet} from 'react-native';
import WebView from 'react-native-webview';

const CartTransportBody = ({paymentWebView}) => {
  return (
    <View style={styles.container}>
      <WebView style={styles.webView} {...paymentWebView} />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  webView: {
    flex: 1,
  },
});

export default CartTransportBody;
