import React from 'react';
import { StyleSheet, View } from 'react-native';
import Button from '@common/components/button/Button';
import { WHITE_COLOR, LIGHT_GRAY_COLOR } from '../../../../../resources/palette';

const CartTransportFooter = ({ buttonPayment }) => {
  return (
    <View style={styles.container}>
      <Button
        {...buttonPayment}
        buttonStyle={styles.buttonStyle}
        textStyle={styles.textStyle}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 70,
    borderTopColor: LIGHT_GRAY_COLOR,
    borderTopWidth: 1,
    backgroundColor: WHITE_COLOR,
    justifyContent: 'center',
  },
  buttonStyle: {
    borderRadius: 22,
    width: 350,
  },
  textStyle: {
    fontSize: 16,
    color: '#ffffff',
    fontWeight: 'bold',
  },
});

export default CartTransportFooter;
