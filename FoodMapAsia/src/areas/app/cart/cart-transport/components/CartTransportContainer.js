import React from 'react';
import {View, StyleSheet, KeyboardAvoidingView, Platform} from 'react-native';
import CartTransportHeader from './CartTransportHeader';
import CartTransportBody from './CartTransportBody';
import CartTransportFooter from './CartTransportFooter';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const CartTransportContainer = ({header, body, footer}) => {
  return (
    <View style={styles.container}>
      <CartTransportHeader style={styles.header} {...header} />
      <KeyboardAvoidingView
        style={{flex: 1}}
        {...(Platform.OS === 'ios' && {
          behavior: 'height',
          ...ifIphoneX(
            {
              keyboardVerticalOffset: 130,
            },
            {
              keyboardVerticalOffset: 50,
            },
          ),
        })}>
        <View style={styles.contentContainer}>
          <CartTransportBody style={styles.body} {...body} />
          {/* <CartTransportFooter style={styles.footer} {...footer} /> */}
        </View>
      </KeyboardAvoidingView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {},
  body: {
    flex: 1,
  },
  contentContainer: {
    height: '100%',
  },
  footer: {},
});

export default CartTransportContainer;
