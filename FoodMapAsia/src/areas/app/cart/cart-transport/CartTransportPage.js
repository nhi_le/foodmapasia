import React, {Component} from 'react';
import {connect} from 'react-redux';
import BackgroundTemplate from '@common/components/background/BackgroundTemplate';
import CartTransportContainer from './components/CartTransportContainer';
import {ICON_PRE} from '@resources/images';
import operations from '@redux/cart/operations';
import {
  TITLE_INFO_DELIVERY,
  TEXT_BUTTON_CONTINUE_PAYMENT,
} from '@resources/string/strings';
import {Platform} from 'react-native';

let wedViewRef = null;

const mapStateToProps = (state) => ({
  total: state.cart.total,
  price: state.cart.total,
  codeDiscount: state.cart.codeDiscount,
  isAuthorized: state.auth.isAuthorized,
  formPayment: state.cart.formPayment,
  citySelection: state.cart.citySelection,
  districtSelection: state.cart.districtSelection,
  wardSelection: state.cart.wardSelection,
});
const mapDispatchToProps = (dispatch, ownProps) => ({
  header: {
    title: TITLE_INFO_DELIVERY,
    backButton: {
      svgIcon: ICON_PRE,
      onPress: async () => {
        dispatch(operations.resetPayment());
        ownProps.navigation.goBack();
      },
    },
  },
  body: {
    paymentWebView: {
      sharedCookiesEnabled: true,
      scrollEnabled: true,
      javaScriptEnabled: true,
      domStorageEnabled: true,
      ref: (ref) => {
        wedViewRef = ref;
      },
    },
  },
  footer: {
    buttonPayment: {
      text: TEXT_BUTTON_CONTINUE_PAYMENT,
      onPress: () => {
        dispatch(operations.setPricePayment());
        const isValid = dispatch(operations.checkValidation());
        if (isValid) {
          ownProps.navigation.navigate('CartPaymentPage');
        } else {
          ownProps.navigation.navigate('CartTransportPage');
        }
      },
    },
  },
  actions: {
    setRootPrice: () => dispatch(operations.setRootPrice()),
  },
});

class CartTransportPage extends Component {
  constructor() {
    super();
  }

  componentDidMount() {
    this.walletListener = this.props.navigation.addListener('willFocus', () => {
      if (wedViewRef) {
        wedViewRef.reload();
      }
    });
  }

  render() {
    const {
      header,
      body,
      footer,
      actions,
      codeDiscount,
      formPayment,
      price,
      citySelection,
      districtSelection,
      wardSelection,
      isAuthorized,
    } = this.props;
    return (
      <BackgroundTemplate
        scrollable={false}
        fullscreen={true}
        onInitialized={() => {
          actions.setRootPrice();
        }}>
        <CartTransportContainer
          header={{
            ...header,
          }}
          body={{
            ...body,
            citySelection: {
              ...body.citySelection,
              options: citySelection.options,
            },
            districtSelection: {
              ...body.districtSelection,
              options: districtSelection.options,
            },
            wardSelection: {
              ...body.wardSelection,
              options: wardSelection.options,
            },
            codeDiscount,
            formPayment,
            price,
            paymentWebView: {
              ...body.paymentWebView,
              source: {
                uri: isAuthorized
                  ? 'https://choraucu.myharavan.com/checkout?ref=' +
                    (Platform.OS === 'ios' ? 'app_ios/' : 'app_android/')
                  : 'https://choraucu.myharavan.com/account/logout',
              },
            },
          }}
          footer={{
            ...footer,
          }}
        />
      </BackgroundTemplate>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CartTransportPage);
