import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import SvgIcon from '@common/components/icon/SvgIcon';
import Title from '@common/components/text/Title';
import TextContent from '@common/components/text/TextContent';

import {
  ICON_CART_PAYMENT,
  ICON_SELECTBOX,
  ICON_SUCCESS,
} from '@resources/images';
import {
  TITLE_ORDER_SUCCESS,
  TITLE_SHOW_CART,
  TITLE_ORDER_CODE,
  TITLE_METHOD_PAYMENT,
  TITLE_INFO_TRANSPORT,
  TITLE_INFO_ORDER,
  CONTENT_ORDER_THANKYOU,
} from '@resources/string/strings';
import HorizontalView from '@common/components/layout/HorizontalView';
import {
  ACCENT_COLOR,
  LIGHT_GRAY_COLOR,
  TEXT_GRAY_LIGHT_COLOR,
} from '@resources/palette';
import {currencyConvert} from '@common/components/StringHelper';
const CartNotiOrderBody = ({price, formPayment}) => {
  return (
    <View style={styles.container}>
      <View style={styles.fisrtInfoOrder}>
        <TouchableOpacity style={styles.showInfoCart}>
          <SvgIcon
            width={16}
            height={16}
            svgIcon={ICON_CART_PAYMENT}
            style={styles.iconCart}
          />
          <TextContent style={styles.textTittle}>{TITLE_SHOW_CART}</TextContent>
          <SvgIcon
            width={14}
            height={14}
            svgIcon={ICON_SELECTBOX}
            tintColor={ACCENT_COLOR}
          />
        </TouchableOpacity>
        <TextContent style={styles.totalPrice}>
          {currencyConvert(price)}
        </TextContent>
      </View>
      <View style={styles.thirdContent}>
        <HorizontalView style={styles.flexArlert}>
          <View>
            <SvgIcon
              width={60}
              height={60}
              svgIcon={ICON_SUCCESS}
              tintColor={ACCENT_COLOR}
            />
          </View>
          <View style={styles.showArlert}>
            <Title style={styles.textTittleOrder}>{TITLE_ORDER_SUCCESS}</Title>
            <TextContent style={styles.textMethodPayment}>
              {TITLE_ORDER_CODE}
            </TextContent>
            <TextContent style={styles.textMethodPayment}>
              {CONTENT_ORDER_THANKYOU}
            </TextContent>
          </View>
        </HorizontalView>
        <View style={styles.detail}>
          <TextContent style={styles.textInfo}>{TITLE_INFO_ORDER}</TextContent>
          <TextContent style={styles.textTransport}>
            {TITLE_INFO_TRANSPORT}
          </TextContent>
          <TextContent style={styles.textMethodPayment}>
            {formPayment.fullname}
          </TextContent>
          <TextContent style={styles.textMethodPayment}>
            {formPayment.address}
            {formPayment.ward ? ', ' + formPayment.ward : null}
            {formPayment.district ? ', ' + formPayment.district : null}
          </TextContent>
          <TextContent style={styles.textMethodPayment}>
            {formPayment.city}
          </TextContent>
          <TextContent
            style={{...styles.textMethodPayment, ...styles.textPhone}}>
            {formPayment.phonenumber}
          </TextContent>
          <TextContent style={styles.titleDetailPayment}>
            {TITLE_METHOD_PAYMENT}
          </TextContent>
          <TextContent style={{...styles.textMethodPayment}}>
            {formPayment.payment}
          </TextContent>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingStart: 10,
    paddingEnd: 10,
    marginTop: 10,
  },
  //first
  fisrtInfoOrder: {
    height: 40,
    paddingBottom: 10,
    paddingTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  iconCart: {
    marginEnd: 10,
  },
  showInfoCart: {
    flexDirection: 'row',
  },
  textTittle: {
    textAlign: 'left',
    fontSize: 14,
    lineHeight: 16,
    color: ACCENT_COLOR,
    marginEnd: 5,
  },
  totalPrice: {
    fontSize: 16,
    letterSpacing: 1,
    color: ACCENT_COLOR,
    fontWeight: 'bold',
    lineHeight: 19,
  },
  //second

  //third
  thirdContent: {
    marginTop: 10,
  },
  textTittleOrder: {
    fontWeight: 'bold',
    fontSize: 20,
    color: ACCENT_COLOR,
  },
  textMethodPayment: {
    flex: 80,
    color: TEXT_GRAY_LIGHT_COLOR,
    fontSize: 14,
  },
  textInfo: {
    textAlign: 'left',
    fontSize: 14,
    lineHeight: 16,
    color: ACCENT_COLOR,
    height: 30,
    padding: 2,
    borderBottomWidth: 1,
    borderBottomColor: ACCENT_COLOR,
    marginBottom: 5,
  },
  showArlert: {
    flexDirection: 'column',
    marginStart: 10,
  },
  flexArlert: {
    marginStart: 10,
    marginBottom: 20,
  },
  textPhone: {
    color: ACCENT_COLOR,
    marginBottom: 15,
  },
  textTransport: {
    marginTop: 5,
    marginBottom: 5,
    color: TEXT_GRAY_LIGHT_COLOR,
    fontWeight: 'bold',
    fontSize: 14,
  },
  detail: {
    height: 235,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: ACCENT_COLOR,
    padding: 10,
  },
  titleDetailPayment: {
    paddingTop: 10,
    marginBottom: 5,
    color: ACCENT_COLOR,
    fontWeight: 'bold',
    fontSize: 14,
    borderTopWidth: 1,
    borderTopColor: LIGHT_GRAY_COLOR,
  },
});

export default CartNotiOrderBody;
