import React from 'react';
import {StyleSheet, View} from 'react-native';
import Button from '@common/components/button/Button';
import {LIGHT_GRAY_COLOR} from '../../../../../resources/palette';

const CartNotiOrderFooter = ({stepPayment, buttonPayment}) => {
  let textButton = '';
  switch (stepPayment) {
    case 0:
      textButton = 'Tiếp Tục Đến Phương Thức Thanh Toán';
      break;
    case 1:
      textButton = 'Hoàn Tất Đơn Hàng';
      break;
    case 2:
      textButton = 'Tiếp Tục Mua Hàng';
      break;
    default:
      break;
  }
  return (
    <View style={styles.container}>
      <Button
        {...buttonPayment}
        text={buttonPayment.text}
        buttonStyle={styles.buttonStyle}
        textStyle={styles.textStyle}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 70,
    borderTopColor: LIGHT_GRAY_COLOR,
    borderTopWidth: 1,
    justifyContent: 'center',
  },
  buttonStyle: {
    borderRadius: 22,
    width: 350,
    alignSelf: 'center',
  },
  textStyle: {
    fontSize: 16,
    color: '#ffffff',
    fontWeight: 'bold',
  },
});

export default CartNotiOrderFooter;
