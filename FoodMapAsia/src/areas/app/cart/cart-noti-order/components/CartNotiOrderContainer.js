import React from 'react';
import {View, StyleSheet} from 'react-native';
import CartNotiOrderHeader from './CartNotiOrderHeader';
import CartNotiOrderBody from './CartNotiOrderBody';
import CartNotiOrderFooter from './CartNotiOrderFooter';

const CartNotiOrderContainer = ({header, body, footer}) => {
  return (
    <View style={styles.container}>
      <CartNotiOrderHeader style={styles.header} {...header} />
      <CartNotiOrderBody style={styles.body} {...body} />
      <CartNotiOrderFooter style={styles.footer} {...footer} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {},
  body: {
    flex: 1,
  },
  footer: {},
});

export default CartNotiOrderContainer;
