import React from 'react';
import {connect} from 'react-redux';
import BackgroundTemplate from '@common/components/background/BackgroundTemplate';
import CartNotiOrderContainer from './components/CartNotiOrderContainer';
import {ICON_PRE} from '@resources/images';
import operations from '@redux/cart/operations';
import {
  TEXT_BUTTON_CONTINUE_BUY,
  TITLE_METHOD_PAYMENT,
} from '@resources/string/strings';
const mapStateToProps = (state) => ({
  formPayment: state.cart.formPayment,
  price: state.cart.price,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  header: {
    title: TITLE_METHOD_PAYMENT,
    backButton: {
      svgIcon: ICON_PRE,
      onPress: async () => {
        ownProps.navigation.goBack();
      },
    },
  },
  body: {
    inputDiscount: {
      placeholder: 'Mã giảm giá',
      onChangeText: (text) => {},
    },
    formDeliveryInfo: {
      inputFullName: {
        placeholder: 'Full Name',
        onChangeText: (text) => {
          dispatch(operations.updateFormPayment('fullname', text));
        },
      },
    },
  },
  footer: {
    buttonPayment: {
      text: TEXT_BUTTON_CONTINUE_BUY,
      onPress: () => {
        dispatch(operations.resetPayment());
        ownProps.navigation.navigate('CartListPage');
      },
    },
  },
  actions: {},
});

const CartNotiOrderPage = ({
  header,
  body,
  footer,
  actions,
  formPayment,
  price,
  ...props
}) => {
  return (
    <BackgroundTemplate scrollable={false} fullscreen={true}>
      <CartNotiOrderContainer
        header={{
          ...header,
        }}
        body={{
          ...body,
          formPayment,
          price,
        }}
        footer={{
          ...footer,
        }}
      />
    </BackgroundTemplate>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(CartNotiOrderPage);
