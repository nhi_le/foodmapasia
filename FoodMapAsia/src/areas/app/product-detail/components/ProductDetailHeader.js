import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import SvgIcon from '@common/components/icon/SvgIcon';
import BadgeTabIcon from '@common/components/icon/BadgeTabIcon';
import {
  DANGEROUS_COLOR,
  WHITE_COLOR,
  TEXT_DARK_COLOR,
} from '@resources/palette';
import SearchFieldInput from '@common/components/input/SearchFieldInput';

const ProductDetailHeader = ({
  style,
  locateButton,
  cartButton,
  searchInput,
}) => {
  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <TouchableOpacity style={styles.icon} onPress={locateButton.onPress}>
        <SvgIcon style={styles.icon} height={16} width={16} {...locateButton} />
      </TouchableOpacity>
      <SearchFieldInput style={styles.search} {...searchInput} />
      <TouchableOpacity style={styles.iconCart} onPress={cartButton.onPress}>
        <BadgeTabIcon
          {...cartButton}
          size={24}
          total={cartButton.count}
          badgeColor={DANGEROUS_COLOR}
          textColor={WHITE_COLOR}
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  titleToolbar: {
    flex: 1,
    color: TEXT_DARK_COLOR,
    textAlign: 'center',
    fontWeight: 'normal',
    alignSelf: 'center',
  },
  container: {
    height: 70,
    alignContent: 'center',
    justifyContent: 'center',
    paddingTop: 20,
    flexDirection: 'row',
  },
  search: {
    flex: 1,
    borderRadius: 60,
    height: 36,
  },
  iconCart: {
    width: 60,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    width: 60,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default ProductDetailHeader;
