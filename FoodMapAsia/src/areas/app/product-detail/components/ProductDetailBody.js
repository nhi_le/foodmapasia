/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Text,
} from 'react-native';
import ListContent from '@common/components/list/ListContent';
import Image from '@common/components/image/Image';
import {
  TEXT_GRAY_LIGHT_COLOR,
  BACKGROUND_PROFILE,
  BUTTON_LOGIN_COLOR,
  WHITE_COLOR,
  BUTTON_FACEBOOK_COLOR,
  RED_COLOR,
  ACCENT_COLOR,
} from '@resources/palette';
import {
  ICON_LIKE_DETAIL,
  ICON_DECREASE,
  ICON_INCREASE,
  ICON_LOGO_YELLOW,
  ICON_Link_YELLOW,
  ICON_CHAMP_YELLOW,
  ICON_PERSON_YELLOW,
  GRAY_COLOR,
} from '@resources/images';
import HorizontalView from '@common/components/layout/HorizontalView';
import TextContent from '@common/components/text/TextContent';
import SvgIcon from '@common/components/icon/SvgIcon';
import Input from '@common/components/input/Input';
import HTML from 'react-native-render-html';
import Swiper from '@common/components/swiper/Swiper';
import {WebView} from 'react-native-webview';
import {currencyConvert} from '@common/components/StringHelper';
import {TEXT_LIKE, TEXT_SHARE} from '@resources/string/strings';
import {renderText} from '@common/components/StringHelper';

const renderItemsProduct = (listProductions, onItemPress) => {
  if (listProductions[0]) {
    return listProductions.map((item, index) => {
      return (
        <View
          style={{
            alignItems: 'center',
            height: 630,
          }}
          key={index}>
          <ListContent
            numColumns={2}
            data={item}
            scrollEnabled={false}
            type={'homeChild'}
            onItemPress={onItemPress}
            style={styles.categoryList}
          />
        </View>
      );
    });
  } else {
    return (
      <View style={{alignItems: 'center', height: 600}} key={1}>
        <ListContent
          numColumns={2}
          data={listProductions}
          type={'homeChild'}
          scrollEnabled={false}
          onItemPress={onItemPress}
          style={styles.categoryList}
        />
      </View>
    );
  }
};

export default ProductDetailBody = ({
  style,
  categoryList,
  productItem,
  listBannerImg,
}) => {
  const baseRateURL =
    'https://choraucu.myharavan.com/products/{handle}?view=review_star_api&themeid=1000608813';
  const rateURL = baseRateURL.replace('{handle}', productItem.data.handle);
  const baseCommentURL =
    'https://choraucu.myharavan.com/products/{handle}?view=review_api&themeid=1000608813';
  const commentURL = baseCommentURL.replace(
    '{handle}',
    productItem.data.handle,
  );

  var a = categoryList.data,
    chunk;

  var listProductions = [];

  while (a.length > 0) {
    chunk = a.splice(0, 4);
    listProductions.push(chunk);
  }
  let isSoldOut =
    productItem &&
    productItem.data &&
    parseInt(productItem.data.inventory_quantity) === 0;
  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <ScrollView>
        <View style={styles.itemDetail}>
          <View>
            {listBannerImg &&
            listBannerImg.data &&
            listBannerImg.data.length > 0 ? (
              <View
                style={styles.scrollCategory}
                showsHorizontalScrollIndicator={false}>
                <Image
                  style={styles.imageStyle}
                  source={{uri: listBannerImg.data[0].src}}
                />
                <ListContent
                  horizontal={true}
                  {...listBannerImg}
                  type="image"
                />
              </View>
            ) : (
              <View style={[styles.imageStyle, {justifyContent: 'center'}]}>
                <Text style={styles.emptyImageTextStyle}>
                  {'Không có hình ảnh'}
                </Text>
              </View>
            )}
          </View>
          <View style={styles.itemDetailContainer}>
            <HorizontalView style={styles.deliveryHori}>
              <HorizontalView>
                <View style={{width: 100}}>
                  <WebView
                    scrollEnabled={false}
                    source={{
                      uri: rateURL,
                    }}
                  />
                </View>
                <HorizontalView style={styles.shipContainer}>
                  <SvgIcon
                    height={22}
                    width={22}
                    svgIcon={ICON_LOGO_YELLOW}
                    style={styles.iconStatusStyle}
                  />
                  <SvgIcon
                    height={22}
                    width={22}
                    svgIcon={ICON_Link_YELLOW}
                    style={styles.iconStatusStyle}
                  />
                  <SvgIcon
                    height={22}
                    width={22}
                    svgIcon={ICON_CHAMP_YELLOW}
                    style={styles.iconStatusStyle}
                  />
                  <SvgIcon
                    height={22}
                    width={22}
                    svgIcon={ICON_PERSON_YELLOW}
                    style={styles.iconStatusStyle}
                  />
                </HorizontalView>
              </HorizontalView>
              <HorizontalView style={styles.containerLikeButton}>
                <TouchableOpacity
                  onPress={() => {
                    productItem.onActionPress &&
                      productItem.onActionPress('share', productItem);
                  }}>
                  <HorizontalView style={styles.likeButtonStyle}>
                    <SvgIcon
                      height={12}
                      width={12}
                      svgIcon={ICON_LIKE_DETAIL}
                    />
                    <Text style={styles.textButtonStyle}>
                      {renderText(TEXT_LIKE)} 0
                    </Text>
                  </HorizontalView>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.containerShareBtn}
                  onPress={() => {
                    productItem.onActionPress &&
                      productItem.onActionPress('love', productItem);
                  }}>
                  <Text style={styles.shareButtonStyle}>
                    {renderText(TEXT_SHARE)}
                  </Text>
                </TouchableOpacity>
              </HorizontalView>
            </HorizontalView>

            <TextContent style={styles.itemTitle}>
              {productItem.data.itemName}
            </TextContent>
            <View style={styles.statusView}>
              <TextContent
                style={isSoldOut ? styles.soldOut : styles.availableText}>
                {isSoldOut ? 'Tạm hết' : 'Còn hàng'}{' '}
              </TextContent>
            </View>
            <HorizontalView style={{marginBottom: 10}}>
              <HorizontalView style={{flex: 1}}>
                <View style={styles.currentPriceContainer}>
                  <TextContent style={styles.currentPriceText}>
                    {currencyConvert(productItem.data.currentPrice)}
                  </TextContent>

                  <TextContent style={styles.currentPriceText}>
                    {' / '}
                  </TextContent>
                  <TextContent style={styles.currentPriceText}>
                    {productItem.data.itemType}
                  </TextContent>
                </View>
                {productItem.data.beforePrice != '0' && (
                  <View style={styles.beforePriceContainer}>
                    <TextContent style={styles.beforePriceText}>
                      {currencyConvert(productItem.data.beforePrice)}
                    </TextContent>
                  </View>
                )}

                {productItem.data.discountPercent
                  .toString()
                  .localeCompare('0%') === 0 ? (
                  <View />
                ) : (
                  <HorizontalView>
                    <View style={styles.percentContainer}>
                      <TextContent style={styles.percentText}>
                        {'-'}
                      </TextContent>
                      <TextContent style={styles.percentText}>
                        {productItem.data.discountPercent}
                      </TextContent>
                    </View>
                  </HorizontalView>
                )}
              </HorizontalView>
              <HorizontalView style={{marginStart: 10}}>
                <TouchableOpacity
                  style={{marginEnd: 8, marginTop: 5}}
                  onPress={() => {
                    productItem.onActionPress &&
                      productItem.onActionPress('decrease');
                  }}>
                  <SvgIcon width={24} height={24} svgIcon={ICON_DECREASE} />
                </TouchableOpacity>
                <Input
                  style={styles.inputNumberStyle}
                  inputStyle={styles.amountContainer}
                  keyboardType={'numeric'}
                  onChangeText={(text) => {
                    productItem.onActionPress(text);
                  }}>
                  {productItem.data.count}
                </Input>
                <TouchableOpacity
                  style={{marginStart: 8, marginTop: 5}}
                  onPress={() => {
                    productItem.onActionPress &&
                      productItem.onActionPress('increase');
                  }}>
                  <SvgIcon width={24} height={24} svgIcon={ICON_INCREASE} />
                </TouchableOpacity>
              </HorizontalView>
            </HorizontalView>
            <View style={styles.rule} />

            {/* web view area */}
          </View>
        </View>
        <View style={styles.itemDetailWeb}>
          <View style={styles.webview}>
            <ScrollView>
              <HTML
                html={productItem.data.htmlBody}
                decodeEntities={true}
                ignoredStyles={['display']}
              />
            </ScrollView>
          </View>
        </View>
        {/* web view area */}
        {productItem.data.tabList && productItem.data.tabList.length > 1 ? (
          <View style={styles.itemDetaiWeb}>
            <View style={styles.webview}>
              <TextContent style={styles.itemDetaiText}>
                {productItem.data.tabList[0].split('@@##@@')[0]}
              </TextContent>
              <ScrollView>
                <HTML
                  html={productItem.data.tabList[0].split('@@##@@')[1]}
                  decodeEntities={true}
                  ignoredStyles={['display']}
                />
              </ScrollView>
            </View>
          </View>
        ) : (
          <View />
        )}
        <View style={{width: '100%', height: 700}}>
          <WebView
            scrollEnabled={true}
            source={{
              uri: commentURL,
            }}
          />
        </View>
        <View>
          {listProductions && listProductions.length > 0 && (
            <TextContent style={styles.buyWithText}>
              {'Sản phẩm này thường được đặt mua cùng:'}
            </TextContent>
          )}

          {listProductions && listProductions.length > 0 && (
            <View style={{height: 630}}>
              <Swiper
                renderItems={() =>
                  renderItemsProduct(
                    listProductions ? listProductions : [],
                    categoryList.onItemPress,
                  )
                }
                showsButtons={false}
                style={{height: 630}}
                dotColor={ACCENT_COLOR}
                activeDotColor={ACCENT_COLOR}
                activeDotStyle={styles.activeDotStyle}
              />
            </View>
          )}
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: BACKGROUND_PROFILE,
  },
  itemDetailContainer: {
    marginHorizontal: 12,
    backgroundColor: 'white',
    marginTop: 20,
  },
  itemDetail: {
    backgroundColor: 'white',
  },
  buyWithText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: BUTTON_LOGIN_COLOR,
    marginLeft: 12,
    marginTop: 12,
    marginBottom: 12,
  },
  itemDetaiText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: BUTTON_LOGIN_COLOR,
    marginTop: 5,
    marginBottom: 12,
  },
  itemDetaiWeb: {
    marginTop: 15,
    paddingHorizontal: 12,
    backgroundColor: 'white',
  },
  itemDetailWeb: {
    paddingHorizontal: 12,
    backgroundColor: 'white',
  },
  webview: {
    marginTop: 12,
    width: '100%',
  },
  itemArena: {
    backgroundColor: 'red',
  },
  HoriView: {
    flex: 1,
    alignSelf: 'center',
    paddingTop: 10,
  },
  categoryList: {
    flex: 1,
  },
  textDeliveryStyle: {
    color: TEXT_GRAY_LIGHT_COLOR,
    fontSize: 12,
    alignSelf: 'center',
    marginLeft: 5,
  },
  deliveryHori: {
    flex: 1,
    alignItems: 'center',
  },
  itemTitle: {
    fontWeight: 'bold',
    fontSize: 18,
    marginTop: 12,
  },
  itemCategorie: {
    fontSize: 14,
    color: TEXT_GRAY_LIGHT_COLOR,
    marginTop: 10,
  },
  currentPriceContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  currentPriceText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: BUTTON_LOGIN_COLOR,
  },

  beforePriceContainer: {
    flexDirection: 'row',
    marginLeft: 8,
    alignItems: 'flex-end',
  },
  beforePriceText: {
    fontSize: 16,
    color: TEXT_GRAY_LIGHT_COLOR,
    textDecorationLine: 'line-through',
  },
  percentContainer: {
    flexDirection: 'row',
    marginLeft: 8,
    alignItems: 'flex-end',
  },
  percentText: {
    fontSize: 16,
    color: RED_COLOR,
  },
  multiPriceTitle: {
    fontSize: 16,
    color: TEXT_GRAY_LIGHT_COLOR,
  },
  multiPrice: {
    fontSize: 16,
    fontWeight: 'bold',
    color: TEXT_GRAY_LIGHT_COLOR,
    alignSelf: 'flex-end',
  },
  multiPriceHori: {
    marginTop: 10,
  },
  amountRow: {
    flex: 1,
  },
  amountContainer: {
    width: 46,
    height: 24,
    borderRadius: 5,
    borderColor: BUTTON_LOGIN_COLOR,
    backgroundColor: 'white',
    textAlign: 'center',
  },
  amountButton: {
    alignItems: 'center',
    alignContent: 'center',
  },
  textAmount: {
    fontSize: 14,
    color: 'white',
  },
  rule: {
    paddingTop: 10,
    borderBottomColor: BUTTON_LOGIN_COLOR,
    borderBottomWidth: 1,
    marginBottom: 5,
  },
  itemNameDescription: {
    fontSize: 14,
    fontWeight: 'bold',
    color: BUTTON_LOGIN_COLOR,
    marginTop: 5,
    marginBottom: 10,
  },
  itemdescription: {
    fontSize: 14,
    fontWeight: 'normal',
    color: BUTTON_LOGIN_COLOR,
  },
  rating: {
    alignSelf: 'center',
  },
  loveIcon: {
    marginLeft: 15,
  },
  shipContainer: {
    paddingHorizontal: 8,
    marginTop: 5,
    top: 5,
  },
  brandText: {
    fontSize: 16,
    color: TEXT_GRAY_LIGHT_COLOR,
    flex: 1,
  },
  nextWeekPrice: {
    fontSize: 14,
    fontWeight: 'bold',
    color: BUTTON_LOGIN_COLOR,
    alignSelf: 'center',
  },
  button: {
    alignContent: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    height: 24,
    width: 24,
    marginHorizontal: 4,
  },
  activeDotStyle: {
    width: 30,
    height: 2,
  },
  soldOut: {
    color: RED_COLOR,
    fontSize: 16,
    marginTop: 5,
  },
  availableText: {
    color: ACCENT_COLOR,
    fontSize: 16,
    marginTop: 5,
  },
  iconStatusStyle: {
    marginEnd: 8,
  },
  likeButtonStyle: {
    backgroundColor: BUTTON_FACEBOOK_COLOR,
    borderRadius: 5,
    alignItems: 'center',
    height: 22,
    paddingHorizontal: 4,
  },
  textButtonStyle: {
    color: WHITE_COLOR,
    fontSize: 12,
    marginStart: 4,
  },
  shareButtonStyle: {
    color: WHITE_COLOR,
    fontSize: 12,
    textAlign: 'center',
    paddingHorizontal: 4,
  },
  containerShareBtn: {
    backgroundColor: BUTTON_FACEBOOK_COLOR,
    borderRadius: 5,
    marginHorizontal: 6,
    height: 22,
    justifyContent: 'center',
  },
  containerLikeButton: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 5,
    top: 5,
  },
  inputNumberStyle: {
    width: 47,
    height: 24,
  },
  statusView: {
    width: '100%',
    height: 25,
    marginVertical: 8,
  },
  imageStyle: {
    width: '100%',
    height: 250,
    marginVertical: 14,
  },
  emptyImageTextStyle: {
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: 24,
    color: GRAY_COLOR,
  },
});
