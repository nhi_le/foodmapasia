import React from 'react';
import {View, StyleSheet} from 'react-native';
import ProductDetailHeader from './ProductDetailHeader';
import ProductDetailBody from './ProductDetailBody';
import ProductDetailFooter from './ProductDetailFooter';
import PaymentBottomSheet from './bottomsheet/PaymentBottomSheet';

const ProductDetailContainer = ({header, body, footer, bottomSheet}) => {
  return (
    <View style={styles.container}>
      <ProductDetailHeader style={styles.header} {...header} />
      <ProductDetailBody style={styles.body} {...body} />
      <ProductDetailFooter style={styles.footer} {...footer} />
      <PaymentBottomSheet {...bottomSheet} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {},
  body: {
    flex: 1,
  },
  footer: {},
});

export default ProductDetailContainer;
