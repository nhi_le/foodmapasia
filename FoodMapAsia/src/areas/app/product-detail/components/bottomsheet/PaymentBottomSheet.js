import React from 'react';
import {StyleSheet, Dimensions} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import PaymentBottomSheetHeader from './PaymentBottomSheetHeader';
import PaymentBottomSheetBody from './PaymentBottomSheetBody';
import {WHITE_COLOR, SHADOW_COLOR} from '@resources/palette';
import PaymentBottomSheetFooter from './PaymentBottomSheetFooter';

const height = Dimensions.get('window').height;

const PaymentBottomSheet = ({
  rbRef,
  continueButton,
  buttonClose,
  products,
  payButton,
  addCartButton,
  countListCartItem,
  total,
}) => {
  let title = 'Bạn có ' + countListCartItem + ' sản phẩm trong giỏ hàng';
  return (
    <RBSheet
      ref={rbRef}
      height={height / 1.5}
      duration={250}
      customStyles={{
        container: styles.container,
      }}>
      <PaymentBottomSheetHeader title={title} buttonClose={buttonClose} />
      <PaymentBottomSheetBody
        style={styles.body}
        products={products}
        total={total}
      />
      <PaymentBottomSheetFooter
        continueButton={continueButton}
        addCartButton={addCartButton}
        payButton={payButton}
      />
    </RBSheet>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    backgroundColor: WHITE_COLOR,
    elevation: 1,
    shadowColor: SHADOW_COLOR,
    shadowOpacity: 0.25,
    shadowRadius: 4,
    shadowOffset: {
      height: 2,
      width: 3,
    },
  },
  body: {
    flex: 1,
  },
});

export default PaymentBottomSheet;
