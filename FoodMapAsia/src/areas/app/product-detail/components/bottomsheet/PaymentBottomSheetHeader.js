import React from 'react';
import {View, StyleSheet} from 'react-native';
import {DARK_GRAY_COLOR} from '@resources/palette';
import TextContent from '@common/components/text/TextContent';
import SvgIcon from '@common/components/icon/SvgIcon';
import {ICON_CART_GREEN} from '../../../../../resources/images';
import {
  TEXT_GREEN,
  GRAY_COLOR,
  LIGHT_GRAY_COLOR,
} from '../../../../../resources/palette';

const PaymentBottomSheetHeader = ({style, title, buttonClose}) => {
  return (
    <View style={[styles.container, style]}>
      <View style={styles.line} />
      <View style={styles.header}>
        <View style={styles.iconWrapper}>
          <SvgIcon svgIcon={ICON_CART_GREEN} height={16} width={16} />
        </View>
        <View style={styles.info}>
          <TextContent style={styles.title} size="small">
            {title}
          </TextContent>
        </View>
        <View style={styles.iconRemove}>
          <SvgIcon {...buttonClose} />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    borderBottomColor: LIGHT_GRAY_COLOR,
    borderBottomWidth: 1,
    marginHorizontal: 16,
    paddingVertical: 16,
  },
  line: {
    backgroundColor: DARK_GRAY_COLOR,
    borderRadius: 3,
    width: 60,
    height: 6,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconWrapper: {},
  iconRemove: {},
  info: {
    justifyContent: 'space-between',
    marginStart: 18,
    flexShrink: 1,
    flexGrow: 1,
  },
  title: {
    color: TEXT_GREEN,
  },
});

export default PaymentBottomSheetHeader;
