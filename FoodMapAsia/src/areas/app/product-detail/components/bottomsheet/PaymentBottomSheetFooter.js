import React from 'react';
import {View, StyleSheet} from 'react-native';
import {
  WHITE_COLOR,
  ACCENT_COLOR,
  LIGHT_GRAY_COLOR,
} from '../../../../../resources/palette';
import Button from '@common/components/button/Button';
import HorizontalView from '../../../../../common/components/layout/HorizontalView';
import OutlineButton from '@common/components/button/OutlineButton';

const PaymentBottomSheetHeader = ({
  style,
  continueButton,
  addCartButton,
  payButton,
}) => {
  return (
    <View style={[styles.container, style]}>
      <View style={styles.buttonContinueContainer}>
        <Button
          buttonStyle={styles.buttonContinue}
          textStyle={styles.textButtonContinue}
          {...continueButton}
        />
      </View>
      <HorizontalView style={styles.buttonsContainer}>
        <OutlineButton
          style={styles.buttonAddCartContainer}
          buttonStyle={styles.buttonAddCart}
          textStyle={styles.textButtonAddCart}
          {...addCartButton}
        />
        <Button
          style={styles.buttonBuyNowContainer}
          buttonStyle={styles.buttonBuyNow}
          textStyle={styles.textButtonContinue}
          {...payButton}
        />
      </HorizontalView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',

    paddingBottom: 16,
  },
  buttonContinue: {
    borderRadius: 30,
  },
  buttonAddCart: {
    borderRadius: 30,
  },
  buttonBuyNow: {
    borderRadius: 30,
  },
  textButtonContinue: {
    color: WHITE_COLOR,
    fontWeight: 'bold',
  },
  textButtonAddCart: {
    color: ACCENT_COLOR,
    fontWeight: 'bold',
  },
  buttonAddCartContainer: {
    flex: 1,
    marginRight: 8,
  },
  buttonBuyNowContainer: {
    flex: 1,
    marginLeft: 8,
  },
  buttonContinueContainer: {
    borderTopColor: LIGHT_GRAY_COLOR,
    borderTopWidth: 1,
    borderBottomColor: LIGHT_GRAY_COLOR,
    borderBottomWidth: 1,
    padding: 16,
  },
  buttonsContainer: {
    height: 70,
    padding: 16,
    marginBottom: 20,
  },
});

export default PaymentBottomSheetHeader;
