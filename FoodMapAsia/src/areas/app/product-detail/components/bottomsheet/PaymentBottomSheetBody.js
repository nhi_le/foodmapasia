import React from 'react';
import {View, StyleSheet} from 'react-native';
import TextContent from '@common/components/text/TextContent';
import {
  ACCENT_COLOR,
  TEXT_GRAY_LIGHT_COLOR,
  LIGHT_GRAY_COLOR,
} from '@resources/palette';
import ListContent from '@common/components/list/ListContent';
import HorizontalView from '@common/components/layout/HorizontalView';
import {currencyConvert} from '@common/components/StringHelper';

const PaymentBottomSheetBody = ({style, products, total}) => {
  return (
    <View style={[styles.container, style]}>
      <ListContent
        {...products}
        type="product-bottomsheet"
        style={styles.notificationList}
      />
      <HorizontalView style={styles.totalContainer}>
        <TextContent style={styles.textTitleTotal} size={'small'}>
          {total.title}
        </TextContent>
        <TextContent style={styles.textTotalValue} size={'large'}>
          {currencyConvert(total.value)}
        </TextContent>
      </HorizontalView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    paddingLeft: 16,
    paddingRight: 16,
  },
  notificationList: {
    flex: 0.3,
  },
  textTotalValue: {
    color: ACCENT_COLOR,
    fontWeight: 'bold',
  },
  textTitleTotal: {
    color: TEXT_GRAY_LIGHT_COLOR,
  },
  totalContainer: {
    justifyContent: 'flex-end',
    paddingVertical: 10,
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: LIGHT_GRAY_COLOR,
  },
});

export default PaymentBottomSheetBody;
