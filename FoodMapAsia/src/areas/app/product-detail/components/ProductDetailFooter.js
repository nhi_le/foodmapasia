import React from 'react';
import {StyleSheet, View} from 'react-native';
import {BUTTON_LOGIN_COLOR} from '@resources/palette';
import Button from '@common/components/button/Button';
import OutlineButton from '@common/components/button/OutlineButton';
import {GRAY_COLOR} from '../../../../resources/palette';
import HorizontalView from '../../../../common/components/layout/HorizontalView';

const ProductDetailFooter = ({buttonAdd, buttonBuy}) => {
  return (
    <View style={styles.container}>
      <HorizontalView style={styles.horiView}>
        <OutlineButton
          style={styles.styleButton}
          buttonStyle={styles.buttonAdd}
          textStyle={styles.textAddButton}
          {...buttonAdd}
        />
        <Button
          style={styles.styleButton}
          buttonStyle={styles.buttonBuy}
          textStyle={styles.textBuyButton}
          {...buttonBuy}
        />
      </HorizontalView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderTopColor: GRAY_COLOR,
    borderTopWidth: 1,
    height: 70,
    paddingTop: 10,
  },
  styleButton: {
    flex: 1,
    borderRadius: 22,
    marginHorizontal: 10,
  },
  buttonAdd: {
    backgroundColor: 'white',
    borderRadius: 22,
  },
  buttonBuy: {
    backgroundColor: BUTTON_LOGIN_COLOR,
    borderRadius: 22,
  },
  textBuyButton: {
    color: 'white',
    fontWeight: 'bold',
  },
  textAddButton: {
    color: BUTTON_LOGIN_COLOR,
    fontWeight: 'bold',
  },
  horiView: {
    justifyContent: 'space-evenly',
  },
});

export default ProductDetailFooter;
