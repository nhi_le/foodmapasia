import React from 'react';
import {connect} from 'react-redux';
import BackgroundTemplate from '@common/components/background/BackgroundTemplate';
import operations from '@redux/product/operations';
import cartOperations from '@redux/cart/operations';
import ProductDetailContainer from './components/ProductDetailContainer';
import {ICON_PRODUCT_CART, ICON_PRE, ICON_CANCEL} from '@resources/images';
import {Share} from 'react-native';

const mapStateToProps = (state) => ({
  detailItem: state.product.productDetail,
  listRelevantItem: state.product.listRelevantItem,
  listCart: state.cart.listShoppingCart,
  total: state.cart.total,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  header: {
    title: 'Product Details',
    searchInput: {
      placeholder: 'Thanh Long Búp Sen',
      onChangeText: () => {},
    },
    locateButton: {
      svgIcon: ICON_PRE,
      onPress: async () => {
        ownProps.navigation.goBack();
      },
    },
    cartButton: {
      svgIcon: ICON_PRODUCT_CART,
      count: 0,
      onPress: async () => {
        ownProps.navigation.navigate('Cart');
      },
    },
  },
  body: {
    categoryList: {
      data: [],
      onItemPress: async (data) => {
        await dispatch(operations.updateProduct(data));
      },
    },
    listBannerImg: {
      data: [],
      onItemPress: (item, index) => {},
    },
    productItem: {
      data: '',
      onItemPress: () => {},
      onActionPress: async (type, data) => {
        switch (type) {
          case 'decrease':
            dispatch(operations.decreaseAmount());
            break;
          case 'increase':
            dispatch(operations.increaseAmount());
            break;
          case 'love':
            let isSuccess = await dispatch(operations.addToFavorite(data));
            if (!isSuccess) {
              ownProps.navigation.navigate('Profile');
            }
            dispatch(operations.updateProductFieldLove());
            break;
          case 'share':
            try {
              const result = await Share.share({
                message: data.data.url,
              });
              if (result.action === Share.sharedAction) {
                if (result.activityType) {
                  // shared with activity type of result.activityType
                } else {
                  // shared
                }
              } else if (result.action === Share.dismissedAction) {
                // dismissed
              }
            } catch (error) {}
            break;
          default:
            dispatch(operations.updateAmount(type));
            break;
        }
      },
    },
  },
  footer: {
    buttonAdd: {
      text: 'Mua riêng',
      onPress: async () => {
        bottomSheetRef && bottomSheetRef.open();
        await dispatch(cartOperations.getListCart());
        await dispatch(cartOperations.updatePrice());
        await dispatch(operations.addToCart());
      },
    },
    buttonBuy: {
      text: 'Mua chung',
      onPress: async () => {
        await dispatch(operations.addToCart());
        ownProps.navigation.navigate('CartTransportPage');
      },
    },
  },
  bottomSheet: {
    rbRef: (ref) => (bottomSheetRef = ref),
    countListCartItem: 0,
    buttonClose: {
      svgIcon: ICON_CANCEL,
      onPress: () => {
        bottomSheetRef.close();
      },
    },
    continueButton: {
      text: 'Tiếp Tục Mua Hàng',
      onPress: () => {
        bottomSheetRef.close();
        ownProps.navigation.goBack();
      },
    },
    addCartButton: {
      text: 'Cập Nhật',
      onPress: () => {
        dispatch(operations.updateCartApi());
        bottomSheetRef.close();
      },
    },
    payButton: {
      text: 'Thanh Toán',
      onPress: () => {
        ownProps.navigation.navigate('CartTransportPage');
        bottomSheetRef.close();
      },
    },
    products: {
      data: [],
      onItemPress: async (item) => {
        await dispatch(operations.updateProduct(item));
        bottomSheetRef.close();
      },
      onActionPress: (type, id, text) => {
        dispatch(operations.updateCountProductDetail(type, id, text));
      },
    },
    total: {
      title: 'Thành tiền: ',
    },
  },
  actions: {
    init: async () => await dispatch(operations.init()),
  },
});

let bottomSheetRef = null;

const ProductDetailPage = ({
  header,
  body,
  footer,
  login,
  bottomSheet,
  actions,
  tabSelected,
  listCategory,
  detailItem,
  listCart,
  total,
  listRelevantItem,
  ...props
}) => {
  return (
    <BackgroundTemplate
      scrollable={false}
      fullscreen={false}
      onInitialized={async () => {
        await actions.init();
      }}>
      <ProductDetailContainer
        header={{
          ...header,
          title: detailItem.itemName,
          searchInput: {
            ...header.searchInput,
            placeholder: detailItem.itemName,
          },
          cartButton: {
            ...header.cartButton,
            count: listCart.data.length,
          },
        }}
        body={{
          ...body,
          categoryList: {
            ...body.categoryList,
            data: listRelevantItem.data,
          },
          productItem: {
            ...body.productItem,
            data: detailItem,
          },
          listBannerImg: {
            ...body.listBannerImg,
            data: detailItem.images,
          },
          tabSelected: tabSelected,
        }}
        footer={{
          ...footer,
          tabSelected: tabSelected,
        }}
        bottomSheet={{
          ...bottomSheet,
          products: {
            ...bottomSheet.products,
            data: listCart.data,
          },
          total: {
            ...bottomSheet.total,
            value: total,
          },
          countListCartItem: listCart.data.length,
        }}
      />
    </BackgroundTemplate>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetailPage);
