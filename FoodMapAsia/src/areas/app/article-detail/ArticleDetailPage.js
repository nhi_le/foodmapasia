import React, {Component} from 'react';
import {connect} from 'react-redux';
import BackgroundTemplate from '@common/components/background/BackgroundTemplate';
import {ICON_CANCEL} from '@resources/images';
import ArticleDetailContainer from './components/ArticleDetailContainer';
import operations from '@redux/news/operations';
import {TITLE_FOODCHANEL} from '@resources/string/strings';
import {renderText} from '@common/components/StringHelper';

const mapStateToProps = (state) => ({
  tabSelected: state.news.tabSelected,
  listFoodChannel: state.news.listFoodChannel,
  listArticle: state.news.listArticle,
  detailArticle: state.news.detailArticle,
  formComment: state.news.formComment,
  isAuthorized: state.auth.isAuthorized,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  header: {
    title: renderText(TITLE_FOODCHANEL),
    backButton: {
      svgIcon: ICON_CANCEL,
      onPress: async () => {
        ownProps.navigation.goBack();
      },
    },
  },
  body: {
    detailArticle: {
      author: '',
      comment_post_url: '',
      comments: [],
      comments_count: '',
      isCommentsEnabled: '',
      handle: '',
      image_alt: '',
      image_src: '',
      moderated: '',
      published_at: '',
      title: '',
      onActionPress: async (data) => {
        await dispatch(operations.postComment());
      },
      onChangeText: (text) => {
        dispatch(operations.setCommentContent({content: text}));
      },
    },
    foodChannel: {
      onItemPress: (data) => {
        dispatch(operations.setCategorySelected(data.id));
      },
    },
    listArticle: {
      data: [''],
      onItemPress: (data) => {},
    },
    commentWebView: {
      sharedCookiesEnabled: true,
      scrollEnabled: true,
      javaScriptEnabled: true,
      domStorageEnabled: true,
      onNavigationStateChange: async (navigationState) => {},
      onMessage: async (event) => {
        dispatch(
          operations.setCommentContent({recaptcha: event.nativeEvent.data}),
        );
      },
    },
  },
  footer: {},
  actions: {
    getAPIBlogList: () => dispatch(operations.getAPIBlogList()),
  },
});
class ArticleDetailPage extends Component {
  constructor() {
    super();
  }

  componentDidMount() {
    this.walletListener = this.props.navigation.addListener(
      'willFocus',
      async () => {},
    );
  }

  render() {
    const {
      header,
      body,
      footer,
      login,
      actions,
      tabSelected,
      listFoodChannel,
      listArticle,
      detailArticle,
      formComment,
      isAuthorized,
    } = this.props;

    return (
      <BackgroundTemplate
        scrollable={false}
        fullscreen={true}
        onInitialized={() => {
          actions.getAPIBlogList();
        }}>
        <ArticleDetailContainer
          header={{...header}}
          body={{
            ...body,
            isAuthorized,
            formComment,
            detailArticle: {
              ...body.detailArticle,
              ...detailArticle,
            },
            foodChannel: {
              ...body.foodChannel,
              data: listFoodChannel.data,
            },
            listArticle: {
              ...body.listArticle,
              data: listArticle.data,
            },

            tabSelected: tabSelected,
          }}
        />
      </BackgroundTemplate>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ArticleDetailPage);
