import React from 'react';
import {View, StyleSheet} from 'react-native';
import ArticleDetailHeader from './ArticleDetailHeader';
import ArticleDetailBody from './ArticleDetailBody';
import ArticleDetailFooter from './ArticleDetailFooter';

const ArticleDetailContainer = ({header, body, footer}) => {
  return (
    <View style={styles.container}>
      <ArticleDetailHeader style={styles.header} {...header} />
      <ArticleDetailBody style={styles.body} {...body} />
      {/* <FoodChanelFooter style={styles.footer} {...footer} /> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {},
  body: {
    flex: 1,
  },
  footer: {},
});

export default ArticleDetailContainer;
