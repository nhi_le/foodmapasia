import React from 'react';
import {View, StyleSheet} from 'react-native';
import Title from '@common/components/text/Title';

import SvgIcon from '@common/components/icon/SvgIcon';
import {TEXT_DARK_COLOR} from '@resources/palette';
import {ICON_PRE} from '@resources/images';
import {TouchableOpacity} from 'react-native-gesture-handler';

const ArticleDetailHeader = ({style, title, backButton}) => {
  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <TouchableOpacity style={styles.icon} onPress={backButton.onPress}>
        <SvgIcon height={18} width={18} {...backButton} svgIcon={ICON_PRE} />
      </TouchableOpacity>

      <Title style={styles.titleToolbar} size="medium-title">
        {title}
      </Title>

      <View style={styles.icon} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 60,
    alignContent: 'center',
    flexDirection: 'row',
    paddingTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleToolbar: {
    flex: 1,
    color: TEXT_DARK_COLOR,
    textAlign: 'center',
    fontWeight: 'normal',
  },
  iconToolbar: {
    width: 24,
    height: 24,
  },
  icon: {
    width: 60,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default ArticleDetailHeader;
