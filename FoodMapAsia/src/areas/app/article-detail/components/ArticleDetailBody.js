import React from 'react';
import {View, StyleSheet, ScrollView, TouchableOpacity} from 'react-native';
import {WebView} from 'react-native-webview';
import ListContent from '@common/components/list/ListContent';
import DetailArticle from './DetailArticle';
import {
  ACCENT_COLOR,
  COLOR_READ,
  COLOR_UNREAD,
  GRAY_COLOR,
  WHITE_COLOR,
  LIGHT_GRAY_COLOR,
} from '@resources/palette';
import TextContent from '@common/components/text/TextContent';
import Image from '@common/components/image/Image';
import Title from '@common/components/text/Title';
import Input from '@common/components/input/Input';
import HorizontalView from '@common/components/layout/HorizontalView';
import SvgIcon from '@common/components/icon/SvgIcon';
import {ICON_FB_LIKE} from '@resources/images';
import HTML from 'react-native-render-html';
const ArticleDetailBody = ({
  style,
  foodChannel,
  tabSelected,
  listArticle,
  articleDetail,
  detailArticle,
  description,
  commentWebView,
  formComment,
  isAuthorized,
}) => {
  const arrayCmt = {
    data: detailArticle.comments,
  };
  let js = `
  var originalPostMessage = window.postMessage;
  var patchedPostMessage = function (message, targetOrigin, transfer) { originalPostMessage(message, targetOrigin, transfer);};
  patchedPostMessage.toString = function () {
  return String(Object.hasOwnProperty).replace('hasOwnProperty', 'postMessage'); };
  window.postMessage = patchedPostMessage; setTimeout(() => {
  var recaptcha = document.querySelectorAll('[name="g-recaptcha-response"]'); if (recaptcha && recaptcha.length > 0) {
  window.ReactNativeWebView.postMessage(recaptcha[0].value); }
  }, 2000);
  const patchPostMessageJsCode = '(' + String(patchPostMessageFunction) + ')();';
`;
  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <ScrollView style={{padding: 10, marginBottom: 5}}>
        <View style={styles.firstItem}>
          <Image
            style={styles.ImageFirst}
            source={{uri: detailArticle.image_src}}
          />
          <Title size="large" style={styles.title}>
            {detailArticle.title}
          </Title>
          <View style={styles.public}>
            <TextContent size="small" style={styles.text}>
              {'Đăng bởi :'}
            </TextContent>
            <TextContent size="small" style={styles.textUser}>
              {detailArticle.author}{' '}
            </TextContent>
            <TextContent size="small" style={styles.text}>
              {'Lúc'}
            </TextContent>
            <TextContent size="small" style={styles.text}>
              {detailArticle.published_at}{' '}
            </TextContent>
          </View>
          <View>
            <HTML
              html={detailArticle.html}
              decodeEntities={true}
              ignoredStyles={['display']}
            />
          </View>
          <View style={styles.rule} />
          {detailArticle.isCommentsEnabled ? (
            <View>
              <Title size="large" style={styles.comment}>
                {'Bình Luận'}
              </Title>
              <Title size="large" style={styles.commentCount}>
                {detailArticle.comments.length} {'bình luận'}
              </Title>
              <View>
                {arrayCmt && arrayCmt.data && arrayCmt.data.length > 0 && (
                  <ListContent {...arrayCmt} type={'newsCommentItem'} />
                )}
              </View>
              {isAuthorized ? (
                <View>
                  <View style={styles.inputContainer}>
                    <Input
                      placeholder={'Thêm bình luận'}
                      inputStyle={styles.inputStyle}
                      onChangeText={(text) => detailArticle.onChangeText(text)}
                      errorMessage={formComment.validation.contentErrorMessage}
                    />
                  </View>
                  <View>
                    <TouchableOpacity
                      onPress={() => {
                        detailArticle.onActionPress &&
                          detailArticle.onActionPress('button');
                      }}>
                      <View style={styles.buttonPost}>
                        <TextContent style={styles.buttonPostText}>
                          {'Post'}
                        </TextContent>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              ) : (
                <View />
              )}
            </View>
          ) : (
            <View />
          )}
          {detailArticle.tag && detailArticle.tag.length > 0 && (
            <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
              <Title size="normal" style={styles.tagText}>
                {'Tags:'}
              </Title>
              {detailArticle.tag.map((item) => {
                return (
                  <View>
                    <View style={styles.tagItems}>
                      <TextContent style={styles.tagItemsText}>
                        {item.tag}
                      </TextContent>
                    </View>
                  </View>
                );
              })}
            </View>
          )}
          <View style={styles.rule} />
          <Title size="large" style={styles.comment}>
            {'Tin nổi bật'}
          </Title>
          <ListContent {...listArticle} type="listArticleHightLight" />
        </View>
      </ScrollView>
      <WebView
        style={{width: 0, height: 0}}
        injectedJavaScript={js}
        {...commentWebView}
        source={{
          uri: detailArticle.captcha,
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tagItems: {
    margin: 5,
    backgroundColor: '#F2F2F2',
    alignSelf: 'center',
    borderRadius: 5,
  },
  tagItemsText: {
    color: COLOR_READ,
    fontSize: 14,
    textAlign: 'center',
    fontWeight: 'bold',
    padding: 5,
  },
  inputContainer: {
    marginHorizontal: 24,
    marginVertical: 12,
  },
  inputStyle: {
    alignSelf: 'center',
    borderRadius: 5,
    borderColor: GRAY_COLOR,
    backgroundColor: 'white',
    height: 72,
  },
  password: {
    marginTop: 20,
  },
  buttonRegister: {
    width: 200,
    marginTop: 30,
    justifyContent: 'center',
    alignSelf: 'center',
  },
  categoryList: {},
  scrollCategory: {
    flexGrow: 0,
  },
  firstItem: {},
  ImageFirst: {
    width: '100%',
    height: 230,
    marginBottom: 10,
  },
  title: {
    color: ACCENT_COLOR,
    marginBottom: 10,
  },
  comment: {
    color: ACCENT_COLOR,
    fontSize: 16,
    padding: 10,
  },
  tagText: {
    color: COLOR_READ,
    fontSize: 14,
    padding: 10,
  },
  commentCount: {
    color: COLOR_READ,
    fontSize: 14,
    marginLeft: 15,
  },
  public: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  text: {
    marginRight: 5,
    color: COLOR_READ,
    fontSize: 12,
  },
  commentInput: {},
  textUser: {
    color: COLOR_READ,
    marginRight: 5,
    fontWeight: 'bold',
  },
  highlight: {
    color: COLOR_UNREAD,
    marginRight: 5,
    marginTop: 5,
    fontSize: 16,
    fontWeight: 'bold',
  },
  textPreview: {
    flex: 1,
    paddingTop: 10,
    height: '100%',
    color: COLOR_UNREAD,
  },
  textSource: {
    fontStyle: 'italic',
    textAlign: 'right',
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    height: '100%',
    color: COLOR_UNREAD,
  },
  nextItems: {
    height: 70,
    flexDirection: 'row',
    marginBottom: 10,
  },
  imagesNext: {
    width: '30%',
    height: '100%',
    marginEnd: 10,
  },
  textItemNext: {
    color: COLOR_READ,
    marginTop: -3,
    flex: 0.2,
  },
  titleItemNext: {
    flex: 0.8,
    justifyContent: 'center',
  },
  rule: {
    padding: 10,
    borderBottomColor: LIGHT_GRAY_COLOR,
    borderBottomWidth: 1,
  },
  textContainer: {
    color: WHITE_COLOR,
    fontSize: 12,
    alignSelf: 'center',
  },
  buttonPostText: {
    color: WHITE_COLOR,
    fontSize: 14,
    padding: 10,
  },
  buttonPost: {
    margin: 5,
    backgroundColor: ACCENT_COLOR,
    alignSelf: 'flex-end',
    alignItems: 'center',
    borderRadius: 22,
    marginRight: 30,
    width: 100,
  },
});

export default ArticleDetailBody;
