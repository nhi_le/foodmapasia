import React from 'react';
import {View, StyleSheet, Text} from 'react-native';

import {
  LOGO_MAIN_PAGE,
  ICON_COFFEE_MAIN_PAGE,
  ICON_MEET_MAIN_PAGE,
  ICON_VEGETABLE_MAIN_PAGE,
  ICON_FRAME_MAIN_PAGE,
  ICON_DRINK_MAIN_PAGE,
  LOGO_WHILE_MAIN_PAGE,
  ICON_WHILE_COFFEE_MAIN_PAGE,
  ICON_WHILE_MEET_MAIN_PAGE,
  ICON_WHILE_VEGETABLE_MAIN_PAGE,
  ICON_WHILE_FRAME_MAIN_PAGE,
  ICON_WHILE_DRINK_MAIN_PAGE,
} from '@resources/images';
import SvgIcon from '@common/components/icon/SvgIcon';
import {
  ACCENT_COLOR,
  WHITE_COLOR,
  COLOR_DARK_GREEN,
} from '../../resources/palette';
import {renderText} from '../../common/components/StringHelper';
import {
  TEXT_KNOW,
  TEXT_YOUR_FARMER,
  TEXT_YOUR_FOOD,
} from '../../resources/string/strings';
import HorizontalView from '../../common/components/layout/HorizontalView';

const LoadingPage = ({style, styleOrange}) => {
  styleOrange = false;
  let logoMain = {};
  let iconCoffee = {};
  let logoVegetable = {};
  let logoMeet = {};
  let logoFrame = {};
  let logoDrink = {};
  let backgroundColor = '';
  let colorIcon = '';
  if (styleOrange) {
    logoMain = LOGO_WHILE_MAIN_PAGE;
    iconCoffee = ICON_WHILE_COFFEE_MAIN_PAGE;
    logoMeet = ICON_WHILE_MEET_MAIN_PAGE;
    logoVegetable = ICON_WHILE_VEGETABLE_MAIN_PAGE;
    logoFrame = ICON_WHILE_FRAME_MAIN_PAGE;
    logoDrink = ICON_WHILE_DRINK_MAIN_PAGE;
    backgroundColor = '#D8412C';
  } else {
    logoMain = LOGO_MAIN_PAGE;
    iconCoffee = ICON_COFFEE_MAIN_PAGE;
    logoMeet = ICON_MEET_MAIN_PAGE;
    logoVegetable = ICON_VEGETABLE_MAIN_PAGE;
    logoFrame = ICON_FRAME_MAIN_PAGE;
    logoDrink = ICON_DRINK_MAIN_PAGE;
    backgroundColor = ACCENT_COLOR;
  }
  return (
    <View
      style={{
        ...styles.container,
        ...style,
        backgroundColor: backgroundColor,
      }}>
      <SvgIcon
        width={195}
        height={195}
        svgIcon={logoMain}
        style={styles.iconStyle}
      />
      <View style={styles.viewTextContainer}>
        <HorizontalView>
          <Text style={styles.textWhite}>{renderText(TEXT_KNOW)}</Text>
          <Text style={styles.textGreen}>{renderText(TEXT_YOUR_FARMER)}</Text>
        </HorizontalView>
        <HorizontalView>
          <Text style={styles.textWhite}>{renderText(TEXT_KNOW)}</Text>
          <Text style={styles.textGreen}>{renderText(TEXT_YOUR_FOOD)}</Text>
        </HorizontalView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  iconStyle: {
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'center',
  },
  viewTextContainer: {
    marginBottom: 30,
  },
  textWhite: {
    fontWeight: '900',
    textTransform: 'uppercase',
    fontSize: 24,
    color: WHITE_COLOR,
  },
  textGreen: {
    fontWeight: 'bold',
    textTransform: 'uppercase',
    fontSize: 24,
    color: COLOR_DARK_GREEN,
  },
  styleIcon: {
    marginEnd: 10,
  },
});

export default LoadingPage;
