import {
  UPDATE_LIST_FAVORITE,
  INCREASE_AMOUNT,
  CLEAR_FORM_PAYMENT,
  CLEAR_STEP_PAYMENT,
  DECREASE_AMOUNT,
  REMOVE_FAVORITE,
} from './constants';

const defaultState = {
  listFavorite: {
    data: [],
  },
  stepFavorite: 0,
};

const reduce = (state = defaultState, action) => {
  switch (action.type) {
    case UPDATE_LIST_FAVORITE:
      return {
        ...state,
        listFavorite: {
          ...state.listFavorite,
          data: action.listFavorite,
        },
      };
    case INCREASE_AMOUNT:
      return {
        ...state,
        listFavorite: {
          ...state.listFavorite,
          ...action.listFavorite,
        },
      };
    case DECREASE_AMOUNT:
      return {
        ...state,
        listFavorite: {
          ...state.listFavorite,
          ...action.listFavorite,
        },
      };
    case REMOVE_FAVORITE:
      return {
        ...state,
        listFavorite: {
          ...state.listFavorite,
          ...action.listFavorite,
        },
      };
    case CLEAR_STEP_PAYMENT:
      return {
        ...state,
        stepFavorite: defaultState.stepFavorite,
      };
    case CLEAR_FORM_PAYMENT:
      return {
        ...state,
        listFavorite: defaultState.listFavorite,
      };
    default:
      return state;
  }
};

export default reduce;
