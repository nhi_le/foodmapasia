import {
  UPDATE_LIST_FAVORITE,
  INCREASE_AMOUNT,
  CLEAR_STEP_PAYMENT,
  CLEAR_FORM_PAYMENT,
  DECREASE_AMOUNT,
  REMOVE_FAVORITE,
} from './constants';

const updateFavoriteList = (listFavorite) => ({
  type: UPDATE_LIST_FAVORITE,
  listFavorite,
});
const increaseAmount = (amount) => ({
  type: INCREASE_AMOUNT,
  amount,
});
const decreaseAmount = (amount) => ({
  type: DECREASE_AMOUNT,
  amount,
});
const removeFavoriteItem = (amount) => ({
  type: REMOVE_FAVORITE,
  amount,
});

export default {
  updateFavoriteList,
  decreaseAmount,
  removeFavoriteItem,
};
