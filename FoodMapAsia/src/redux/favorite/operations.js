/* eslint-disable radix */
import actions from './actions';
import _ from 'lodash';
import appOperations from '@redux/app/operations';
import api from './api';
import {discountPercent, isSale} from '@common/components/StringHelper';

const updateFavoriteList = (key, value) => (dispatch, getState) => {
  const changes = {};
  changes[key] = value;
  return dispatch(actions.updateFavoriteList(changes));
};

const addToCart = () => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    const {listFavorite} = getState().favorite;
    await listFavorite.data.forEach(async (element) => {
      await postAddItemCart(element.id, element.count);
    });
  } catch (error) {
    console.log('error', error);
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

export async function postAddItemCart(id, quantity) {
  if (id && quantity) {
    const url = 'https://choraucu.myharavan.com/cart/add.js';
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
    };

    let body = {
      id: id,
      quantity: quantity,
    };

    try {
      var result = await fetch(url, {
        method: 'POST',
        credentials: 'include',
        headers,
        body: JSON.stringify(body),
      });
      if (result) {
        if (result.status !== 200) {
          return false;
        } else {
          let data = await result.text();
          const resultAddCart = JSON.parse(data);
          if (resultAddCart && resultAddCart.id) {
            return resultAddCart;
          } else {
            return false;
          }
        }
      } else {
        return false;
      }
    } catch (error) {
      return false;
    }
  }
}

const increaseAmount = (type, id) => (dispatch, getState) => {
  var {listFavorite} = getState().favorite;

  let listNew = listFavorite.data.map((item) => {
    return {
      ...item,
      count:
        item.id === id
          ? item.count === 99
            ? item.count
            : item.count + 1
          : item.count,
    };
  });
  dispatch(actions.updateFavoriteList(listNew));
};

const updateAmount = (type, id) => (dispatch, getState) => {
  var {listFavorite} = getState().favorite;
  if (type.isEmpty() || parseInt(type) >= 99) {
    return;
  }
  let listNew = listFavorite.data.map((item) => {
    return {
      ...item,
      count: item.id === id ? (item.count = parseInt(type)) : item.count,
    };
  });
  dispatch(actions.updateFavoriteList(listNew));
};

const decreaseAmount = (type, id) => (dispatch, getState) => {
  var {listFavorite} = getState().favorite;
  listFavorite.data.forEach((item) => {
    if (item.id === id) {
      if (item.count === 1) {
        return true;
      }
    }
  });
  let listNew = listFavorite.data.map((item) => {
    return {
      ...item,
      count:
        item.id === id
          ? item.count === 1
            ? item.count
            : item.count - 1
          : item.count,
    };
  });
  dispatch(actions.updateFavoriteList(listNew));
};

const removeFavoriteItem = (type, id) => (dispatch, getState) => {
  var amount = getState().favorite;

  _.remove(amount.listFavorite.data, {id: id});
  amount.listFavorite.data = [...amount.listFavorite.data];
  dispatch(removeFavoriteAPI(id));
  dispatch(actions.removeFavoriteItem(id));
};

const getListFavorite = () => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    const {user} = getState().auth;
    const browserIDResponse = await api.getBrowserID();
    const browserID = browserIDResponse.wishlist_browser_id;
    const customerID = user.id;
    const details = {
      shop: 'choraucu.myharavan.com',
      customer_id: customerID,
      browser_id: browserID,
    };

    const formBody = [];
    for (const property in details) {
      const encodeKey = encodeURIComponent(_.toString(property));
      const encodeValue = encodeURIComponent(_.toString(details[property]));
      formBody.push(encodeKey + '=' + encodeValue);
    }
    const body = formBody.join('&');
    const response = await api.getFavoriteList(body);
    let favoriteResponse = await response.text();
    const favoriteParsed = JSON.parse(favoriteResponse);

    if (favoriteParsed && favoriteParsed.data) {
      const listNew = favoriteParsed.data.map((item, index) => {
        const data = JSON.parse(unescape(item.product_handle));

        return {
          id: data.id,
          image: data.images[0].src
            ? data.images[0].src
            : 'https://icdn.dantri.com.vn/thumb_w/640/2019/05/18/ca-chua-1558139604071.jpg',
          itemName: data.itemName,
          priceAfter: data.currentPrice,
          priceBefore: data.beforePrice,
          isDiscount: isSale(data.beforePrice, data.currentPrice),
          discount: discountPercent(data.beforePrice, data.currentPrice),
          count: 1,
          handle: data.handle,
          variants: data.variants ? data.variants : [],
        };
      });
      if (listNew.length > 0) {
        dispatch(actions.updateFavoriteList(listNew));
        return;
      }
    }
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const removeFavoriteAPI = (id) => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    const {user} = getState().auth;
    const browserIDResponse = await api.getBrowserID();
    const browserID = browserIDResponse.wishlist_browser_id;
    const customerID = user.id;
    const details = {
      shop: 'choraucu.myharavan.com',
      customer_id: customerID,
      browser_id: browserID,
      product_id: id,
    };

    const formBody = [];
    for (const property in details) {
      const encodeKey = encodeURIComponent(_.toString(property));
      const encodeValue = encodeURIComponent(_.toString(details[property]));
      formBody.push(encodeKey + '=' + encodeValue);
    }
    const body = formBody.join('&');
    await api.removeFavoriteItem(body);
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

export default {
  updateFavoriteList,
  increaseAmount,
  decreaseAmount,
  removeFavoriteItem,
  updateAmount,
  getListFavorite,
  removeFavoriteAPI,
  addToCart,
};
