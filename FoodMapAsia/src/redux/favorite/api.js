import api from '../../services/remote/baseApi';
import apiEndPoints from '../../services/remote/apiEndPoints';

const withQuery = require('with-query').default;
const getBrowserID = () => {
  return api.get(apiEndPoints.GET_BROWSER_ID);
};
const getFavoriteList = (body) => {
  return fetch(apiEndPoints.GET_FAVORITE_LIST, {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-requested-With': 'XMLHttpRequest',
    },
    body,
  });
};
const removeFavoriteItem = (body) => {
  return fetch(apiEndPoints.REMOVE_FAVORITE_ITEM, {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-requested-With': 'XMLHttpRequest',
    },
    body,
  });
};
const getProductDetail = (productHandle) => {
  return api.get(
    apiEndPoints.GET_PRODUCT_DETAIL.replace('{Handle}', productHandle),
  );
};
export default {
  getFavoriteList,
  getBrowserID,
  getProductDetail,
  removeFavoriteItem,
};
