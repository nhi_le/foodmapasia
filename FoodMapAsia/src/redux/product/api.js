import api from '../../services/remote/baseApi';
import apiEndPoints from '../../services/remote/apiEndPoints';

const getProductDetail = (productHandle) => {
  return api.get(
    apiEndPoints.GET_PRODUCT_DETAIL.replace('{Handle}', productHandle),
  );
};
const getRelevantProduct = (handle) => {
  return api.get(apiEndPoints.GET_PRODUCT_RELEVANT.replace('{Handle}', handle));
};
const getProductHTML = (handle) => {
  return api.get(apiEndPoints.GET_PRODUCT_HTML.replace('{Handle}', handle));
};
const getProductTabList = (handle) => {
  return api.get(apiEndPoints.GET_PRODUCT_TAB_LIST.replace('{Handle}', handle));
};
const addToCart = (body) => {
  return fetch(apiEndPoints.ADD_TO_CART, {
    method: 'POST',
    credentials: 'include',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'X-requested-With': 'XMLHttpRequest',
    },
    body,
  });
};
const getCurrentCart = () => {
  return fetch(apiEndPoints.GET_CURRENT_CART, {
    method: 'GET',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-requested-With': 'XMLHttpRequest',
    },
  });
};
const updateCart = (body) => {
  return fetch(apiEndPoints.UPDATE_CART, {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-requested-With': 'XMLHttpRequest',
    },
    body,
  });
};
const getBrowserID = () => {
  return api.get(apiEndPoints.GET_BROWSER_ID);
};
const getInfoAccount = () => {
  return fetch(apiEndPoints.GET_INFO_ACCOUNT, {
    method: 'GET',
    credentials: 'include',
  });
};
const addToFavorite = (body) => {
  return fetch(apiEndPoints.ADD_TO_FAVORITE, {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-requested-With': 'XMLHttpRequest',
    },
    body,
  });
};
export default {
  getProductDetail,
  getRelevantProduct,
  addToCart,
  getProductHTML,
  getCurrentCart,
  updateCart,
  getBrowserID,
  getInfoAccount,
  addToFavorite,
  getProductTabList,
};
