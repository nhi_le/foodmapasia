import {
  SET_PRODUCT,
  SET_PRODUCT_RELEVANT,
  UPDATE_PRODUCT_FIELD,
  CLEAR_PRODUCT_DETAIL,
  UPDATE_PRODUCT_FORM,
} from './constants';

const defaultState = {
  productDetail: {
    inventory_quantity: '',
    id: '',
    htmlBody: '',
    handle: '',
    rateScore: 0,
    url: '',
    deliveryType: '',
    itemName: '',
    categorie: '',
    currentPrice: '',
    beforePrice: '',
    discountPercent: 0,
    itemType: '',
    variants: [],
    isLove: '',
    nextWeekPriceUp: '',
    count: 1,
    decription: '',
  },
  listRelevantItem: {
    data: [
      {
        handle: 'peas-freeze-dried-dehydrated-survival-food',
        id: '1026801985',
        image: '',
        isSale: false,
        item100: '96000',
        item20: '96000',
        item50: '96000',
        itemName: 'Peas Freeze Dried Dehydrated Survival Food',
        itemType: 'hộp',
        priceAlfter: '96000',
        priceBefore: '0',
        salePercent: '0%',
        variants: [],
      },
    ],
  },
};

const reduce = (state = defaultState, action) => {
  switch (action.type) {
    case SET_PRODUCT:
      return {
        ...state,
        productDetail: action.productDetail,
      };
    case UPDATE_PRODUCT_FIELD:
      return {
        ...state,
        productDetail: {
          ...state.productDetail,
          ...action.form,
        },
      };
    case SET_PRODUCT_RELEVANT:
      return {
        ...state,
        listRelevantItem: {
          data: action.listProductRelevant,
        },
      };
    case CLEAR_PRODUCT_DETAIL:
      return {
        ...state,
        productDetail: defaultState.productDetail,
        listRelevantItem: {
          data: [],
        },
      };
    default:
      return state;
  }
};

export default reduce;
