import actions from './actions';
import appOperations from '@redux/app/operations';
import api from './api';
import apiFavorite from '../favorite/api';
import _ from 'lodash';
import cartOperations from '../cart/operations';
import {discountPercent, isSale} from '@common/components/StringHelper';

const getProductHTML = (handle) => async (dispatch, getState) => {
  try {
    const response = await api.getProductHTML(handle);
    dispatch(actions.updateProductField({htmlBody: response}));
  } catch (error) {
  } finally {
  }
};

const getTabList = (handle) => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    const response = await api.getProductTabList(handle);
    const arrayTabList = response.split('@@###@@');
    dispatch(
      actions.updateProductField({tabList: arrayTabList[0].split('@@##@@')}),
    );
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const init = () => async (dispatch, getState) => {
  const {productDetail} = getState().product;
  const {isAuthorized} = getState().auth;
  dispatch(appOperations.showLoading());
  try {
    dispatch(isProductFavorited(productDetail.handle));
    dispatch(getProductHTML(productDetail.handle));
    dispatch(getTabList(productDetail.handle));
    await dispatch(getProductDetail(productDetail.handle));
    await dispatch(getProductRelevant());
    isAuthorized && dispatch(isProductFavorited(productDetail.handle));
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const getProductDetail = () => async (dispatch, getState) => {
  try {
    const {productDetail} = getState().product;
    const response = await api.getProductDetail(productDetail.handle);
    const htmlResponse = await api.getProductHTML(productDetail.handle);
    const tabListResponse = await api.getProductTabList(productDetail.handle);
    const arrayTabList = tabListResponse.split('@@###@@');
    const newProductDetail = {
      inventory_quantity: response.variants[0].inventory_quantity,
      id: response.variants[0].id,
      handle: response.handle,
      images: response.images ? response.images : [],
      url: response.url,
      rateScore: 3,
      deliveryType: 'Giao hàng toàn quốc',
      htmlBody: htmlResponse,
      itemName: response.title,
      categorie: response.vendor,
      currentPrice: response.variants[0].price,
      beforePrice: response.variants[0].compare_at_price,

      discountPercent: discountPercent(
        response.variants[0].compare_at_price,
        response.variants[0].price,
      ),
      itemType: response.money_unit,
      variants: response.variants,
      tabList: arrayTabList,
      nextWeekPriceUp: '5',
      count: 1,
      isLove: false,
      isCart: false,
    };
    dispatch(actions.setProductDetail(newProductDetail));
  } catch (error) {
  } finally {
  }
};

const updateProduct = (data) => async (dispatch, getState) => {
  try {
    const {isAuthorized} = getState().auth;
    const response = await api.getProductDetail(data.handle);
    const htmlResponse = await api.getProductHTML(data.handle);
    const tabListResponse = await api.getProductTabList(data.handle);
    isAuthorized && dispatch(isProductFavorited(data.handle));
    const arrayTabList = tabListResponse.split('@@###@@');
    const newProductDetail = {
      inventory_quantity: response.variants[0].inventory_quantity,
      id: response.variants[0].id,
      handle: response.handle,
      images: response.images ? response.images : [],
      rateScore: 3,
      deliveryType: 'Giao hàng toàn quốc',
      url: response.url,
      htmlBody: htmlResponse,
      itemName: response.title,
      categorie: response.vendor,
      currentPrice: response.variants[0].price,
      beforePrice: response.variants[0].compare_at_price,
      discountPercent: discountPercent(
        response.variants[0].compare_at_price,
        response.variants[0].price,
      ),
      itemType: response.money_unit,
      variants: response.variants,
      tabList: arrayTabList,
      nextWeekPriceUp: '5',
      count: 1,
      isLove: false,
      isCart: false,
    };
    await dispatch(actions.setProductDetail(newProductDetail));
    await dispatch(getProductRelevant());
  } catch (error) {
  } finally {
  }
};

const updateCartApi = () => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());

  try {
    const {listShoppingCart} = getState().cart;

    let body = '';
    for (var i = 0; i < listShoppingCart.data.length; i++) {
      if (i < listShoppingCart.data.length - 1) {
        body += 'updates[]=' + listShoppingCart.data[i].count + '&';
      } else {
        body += 'updates[]=' + listShoppingCart.data[i].count;
      }
    }

    const response = await api.updateCart(body);
    let data = await response.text();
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const addToCart = () => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    const {productDetail} = getState().product;

    if (parseInt(productDetail.inventory_quantity) === 0) {
      alert('Sản phẩm hiện tại tạm hết');
      return;
    }
    postAddItemCart(productDetail.id, productDetail.count);
  } catch (error) {
    alert(error);
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

export async function postAddItemCart(id, quantity) {
  const fnc = 'postAddItemCart';
  if (id && quantity) {
    const url = 'https://choraucu.myharavan.com/cart/add.js';
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
    };

    let body = {
      id: id,
      quantity: quantity,
    };

    try {
      var result = await fetch(url, {
        method: 'POST',
        credentials: 'include',
        headers,
        body: JSON.stringify(body),
      });

      if (result) {
        if (result.status != 200) {
          return false;
        } else {
          let data = await result.text();

          const resultAddCart = JSON.parse(data);
          if (resultAddCart && resultAddCart.id) {
            return resultAddCart;
          } else {
            return false;
          }
        }
      } else {
        return false;
      }
    } catch (error) {
      return false;
    }
  }
}

const updateCountProductDetail = (type, id, text) => (dispatch, getState) => {
  const {listShoppingCart} = getState().cart;
  let sum = 0;
  const listCartNew = listShoppingCart.data.map((item) => {
    switch (type) {
      case 'increase':
        if (item.id === id) {
          item.count += 1;
        }
        break;
      case 'decrease':
        if (item.id === id) {
          if (item.count <= 1) {
            item.count = 1;
          } else {
            item.count -= 1;
          }
        }
        break;
      case 'inputValue':
        if (item.id === id) {
          item.count = text;
        }
        break;
      default:
        item.count = 1;
        break;
    }
    if (item.count > 0) {
      sum += item.price * item.count;
    }
    return {
      ...item,
    };
  });

  dispatch(cartOperations.updateListCart(listCartNew, sum));
};

const getTotalPrice = () => (dispatch, getState) => {
  const {total} = getState().product;
  return total.value;
};

const getProductRelevant = () => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    const {productDetail} = getState().product;
    const productHandle = productDetail.handle;
    const response = await api.getRelevantProduct(productHandle);
    if (response && response.products) {
      const listProduct = response.products.map((item, index) => {
        return {
          id: item.id,
          itemName: item.title,
          image: item.images[0].src,
          priceAlfter: item.variants[0].price,
          priceBefore: item.variants[0].compare_at_price,
          isSale: isSale(
            item.variants[0].compare_at_price,
            item.variants[0].price,
          ),

          salePercent: discountPercent(
            item.variants[0].compare_at_price,
            item.variants[0].price,
          ),
          handle: item.handle,
          variants: item.variants,
        };
      });

      dispatch(actions.setListRelevantProduct(listProduct));
    }
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const increaseAmount = () => (dispatch, getState) => {
  var {productDetail} = getState().product;
  const newProductDetail = {
    ...productDetail,
    count: productDetail.count + 1,
  };
  dispatch(actions.setProductDetail(newProductDetail));
};

const decreaseAmount = (type, id) => (dispatch, getState) => {
  var {productDetail} = getState().product;
  if (productDetail.count <= 1) {
    return;
  }
  const newProductDetail = {
    ...productDetail,
    count: productDetail.count - 1,
  };
  dispatch(actions.setProductDetail(newProductDetail));
};

const updateAmount = (number) => (dispatch, getState) => {
  var {productDetail} = getState().product;
  if (number.isEmpty() || parseInt(number) >= 99) {
    return;
  }
  const newProductDetail = {
    ...productDetail,
    count: number,
  };
  dispatch(actions.setProductDetail(newProductDetail));
};

const updateProductFieldLove = (form) => async (dispatch, getState) => {
  const {productDetail} = getState().product;
  const {isAuthorized} = getState().auth;
  if (!isAuthorized) {
    return;
  }
  const a = {isLove: !productDetail.isLove};
  await dispatch(actions.updateProductField(a));
};

const addToFavorite = (item) => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    const {productDetail} = getState().product;
    const {user, isAuthorized} = getState().auth;
    if (!isAuthorized) {
      return false;
    }
    const browserIDResponse = await api.getBrowserID();
    const browserID = browserIDResponse.wishlist_browser_id;
    const customerID = user.id;
    const details = {
      shop: 'choraucu.myharavan.com',
      customer_id: customerID,
      browser_id: browserID,
      product_id: productDetail.id,
      product_handle: JSON.stringify(productDetail), // Object String
    };
    const formBody = [];
    for (const property in details) {
      const encodeKey = encodeURIComponent(_.toString(property));
      const encodeValue = encodeURIComponent(_.toString(details[property]));
      formBody.push(encodeKey + '=' + encodeValue);
    }
    const body = formBody.join('&');
    const response = await api.addToFavorite(body);
    let data = await response.text();
    return true;
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const isProductFavorited = (handle) => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    const {user} = getState().auth;
    const browserIDResponse = await api.getBrowserID();
    const browserID = browserIDResponse.wishlist_browser_id;
    const customerID = user.id;
    const details = {
      shop: 'choraucu.myharavan.com',
      customer_id: customerID,
      browser_id: browserID,
    };

    const formBody = [];
    for (const property in details) {
      const encodeKey = encodeURIComponent(_.toString(property));
      const encodeValue = encodeURIComponent(_.toString(details[property]));
      formBody.push(encodeKey + '=' + encodeValue);
    }
    const body = formBody.join('&');
    const response = await apiFavorite.getFavoriteList(body);
    let favoriteResponse = await response.text();
    const favoriteParsed = JSON.parse(favoriteResponse);
    if (favoriteParsed && favoriteParsed.data) {
      favoriteParsed.data.forEach((item, index) => {
        const data = JSON.parse(unescape(item.product_handle));
        if (handle === data.handle) {
          dispatch(actions.updateProductField({isLove: true}));
        }
      });
    }
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

export default {
  getProductDetail,
  updateProductFieldLove,
  getProductRelevant,
  updateCountProductDetail,
  getTotalPrice,
  addToCart,
  updateProduct,
  updateAmount,
  decreaseAmount,
  increaseAmount,
  updateCartApi,
  addToFavorite,
  isProductFavorited,
  getProductHTML,
  getTabList,
  init,
};
