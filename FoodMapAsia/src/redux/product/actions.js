import {
  SET_PRODUCT,
  SET_PRODUCT_RELEVANT,
  UPDATE_PRODUCT_FIELD,
  CLEAR_PRODUCT_DETAIL,
  UPDATE_PRODUCT_FORM,
} from './constants';

const setProductDetail = (productDetail) => ({
  type: SET_PRODUCT,
  productDetail,
});

const setListRelevantProduct = (listProductRelevant) => ({
  type: SET_PRODUCT_RELEVANT,
  listProductRelevant,
});

const updateProductField = (form) => ({
  type: UPDATE_PRODUCT_FIELD,
  form,
});
const updateProductForm = (form) => ({
  type: UPDATE_PRODUCT_FORM,
  form,
});
export default {
  setProductDetail,
  updateProductField,
  setListRelevantProduct,
  updateProductForm,
};
