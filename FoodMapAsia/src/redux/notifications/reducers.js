import {SET_LIST_NOTIFICATION} from './constants';

const defaultState = {
  listNotification: [
    {
      id: 0,
      title: 'Đơn hàng đang được chuyển đến bạn',
      type: 'notifi_bill',
      isRead: false,
      date: '15/08/2020',
      content:
        'Đơn hàng #123456 đã được giao đến đơn vị vận chuyển và đang được chuyển đến bạn.',
    },
    {
      id: 1,
      title: 'Chúc mừng bạn đã đặt hàng thành công!',
      isRead: true,
      type: 'notifi_news',
      date: '15/08/2020',
      content:
        'Đơn hàng #123456 đã được giao đến đơn vị vận chuyển và đang được chuyển đến bạn.',
    },
    {
      id: 2,
      title: 'Đơn hàng đang được chuyển đến bạn',
      isRead: true,
      type: 'notifi_promotion',
      date: '15/08/2020',
      content:
        'Đơn hàng #123456 đã được giao đến đơn vị vận chuyển và đang được chuyển đến bạn.',
    },
    {
      id: 3,
      title: 'Đơn hàng đang được chuyển đến bạn',
      type: 'default',
      date: '15/08/2020',
      content:
        'Đơn hàng #123456 đã được giao đến đơn vị vận chuyển và đang được chuyển đến bạn.',
    },
    {
      title: 'Chúc mừng bạn đã đặt hàng thành công!',
      IsRead: true,
      type: 'notifi_news',
      date: '15/08/2020',
      content:
        'Đơn hàng #123456 đã được giao đến đơn vị vận chuyển và đang được chuyển đến bạn.',
    },
    {
      title: 'Chúc mừng bạn đã đặt hàng thành công!',
      IsRead: true,
      type: 'notifi_news',
      date: '15/08/2020',
      content:
        'Đơn hàng #123456 đã được giao đến đơn vị vận chuyển và đang được chuyển đến bạn.',
    },
    {
      title: 'Chúc mừng bạn đã đặt hàng thành công!',
      IsReaded: true,
      type: 'notifi_news',
      date: '15/08/2020',
      content:
        'Đơn hàng #123456 đã được giao đến đơn vị vận chuyển và đang được chuyển đến bạn.',
    },
    {
      title: 'Chúc mừng bạn đã đặt hàng thành công!',
      IsReaded: true,
      type: 'notifi_news',
      date: '15/08/2020',
      content:
        'Đơn hàng #123456 đã được giao đến đơn vị vận chuyển và đang được chuyển đến bạn.',
    },
  ],
  countUnread: 12,
};

const reduce = (state = defaultState, action) => {
  switch (action.type) {
    case SET_LIST_NOTIFICATION:
      return {
        ...state,
        listNotification: action.listNotification,
      };
    default:
      return state;
  }
};

export default reduce;
