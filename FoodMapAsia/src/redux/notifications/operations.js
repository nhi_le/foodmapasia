/* eslint-disable radix */
import actions from './actions';
import appOperations from '@redux/app/operations';
import api from './api';

const setListNotification = (listNotifications) => async (
  dispatch,
  getState,
) => {
  dispatch(actions.setListNotification(listNotifications));
};

const updateReadNotification = (id) => async (dispatch, getState) => {
  const {listNotification} = getState().notifications;

  let listNew = listNotification.map((item) => {
    if (item.id === id) {
      return {
        ...item,
        isRead: true,
      };
    } else {
      return {
        ...item,
      };
    }
  });
  dispatch(actions.setListNotification(listNew));
};

export default {
  setListNotification,
  updateReadNotification,
};
