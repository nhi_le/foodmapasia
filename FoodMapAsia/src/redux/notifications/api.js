import api from '../../services/remote/baseApi';
import apiEndPoints from '../../services/remote/apiEndPoints';

const withQuery = require('with-query').default;
const getSearch = (text) => {
  return api.get(apiEndPoints.GET_SEARCH_PRODUCT.replace('{0}', text));
};
export default {getSearch};
