import {SET_LIST_NOTIFICATION} from './constants';

const setListNotification = (listNotification) => ({
  type: SET_LIST_NOTIFICATION,
  listNotification,
});

export default {
  setListNotification,
};
