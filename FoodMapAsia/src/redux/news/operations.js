import actions from './actions';
import appOperations from '@redux/app/operations';
import api from './api';
import _ from 'lodash';
import {
  requiredValidation,
  notEmptyStringValidation,
} from '../helpers/validations';
export const loginPageValidation = ({content, recaptcha}) => {
  var validation = {
    isValid: true,
  };

  if (!requiredValidation(content) || !notEmptyStringValidation(content)) {
    validation.contentErrorMessage = 'Xin hãy nhập bình luận';
    validation.isValid = false;
  }

  if (!requiredValidation(recaptcha) || !notEmptyStringValidation(recaptcha)) {
    validation.contentErrorMessage = 'Xin vui lòng thử lại';
    validation.isValid = false;
  }
  return validation;
};
const setCategorySelected = (id) => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    const {tabSelected, listFoodChannel} = getState().news;
    if (tabSelected === id) {
      return;
    }

    let listNew = listFoodChannel.data.map((item) => {
      return {
        id: item.id,
        label: item.label,
        handle: item.handle,
        url: item.url,
        idBlog: item.idBlog,
        isSelected: item.id === id,
      };
    });
    dispatch(actions.updateListFoodChannel({data: listNew}));
    dispatch(actions.setFoodChanneSelected(id));
    const response = await api.getAPIArticleList(
      listFoodChannel.data[id].handle,
    );
    let tmp = 0;
    if (response) {
      let listArticleNew = response.articles.map((item) => {
        return {
          id: tmp++,
          title: item.title,
          handle: item.handle,
          image: item.image_src,
          author: item.author,
          published_at: item.published_at,
          content_preview: item.content_preview,
          comments_enabled: item.item,
          comments_count: item.comments_count,
          url: item.url,
          image_alt: item.image_alt,
        };
      });
      dispatch(actions.updateListArticle({data: listArticleNew}));
    }
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const getAPIBlogList = () => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    //update list foodchannel
    const response1 = await api.getAPIBlogList();
    let checked = false;
    let id = 0;
    if (response1 && response1.blogs) {
      let listNew = response1.blogs.map((item, index) => {
        if (index == 0) {
          checked = true;
        } else {
          checked = false;
        }
        return {
          id: id++,
          label: item.title,
          handle: item.handle,
          url: item.url,
          idBlog: item.id,
          isSelected: checked,
        };
      });
      dispatch(actions.updateListFoodChannel({data: listNew}));
    }
    const {listFoodChannel} = getState().news;
    const response2 = await api.getAPIArticleList(
      listFoodChannel.data[0].handle,
    );
    id = 0;
    if (response2) {
      let listArticleNew = response2.articles.map((item) => {
        return {
          id: id++,
          title: item.title,
          handle: item.handle,
          image: item.image_src,
          userOwn: 'Chợ rau củ team',
          published_at: item.published_at,
          content_preview: item.content_preview,
          comments_enabled: item.item,
          comments_count: item.comments_count,
          url: item.url,
          image_alt: item.image_alt,
        };
      });
      dispatch(actions.updateListArticle({data: listArticleNew}));
    }
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};
const getArticleDetail = (data) => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  const {listFoodChannel} = getState().news;
  let handle;
  listFoodChannel.data.forEach((item) => {
    item.isSelected ? (handle = item.handle) : item;
  });
  try {
    const response = await api.getDetailArticle(handle, data.handle);
    const htmlResponse = await api.getHtmlDetailArticle(handle, data.handle);

    const newArticle = {
      author: response.author,
      comment_post_url: response.comment_post_url,
      captcha: response.url,
      comments: response.comments,
      comments_count: response.comments_count,
      isCommentsEnabled: response.comments_enabled,
      handle: response.handle,
      blog_handle: response.blog_handle,
      image_alt: response.image_alt,
      image_src: response.image_src
        ? response.image_src
        : 'https://specials-images.forbesimg.com/imageserve/1152308114/960x0.jpg?fit=scale',
      moderated: response.moderated,
      published_at: response.published_at,
      title: response.title,
      source: 'Báo Tuổi Trẻ',
      html: htmlResponse,
      tag: response.tags,
    };

    dispatch(actions.updateDetailArticle(newArticle));
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};
const setCommentContent = (form) => (dispatch, getState) => {
  dispatch(actions.clearCommentValidateForm());
  dispatch(actions.updateCommentForm(form));
};

const postComment = () => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    const {formComment, detailArticle} = getState().news;
    const {comment_post_url} = detailArticle;
    const {content, recaptcha} = formComment;
    const {user} = getState().auth;

    const validation = loginPageValidation({content, recaptcha});
    if (!validation.isValid) {
      dispatch(actions.updateCommentForm({validation: validation}));
      return;
    }

    const details = {
      form_type: 'new_comment',
      utf8: true,
      'comment[author]': user.first_name + ' ' + user.last_name,
      'comment[email]': user.email,
      'comment[body]': formComment.content,
      'g-recaptcha-response': formComment.recaptcha,
    };
    const formBody = [];
    for (const property in details) {
      const encodeKey = encodeURIComponent(_.toString(property));
      const encodeValue = encodeURIComponent(_.toString(details[property]));
      formBody.push(encodeKey + '=' + encodeValue);
    }
    const body = formBody.join('&');
    const response = await api.postComment(body, comment_post_url);
    dispatch(actions.clearCommentForm());
    dispatch(refreshArticle());
  } catch (error) {
  } finally {
    dispatch(actions.clearCommentForm());
    dispatch(refreshArticle());
    dispatch(appOperations.hideLoading());
  }
};
const refreshArticle = () => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  const {detailArticle} = getState().news;
  try {
    const response = await api.getDetailArticle(
      detailArticle.blog_handle,
      detailArticle.handle,
    );
    const htmlResponse = await api.getHtmlDetailArticle(
      detailArticle.blog_handle,
      detailArticle.handle,
    );
    const newArticle = {
      author: response.author,
      comment_post_url: response.comment_post_url,
      captcha: response.url,
      comments: response.comments,
      comments_count: response.comments_count,
      isCommentsEnabled: response.comments_enabled,
      handle: response.handle,
      blog_handle: response.blog_handle,
      image_alt: response.image_alt,
      image_src: response.image_src
        ? response.image_src
        : 'https://specials-images.forbesimg.com/imageserve/1152308114/960x0.jpg?fit=scale',
      moderated: response.moderated,
      published_at: response.published_at,
      title: response.title,
      source: 'Báo Tuổi Trẻ',
      html: htmlResponse,
      tag: response.tags,
    };
    dispatch(actions.updateDetailArticle(newArticle));
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};
export default {
  setCategorySelected,
  getAPIBlogList,
  getArticleDetail,
  setCommentContent,
  postComment,
  refreshArticle,
};
