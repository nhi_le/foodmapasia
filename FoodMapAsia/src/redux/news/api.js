import api from '../../services/remote/baseApi';
import apiEndPoints from '../../services/remote/apiEndPoints';

const withQuery = require('with-query').default;

const getAPIBlogList = () => {
  return api.get(apiEndPoints.GET_API_BLOG_LIST);
};
const getAPIArticleList = (handle) => {
  return api.get(apiEndPoints.GET_API_ARTICLE_LIST.replace('{Handle}', handle));
};
const getDetailArticle = (blogHandle, articleHandle) => {
  var newString = apiEndPoints.GET_DETAIL_ARTICLE.replace(
    '{Handle1}',
    blogHandle,
  );
  return api.get(newString.replace('{Handle2}', articleHandle));
};
const getHtmlDetailArticle = (blogHandle, articleHandle) => {
  var newString = apiEndPoints.GET_API_ARTICLE_HTML.replace(
    '{Handle1}',
    blogHandle,
  );
  return api.get(newString.replace('{Handle2}', articleHandle));
};
const postComment = (body, url) => {
  return api.post(url, body);
};
export default {
  getAPIBlogList,
  getAPIArticleList,
  getDetailArticle,
  getHtmlDetailArticle,
  postComment,
};
