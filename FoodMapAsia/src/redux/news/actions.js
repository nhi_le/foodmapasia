import {
  SET_FOOD_CHANNEL_SELECTED,
  SET_LIST_FOOD_CHANNEL,
  SET_LIST_ARTICLE,
  SET_PRODUCT_ARTICLE,
  UPDATE_FORM_COMMENT,
  CLEAR_FORM_COMMENT,
  CLEAR_FORM_VALIDATE_COMMENT,
} from './constants';

const setFoodChanneSelected = (foodchannelId) => ({
  type: SET_FOOD_CHANNEL_SELECTED,
  foodchannelId,
});

const updateListFoodChannel = (listFoodChannel) => ({
  type: SET_LIST_FOOD_CHANNEL,
  listFoodChannel,
});

const updateListArticle = (listArticle) => ({
  type: SET_LIST_ARTICLE,
  listArticle,
});

const updateDetailArticle = (detailArticle) => ({
  type: SET_PRODUCT_ARTICLE,
  detailArticle,
});
const updateCommentForm = (form) => ({
  type: UPDATE_FORM_COMMENT,
  form,
});
const clearCommentForm = () => ({
  type: CLEAR_FORM_COMMENT,
});
const clearCommentValidateForm = () => ({
  type: CLEAR_FORM_VALIDATE_COMMENT,
});
export default {
  setFoodChanneSelected,
  updateListFoodChannel,
  updateListArticle,
  updateDetailArticle,
  updateCommentForm,
  clearCommentForm,
  clearCommentValidateForm,
};
