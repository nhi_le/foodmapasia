import {
  SET_FOOD_CHANNEL_SELECTED,
  SET_LIST_FOOD_CHANNEL,
  SET_LIST_ARTICLE,
  SET_DETAIL_ARTICLE,
  SET_PRODUCT_ARTICLE,
  UPDATE_FORM_COMMENT,
  CLEAR_FORM_COMMENT,
  CLEAR_FORM_VALIDATE_COMMENT,
} from './constants';

const defaultState = {
  tabSelected: 0,
  detailArticle: {
    author: '',
    comment_post_url: '',
    captcha: '',
    comments: [],
    comments_count: '',
    isCommentsEnabled: '',
    handle: '',
    image_alt: '',
    image_src: '',
    moderated: '',
    published_at: '',
    description: '',
    highlight: '',
    title: '',
    source: '',
    blog_handle: '',
  },
  formComment: {
    content: '',
    recaptcha: '',
    validation: {
      isValid: false,
      contentErrorMessage: '',
    },
  },

  listFoodChannel: {
    data: [],
  },
  listArticle: {
    data: [
      {
        id: 0,
        title: 'this is the my title',
        image:
          'https://calories-info.com/site/assets/files/1182/persymona-hurma-hebanek.650x0.jpg',
        userOwn: 'Chợ rau củ team',
        published_at: '22/07/2020',
        content_preview:
          'Preview Content Preview Content Preview Content Preview Content Preview Content Preview',
      },
    ],
  },
};

const reduce = (state = defaultState, action) => {
  switch (action.type) {
    case SET_FOOD_CHANNEL_SELECTED:
      return {
        ...state,
        tabSelected: action.foodchannelId,
      };
    case SET_LIST_FOOD_CHANNEL:
      return {
        ...state,
        listFoodChannel: {
          ...state.listFoodChannel,
          ...action.listFoodChannel,
        },
      };
    case SET_LIST_ARTICLE:
      return {
        ...state,
        listArticle: {
          ...state.listArticle,
          ...action.listArticle,
        },
      };
    case SET_PRODUCT_ARTICLE:
      return {
        ...state,
        detailArticle: action.detailArticle,
      };
    case UPDATE_FORM_COMMENT:
      return {
        ...state,
        formComment: {
          ...state.formComment,
          ...action.form,
        },
      };
    case CLEAR_FORM_COMMENT:
      return {
        ...state,
        formComment: defaultState.formComment,
      };
    case CLEAR_FORM_VALIDATE_COMMENT:
      return {
        ...state,
        formComment: {
          ...state.formComment,
          validation: defaultState.formComment.validation,
        },
      };
    default:
      return state;
  }
};

export default reduce;
