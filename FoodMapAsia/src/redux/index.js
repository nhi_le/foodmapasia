import {combineReducers} from 'redux';
import app from './app/index';
import auth from './auth/index';
import profile from './profile/index';
import news from './news/index';
import categories from './categories/index';
import cart from './cart/index';
import home from './home/index';
import favorite from './favorite/index';
import product from './product/index';
import notifications from './notifications/index';
import {reducer as network} from 'react-native-offline';

const reducers = combineReducers({
  app,
  auth,
  network,
  profile,
  news,
  categories,
  product,
  cart,
  favorite,
  home,
  notifications,
});

export default reducers;
