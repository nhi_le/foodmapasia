export const isUnauthorized = (error) => {
  return error.status === 401;
};
