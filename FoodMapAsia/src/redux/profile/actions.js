import {
  SET_CATEGORY_PROFILE_SELECTED,
  SET_LIST_CATEGORY_PROFILE,
  SET_LIST_ORDER_HISTORY,
  SET_LIST_ADDRESS,
  SET_INFO_ACCOUNT,
  SET_LIST_ORDER_TODAY,
  SET_IS_ADDRESS_INPUT,
  SET_INPUT_FORM,
  SET_SELECT_PLACE_HOLDER,
  SET_FORM_INPUT_ADDRESS,
  UPDATE_INPUT_VALIDATE_FORM,
  SET_TAB_ORDER_SELECTED,
  UPDATE_DETAIL_ORDER,
  SET_DETAIL_ORDER,
  SET_ORDER_DETAIL,
} from './constants';

const setCategorySelected = (categoryId) => ({
  type: SET_CATEGORY_PROFILE_SELECTED,
  categoryId,
});

const setListCategory = (listCategory) => ({
  type: SET_LIST_CATEGORY_PROFILE,
  listCategory,
});

const setListOrderHistory = (listOrderHistory) => ({
  type: SET_LIST_ORDER_HISTORY,
  listOrderHistory,
});

const setListAddress = (listAddress) => ({
  type: SET_LIST_ADDRESS,
  listAddress,
});

const setInfoAccount = (infoAccount) => ({
  type: SET_INFO_ACCOUNT,
  infoAccount,
});

const setListOrderToday = (listOrderToday) => ({
  type: SET_LIST_ORDER_TODAY,
  listOrderToday,
});

const setIsAddressInput = (isAddressInput) => ({
  type: SET_IS_ADDRESS_INPUT,
  isAddressInput,
});

const setFormInput = (form) => ({
  type: SET_INPUT_FORM,
  form,
});

const setCityPlaceHolder = (placeholder) => ({
  type: SET_SELECT_PLACE_HOLDER,
  placeholder,
});
const setFormAddressInput = (InputAddress) => ({
  type: SET_FORM_INPUT_ADDRESS,
  InputAddress,
});
const updateValidateForm = (formValidate) => ({
  type: UPDATE_INPUT_VALIDATE_FORM,
  formValidate,
});

const setTabOrderSelected = (tabOrderSelected) => ({
  type: SET_TAB_ORDER_SELECTED,
  tabOrderSelected,
});

const updateOrderForm = (detailOrder) => ({
  type: UPDATE_DETAIL_ORDER,
  detailOrder,
});
const setOrderForm = (detailOrder) => ({
  type: SET_DETAIL_ORDER,
  detailOrder,
});
const setOrderDetail = (cartDetail) => ({
  type: SET_ORDER_DETAIL,
  cartDetail,
});

export default {
  updateValidateForm,
  setCategorySelected,
  setListCategory,
  setListOrderHistory,
  setListAddress,
  setInfoAccount,
  setListOrderToday,
  setIsAddressInput,
  setFormInput,
  setCityPlaceHolder,
  setFormAddressInput,
  setTabOrderSelected,
  setOrderForm,
  updateOrderForm,
  setOrderDetail,
};
