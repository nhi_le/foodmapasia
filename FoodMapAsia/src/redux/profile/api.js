import api from '../../services/remote/baseApi';
import apiEndPoints from '../../services/remote/apiEndPoints';

const getListOrderHistory = () => {
  return fetch(apiEndPoints.GET_ORDERS_HISTORY, {
    credentials: 'same-origin',
  });
};

const getListAddress = () => {
  return fetch(apiEndPoints.GET_LIST_ADDRESS, {
    credentials: 'same-origin',
  });
};

const getInfoAccount = () => {
  return fetch(apiEndPoints.GET_INFO_ACCOUNT, {
    method: 'GET',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-requested-With': 'XMLHttpRequest',
    },
  });
};

const getOrderList = () => {
  return fetch(apiEndPoints.GET_ORDER_LIST, {
    method: 'GET',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-requested-With': 'XMLHttpRequest',
    },
  });
};
const deleteAddress = (body, id) => {
  return fetch(apiEndPoints.DELETE_ADDRESS.replace('{AddressID}', id), {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-requested-With': 'XMLHttpRequest',
    },
    body,
  });
};
const addNewAddress = (body) => {
  return fetch(apiEndPoints.ADD_NEW_ADDRESS, {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-requested-With': 'XMLHttpRequest',
    },
    body,
  });
};
const updateAddress = (body, id) => {
  return fetch(apiEndPoints.UPDATE_ADDRESS.replace('{id}', id), {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-requested-With': 'XMLHttpRequest',
    },
    body,
  });
};
const getDetailOrder = (token) => {
  return api.get(apiEndPoints.GET_DETAIL_ORDER.replace('{token}', token));
};
export default {
  getListAddress,
  getListOrderHistory,
  getInfoAccount,
  getOrderList,
  deleteAddress,
  addNewAddress,
  updateAddress,
  getDetailOrder,
};
