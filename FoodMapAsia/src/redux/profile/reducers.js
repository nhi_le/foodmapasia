import {
  SET_CATEGORY_PROFILE_SELECTED,
  SET_LIST_CATEGORY_PROFILE,
  SET_LIST_ADDRESS,
  SET_LIST_ORDER_HISTORY,
  SET_INFO_ACCOUNT,
  SET_LIST_ORDER_TODAY,
  SET_IS_ADDRESS_INPUT,
  SET_INPUT_FORM,
  SET_SELECT_PLACE_HOLDER,
  SET_FORM_INPUT_ADDRESS,
  UPDATE_INPUT_VALIDATE_FORM,
  SET_TAB_ORDER_SELECTED,
  UPDATE_DETAIL_ORDER,
  SET_DETAIL_ORDER,
  SET_ORDER_DETAIL,
} from './constants';
import {
  TITLE_CART_DETAIL,
  CONTENT_ODER,
  CONTENT_ODER_TIME,
  CONTENT_CONTACT,
  CONTENT_TOTAL_MONEY,
  CONTENT_PAYMENT_STATUTS,
  CONTENT_SHIP_STATUS,
  CONTENT_PRODUCT_CODE,
  CONTENT_UNIT_PRICE,
  CONTENT_AMOUNT,
  CONTENT_DELIVERY,
  CONTENT_ADDRESS_RECIVIE,
  CONTENT_ADDRESS_SEND,
} from '@resources/string/strings';

const defaultState = {
  isAddressInput: false,
  detailOrder: {
    code: '',
    dateOrder: '',
    delivery: '',
    status: '',
    token: '',
    total: '',
  },
  cartDetail: {
    oderInfo: {
      oderName: {
        title: CONTENT_ODER,
        value: '',
      },
      info: {
        oderTime: {
          title: CONTENT_ODER_TIME,
          value: {
            date: '',
            time: '',
          },
        },
        contact: CONTENT_CONTACT,
        totalMoney: {
          title: CONTENT_TOTAL_MONEY,
          value: '',
        },
        paymentStatus: {
          title: CONTENT_PAYMENT_STATUTS,
          value: '',
        },
        shipStatus: {
          title: CONTENT_SHIP_STATUS,
          value: '',
        },
      },
    },
    data: [
      {
        oderItemName: '',
        oderCombo: '',
        productCode: {
          title: CONTENT_PRODUCT_CODE,
          value: '',
        },
        unitPrice: {
          title: CONTENT_UNIT_PRICE,
          value: '',
        },
        total: {
          title: CONTENT_AMOUNT,
          value: '',
        },
        delivery: {
          title: CONTENT_DELIVERY,
          value: '',
        },
        totalMoney: {
          title: CONTENT_TOTAL_MONEY,
          value: '',
        },
      },
    ],
    recivieAddress: {
      title: CONTENT_ADDRESS_RECIVIE,
      paymentStatus: '',
      storeName: '',
      address: {
        street: '',
        city: '',
      },
      phoneNumber: '',
    },
    shippingAddress: {
      title: CONTENT_ADDRESS_SEND,
      paymentStatus: '',
      storeName: '',
      address: {
        street: '',
        city: '',
      },
      phoneNumber: '',
    },
  },
  tabSelected: 0,
  formValidate: {
    address1: '',
    firstName: '',
    lastName: '',
    isValid: false,
    phoneNumber: '',
  },
  formInputAddress: {
    isUpdate: false,
    id: 0,
    isDefault: 0,
    firstName: '',
    lastName: '',
    address1: '',
    address2: '',
    phoneNumber: '',
    company: '',
    country: '',
  },
  listCity: {
    isShow: true,
    options: [
      'Hồ Chí Minh',
      'Hà Nội',
      'Hải Phòng',
      'Hưng Yên',
      'Cao Bằng',
      'Thái Nguyên',
    ],
    placeholder: 'Please Select',
  },
  listCategory: [
    {
      id: 0,
      label: 'Thông Tin Tài Khoản',
      isSelected: true,
    },
    {
      id: 1,
      label: 'Danh Sách Địa Chỉ',
      isSelected: false,
    },
    {
      id: 2,
      label: 'Đơn Hàng',
      isSelected: false,
    },
  ],
  listOrderHistory: [],
  listAddress: [],
  infoAccount: {
    nameAccount: 'Bách Hoá 711 - CN Q1',
    address: '10 đường Mã Lộ, Tân Định, Quận 1, Hồ Chí Minh, Việt Nam',
    numberPhone: '',
  },
  listOrderToday: [],
  tabOrderSelected: 0,
};

const reduce = (state = defaultState, action) => {
  switch (action.type) {
    case SET_CATEGORY_PROFILE_SELECTED:
      return {
        ...state,
        tabSelected: action.categoryId,
      };
    case SET_LIST_CATEGORY_PROFILE:
      return {
        ...state,
        listCategory: action.listCategory,
      };
    case SET_LIST_ADDRESS:
      return {
        ...state,
        listAddress: action.listAddress,
      };
    case SET_LIST_ORDER_HISTORY:
      return {
        ...state,
        listOrderHistory: action.listOrderHistory,
      };
    case SET_INFO_ACCOUNT:
      return {
        ...state,
        infoAccount: action.infoAccount,
      };
    case SET_LIST_ORDER_TODAY:
      return {
        ...state,
        listOrderToday: action.listOrderToday,
      };
    case SET_IS_ADDRESS_INPUT:
      return {
        ...state,
        isAddressInput: action.isAddressInput,
      };
    case SET_INPUT_FORM:
      return {
        ...state,
        formInputAddress: {
          ...state.formInputAddress,
          ...action.form,
        },
      };
    case SET_SELECT_PLACE_HOLDER:
      return {
        ...state,
        listCity: {
          ...state.listCity,
          placeholder: action.placeholder,
        },
      };

    case SET_FORM_INPUT_ADDRESS:
      return {
        ...state,
        formInputAddress: action.InputAddress,
      };
    case UPDATE_INPUT_VALIDATE_FORM:
      return {
        ...state,
        formValidate: action.formValidate.validation,
      };
    case SET_TAB_ORDER_SELECTED:
      return {
        ...state,
        tabOrderSelected: action.tabOrderSelected,
      };
    case SET_DETAIL_ORDER:
      return {
        ...state,
        detailOrder: action.detailOrder,
      };
    case UPDATE_DETAIL_ORDER:
      return {
        ...state,
        detailOrder: {
          ...state.detailOrder,
          ...action.detailOrder,
        },
      };
    case SET_ORDER_DETAIL:
      return {...state, cartDetail: action.cartDetail};
    default:
      return state;
  }
};

export default reduce;
