import actions from './actions';
import appOperations from '@redux/app/operations';
import api from './api';
import _ from 'lodash';
import {
  requiredValidation,
  notEmptyStringValidation,
} from '../helpers/validations';
import {
  FIELD_LAST_NAME_REQUIRED,
  FIELD_PHONE_NUMBER_REQUIRED,
  FIELD_ADDRES_REQUIRED,
  CONTENT_TOTAL_MONEY,
  CONTENT_PRODUCT_CODE,
  CONTENT_UNIT_PRICE,
  CONTENT_AMOUNT,
  CONTENT_ODER,
  CONTENT_ODER_TIME,
  CONTENT_CONTACT,
  CONTENT_PAYMENT_STATUTS,
  CONTENT_SHIP_STATUS,
  CONTENT_ADDRESS_RECIVIE,
  CONTENT_ADDRESS_SEND,
} from '@resources/string/strings';

export const addressValidation = ({
  firstName,
  lastName,
  address1,
  phoneNumber,
}) => {
  var validation = {
    isValid: true,
  };

  if (!requiredValidation(firstName) || !notEmptyStringValidation(firstName)) {
    validation.lastName = 'FIELD FULL NAME IS REQUIRED';
    validation.isValid = false;
  }
  if (!requiredValidation(lastName) || !notEmptyStringValidation(lastName)) {
    validation.lastName = FIELD_LAST_NAME_REQUIRED;
    validation.isValid = false;
  }

  if (!requiredValidation(address1) || !notEmptyStringValidation(address1)) {
    validation.address1 = FIELD_ADDRES_REQUIRED;
    validation.isValid = false;
  }

  if (
    !requiredValidation(phoneNumber) ||
    !notEmptyStringValidation(phoneNumber)
  ) {
    validation.phoneNumber = FIELD_PHONE_NUMBER_REQUIRED;
    validation.isValid = false;
  }
  return validation;
};
const setCategorySelected = (id) => (dispatch, getState) => {
  const {tabSelected, listCategory} = getState().profile;

  if (tabSelected === id) {
    return;
  }

  let listNew = listCategory.map((item) => {
    return {
      id: item.id,
      label: item.label,
      isSelected: item.id === id,
    };
  });

  dispatch(actions.setListCategory(listNew));
  dispatch(actions.setCategorySelected(id));
};

const getInfoAccount = () => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    const response = await api.getInfoAccount();
    let info = await response.text();

    let infoData = JSON.parse(info);

    const infoAccount = {
      nameAccount: 'Bách Hoá 711 - CN Q1',
      address: infoData.default_address_address1,
      numberPhone: infoData.default_address_phone,
    };
    dispatch(actions.setInfoAccount(infoAccount));
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const getListOrderHistory = () => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    dispatch(actions.setListOrderHistory([]));
    const response = await api.getListOrderHistory();
    let dataOrder = await response.text();
    let listOder = JSON.parse(dataOrder);
    if (listOder && listOder.order_history) {
      const listOrderHistory = listOder.order_history.map((item, index) => {
        return {
          code: item.name,
          token: item.token,
          dateOrder: item.created_at.slice(0, 10),
          total: item.total_price,
          status: item.financial_status,
          delivery: item.fulfillment_status,
        };
      });

      dispatch(actions.setListOrderHistory(listOrderHistory));
    }

    if (listOder && listOder.today_shipping) {
      const listOrderToday = listOder.today_shipping.map((item, index) => {
        return {
          code: item.name,
          dateOrder: item.created_at.slice(0, 10),
          total: item.total_price,
          status: item.financial_status,
          delivery: item.fulfillment_status,
        };
      });

      dispatch(actions.setListOrderToday(listOrderToday));
    }
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const getListAddress = () => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    dispatch(actions.setListAddress([]));
    const response = await api.getListAddress();
    let dataAddress = await response.text();

    let listAdd = await JSON.parse(dataAddress);

    if (listAdd && listAdd.address_list) {
      const listAddress = listAdd.address_list.map((item) => {
        return {
          id: item.id,
          code: item.street,
          isDefault: item.is_default_address,
          receiver:
            item.first_name +
            (item.first_name === item.last_name ? '' : ' ' + item.last_name),
          firstName: item.first_name,
          lastName: item.last_name,
          address:
            item.address1 +
            (item.province && item.province.trim() !== ''
              ? ', ' + item.province
              : '') +
            (item.country && item.country.trim() !== ''
              ? ', ' + item.country
              : ''),
          numberPhone: item.phone,
        };
      });

      dispatch(actions.setListAddress(listAddress));
    }
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const removeAddress = (id) => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  const {listAddress} = getState().profile;
  var newList = [...listAddress];
  _.remove(newList, {id: id});
  try {
    const details = {
      utf8: true,
      _method: 'delete',
    };
    const formBody = [];
    for (const property in details) {
      const encodeKey = encodeURIComponent(_.toString(property));
      const encodeValue = encodeURIComponent(_.toString(details[property]));
      formBody.push(encodeKey + '=' + encodeValue);
    }
    const body = formBody.join('&');
    await api.deleteAddress(body, id);
  } catch (error) {
  } finally {
    dispatch(actions.setListAddress(newList));
    dispatch(appOperations.hideLoading());
  }
};

const addNewAddress = () => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  const {formInputAddress} = getState().profile;
  try {
    const {lastName, address1, phoneNumber, firstName} = formInputAddress;
    const validation = addressValidation({
      firstName,
      lastName,
      address1,
      phoneNumber,
    });
    if (!validation.isValid) {
      dispatch(actions.updateValidateForm({validation}));
      return false;
    }

    const details = {
      form_type: 'customer_address',
      utf8: true,
      'address[first_name]': formInputAddress.firstName,
      'address[last_name]': formInputAddress.lastName,
      'address[address1]': formInputAddress.address1,
      'address[phone]': formInputAddress.phoneNumber,
      'address[default]': formInputAddress.isDefault,
    };
    const formBody = [];
    for (const property in details) {
      const encodeKey = encodeURIComponent(_.toString(property));
      const encodeValue = encodeURIComponent(_.toString(details[property]));
      formBody.push(encodeKey + '=' + encodeValue);
    }
    const body = formBody.join('&');
    await api.addNewAddress(body);
    dispatch(setAddressInput());
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const setAddressInput = () => (dispatch, getState) => {
  const {isAddressInput} = getState().profile;
  const newIsInput = !isAddressInput;
  const newAddress = {
    isUpdate: false,
    address1: '',
    address2: '',
    company: '',
    country: '',
    firstName: '',
    isDefault: 1,
    lastName: '',
    phoneNumber: '',
  };
  const validation = {
    validation: {address1: '', firstName: '', isValid: false, phoneNumber: ''},
  };
  dispatch(actions.updateValidateForm(validation));
  dispatch(actions.setFormAddressInput(newAddress));
  dispatch(actions.setIsAddressInput(newIsInput));
};

const setFormInput = (form) => (dispatch, getState) => {
  return dispatch(actions.setFormInput(form));
};

const setSelectCity = (index) => (dispatch, getState) => {
  const {listCity} = getState().profile;
  if (index === listCity.options.length) {
    return;
  }
  const newPlaceHolder = listCity.options[index];
  return dispatch(actions.setCityPlaceHolder(newPlaceHolder));
};
const setAddressInputUpdate = (id) => async (dispatch, getState) => {
  const {isAddressInput, listAddress} = getState().profile;
  const newIsInput = !isAddressInput;
  let newAddress;
  listAddress.forEach((item) => {
    if (item.id === id) {
      newAddress = {
        id: item.id,
        isUpdate: true,
        address1: item.address,
        address2: '',
        company: '',
        country: '',
        firstName: item.firstName,
        isDefault: item.isDefault === 'true' ? 1 : 0,
        lastName: item.lastName,
        phoneNumber: item.numberPhone,
      };
    }
  });
  await dispatch(actions.setFormAddressInput(newAddress));
  dispatch(actions.setIsAddressInput(newIsInput));
};
const updateAddress = () => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  const {formInputAddress} = getState().profile;
  try {
    const {firstName, lastName, address1, phoneNumber} = formInputAddress;
    const validation = addressValidation({
      firstName,
      lastName,
      address1,
      phoneNumber,
    });
    if (!validation.isValid) {
      dispatch(actions.updateValidateForm({validation}));
      return false;
    }

    const details = {
      form_type: 'customer_address',
      utf8: true,
      'address[first_name]': formInputAddress.firstName,
      'address[last_name]': formInputAddress.lastName,
      'address[address1]': formInputAddress.address1,
      'address[phone]': formInputAddress.phoneNumber,
      'address[default]': formInputAddress.isDefault,
    };
    const formBody = [];
    for (const property in details) {
      const encodeKey = encodeURIComponent(_.toString(property));
      const encodeValue = encodeURIComponent(_.toString(details[property]));
      formBody.push(encodeKey + '=' + encodeValue);
    }
    const body = formBody.join('&');
    const response = await api.updateAddress(body, formInputAddress.id);
    let data = await response.text();
    dispatch(setUpdateAddressInput());
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};
const setUpdateAddressInput = () => (dispatch, getState) => {
  const {isAddressInput} = getState().profile;
  const newIsInput = !isAddressInput;
  dispatch(actions.setIsAddressInput(newIsInput));
};

const setTabOrderSelected = (tab) => (dispatch, getState) => {
  const {tabOrderSelected} = getState().profile;

  if (tab === tabOrderSelected) {
    return;
  }

  dispatch(actions.setTabOrderSelected(tab));
};
const setOrderForm = (data) => (dispatch, getState) => {
  dispatch(actions.setOrderForm(data));
};
const getDetailOrder = () => async (dispatch, getState) => {
  const {detailOrder} = getState().profile;
  const {token} = detailOrder;
  const response = await api.getDetailOrder(token);
  if (response && response.line_items) {
    const cartDetailList = response.line_items.map((item, index) => {
      return {
        oderItemName: item.product_title,
        oderCombo: item.sku,
        productCode: {
          title: CONTENT_PRODUCT_CODE,
          value: item.sku,
        },
        unitPrice: {
          title: CONTENT_UNIT_PRICE,
          value: item.price,
        },
        total: {
          title: CONTENT_AMOUNT,
          value: item.amount_price,
        },
        handle: item.product_handle,
        totalMoney: {
          title: CONTENT_TOTAL_MONEY,
          value: response.total_price,
        },
        delivery: {
          title: response.shipping_method[index]
            ? response.shipping_method[index].title
            : '',
          value: response.shipping_method[index]
            ? response.shipping_method[index].price
            : '',
        },
      };
    });
    const cartDetail = {
      oderInfo: {
        oderName: {
          title: CONTENT_ODER,
          value: response.order_name,
        },
        info: {
          oderTime: {
            title: CONTENT_ODER_TIME,
            value: {
              date: response.order_created_at,
              time: '',
            },
          },
          contact: CONTENT_CONTACT,
          totalMoney: {
            title: CONTENT_TOTAL_MONEY,
            value: response.total_price,
          },
          paymentStatus: {
            title: CONTENT_PAYMENT_STATUTS,
            value: response.payment_address.fulfillment_status,
          },
          shipStatus: {
            title: CONTENT_SHIP_STATUS,
            value: response.shipping_address.fulfillment_status,
          },
        },
      },
      data: cartDetailList,
      recivieAddress: {
        title: CONTENT_ADDRESS_RECIVIE,
        paymentStatus: response.payment_address.fulfillment_status,
        storeName: response.payment_address.billing_address_company,
        address: {
          street: response.payment_address.billing_address_address1,
          city:
            response.payment_address.billing_address_province +
            ' ' +
            response.payment_address.billing_address_country,
        },
        phoneNumber: response.payment_address.billing_address_phone,
      },
      shippingAddress: {
        title: CONTENT_ADDRESS_SEND,
        paymentStatus: response.shipping_address.fulfillment_status,
        storeName: response.shipping_address.shipping_address_company,
        address: {
          street: response.shipping_address.shipping_address_address1,
          city:
            response.shipping_address.shipping_address_province +
            ' ' +
            response.shipping_address.shipping_address_country,
        },
        phoneNumber: response.shipping_address.shipping_address_phone,
      },
    };
    dispatch(actions.setOrderDetail(cartDetail));
  }
};
export default {
  addNewAddress,
  setCategorySelected,
  getListOrderHistory,
  getListAddress,
  getInfoAccount,
  removeAddress,
  setAddressInput,
  setFormInput,
  setSelectCity,
  setAddressInputUpdate,
  updateAddress,
  setUpdateAddressInput,
  setTabOrderSelected,
  setOrderForm,
  getDetailOrder,
};
