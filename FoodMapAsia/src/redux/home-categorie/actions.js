import {SET_CATEGORY_SELECTED, SET_LIST_CATEGORY} from './constants';

const setCategorySelected = (categoryId) => ({
  type: SET_CATEGORY_SELECTED,
  categoryId,
});
const setListCategory = (listCategory) => ({
  type: SET_LIST_CATEGORY,
  listCategory,
});

export default {
  setCategorySelected,
  setListCategory,
};
