import actions from './actions';
import appOperations from '@redux/app/operations';

const setCategorySelected = (id) => (dispatch, getState) => {
  const {tabSelected, listCategory} = getState().categorie;

  if (tabSelected === id) {
    return;
  }

  let listNew = listCategory.map((item) => {
    return {
      id: item.id,
      label: item.label,
      isSelected: item.id === id,
    };
  });

  dispatch(actions.setListCategory(listNew));
  dispatch(actions.setCategorySelected(id));
};

export default {
  setCategorySelected,
};
