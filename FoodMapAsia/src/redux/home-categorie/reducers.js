import {SET_CATEGORY_SELECTED, SET_LIST_CATEGORY} from './constants';
import {
  ICON_HOME_BADGE,
  ICON_HOME_LEAF,
  ICON_HOME_FRUIT,
  ICON_HOME_SEED,
  ICON_HOME_SPICE,
} from '@resources/images';
const defaultState = {
  tabSelected: 0,
  listCategory: {
    data: [
      {
        title: 'Trà Cà Phê',
        icon: ICON_HOME_BADGE,
      },
      {
        title: 'Đặc Sản',
        icon: ICON_HOME_LEAF,
      },
      {
        title: 'Rau Củ Quả',
        icon: ICON_HOME_FRUIT,
      },
      {
        title: 'Sức Khoẻ',
        icon: ICON_HOME_SEED,
      },
      {
        title: 'Đi Chợ',
        icon: ICON_HOME_SPICE,
      },
    ],
  },
};

const reduce = (state = defaultState, action) => {
  switch (action.type) {
    case SET_CATEGORY_SELECTED:
      return {
        ...state,
        tabSelected: action.categoryId,
      };
    case SET_LIST_CATEGORY:
      return {
        ...state,
        listCategory: action.listCategory,
      };
    default:
      return state;
  }
};

export default reduce;
