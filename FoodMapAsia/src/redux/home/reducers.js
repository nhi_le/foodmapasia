import {
  SET_LIST_PRODUCT_HOME,
  UPDATE_SHOW_MODAL,
  UPDATE_SELECT_MODAL,
  UPDATE_HANDLE,
  SET_LIST_CATEGORY_HOME,
  SET_SLIDER_IMAGES,
  UPDATE_PAGE,
  SET_LIST_PRODUCT_SEARCH,
  CLEAR_LIST_PRODUCT,
} from './constants';

const defaultState = {
  categoryHandle: '',
  listCategories: {
    data: [],
  },
  swiperImages: {
    data: [],
  },
  listLocation: {
    data: [
      {
        id: 0,
        title: 'Hà Nội',
        isSelected: true,
        description: '',
      },
      {id: 1, title: 'Đà Nẵng', isSelected: false, description: ''},
      {id: 2, title: 'Hồ Chí Minh', isSelected: false, description: ''},
    ],
  },
  isVisible: false,
  listHomeItem: {
    data: [],
    page: 0,
    totalPage: 0,
    query: '',
  },
  stepFavorite: 0,
};

const reduce = (state = defaultState, action) => {
  switch (action.type) {
    case SET_LIST_PRODUCT_HOME:
      return {
        ...state,
        listHomeItem: {
          ...state.listHomeItem,
          data: action.listProduct.data,
          totalPage: action.listProduct.totalPage,
          page: action.listProduct.totalPage,
          query: action.listProduct.query,
        },
      };
    case SET_LIST_PRODUCT_SEARCH:
      return {
        ...state,
        listHomeItem: {
          ...state.listHomeItem,
          data: action.listProduct.data,
          totalPage: action.listProduct.totalPage,
          page: action.listProduct.totalPage,
        },
      };
    case UPDATE_PAGE:
      return {
        ...state,
        listHomeItem: {
          ...state.listHomeItem,
          page: action.page,
        },
      };
    case UPDATE_SHOW_MODAL:
      return {
        ...state,
        isVisible: action.isVisible,
      };
    case UPDATE_SELECT_MODAL:
      return {
        ...state,
        listLocation: {
          data: action.listWithSelected,
        },
      };
    case SET_LIST_CATEGORY_HOME:
      return {
        ...state,
        listCategories: {
          data: action.listCategory,
        },
      };
    case UPDATE_HANDLE:
      return {
        ...state,
        categoryHandle: action.handle,
      };
    case SET_SLIDER_IMAGES:
      return {
        ...state,
        swiperImages: {
          data: action.data,
        },
      };
    case CLEAR_LIST_PRODUCT:
      return {
        ...state,
        listHomeItem: defaultState.listHomeItem,
      };
    default:
      return state;
  }
};

export default reduce;
