import actions from './actions';
import actionsProduct from '../../redux/product/actions';
import appOperations from '@redux/app/operations';
import api from './api';
import {discountPercent, isSale} from '@common/components/StringHelper';
import {ICON_VEGETABLE, ICON_HOME_VEGETABLE} from '../../resources/images';
import {
  homeCategoryEnum,
  childCategoryEnum,
} from '@resources/enum/home-category';

const searchProduct = (text) => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    const {listHomeItem} = getState().home;
    const response = await api.getSearch(text, listHomeItem.page);
    if (response && response.result) {
      const listProduct = response.result.map((item, index) => {
        return {
          id: item.id,
          itemName: item.title,
          image: item.images ? item.images : [],
          priceAlfter: item.variants[0].price,
          priceBefore: item.variants[0].compare_at_price,
          isSale: isSale(
            item.variants[0].compare_at_price,
            item.variants[0].price,
          ),
          salePercent: discountPercent(
            item.variants[0].compare_at_price,
            item.variants[0].price,
          ),
          handle: item.handle,
          variants: item.variants,
        };
      });
      dispatch(
        actions.setListProductHome({
          data: [...listProduct, ...listHomeItem.data],
          page: 0,
          totalPage: response.total_page,
          query: text,
        }),
      );
    }
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const getListCategory = () => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    const response = await api.getListCategory();
    if (response && response.collections) {
      const listCategory = response.collections.map((item, index) => {
        return {
          id: item.id,
          type: 'vegetables',
          title: item.title,
          isSelected: index === 0 ? true : false,
          icon: homeCategoryEnum[item.handle]
            ? homeCategoryEnum[item.handle].svgIcon
            : ICON_HOME_VEGETABLE,
          handle: item.handle,
          child_collection: {
            data: item.child_collection.map((itemChildren, indexChildren) => {
              return {
                id: itemChildren.id,
                type: 'vegetables',
                title: item.title,
                isSelected: indexChildren === 0 ? true : false,
                icon: childCategoryEnum[itemChildren.handle]
                  ? childCategoryEnum[itemChildren.handle].svgIcon
                  : ICON_VEGETABLE,
                handle: itemChildren.handle,
              };
            }),
          },
        };
      });
      dispatch(actions.setCategoryHandle(listCategory[0].handle));
      dispatch(actions.setListCategory(listCategory));
    }
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const getListHomeProduct = () => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    const {categoryHandle, listHomeItem} = getState().home;

    const response = await api.getListHomeProduct(
      categoryHandle,
      listHomeItem.page + 1,
    );

    if (response && response.products) {
      const listProduct = response.products.map((item, index) => {
        return {
          id: item.id,
          itemName: item.title,
          image: item.images ? item.images : [],
          priceAlfter: item.variants[0].price,
          priceBefore: item.variants[0].compare_at_price,
          isSale: isSale(
            item.variants[0].compare_at_price,
            item.variants[0].price,
          ),

          salePercent: discountPercent(
            item.variants[0].compare_at_price,
            item.variants[0].price,
          ),
          handle: item.handle,
          variants: item.variants,
        };
      });

      dispatch(
        actions.setListProductHome({
          data: [...listProduct, ...listHomeItem.data],
          totalPage: response.total_page,
          page: listHomeItem.page + 1,
          query: '',
        }),
      );
    }
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const updateProduct = (data) => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    dispatch(actionsProduct.updateProductField({handle: data.handle}));
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const updateProductId = (id) => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    let {listHomeItem} = getState().home;
    let data = listHomeItem.data.find((item) => {
      return item.id === id;
    });

    dispatch(actionsProduct.updateProductField({handle: data.handle}));
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const setShowModal = (data) => (dispatch, getState) => {
  const isVisible = true;
  dispatch(actions.UpdateShowModal(isVisible));
};

const setHideModal = (data) => (dispatch, getState) => {
  const isVisible = false;
  dispatch(actions.UpdateShowModal(isVisible));
};

const updateSelectModal = (data) => (dispatch, getState) => {
  var {listLocation} = getState().home;
  const id = data.id;
  let listWithSelected = listLocation.data.map((item) => {
    return {
      ...item,
      isSelected: item.id === id ? true : false,
    };
  });
  dispatch(actions.UpdateSelectModal(listWithSelected));
};

const updateHandle = (data) => async (dispatch, getState) => {
  var {listCategories} = getState().home;
  const id = data.id;
  var handle;
  let listCategorySelected = listCategories.data.map((item) => {
    item.id === id ? (handle = item.handle) : item.id;
    return {
      ...item,
      isSelected: item.id === id ? true : false,
    };
  });
  await dispatch(actions.setCategoryHandle(handle));
  await dispatch(actions.setListCategory(listCategorySelected));
  await dispatch(actions.clearListProduct());
  await dispatch(getListHomeProduct());
};

const getHomeSlider = () => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    const response = await api.getSliderImage();
    dispatch(actions.setSliderImages(response.slider));
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const loadMoreProduct = () => async (dispatch, getState) => {
  const {listHomeItem} = getState().home;
  if (listHomeItem.page >= 1 && listHomeItem.page < listHomeItem.totalPage) {
    if (listHomeItem.query === '') {
      dispatch(getListHomeProduct());
    } else {
      dispatch(searchProduct());
    }
  }
};

const clearListProduct = () => async (dispatch) => {
  await dispatch(actions.clearListProduct());
};

export default {
  searchProduct,
  updateProduct,
  setShowModal,
  setHideModal,
  updateSelectModal,
  getListCategory,
  getListHomeProduct,
  updateHandle,
  getHomeSlider,
  loadMoreProduct,
  clearListProduct,
  updateProductId,
};
