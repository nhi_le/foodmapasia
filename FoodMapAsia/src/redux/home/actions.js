import {
  SET_LIST_PRODUCT_HOME,
  UPDATE_SHOW_MODAL,
  UPDATE_SELECT_MODAL,
  SET_LIST_CATEGORY_HOME,
  UPDATE_HANDLE,
  SET_SLIDER_IMAGES,
  UPDATE_PAGE,
  SET_LIST_PRODUCT_SEARCH,
  CLEAR_LIST_PRODUCT,
} from './constants';

const setListProductHome = (listProduct) => ({
  type: SET_LIST_PRODUCT_HOME,
  listProduct,
});

const updatePage = (page) => ({
  type: UPDATE_PAGE,
  page,
});

const UpdateShowModal = (isVisible) => ({
  type: UPDATE_SHOW_MODAL,
  isVisible,
});

const UpdateSelectModal = (listWithSelected) => ({
  type: UPDATE_SELECT_MODAL,
  listWithSelected,
});

const setCategoryHandle = (handle) => ({
  type: UPDATE_HANDLE,
  handle,
});

const setListCategory = (listCategory) => ({
  type: SET_LIST_CATEGORY_HOME,
  listCategory,
});

const setSliderImages = (data) => ({
  type: SET_SLIDER_IMAGES,
  data,
});

const clearListProduct = () => ({
  type: CLEAR_LIST_PRODUCT,
});

export default {
  setListProductHome,
  UpdateShowModal,
  UpdateSelectModal,
  setListCategory,
  setCategoryHandle,
  setSliderImages,
  updatePage,
  clearListProduct,
};
