import api from '../../services/remote/baseApi';
import apiEndPoints from '../../services/remote/apiEndPoints';

const getSearch = (text, page) => {
  return api.get(
    apiEndPoints.GET_SEARCH_PRODUCT.replace('{0}', encodeURI(text)).replace(
      '{1}',
      page,
    ),
  );
};
const getListCategory = () => {
  return api.get(apiEndPoints.GET_LIST_CATEGORY_HOME);
};

const getListHomeProduct = (handle, page) => {
  const url = apiEndPoints.GET_LIST_HOME_PRODUCT.replace('{Handle}', handle);
  return api.get(url.replace('{PageNumber}', page));
};
const getSliderImage = () => {
  return api.get(apiEndPoints.GET_HOME_SLIDER);
};
export default {getSearch, getListCategory, getListHomeProduct, getSliderImage};
