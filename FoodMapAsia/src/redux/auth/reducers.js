import {
  UPDATE_LOGIN_FORM,
  REFRESH_LOGIN_FORM,
  UPDATE_SHOW_SIGN_UP_MODAL,
  SET_USER,
  EXTERNAL_LOGIN,
  EXTERNAL_REGISTER,
  CHANGE_SHOW_PASSWORD,
  CHANGE_FORGOT_PASSWORD_MODAL_VISIBLE,
  CHANGE_RESET_MAIL_SENT_MODAL_VISIBLE,
  CHANGE_EMAIL_INPUT_MODAL_VISIBLE,
  SET_EXTERNAL_LOGIN_RESPONSE,
  SET_SIGNUP_FORM,
  SET_SIGNUP_MODAL_VISIBLE,
  CHANGE_SIGNUP_SHOW_PASSWORD,
  SET_AUTHORIZED,
  UPDATE_REGISTER_VALIDATE_FORM,
  SET_SELECT_CITY_PLACE_HOLDER,
  SET_SELECT_DISTRICT_PLACE_HOLDER,
  SET_SELECT_WARD_PLACE_HOLDER,
  UPDATE_SIGNUP_VALIDATE_FORM,
} from './constants';

const defaultState = {
  isAuthorized: undefined,
  isShowingSignUpModal: false,
  login: {
    email: __DEV__ && 'zhaoyun932@gmail.com',
    recaptcha: '',
    isShowPassword: false,
    password: __DEV__ && '123456',
    validation: {
      isEmailExits: false,
    },
  },
  formValidate: {
    company: '',
    fullName: '',
    isValid: false,
    email: '',
    password: '',
    phoneNumber: '',
    address: '',
  },
  formSignInValidate: {
    isValid: false,
    email: '',
    password: '',
  },
  user: undefined,
  isForgotPasswordVisible: false,
  isResetEmailSentVisible: false,
  externalLogin: {
    isEmailModalVisible: false,
  },
  signup: {
    form: {
      email: '',
      fullName: '',
      address: '',
      company: '',
      phoneNumber: '',
      password: '',
      confirmPassword: '',
    },
    passwordVisible: {
      password: false,
      confirmPassword: false,
    },
    modal: {
      isModalEmailRequiredVisible: false,
      isModalEmailSentVisible: false,
      isModalEmailSuccessVisible: false,
    },
  },

  selection: {
    citySelection: {
      options: ['Hồ Chí Minh', 'Hà Nội', 'Đà Nẵng'],
      placeholder: 'Chọn thành phố',
      isValid: false,
      errorMessage: 'Vui lòng chọn',
    },
    districtSelection: {
      options: ['Quận 1', 'Quận 2', 'Quận 3'],
      placeholder: 'Chọn quận / huyện',
      isValid: false,
      errorMessage: 'Vui lòng chọn',
    },
    wardSelection: {
      options: ['Phường 1', 'Phường 2', 'Phường 3'],
      placeholder: 'Chọn phường / xã',
      isValid: false,
      errorMessage: 'Vui lòng chọn',
    },
  },
};

const reduce = (state = defaultState, action) => {
  switch (action.type) {
    case UPDATE_LOGIN_FORM:
      return {
        ...state,
        login: {
          ...state.login,
          ...action.form,
        },
      };
    case REFRESH_LOGIN_FORM:
      return {
        ...state,
        isShowingSignUpModal: false,
        login: defaultState.login,
      };
    case UPDATE_SHOW_SIGN_UP_MODAL: {
      return {
        ...state,
        isShowingSignUpModal: action.isShowingSignUpModal,
      };
    }
    case SET_USER:
      return {
        ...state,
        user: action.user,
        isAuthorized: !!action.user,
      };
    case EXTERNAL_LOGIN:
      return {
        ...state,
      };
    case EXTERNAL_REGISTER:
      return {
        ...state,
      };
    case CHANGE_SHOW_PASSWORD:
      return {
        ...state,
        login: {
          ...state.login,
          isShowPassword: action.isShowPassword,
        },
      };
    case CHANGE_FORGOT_PASSWORD_MODAL_VISIBLE:
      return {
        ...state,
        isForgotPasswordVisible: action.isVisible,
      };
    case CHANGE_RESET_MAIL_SENT_MODAL_VISIBLE:
      return {
        ...state,
        isResetEmailSentVisible: action.isVisible,
      };
    case CHANGE_EMAIL_INPUT_MODAL_VISIBLE:
      return {
        ...state,
        externalLogin: {
          ...state.externalLogin,
          isEmailModalVisible: action.isVisible,
        },
      };
    case SET_EXTERNAL_LOGIN_RESPONSE:
      return {
        ...state,
        externalLogin: {
          ...state.externalLogin,
          ...action.response,
        },
      };
    case SET_SIGNUP_FORM:
      return {
        ...state,
        signup: {
          ...state.signup,
          form: {
            ...state.signup.form,
            ...action.form,
          },
        },
      };
    case CHANGE_SIGNUP_SHOW_PASSWORD:
      return {
        ...state,
        signup: {
          ...state.signup,
          passwordVisible: {
            ...state.passwordVisible,
            password:
              action.passwordVisible == 'password'
                ? !state.signup.passwordVisible.password
                : state.signup.passwordVisible.password,
            confirmPassword:
              action.passwordVisible == 'confirmPassword'
                ? !state.signup.passwordVisible.confirmPassword
                : state.signup.passwordVisible.confirmPassword,
          },
        },
      };
    case SET_SIGNUP_MODAL_VISIBLE:
      return {
        ...state,
        signup: {
          ...state.signup,
          modal: {
            ...state.signup.modal,
            ...action.modal,
          },
        },
      };
    case UPDATE_REGISTER_VALIDATE_FORM:
      return {
        ...state,
        formValidate: action.formValidate.validation,
      };
    case UPDATE_SIGNUP_VALIDATE_FORM:
      return {
        ...state,
        formSignInValidate: action.formValidate.validation,
      };
    case SET_AUTHORIZED:
      return {
        ...state,
        isAuthorized: action.isAuthorized,
      };
    case SET_SELECT_CITY_PLACE_HOLDER:
      return {
        ...state,
        selection: {
          ...state.selection,
          citySelection: {
            ...state.selection.citySelection,
            placeholder: action.placeholder,
            isValid: true,
          },
        },
      };
    case SET_SELECT_DISTRICT_PLACE_HOLDER:
      return {
        ...state,
        selection: {
          ...state.selection,
          districtSelection: {
            ...state.selection.districtSelection,
            placeholder: action.placeholder,
            isValid: true,
          },
        },
      };
    case SET_SELECT_WARD_PLACE_HOLDER:
      return {
        ...state,
        selection: {
          ...state.selection,
          wardSelection: {
            ...state.selection.wardSelection,
            placeholder: action.placeholder,
            isValid: true,
          },
        },
      };
    default:
      return state;
  }
};

export default reduce;
