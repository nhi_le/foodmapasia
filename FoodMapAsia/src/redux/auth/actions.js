import {
  UPDATE_LOGIN_FORM,
  REFRESH_LOGIN_FORM,
  SET_USER,
  LOGIN,
  LOGOUT,
  SET_AUTHORIZED,
  SET_SIGNUP_FORM,
  SET_SELECT_CITY_PLACE_HOLDER,
  SET_SELECT_WARD_PLACE_HOLDER,
  SET_SELECT_DISTRICT_PLACE_HOLDER,
  UPDATE_REGISTER_VALIDATE_FORM,
  UPDATE_SIGNUP_VALIDATE_FORM,
} from './constants';

const refreshLoginForm = () => ({
  type: REFRESH_LOGIN_FORM,
});

const updateLoginForm = (form) => ({
  type: UPDATE_LOGIN_FORM,
  form,
});

const setUser = (user) => ({
  type: SET_USER,
  user,
});

const login = ({username, password}) => ({
  type: LOGIN,
  username,
  password,
});

const logout = () => ({
  type: LOGOUT,
});

const setAuthorized = (isAuthorized) => ({
  type: SET_AUTHORIZED,
  isAuthorized,
});

const setSignUpForm = (form) => ({
  type: SET_SIGNUP_FORM,
  form,
});
const updateValidateForm = (formValidate) => ({
  type: UPDATE_REGISTER_VALIDATE_FORM,
  formValidate,
});
const setCityPlaceHolder = (placeholder) => ({
  type: SET_SELECT_CITY_PLACE_HOLDER,
  placeholder,
});
const setWardPlaceHolder = (placeholder) => ({
  type: SET_SELECT_WARD_PLACE_HOLDER,
  placeholder,
});
const setDistrictPlaceHolder = (placeholder) => ({
  type: SET_SELECT_DISTRICT_PLACE_HOLDER,
  placeholder,
});
const updateValidateSignInForm = (formValidate) => ({
  type: UPDATE_SIGNUP_VALIDATE_FORM,
  formValidate,
});
export default {
  updateLoginForm,
  refreshLoginForm,
  setUser,
  login,
  logout,
  setAuthorized,
  setSignUpForm,
  updateValidateForm,
  setCityPlaceHolder,
  setDistrictPlaceHolder,
  setWardPlaceHolder,
  updateValidateSignInForm,
};
