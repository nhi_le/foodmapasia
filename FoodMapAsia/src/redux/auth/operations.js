import actions from './actions';
import appOperations from '@redux/app/operations';
import cartOperations from '@redux/cart/operations';
import {
  requiredValidation,
  notEmptyStringValidation,
  emailValidation,
} from '../helpers/validations';
import api from './api';
import authTokens from '../auth-token';
import {
  FIELD_EMAIL_REQUIRED,
  FIELD_PASSWORD_REQUIRED,
  FIELD_FULL_NAME_REQUIRED,
  FIELD_ADDRESS_REQUIRED,
  FIELD_PHONE_NUMBER_REQUIRED,
  FIELD_RECAPTCHA_REQUIRED,
  FIELD_COMPANY_REQUIRED,
} from '../../resources/string/strings';
import _ from 'lodash';

const updateLoginForm = (key, value) => (dispatch) => {
  const changes = {};
  changes[key] = value;
  return dispatch(actions.updateLoginForm(changes));
};

const refreshLoginForm = () => (dispatch) => {
  return dispatch(actions.refreshLoginForm());
};

export const loginPageValidation = ({email, password, recaptcha}) => {
  var validation = {
    isValid: true,
  };

  if (!emailValidation(email) || !notEmptyStringValidation(email)) {
    validation.email = FIELD_EMAIL_REQUIRED;
    validation.isValid = false;
  }

  if (!requiredValidation(password) || !notEmptyStringValidation(password)) {
    validation.password = FIELD_PASSWORD_REQUIRED;
    validation.isValid = false;
  }

  if (!requiredValidation(recaptcha) || !notEmptyStringValidation(recaptcha)) {
    validation.recaptcha = FIELD_PASSWORD_REQUIRED;
    validation.isValid = false;
  }
  return validation;
};
export const forgotPasswordPageValidation = ({email, recaptcha}) => {
  var validation = {
    isValid: true,
  };
  if (
    !emailValidation(email) ||
    !notEmptyStringValidation(email) ||
    !requiredValidation(email)
  ) {
    validation.email = FIELD_EMAIL_REQUIRED;
    validation.isValid = false;
  }
  if (!requiredValidation(recaptcha) || !notEmptyStringValidation(recaptcha)) {
    validation.recaptcha = FIELD_PASSWORD_REQUIRED;
    validation.isValid = false;
  }
  return validation;
};

export const signupPageValidation = ({
  email,
  password,
  fullName,
  address,
  phoneNumber,
  recaptcha,
  company,
}) => {
  var validation = {
    isValid: true,
  };
  if (!emailValidation(email) || !notEmptyStringValidation(email)) {
    validation.email = FIELD_EMAIL_REQUIRED;
    validation.isValid = false;
  }
  if (!requiredValidation(company) || !notEmptyStringValidation(company)) {
    validation.company = FIELD_COMPANY_REQUIRED;
    validation.isValid = false;
  }
  if (!requiredValidation(password) || !notEmptyStringValidation(password)) {
    validation.password = FIELD_PASSWORD_REQUIRED;
    validation.isValid = false;
  }

  if (!requiredValidation(recaptcha) || !notEmptyStringValidation(recaptcha)) {
    validation.recaptcha = FIELD_RECAPTCHA_REQUIRED;
    validation.isValid = false;
  }

  if (
    !requiredValidation(phoneNumber) ||
    !notEmptyStringValidation(phoneNumber)
  ) {
    validation.phoneNumber = FIELD_PHONE_NUMBER_REQUIRED;
    validation.isValid = false;
  }

  if (!requiredValidation(address) || !notEmptyStringValidation(address)) {
    validation.address = FIELD_ADDRESS_REQUIRED;
    validation.isValid = false;
  }

  if (!requiredValidation(fullName) || !notEmptyStringValidation(fullName)) {
    validation.fullName = FIELD_FULL_NAME_REQUIRED;
    validation.isValid = false;
  }
  return validation;
};

const loginEmailValidation = () => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());

  try {
    const {login} = getState().auth;
    const {email} = login;
    let validation = emailValidation(email);
    if (validation) {
      dispatch(actions.updateLoginForm({validation}));
    } else {
      return dispatch(
        actions.updateLoginForm({validation: {email: FIELD_EMAIL_REQUIRED}}),
      );
    }
  } catch (error) {
    console.log(error);
    if (error.data && error.data.message) {
      const validation = {
        isValid: false,
      };
      dispatch(appOperations.hideLoading());
      return dispatch(actions.updateLoginForm({validation}));
    }
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const updateShowSignUpModal = (isShowSignUpModal) => async (dispatch) => {
  dispatch(actions.updateShowSignUpModal(isShowSignUpModal));
};

const logout = () => async (dispatch, getState) => {
  await api.logout();
  await authTokens.clear(null);
  await api.checkUsername();
  await dispatch(cartOperations.getListCart());
  dispatch(actions.setUser(null));
};

const setUser = (user) => async (dispatch) => {
  return dispatch(actions.setUser(user));
};

const login = () => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    const {login} = getState().auth;
    const {email, password, recaptcha} = login;

    const validation = loginPageValidation({email, password, recaptcha});
    if (!validation.isValid) {
      dispatch(actions.updateValidateSignInForm({validation}));
      return false;
    }

    const details = {
      form_type: 'customer_login',
      utf8: true,
      'customer[email]': email,
      'customer[password]': password,
      'g-recaptcha-response': recaptcha,
    };

    const formBody = [];
    for (const property in details) {
      const encodeKey = encodeURIComponent(_.toString(property));
      const encodeValue = encodeURIComponent(_.toString(details[property]));
      formBody.push(encodeKey + '=' + encodeValue);
    }
    const body = formBody.join('&');

    const result = await api.login(body);

    let data = await result.text();
    if (data && data.indexOf(data.email) > -1) {
      const responseUser = await api.checkUsername();
      let dataUser = await responseUser.text();
      await authTokens.storeUser(JSON.parse(dataUser));
      dispatch(actions.setUser(JSON.parse(dataUser)));
      return true;
    } else {
      return false;
    }
  } catch (error) {
    alert('Đăng nhập thất bại');
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const changeShowPassword = () => (dispatch, getState) => {};

const changeForgotPasswordVisible = (isVisible) => (dispatch) => {};

const changeResetEmailSentVisible = (isVisible) => (dispatch) => {};

const saveDevice = async () => {};

const signUp = () => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    const {signup} = getState().auth;
    const {form} = signup;
    const {
      email,
      password,
      phoneNumber,
      recaptcha,
      fullName,
      address,
      company,
      confirmPassword,
    } = form;
    const validation = signupPageValidation({
      email,
      password,
      fullName,
      address,
      phoneNumber,
      recaptcha,
      company,
    });
    if (!validation.isValid) {
      dispatch(actions.updateValidateForm({validation}));
      return false;
    }

    const details = {
      form_type: 'create_customer',
      utf8: true,
      'customer[email]': email,
      'customer[password]': password,
      'customer[first_name]': fullName,
      'customer[last_name]': fullName,
      'customer[phone]': phoneNumber,
      'customer[address]': address,
      'g-recaptcha-response': recaptcha,
    };

    const formBody = [];
    for (const property in details) {
      const encodeKey = encodeURIComponent(_.toString(property));
      const encodeValue = encodeURIComponent(_.toString(details[property]));
      formBody.push(encodeKey + '=' + encodeValue);
    }
    const body = formBody.join('&');

    const result = await api.signup(body);

    let data = await result.text();
    if (data && data.indexOf('Tạo tài khoản') > -1) {
      return false;
    } else if (data && data.indexOf(data.email) > -1) {
      return true;
    }
  } catch (error) {
    dispatch(appOperations.handlerError(error));
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const setAuthorized = (isAuthorized) => (dispatch) => {
  dispatch(actions.setAuthorized(isAuthorized));
};

const setSignUpForm = (form) => (dispatch) => {
  return dispatch(actions.setSignUpForm(form));
};
const setSelectCity = (index) => (dispatch, getState) => {
  const {selection} = getState().auth;
  const {citySelection} = selection;
  if (index === citySelection.options.length) {
    return;
  }
  const newPlaceHolder = citySelection.options[index];
  return dispatch(actions.setCityPlaceHolder(newPlaceHolder));
};
const setSelectWard = (index) => (dispatch, getState) => {
  const {selection} = getState().auth;
  const {wardSelection} = selection;
  if (index === wardSelection.options.length) {
    return;
  }
  const newPlaceHolder = wardSelection.options[index];
  return dispatch(actions.setWardPlaceHolder(newPlaceHolder));
};
const setSelectDistrict = (index) => (dispatch, getState) => {
  const {selection} = getState().auth;
  const {districtSelection} = selection;
  if (index === districtSelection.options.length) {
    return;
  }
  const newPlaceHolder = districtSelection.options[index];
  return dispatch(actions.setDistrictPlaceHolder(newPlaceHolder));
};

const onForgotPassword = () => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    const {login} = getState().auth;
    const {email, recaptcha} = login;

    const validation = forgotPasswordPageValidation({email, recaptcha});
    if (!validation.isValid) {
      dispatch(actions.updateValidateSignInForm({validation}));
      return;
    }
    const details = {
      form_type: 'recover_customer_password',
      utf8: true,
      email: email,
      'g-recaptcha-response': recaptcha,
    };

    const formBody = [];
    for (const property in details) {
      const encodeKey = encodeURIComponent(_.toString(property));
      const encodeValue = encodeURIComponent(_.toString(details[property]));
      formBody.push(encodeKey + '=' + encodeValue);
    }
    const body = formBody.join('&');

    const result = await api.forgotPassword(body);

    let data = await result.text();

    let textCheck = 'Không tìm thấy tài khoản nào với email này';
    if (data && data.indexOf(textCheck) > -1) {
      alert(textCheck);
    } else if (data && data.indexOf(data.email) > -1) {
      alert('Đã gửi mail xin vui lòng kiểm tra');
    }
  } catch (error) {
    dispatch(appOperations.handlerError(error));
  } finally {
    dispatch(appOperations.hideLoading());
  }
};
export default {
  updateLoginForm,
  refreshLoginForm,
  setUser,
  login,
  loginEmailValidation,
  updateShowSignUpModal,
  logout,
  changeShowPassword,
  changeForgotPasswordVisible,
  changeResetEmailSentVisible,
  saveDevice,
  signUp,
  setAuthorized,
  setSignUpForm,
  setSelectCity,
  setSelectWard,
  setSelectDistrict,
  onForgotPassword,
};
