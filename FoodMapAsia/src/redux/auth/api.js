import api from '../../services/remote/baseApi';
import apiEndPoints from '../../services/remote/apiEndPoints';

const withQuery = require('with-query').default;

const checkUsername = () => {
  return fetch(apiEndPoints.GET_USER_INFO, {
    credentials: 'same-origin',
  });
};

const login = (body) => {
  return fetch(apiEndPoints.LOGIN, {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-requested-With': 'XMLHttpRequest',
    },
    body,
  });
};
const forgotPassword = (body) => {
  return fetch(apiEndPoints.FORGOT_PASSWORD, {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-requested-With': 'XMLHttpRequest',
    },
    body,
  });
};

const signup = (body) => {
  return fetch(apiEndPoints.REGISTER, {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-requested-With': 'XMLHttpRequest',
    },
    body,
  });
};

const sendEmailVerification = (userId) => {
  return api.get(`/auth-api/users/${userId}/request`);
};

const externalLogin = (body) => {
  return api.post('auth-api/auth/external-login', body);
};

const externalRegister = (body) => {
  return api.post('auth-api/users/external-register', body);
};

const checkExternalLoginExist = (params) => {
  return api.get(withQuery('auth-api/auth/external-login/exist', params));
};

const logout = () => {
  return fetch(apiEndPoints.LOGOUT_USER, {
    credentials: 'same-origin',
  });
};

const sendEmailResetPassword = (email) => {
  return api.get(`/auth-api/users/${email}/password/request`);
};

const saveDevice = (fcmToken, deviceId, deviceType) => {
  const body = {
    token: fcmToken,
    deviceId,
    deviceType,
  };
  return api.post(apiEndPoints.POST_FCM_TOKEN, body);
};

export default {
  checkUsername,
  login,
  signup,
  sendEmailVerification,
  externalLogin,
  externalRegister,
  checkExternalLoginExist,
  logout,
  sendEmailResetPassword,
  saveDevice,
  forgotPassword,
};
