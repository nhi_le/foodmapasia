import {AsyncStorage} from 'react-native';
import {setAuthorizationToken} from '../services/remote/baseApi';

const USER_TOKEN_KEY = 'userToken';

const storeUser = async (user) => {
  const {token} = user;
  const userString = JSON.stringify(user);
  setAuthorizationToken(token);
  await AsyncStorage.setItem(USER_TOKEN_KEY, userString);
  return user;
};

const getUser = async () => {
  const userString = await AsyncStorage.getItem(USER_TOKEN_KEY);
  if (!userString) {
    return null;
  }
  let user = JSON.parse(userString);
  setAuthorizationToken(user.jwt);
  return user;
};

const getAccessToken = async () => {
  const tokenString = await AsyncStorage.getItem(USER_TOKEN_KEY);
  if (!tokenString) {
    return null;
  }
  const {token} = JSON.parse(tokenString);
  return token;
};

const clear = async () => {
  await AsyncStorage.removeItem(USER_TOKEN_KEY);
};

const hasValidToken = async () => {
  const tokenString = await AsyncStorage.getItem(USER_TOKEN_KEY);
  return Boolean(tokenString);
};

export default {
  storeUser,
  getUser,
  getAccessToken,
  hasValidToken,
  clear,
};
