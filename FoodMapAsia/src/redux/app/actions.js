import {
  SHOW_LOADING,
  HIDE_LOADING,
  SET_PERMISSION_MODAL_VISIBLE,
} from './constants';

const showLoading = () => ({
  type: SHOW_LOADING,
});

const hideLoading = () => ({
  type: HIDE_LOADING,
});

const setPermissionModalVisible = (isVisible) => ({
  type: SET_PERMISSION_MODAL_VISIBLE,
  isVisible,
});

export default {
  showLoading,
  hideLoading,
  setPermissionModalVisible,
};
