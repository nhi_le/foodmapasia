import actions from './actions';
import authOperations from '@redux/auth/operations';
import {isUnauthorized} from '../helpers/errorHandler';

const showLoading = () => (dispatch, getState) => {
  const {isLoading} = getState().app;
  if (!isLoading) {
    dispatch(actions.showLoading());
  }
};

const hideLoading = () => (dispatch, getState) => {
  const {isLoading} = getState().app;
  if (isLoading) {
    dispatch(actions.hideLoading());
  }
};

const handlerError = (error) => (dispatch) => {
  console.log(error);
  dispatch(actions.hideLoading());
  if (isUnauthorized(error)) {
    dispatch(authOperations.logout());
  } else if (error.message) {
    alert(error.message);
  } else {
    alert('Something when wrong!');
  }
  return false;
};

const setPermissionModalVisible = (isVisible) => (dispatch) => {
  return dispatch(actions.setPermissionModalVisible(isVisible));
};

export default {
  showLoading,
  hideLoading,
  handlerError,
  setPermissionModalVisible,
};
