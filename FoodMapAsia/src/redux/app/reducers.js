import {
  SHOW_LOADING,
  HIDE_LOADING,
  SET_PERMISSION_MODAL_VISIBLE,
} from './constants';

const defaultState = {
  isLoading: false,
  isPermissionModalVisible: false,
};

const reduce = (state = defaultState, action) => {
  switch (action.type) {
    case SHOW_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case HIDE_LOADING:
      return {
        ...state,
        isLoading: false,
      };
    case SET_PERMISSION_MODAL_VISIBLE:
      return {
        ...state,
        isPermissionModalVisible: action.isVisible,
      };
    default:
      return state;
  }
};

export default reduce;
