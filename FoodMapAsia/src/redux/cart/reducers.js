import {
  UPDATE_FORM_PAYMENT,
  CLEAR_FORM_PAYMENT,
  SET_DISCOUNT,
  UPDATE_CART_PRICE,
  UPDATE_LIST_CART,
  UPDATE_LIST_TRANSPORT,
  UPDATE_LIST_PAYMENT,
  UPDATE_PRICE,
  SET_VALIDATE,
} from './constants';

const defaultState = {
  codeDiscount: null,
  total: 0,
  price: {
    discount: 0,
    rootPrice: 0,
    totalPrice: 0,
  },
  listTransport: {
    data: [
      {
        id: 0,
        type: 'transport',
        title: 'Giao hàng tận nơi - Trừ gạo, trái cây tươi',
        isSelected: false,
        description: 55000,
      },
      {
        id: 1,
        type: 'transport',
        title: 'Phí giao hàng nội thành - Gạo, trái cây tươi',
        isSelected: false,
        description: 25000,
      },
    ],
  },
  listPayment: {
    data: [
      {
        id: 0,
        isSelected: false,
        type: 'payment',
        title: 'Thanh toán bằng tiền mặt (COD)',
        description: 'Khách hàng sẽ thanh toán đơn hàng khi nhận hàng',
      },
      {
        id: 1,
        isSelected: false,
        type: 'payment',
        title: 'Thanh toán online',
        description: '(ATM/Visa/MasterCard/JCB/QR Pay trên Mobile Banking)',
      },
      {
        id: 2,
        isSelected: false,
        type: 'payment',
        title: 'Thanh toán online qua ví MoMo',
        description: '',
      },
    ],
  },
  citySelection: {
    options: ['Hồ Chí Minh', 'Hà Nội', 'Đà Nẵng'],
  },
  districtSelection: {
    options: ['Quận 1', 'Quận 2', 'Quận 3'],
  },
  wardSelection: {
    options: ['Phường 1', 'Phường 2', 'Phường 3'],
  },
  formPayment: {
    fullname: '',
    email: '',
    phonenumber: '',
    address: '',
    city: '',
    district: '',
    ward: '',
    validation: {
      errorFullname: null,
      errorEmail: null,
      errorPhoneNumber: null,
      errorAddress: null,
      errorCity: null,
      errorDistrict: null,
      errorWard: null,
    },
  },
  stepPayment: 0,
  listShoppingCart: {
    data: [],
  },
  stepFavorite: 0,
};

const reduce = (state = defaultState, action) => {
  switch (action.type) {
    case UPDATE_LIST_CART:
      return {
        ...state,
        listShoppingCart: {
          data: action.listShoppingCart,
        },
      };
    case UPDATE_CART_PRICE:
      return {
        ...state,
        total: action.updatedPrice,
      };
    case UPDATE_FORM_PAYMENT:
      return {
        ...state,
        formPayment: {
          ...state.formPayment,
          ...action.formPayment,
        },
      };
    case CLEAR_FORM_PAYMENT:
      return {
        ...state,
        formPayment: defaultState.formPayment,
      };

    case UPDATE_LIST_TRANSPORT:
      return {
        ...state,
        listTransport: {
          ...state.listTransport,
          data: action.listTransport,
        },
      };

    case UPDATE_LIST_PAYMENT:
      return {
        ...state,
        listPayment: {
          ...state.listPayment,
          data: action.listPayment,
        },
      };
    case UPDATE_PRICE:
      return {
        ...state,
        price: {
          ...state.price,
          ...action.price,
        },
      };
    case SET_DISCOUNT:
      return {
        ...state,
        codeDiscount: action.codeDiscount,
      };
    case SET_VALIDATE:
      return {
        ...state,
        formPayment: {
          ...state.formPayment,
          ...(state.formPayment.validation = action.formValidate),
        },
      };
    default:
      return state;
  }
};

export default reduce;
