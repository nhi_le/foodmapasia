import api from '../../services/remote/baseApi';
import apiEndPoints from '../../services/remote/apiEndPoints';

const withQuery = require('with-query').default;
const getCurrentCart = () => {
  return fetch(apiEndPoints.GET_CURRENT_CART, {
    method: 'GET',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-requested-With': 'XMLHttpRequest',
    },
  });
};
const deleteItemCart = (body) => {
  return fetch(apiEndPoints.DELETE_ITEM_CART, {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-requested-With': 'XMLHttpRequest',
    },
    body,
  });
};
export default {getCurrentCart, deleteItemCart};
