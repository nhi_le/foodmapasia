import actions from './actions';
import appOperations from '@redux/app/operations';
import api from './api';
import _ from 'lodash';
import apiProduct from '../product/api';
import {
  notEmptyStringValidation,
  emailValidation,
} from '../helpers/validations';
import {
  ERROR_MESS_EMPTY_VALUE,
  ERROR_EMAIL_ISVALID,
} from '@resources/string/strings';
import {discountPercent, isSale} from '@common/components/StringHelper';

const checkValidation = () => (dispatch, getState) => {
  const {formPayment} = getState().cart;
  if (!formPayment.fullname) {
    formPayment.validation.errorFullname = ERROR_MESS_EMPTY_VALUE;
  }
  if (!formPayment.email) {
    formPayment.validation.errorEmail = ERROR_MESS_EMPTY_VALUE;
  }
  if (!formPayment.phonenumber) {
    formPayment.validation.errorPhoneNumber = ERROR_MESS_EMPTY_VALUE;
  }
  if (!formPayment.address) {
    formPayment.validation.errorAddress = ERROR_MESS_EMPTY_VALUE;
  }
  if (!formPayment.city) {
    formPayment.validation.errorCity = ERROR_MESS_EMPTY_VALUE;
  }
  if (!formPayment.district) {
    formPayment.validation.errorDistrict = ERROR_MESS_EMPTY_VALUE;
  }
  if (!formPayment.ward) {
    formPayment.validation.errorWard = ERROR_MESS_EMPTY_VALUE;
  }
  if (
    formPayment.validation.errorFullname ||
    formPayment.validation.errorEmail ||
    formPayment.validation.errorPhoneNumber ||
    formPayment.validation.errorAddress ||
    formPayment.validation.errorCity ||
    formPayment.validation.errorDistrict ||
    formPayment.validation.errorWard
  ) {
    dispatch(actions.setValidate(formPayment.validation));
    return false;
  }
  return true;
};
const setRootPrice = () => (dispatch, getState) => {
  const {total} = getState().cart;
  // let tmp = total.value;
  // const {price} = getState().cart;
  // dispatch(actions.updatePrice({rootPrice: tmp}));
  if (total) {
    dispatch(actions.updatePrice({totalPrice: total}));
  }
};
const updateFormPayment = (key, value) => (dispatch, getState) => {
  const {citySelection} = getState().cart;
  const {districtSelection} = getState().cart;
  const {wardSelection} = getState().cart;
  const {validation} = getState().cart.formPayment;
  const changes = {};
  switch (key) {
    case 'fullname':
      if (!notEmptyStringValidation(value)) {
        validation.errorFullname = ERROR_MESS_EMPTY_VALUE;
      } else {
        validation.errorFullname = null;
      }
      changes[key] = value;
      break;
    case 'email':
      if (!notEmptyStringValidation(value)) {
        validation.errorEmail = ERROR_MESS_EMPTY_VALUE;
      } else if (!emailValidation(value)) {
        validation.errorEmail = ERROR_EMAIL_ISVALID;
      } else {
        validation.errorEmail = null;
      }
      changes[key] = value;
      break;
    case 'phonenumber':
      if (!notEmptyStringValidation(value)) {
        validation.errorPhoneNumber = ERROR_MESS_EMPTY_VALUE;
      } else {
        validation.errorPhoneNumber = null;
      }
      changes[key] = value;
      break;
    case 'address':
      if (!notEmptyStringValidation(value)) {
        validation.errorAddress = ERROR_MESS_EMPTY_VALUE;
      } else {
        validation.errorAddress = null;
      }
      changes[key] = value;
      break;
    case 'citySelection':
      if (value === citySelection.options.length) {
        validation.errorCity = ERROR_MESS_EMPTY_VALUE;
      } else {
        validation.errorCity = null;
      }
      changes.city = citySelection.options[value];
      break;
    case 'districtSelection':
      if (value === districtSelection.options.length) {
        validation.errorDistrict = ERROR_MESS_EMPTY_VALUE;
      } else {
        validation.errorDistrict = null;
      }
      changes.district = districtSelection.options[value];
      break;
    case 'wardSelection':
      if (value === wardSelection.options.length) {
        validation.errorWard = ERROR_MESS_EMPTY_VALUE;
      } else {
        validation.errorWard = null;
      }
      changes.ward = wardSelection.options[value];
      break;
    default:
      changes[key] = value;
      break;
  }
  return dispatch(actions.updateFormPayment(changes));
};

const getListCart = () => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    const response = await api.getCurrentCart();
    let data = await response.text();
    let ListCart = JSON.parse(data);
    if (ListCart && ListCart.item) {
      const listNew = ListCart.item.map((item, index) => {
        return {
          id: item.id,
          image: item.image,
          itemName: item.title,
          price: item.price,
          isDiscount: isSale(item.compare_at_price, item.price),
          discount: discountPercent(item.compare_at_price, item.price),
          count: parseInt(item.quantity),
          handle: item.product_handle,
          line: item.line,
        };
      });
      if (listNew.length > 0) {
        dispatch(actions.updateListCart(listNew));
        return;
      }
    }
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const increaseAmount = (type, id) => async (dispatch, getState) => {
  var {listShoppingCart, total} = getState().cart;
  var updatedPrice = total;
  const data = {
    line: 0,
    quantity: 0,
  };

  let listNew = listShoppingCart.data.map((item) => {
    if (item.id === id) {
      data.line = item.line;
      data.quantity = item.count + 1;
    }

    return {
      ...item,
      count: item.id === id ? item.count + 1 : item.count,
    };
  });

  updateCartQuantity(data.line, data.quantity);
  dispatch(actions.updateListCart(listNew, updatedPrice));
};

const updateAmount = (number, id) => async (dispatch, getState) => {
  var {listShoppingCart} = getState().cart;

  if (number.isEmpty() || parseInt(number) >= 99 || parseInt(number) === 0) {
    return;
  }
  let listNew = listShoppingCart.data.map((item) => {
    return {
      ...item,
      count: item.id === id ? (item.count = parseInt(number)) : item.count,
    };
  });
  const data = {
    line: 0,
    quantity: parseInt(number),
  };
  listShoppingCart.data.forEach((item) => {
    if (item.id === id) {
      data.line = item.line;
    }
  });
  await updateCartQuantity(data.line, data.quantity);
  dispatch(actions.updateListCart(listNew));
};
const updatePrice = () => (dispatch, getState) => {
  const {listShoppingCart} = getState().cart;
  var sum = 0;
  listShoppingCart.data.map((item) => {
    sum = sum + item.price * item.count;
  });
  dispatch(actions.updateCartPrice(sum));
};

const decreaseAmount = (type, id) => async (dispatch, getState) => {
  var {listShoppingCart} = getState().cart;
  const data = {
    line: 0,
    quantity: 0,
  };

  let listNew = listShoppingCart.data.map((item) => {
    if (item.id === id) {
      data.line = item.line;
      data.quantity = item.count - 1;
    }
    return {
      ...item,
      count:
        item.id === id
          ? item.count === 1
            ? item.count
            : item.count - 1
          : item.count,
    };
  });

  updateCartQuantity(data.line, data.quantity);
  dispatch(actions.updateListCart(listNew));
};

const resetPayment = () => (dispatch, getState) => {
  dispatch(actions.clearFormPayment());
};

const setDiscount = (text) => (dispatch) => {
  dispatch(actions.setDiscount(text));
};
const removeCartItem = (type, id) => async (dispatch, getState) => {
  var {listShoppingCart} = getState().cart;
  var newList = [...listShoppingCart.data];
  _.remove(newList, {id: id});
  var line;
  listShoppingCart.data.forEach((item) => {
    item.id === id ? (line = item.line) : item.id;
  });
  dispatch(appOperations.showLoading());
  try {
    const details = {
      quantity: 0,
      line: line,
    };

    const formBody = [];
    for (const property in details) {
      const encodeKey = encodeURIComponent(_.toString(property));
      const encodeValue = encodeURIComponent(_.toString(details[property]));
      formBody.push(encodeKey + '=' + encodeValue);
    }
    const body = formBody.join('&');
    await api.deleteItemCart(body);
    newList = [...newList];
    dispatch(actions.updateListCart(newList));
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

async function updateCartQuantity(line, newQuantity) {
  try {
    const details = {
      quantity: newQuantity,
      line: line,
    };
    const formBody = [];
    for (const property in details) {
      const encodeKey = encodeURIComponent(_.toString(property));
      const encodeValue = encodeURIComponent(_.toString(details[property]));
      formBody.push(encodeKey + '=' + encodeValue);
    }
    const body = formBody.join('&');
    await api.deleteItemCart(body);
  } catch (error) {}
}

const setPricePayment = () => (dispatch, getState) => {
  const {price} = getState().cart;
  const {listTransport} = getState().cart;
  let sum = 0;
  listTransport.data.map((item) => {
    if (item.isSelected) {
      sum = price.rootPrice + item.description;
      if (price.discount > 0) {
        sum -= sum * (price.discount / 100);
      }
    } else {
      sum = price.rootPrice;
    }
  });
  dispatch(actions.updatePrice({totalPrice: sum}));
};

const updateMethodTransport = (itemTransport) => (dispatch, getState) => {
  const {listTransport} = getState().cart;
  const {price} = getState().cart;
  let sum = 0;
  let listTransportNew = listTransport.data.map((item) => {
    return {
      ...item,
      isSelected: item.id === itemTransport.id,
    };
  });
  listTransportNew.map((item) => {
    if (item.isSelected) {
      sum = price.rootPrice + item.description;
      if (price.discount > 0) {
        sum -= sum * (price.discount / 100);
      }
    }
  });
  dispatch(actions.updatePrice({totalPrice: sum}));
  dispatch(actions.updateListTransport(listTransportNew));
};

const updateMethodPayment = (itemPayment) => (dispatch, getState) => {
  const {listPayment} = getState().cart;
  let listPaymentNew = listPayment.data.map((item) => {
    return {
      ...item,
      isSelected: item.id === itemPayment.id,
    };
  });
  dispatch(actions.updateListPayment(listPaymentNew));
  listPaymentNew.map((item) => {
    if (item.isSelected) {
      const changes = {};
      changes.payment = item.title;
      dispatch(actions.updateFormPayment(changes));
    }
  });
};

const updateListCart = (listNew, updatedPrice) => (dispatch, getState) => {
  dispatch(actions.updateListCart(listNew));
  dispatch(actions.updateCartPrice(updatedPrice));
};
const clearCartAPI = () => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());

  try {
    const {listShoppingCart} = getState().cart;

    let body = '';
    for (var i = 0; i < listShoppingCart.data.length; i++) {
      if (i < listShoppingCart.data.length - 1) {
        body += 'updates[]=' + 0 + '&';
      } else {
        body += 'updates[]=' + 0;
      }
    }

    await apiProduct.updateCart(body);
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

export default {
  updateFormPayment,
  resetPayment,
  setDiscount,
  updateAmount,
  increaseAmount,
  decreaseAmount,
  updatePrice,
  getListCart,
  removeCartItem,
  setPricePayment,
  updateMethodPayment,
  updateMethodTransport,
  checkValidation,
  setRootPrice,
  updateListCart,
  clearCartAPI,
};
