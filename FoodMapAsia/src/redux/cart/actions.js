import {
  UPDATE_FORM_PAYMENT,
  CLEAR_FORM_PAYMENT,
  SET_DISCOUNT,
  UPDATE_LIST_TRANSPORT,
  UPDATE_LIST_PAYMENT,
  UPDATE_PRICE,
  UPDATE_LIST_CART,
  UPDATE_CART_PRICE,
  SET_VALIDATE,
} from './constants';

const updateFormPayment = (formPayment) => ({
  type: UPDATE_FORM_PAYMENT,
  formPayment,
});

const updateListCart = (listShoppingCart, updatedPrice) => ({
  type: UPDATE_LIST_CART,
  listShoppingCart,
  updatedPrice,
});

const updateCartPrice = (updatedPrice) => ({
  type: UPDATE_CART_PRICE,
  updatedPrice,
});

const clearFormPayment = () => ({
  type: CLEAR_FORM_PAYMENT,
});

const setDiscount = (codeDiscount) => ({
  type: SET_DISCOUNT,
  codeDiscount,
});
const updateListTransport = (listTransport) => ({
  type: UPDATE_LIST_TRANSPORT,
  listTransport,
});
const updateListPayment = (listPayment) => ({
  type: UPDATE_LIST_PAYMENT,
  listPayment,
});
const updatePrice = (price) => ({
  type: UPDATE_PRICE,
  price,
});

const setValidate = (formValidate) => ({
  type: SET_VALIDATE,
  formValidate,
});
export default {
  updateCartPrice,
  updateListCart,
  updateFormPayment,
  clearFormPayment,
  setDiscount,
  updateListPayment,
  updateListTransport,
  updatePrice,
  setValidate,
};
