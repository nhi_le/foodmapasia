import actions from './actions';
import appOperations from '@redux/app/operations';
import api from './api';
import apiHome from '../home/api';
import {category} from '@resources/enum/delivery-statuses';
import {productFilterData} from '@resources/enum/product-filter-statuses';
import {ICON_VEGETABLE, ICON_HOME_VEGETABLE} from '../../resources/images';
import {childCategoryEnum} from '@resources/enum/home-category';
import {discountPercent, isSale} from '@common/components/StringHelper';

const searchProduct = (text) => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    const {listProduct} = getState().categories;
    const response = await apiHome.getSearch(text, listProduct.page + 1);
    if (response && response.result) {
      const listProductNew = response.result.map((item) => {
        return {
          itemName: item.title,
          img: item.featured_image_src,
          itemType: item.money_unit !== '' ? ' ' + item.money_unit : '',
          variants: item.variants,
          priceAfter: item.variants[0].price,
          priceBefore: item.variants[0].compare_at_price,
          handle: item.handle,
          variants: item.variants,
          isSale:
            ((parseFloat(item.variants[0].price) -
              parseFloat(item.variants[0].compare_at_price)) /
              parseFloat(item.variants[0].compare_at_price)) *
              100 <
            0
              ? true
              : false,
          salePercent:
            parseFloat(item.variants[0].compare_at_price) === 0
              ? '0%'
              : Math.abs(
                  Math.round(
                    ((parseFloat(item.variants[0].price) -
                      parseFloat(item.variants[0].compare_at_price)) /
                      parseFloat(item.variants[0].compare_at_price)) *
                      100,
                  ),
                ) + '%',
        };
      });
      dispatch(
        actions.setListProduct({
          data: [...listProductNew, ...listProduct.data],
          page: listProduct.page,
          totalPage: response.total_page,
          query: text,
        }),
      );
    }
  } catch (error) {
    console.log('error', error);
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const setCategoryProductSelected = (data) => (dispatch, getState) => {
  const {listCategoryProduct} = getState().categories;
  let listNew = listCategoryProduct.map((item) => {
    return {
      ...item,
      isSelected: item.id === data.id,
    };
  });
  dispatch(actions.setListCategoryProduct(listNew));
};

const setCategorySelected = (id) => (dispatch, getState) => {
  const {listCategory} = getState().categories;
  let listNew = listCategory.map((item) => {
    return {
      ...item,
      isSelected: item.id === id,
    };
  });

  dispatch(actions.setListCategory(listNew));
};

const setCategoryItemSelected = (id) => (dispatch, getState) => {
  try {
    const {listCategory} = getState().categories;
    let listCategoryNew = listCategory.map((item) => {
      return {
        ...item,
        child_collection: {
          ...item.child_collection,
          data: item.child_collection.data.map((itemChildren) => {
            return {
              ...itemChildren,
              isSelected: itemChildren.id === id,
            };
          }),
        },
      };
    });
    dispatch(actions.setListCategory(listCategoryNew));
  } catch (error) {
    console.log(error);
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const getListCategory = () => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    const {categoryHandle} = getState().home;
    const response = await api.getListCategory();
    if (response && response.collections) {
      var indexSelected = 0;
      const listCategory = response.collections.map((item, index) => {
        if (item.handle === categoryHandle) {
          indexSelected = index;
        }
        return {
          id: item.id,
          title: item.title,
          isSelected: item.handle === categoryHandle,
          icon: category[item.handle]
            ? category[item.handle].svgIcon
            : ICON_HOME_VEGETABLE,
          handle: item.handle,
          child_collection: {
            data: item.child_collection.map((itemChildren, indexChildren) => {
              return {
                id: itemChildren.id,
                title: itemChildren.title,
                isSelected: indexChildren === 0,
                icon: childCategoryEnum[itemChildren.handle]
                  ? childCategoryEnum[itemChildren.handle].svgIcon
                  : ICON_VEGETABLE,
                handle: itemChildren.handle,
              };
            }),
          },
        };
      });

      await dispatch(
        getListCategoryProduct(
          listCategory[indexSelected].child_collection.data[0].handle,
        ),
      );
      await dispatch(actions.setListCategory(listCategory));
    }
  } catch (error) {
    console.log(error);
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const getListCategoryProduct = (handle) => async (dispatch) => {
  dispatch(appOperations.showLoading());
  try {
    const response = await api.getListCategoryProduct(handle);

    if (response && response.collections) {
      const listCategoryProduct = response.collections.map((item, index) => {
        return {
          id: item.id,
          label: item.title,
          handle: item.handle,
          isSelected: index === 0,
        };
      });

      await dispatch(actions.setListCategoryProduct(listCategoryProduct));
      let isNotEmpty = listCategoryProduct.length > 0;
      dispatch(
        getListProduct(isNotEmpty ? listCategoryProduct[0].handle : handle),
      );
    }
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const getListProduct = (handle) => async (dispatch, getState) => {
  dispatch(appOperations.showLoading());
  try {
    await dispatch(actions.setCollectionHandle(handle));
    const {listProduct} = getState().categories;
    const response = await api.getListProduct(handle, listProduct.page + 1);
    if (response && response.products) {
      const listProductNew = response.products.map((item, index) => {
        return {
          itemName: item.title,
          img: item.featured_image,
          itemType: item.money_unit !== '' ? ' ' + item.money_unit : '',
          priceAfter: item.variants[0].price,
          priceBefore: item.variants[0].compare_at_price,
          handle: item.handle,
          isSale: isSale(
            item.variants[0].compare_at_price,
            item.variants[0].price,
          ),
          salePercent: discountPercent(
            item.variants[0].compare_at_price,
            item.variants[0].price,
          ),
          variants: item.variants,
        };
      });

      dispatch(
        actions.setListProduct({
          data: [...listProductNew, ...listProduct.data],
          page: listProduct.page + 1,
          totalPage: response.total_page,
          query: '',
        }),
      );
    }
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const setListProduct = () => async (dispatch) => {
  dispatch(appOperations.showLoading());
  try {
    const listProduct = [];
    dispatch(actions.setListProduct(listProduct));
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const setSelectFilter = (index) => async (dispatch, getState) => {
  try {
    dispatch(appOperations.showLoading());
    const {listFilter, listProduct, collectionHandle} = getState().categories;
    let data = listFilter.options[index];
    let condition = productFilterData[data].key;
    console.log('Filter Page: ', collectionHandle);
    const response = await api.getListProductFilter(
      collectionHandle,
      condition,
      listProduct.page + 1,
    );
    if (response && response.products) {
      const listProductNew = response.products.map((item) => {
        return {
          itemName: item.title,
          img: item.featured_image,
          itemType: item.money_unit !== '' ? ' ' + item.money_unit : '',
          priceAfter: item.variants[0].price,
          priceBefore: item.variants[0].compare_at_price,
          handle: item.handle,
          variants: item.variants,
          isSale:
            ((parseFloat(item.variants[0].price) -
              parseFloat(item.variants[0].compare_at_price)) /
              parseFloat(item.variants[0].compare_at_price)) *
              100 <
            0
              ? true
              : false,
          salePercent:
            parseFloat(item.variants[0].compare_at_price) === 0
              ? '0%'
              : Math.abs(
                  Math.round(
                    ((parseFloat(item.variants[0].price) -
                      parseFloat(item.variants[0].compare_at_price)) /
                      parseFloat(item.variants[0].compare_at_price)) *
                      100,
                  ),
                ) + '%',
        };
      });
      await dispatch(
        actions.setListProduct({
          data: [...listProductNew, ...listProduct.data],
          page: listProduct.page,
          totalPage: response.total_page,
          query: '',
        }),
      );
    }

    await dispatch(actions.setSelectFilter(listFilter.options[index]));
  } catch (error) {
  } finally {
    dispatch(appOperations.hideLoading());
  }
};

const loadMoreProduct = () => async (dispatch, getState) => {
  const {listProduct} = getState().categories;
  if (listProduct.page >= 1 && listProduct.page < listProduct.totalPage) {
    if (listProduct.query === '') {
      dispatch(getListProduct());
    } else {
      dispatch(searchProduct());
    }
  }
};

const clearListProduct = () => async (dispatch) => {
  await dispatch(actions.clearListProduct());
};

export default {
  setCategoryProductSelected,
  setCategorySelected,
  getListCategoryProduct,
  getListCategory,
  getListProduct,
  setSelectFilter,
  setListProduct,
  setCategoryItemSelected,
  searchProduct,
  loadMoreProduct,
  clearListProduct,
};
