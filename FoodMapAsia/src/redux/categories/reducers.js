import {
  SET_CATEGORY_PRODUCT_SELECTED,
  SET_LIST_CATEGORY,
  SET_LIST_CATEGORY_PRODUCT,
  SET_LIST_PRODUCT,
  SET_SELECT_FILTER,
  SET_CATEGORIES_HOME_CATEGORY_SELECTED,
  CLEAR_LIST_PRODUCT_CATEGORY,
  SET_COLLECTION_HANDLE,
} from './constants';
import {productFilterData} from '@resources/enum/product-filter-statuses';

const defaultState = {
  categoryHomeSelected: '',
  listFilter: {
    isShow: true,
    options: [
      productFilterData['Sản phẩm nổi bật'].title,
      productFilterData['Giá tăng dần'].title,
      productFilterData['Giá giảm dần'].title,
      productFilterData['Tên từ A-Z'].title,
      productFilterData['Tên từ Z-A'].title,
      productFilterData['Cũ nhất'].title,
      productFilterData['Mới nhất'].title,
      productFilterData['Bán chạy nhất'].title,
    ],
    placeholder: productFilterData['Sản phẩm nổi bật'].title,
  },
  listProduct: {
    data: [],
    page: 0,
    totalPage: 0,
    query: '',
  },
  collectionHandle: '',
};
const reduce = (state = defaultState, action) => {
  switch (action.type) {
    case SET_LIST_CATEGORY_PRODUCT:
      return {
        ...state,
        listCategoryProduct: action.listCategoryProduct,
      };
    case SET_CATEGORY_PRODUCT_SELECTED:
      return {
        ...state,
        listCategoryProduct: action.listCategoryProduct,
      };
    case SET_LIST_CATEGORY:
      return {
        ...state,
        listCategory: action.listCategory,
      };
    case SET_LIST_PRODUCT:
      return {
        ...state,
        listProduct: {
          ...state.listProduct,
          data: action.listProduct.data,
          totalPage: action.listProduct.totalPage,
          page: action.listProduct.page,
        },
      };

    case SET_SELECT_FILTER:
      return {
        ...state,
        listFilter: {
          ...state.listFilter,
          placeholder: action.placeholder,
        },
      };
    case SET_CATEGORIES_HOME_CATEGORY_SELECTED:
      return {
        ...state,
        categoryHomeSelected: action.id,
      };
    case CLEAR_LIST_PRODUCT_CATEGORY:
      return {
        ...state,
        listProduct: defaultState.listProduct,
      };
    case SET_COLLECTION_HANDLE:
      console.log('Set handle: ', action.collectionHandle);
      return {
        ...state,
        collectionHandle: action.collectionHandle,
      };
    default:
      return state;
  }
};

export default reduce;
