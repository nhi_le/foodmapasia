import {
  SET_LIST_CATEGORY,
  SET_LIST_CATEGORY_PRODUCT,
  SET_LIST_PRODUCT,
  UPDATE_HANDLE,
  SET_SELECT_FILTER,
  SET_CATEGORIES_HOME_CATEGORY_SELECTED,
  CLEAR_LIST_PRODUCT_CATEGORY,
  SET_COLLECTION_HANDLE,
} from './constants';

const setListCategory = (listCategory) => ({
  type: SET_LIST_CATEGORY,
  listCategory,
});

const setListCategoryProduct = (listCategoryProduct) => ({
  type: SET_LIST_CATEGORY_PRODUCT,
  listCategoryProduct,
});

const setListProduct = (listProduct) => ({
  type: SET_LIST_PRODUCT,
  listProduct,
});

const setCategoryHandle = (handle) => ({
  type: UPDATE_HANDLE,
  handle,
});

const setSelectFilter = (placeholder) => ({
  type: SET_SELECT_FILTER,
  placeholder,
});

const setCategoryHomeSelected = (id) => ({
  type: SET_CATEGORIES_HOME_CATEGORY_SELECTED,
  id,
});

const clearListProduct = () => ({
  type: CLEAR_LIST_PRODUCT_CATEGORY,
});

const setCollectionHandle = (collectionHandle) => ({
  type: SET_COLLECTION_HANDLE,
  collectionHandle,
});

export default {
  setListCategory,
  setListCategoryProduct,
  setListProduct,
  setCategoryHandle,
  setSelectFilter,
  setCategoryHomeSelected,
  clearListProduct,
  setCollectionHandle,
};
