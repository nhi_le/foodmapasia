import api from '../../services/remote/baseApi';
import apiEndPoints from '../../services/remote/apiEndPoints';

const getListCategoryProduct = (handle) => {
  return api.get(
    apiEndPoints.GET_LIST_CATEGORY_PRODUCT.replace('{handle}', handle),
  );
};

const getListCategory = () => {
  return api.get(apiEndPoints.GET_LIST_CATEGORY_HOME);
};

const getListProduct = (handle, condition, page) => {
  return api.get(
    apiEndPoints.GET_LIST_PRODUCT.replace('{handle}', handle)
      .replace('{condition}', condition)
      .replace('{0}', page),
  );
};

const getListProductFilter = (collectionHandle, condition, page) => {
  return api.get(
    apiEndPoints.GET_LIST_PRODUCT_FILTER.replace(
      '{collectionHandle}',
      collectionHandle,
    )
      .replace('{condition}', condition)
      .replace('{0}', page),
  );
};

export default {
  getListCategoryProduct,
  getListCategory,
  getListProduct,
  getListProductFilter,
};
