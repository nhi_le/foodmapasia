import React, {Component} from 'react';
import {TouchableOpacity, StyleSheet} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5Free from 'react-native-vector-icons/FontAwesome5';
import Foundation from 'react-native-vector-icons/Foundation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Octicons from 'react-native-vector-icons/Octicons';
import Zocial from 'react-native-vector-icons/Zocial';

const Controls = {
  AntDesign: AntDesign,
  Entypo: Entypo,
  EvilIcons: EvilIcons,
  Feather: Feather,
  FontAwesome: FontAwesome,
  FontAwesome5: FontAwesome5Free,
  Foundation: Foundation,
  Ionicons: Ionicons,
  MaterialIcons: MaterialIcons,
  MaterialCommunityIcons: MaterialCommunityIcons,
  Octicons: Octicons,
  Zocial: Zocial,
};

export default Icon = ({
  style,
  iconStyle,
  type,
  name,
  color,
  onPress,
  ...props
}) => {
  let Control = Controls[type];

  if (!Control) {
    return null;
  }

  return (
    <TouchableOpacity
      style={StyleSheet.compose(styles.container, style)}
      onPress={() => onPress && onPress()}
      disabled={!onPress}>
      <Control
        {...props}
        name={name}
        color={color}
        style={StyleSheet.compose(styles.icon, iconStyle)}
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 20,
  },
  icon: {
    width: 30,
    height: 30,
    margin: 5,
    fontSize: 22,
    lineHeight: 30,
    textAlign: 'center',
  },
});
