import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';

const SvgIcon = ({
  width = 13,
  height = 13,
  tintColor = undefined,
  style,
  buttonStyle,
  svgIcon,
  onPress,
}) => {
  let isButton = onPress !== null && onPress !== undefined;
  const Icon = svgIcon;

  const iconProps = {
    width,
    height,
  };
  if (tintColor) {
    iconProps.fill = tintColor;
  }

  return (
    <View style={[styles.container, {width, height}, style]}>
      <TouchableOpacity
        style={[styles.button, buttonStyle]}
        disabled={!isButton}
        onPress={() => onPress && onPress()}>
        <Icon {...iconProps} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {},
  button: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default SvgIcon;
