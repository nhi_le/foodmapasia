/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {connect} from 'react-redux';
import TextContent from '../text/TextContent';
import SvgIcon from './SvgIcon';
import HorizontalView from '../layout/HorizontalView';

const mapStateToProps = (state) => ({});

class BadgeTabIcon extends Component {
  render() {
    const {
      size = 20,
      svgIcon,
      tintColor = undefined,
      badgeColor,
      textColor,
      style,
      total,
    } = this.props;

    const iconSize = size;
    const containerWith = size + size / 1.2;
    const containerHeight = size + size / 1.2;
    const badgeSize = size / 1.2;
    const fontSize = size / 2;

    return (
      <HorizontalView
        style={[
          styles.container,
          {width: containerWith, height: containerHeight},
          style,
        ]}>
        <SvgIcon
          width={iconSize}
          height={iconSize}
          svgIcon={svgIcon}
          tintColor={tintColor}
          style={{marginTop: badgeSize / 2, marginStart: badgeSize / 2}}
        />
        {total > 0 && (
          <View
            style={{
              width: badgeSize,
              height: badgeSize,
              backgroundColor: badgeColor,
              borderRadius: badgeSize / 2,
              marginTop: 2,
              marginStart: -10,
              justifyContent: 'center',
            }}>
            <TextContent
              weight="semi-bold"
              style={{
                fontSize: fontSize,
                color: textColor,
                textAlign: 'center',
              }}>
              {total}
            </TextContent>
          </View>
        )}
      </HorizontalView>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
});

export default connect(mapStateToProps)(BadgeTabIcon);
