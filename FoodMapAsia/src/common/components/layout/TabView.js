import React from 'react';
import {View, StyleSheet} from 'react-native';
import {ScrollableTabView} from '@valdio/react-native-scrollable-tabview';
import {
  BLACK_COLOR,
  TEXT_GRAY_LIGHT_COLOR,
  BACKGROUND_COLOR,
  ACCENT_COLOR,
} from '@resources/palette';
import DefaultTabBar from './DefaultTabBar';

export default TabView = ({
  style,
  children,
  scrollTabRef,
  onChangeTab,
  locked,
  ...props
}) => (
  <ScrollableTabView
    tabBarActiveTextColor={BLACK_COLOR}
    tabBarInactiveTextColor={TEXT_GRAY_LIGHT_COLOR}
    tabBarBackgroundColor={BACKGROUND_COLOR}
    tabBarUnderlineStyle={styles.tabBarUnderlineStyle}
    tabBarTextStyle={styles.textStyle}
    ref={scrollTabRef}
    onChangeTab={(obj) => {
      onChangeTab && onChangeTab(obj);
    }}
    locked={locked}
    renderTabBar={() => <DefaultTabBar />}
    {...props}
    style={StyleSheet.compose(styles.container, style)}>
    {children}
  </ScrollableTabView>
);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  tabBarUnderlineStyle: {
    backgroundColor: ACCENT_COLOR,
    height: 3,
  },
  textStyle: {
    fontWeight: 'normal',
  },
});
