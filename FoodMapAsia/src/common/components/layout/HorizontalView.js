import React from 'react';
import {View, StyleSheet} from 'react-native';

export default HorizontalView = ({style, children, ...props}) => (
  <View {...props} style={StyleSheet.compose(styles.container, style)}>
    {children}
  </View>
);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
});
