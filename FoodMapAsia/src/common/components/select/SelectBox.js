import {ActionSheetCustom as ActionSheet} from 'react-native-actionsheet';
import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import SvgIcon from '@common/components/icon/SvgIcon';
import {renderText} from '@common/components/StringHelper';
import {ICON_DROPDOWN} from '@resources/images';
const options = {array: [1, 2, 3, 4, 5]};
class SelectBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      textContent: 'aaa',
      tittle: '',
      options: [renderText('CANCEL')],
    };
  }
  componentDidMount() {
    let textContent = this.props.textContent;
    let options = [renderText('CANCEL'), this.props.options];
    let tittle = this.props.tittle;
    this.setState({
      textContent,
      options,
      tittle,
    });
  }
  showActionSheet = () => {
    this.ActionSheet.show();
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.textContent} onPress={this.showActionSheet}>
          {this.state.textContent}
        </Text>
        <SvgIcon
          width={10}
          height={6}
          svgIcon={ICON_DROPDOWN}
          style={styles.rigthIcon}
          onPress={this.showActionSheet}
        />
        <ActionSheet
          ref={(o) => (this.ActionSheet = o)}
          title={<Text>{this.state.tittle}</Text>}
          options={this.state.options}
          cancelButtonIndex={0}
          destructiveButtonIndex={4}
          onPress={(index) => {
            /* do something */
          }}
        />
      </View>
    );
  }
}
export const FONT_SIZES = {
  regular: 12,
  large: 16,
};
const styles = StyleSheet.create({
  container: {
    width: '100%',
    flex: 1,
    marginTop: 10,
    borderRadius: 22,
    borderWidth: 1,
    height: 40,
    borderColor: '#BDBDBD',
    backgroundColor: 'white',
  },
  textContent: {
    textAlignVertical: 'center',
    fontSize: FONT_SIZES.large,
    width: '100%',
    paddingHorizontal: 12,
    marginTop: 10,
    color: '#BDBDBD',
  },
  rigthIcon: {
    alignSelf: 'flex-end',
    marginEnd: 20,
    marginTop: -12,
  },
});
export default SelectBox;
