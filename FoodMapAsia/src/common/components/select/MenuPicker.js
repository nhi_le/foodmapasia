import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { renderText } from '../StringHelper';
import { DEFAULT_ACTION_SHEET_TITLE, CANCEL } from '../../../resources/string/strings';
import HorizontalView from '../layout/HorizontalView';
import BottomActionSheet from './BottomActionSheet';
import SvgIcon from '../icon/SvgIcon';

export default class MenuPicker extends Component {

    constructor() {
        super();

        this.state = {
            options: [renderText(CANCEL)],
        };
    }

    componentDidMount() {
        let options = [...this.props.options, renderText(CANCEL)];
        this.setState({
            options,
        });
    }

    componentWillReceiveProps(nextProps) {
        if (JSON.stringify(nextProps.options) !== JSON.stringify(this.state.options)) {
            let options = [...nextProps.options, renderText(CANCEL)];
            this.setState({
                options,
            });
        }
    }


    onOpenActionSheet = () => {
        if (this.props.isShow) {
            this.actionSheet.show();
        }

    }

    render() {
        const {
            placeholder = renderText(DEFAULT_ACTION_SHEET_TITLE),
            style,
            icon,
            svgIcon
        } = this.props;

        const {
            options,
        } = this.state;
        return (
            <TouchableOpacity onPress={this.onOpenActionSheet}>
                <HorizontalView
                    style={[styles.container, style]}
                >
                    {icon &&
                        <Icon
                            iconStyle={styles.icon}
                            {...icon}
                        />
                    }

                    {svgIcon &&
                        <SvgIcon
                            style={styles.icon}
                            svgIcon={svgIcon}
                            height={24}
                            width={24}
                        />
                    }

                    <BottomActionSheet
                        ref={o => this.actionSheet = o}
                        {...this.props}
                        title={placeholder}
                        options={options}
                        cancelButtonIndex={options.length - 1}
                    />
                </HorizontalView>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: 45,
        alignItems: 'center',
        paddingHorizontal: 20,
    },
    icon: {

    },
})