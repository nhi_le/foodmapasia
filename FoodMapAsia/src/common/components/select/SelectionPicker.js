import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {renderText} from '../StringHelper';
import {
  DEFAULT_ACTION_SHEET_TITLE,
  CANCEL,
} from '../../../resources/string/strings';
import {
  BORDER_COLOR,
  WHITE_COLOR,
  DANGEROUS_COLOR,
} from '../../../resources/palette';
import HorizontalView from '../layout/HorizontalView';
import TextContent from '../text/TextContent';
import BottomActionSheet from './BottomActionSheet';
import SvgIcon from '../icon/SvgIcon';
import {ICON_DROPDOWN} from '../../../resources/images';

export default class SelectionPicker extends Component {
  constructor() {
    super();

    this.state = {
      options: [renderText(CANCEL)],
    };
  }

  componentDidMount() {
    let options = [...this.props.options, renderText(CANCEL)];
    this.setState({
      options,
    });
  }

  componentWillReceiveProps(nextProps) {
    if (
      JSON.stringify(nextProps.options) !== JSON.stringify(this.state.options)
    ) {
      let options = [...this.props.options, renderText(CANCEL)];
      this.setState({
        options,
      });
    }
  }

  onOpenActionSheet = () => {
    if (this.props.isShow) {
      this.actionSheet.show();
    }
  };

  render() {
    const {
      containerStyle,
      placeholder = renderText(DEFAULT_ACTION_SHEET_TITLE),
      isShowIcon,
      textStyle,
    } = this.props;
    const {options} = this.state;
    return (
      <TouchableOpacity onPress={this.onOpenActionSheet}>
        <HorizontalView style={[styles.container, containerStyle]}>
          <TextContent
            style={[
              isShowIcon !== false ? styles.text : styles.textCenter,
              textStyle,
            ]}>
            {placeholder}
          </TextContent>
          {isShowIcon !== false && (
            <SvgIcon style={styles.icon} svgIcon={ICON_DROPDOWN} />
          )}
          <BottomActionSheet
            ref={(o) => (this.actionSheet = o)}
            {...this.props}
            title={placeholder}
            options={options}
            cancelButtonIndex={options.length - 1}
          />
        </HorizontalView>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 45,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: BORDER_COLOR,
    backgroundColor: WHITE_COLOR,
    alignItems: 'center',
    paddingHorizontal: 8,
  },
  text: {
    marginRight: 8,
    minWidth: 40,
  },
  textCenter: {
    flex: 1,
    color: DANGEROUS_COLOR,
    textAlign: 'center',
  },
  icon: {},
});
