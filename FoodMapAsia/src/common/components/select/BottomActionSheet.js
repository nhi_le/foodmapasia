
import React from 'react';
import {ActionSheetCustom as ActionSheet} from 'react-native-actionsheet';
import {renderText} from '../StringHelper';
import {DEFAULT_ACTION_SHEET_TITLE} from '../../../resources/string/strings';
import {TEXT_DARK_COLOR} from '../../../resources/palette';

const BottomActionSheet = React.forwardRef(
  ({style, options, onItemPress, ...props}, ref) => (
    <ActionSheet
      ref={ref}
      title={renderText(DEFAULT_ACTION_SHEET_TITLE)}
      tintColor={TEXT_DARK_COLOR}
      {...props}
      options={options}
      onPress={(index) => onItemPress && onItemPress(index)}
    />
  ),
);
export default BottomActionSheet;
