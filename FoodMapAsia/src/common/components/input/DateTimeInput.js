import React, { useState } from 'react';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { View, StyleSheet } from 'react-native';
import moment from 'moment';
import FieldInput from './FieldInput';

export default DateTimeInput = ({
  style,
  inputStyle,
  mode = 'date',
  label,
  placeholder,
  isAlwayShowingLabel = false,
  value,
  onDateTimeSelected,
  renderCustomField,
  ...props
}) => {
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = date => {
    onDateTimeSelected && onDateTimeSelected(date, mode);
    hideDatePicker();
  };

  const format = mode === 'time' ? 'HH:mm' : 'DD MMMM YYYY';
  return (
    <View style={StyleSheet.compose(styles.container, style)}>
      {renderCustomField ?
        renderCustomField({ showDatePicker, value, ...props })
        :
        <FieldInput
          label={label}
          placeholder={placeholder}
          isAlwayShowingLabel={isAlwayShowingLabel}
          onPress={showDatePicker}
          inputStyle={inputStyle}
          {...props}
          value={value ? moment(value).format(format) : null}
          editable={false}
          
        />
      }
      <DateTimePickerModal
        date={new Date(value) || new Date()}
        isVisible={isDatePickerVisible}
        mode={mode}
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
  },
});