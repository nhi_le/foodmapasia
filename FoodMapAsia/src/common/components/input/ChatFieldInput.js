import React, { Component } from 'react';
import { View, TextInput, StyleSheet, TouchableOpacity, KeyboardAvoidingView, Platform } from 'react-native';
import _ from "lodash";
import { BORDER_COLOR, BLACK_COLOR, PLACEHOLDER_COLOR } from '../../../resources/palette';
import FieldInput from './FieldInput';
import SvgIcon from '../icon/SvgIcon';
import { ICON_LIKE } from '../../../resources/images';
import { FIELD_CHAT_PLACEHOLDER } from '../../../resources/string/strings';
import Icon from '../icon/Icon';

class ChatFieldInput extends Component {
    constructor(props) {
        super(props);
        // this.state = {
        //     value: ''
        // };
    }

    // componentDidMount() {
    //     this.setState({
    //         value: this.props.value
    //     });
    // }

    onChangeText = (text) => {
        const { onChangeText } = this.props;
        // this.setState({ value: text });
        onChangeText && onChangeText(text);
    }

    onPress = (type) => {
        const { onSend, onLike, onPhotoSelect } = this.props;
        const { value } = this.props;

        type === 'send' && onSend && onSend(value);
        type === 'like' && onLike && onLike();
        type === 'photo' && onPhotoSelect && onPhotoSelect();
    }

    render() {
        const {
            style,
            placeholder = FIELD_CHAT_PLACEHOLDER,
        } = this.props;
        const { value } = this.props;

        return (
            <HorizontalView style={StyleSheet.compose(styles.container, style)}>
                {/* <Icon
                    style={styles.leftIcon}
                    name={'photo'}
                    type={'MaterialIcons'}
                    onPress={() => this.onPress('photo')}
                /> */}
                <FieldInput
                    {...this.props}
                    value={value}
                    placeholder={placeholder}
                    style={styles.input}
                    onChangeText={this.onChangeText}
                />

                <Icon
                    style={styles.rightIcon}
                    name={'ios-send'}
                    type={'Ionicons'}
                    onPress={() => this.onPress('send')}
                />

                {/* <Icon
                        style={styles.rightIcon}
                        name={'like1'}
                        type={'AntDesign'}
                        onPress={() => this.onPress('like')}
                    />
               */}
            </HorizontalView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingVertical: 12,
        paddingHorizontal: 15,
        width: '100%',
        alignItems: 'center'
    },
    input: {
        flex: 1
    },
    leftIcon: {
        marginLeft: -10,
        marginRight: 5
    },
    rightIcon: {
        marginLeft: 5,
        marginRight: -10
    }
});

export default ChatFieldInput;