import React from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Image,
} from 'react-native';
import {
  WHITE_COLOR,
  TEXT_LIGHT_COLOR,
  ERROR_COLOR,
  GRAY_COLOR,
  TEXT_DARK_COLOR,
  PLACEHOLDER_COLOR,
  ACCENT_COLOR,
} from '@resources/palette';
import TextContent from '../text/TextContent';
import {renderText} from '../StringHelper';
import SvgIcon from '../icon/SvgIcon';
import TextError from '../text/TextError';
import {DANGEROUS_COLOR} from '../../../resources/palette';

export const FONT_SIZES = {
  regular: 12,
  large: 16,
};

const Input = ({
  label,
  labelStyle,
  labelTextSize = 'little-smaller',
  labelNotHasValueStyle,
  labelHasValueStyle,
  size = 'large',
  style,
  inputStyle,
  iconLeft,
  iconRight,
  errorMessage,
  errorMessageStyle,
  errorEnabled,
  showingErrorIcon = true,
  textValue,
  isActive = false,
  hasValue = false,
  disableInputStyle,
  placeholderErrorColor = null,
  numberOfLines,
  inputRef,
  ...props
}) => {
  return (
    <View style={[styles.container, style]}>
      {label ? (
        <TextContent
          style={[
            styles.label,
            labelStyle,
            hasValue ? labelHasValueStyle : labelNotHasValueStyle,
          ]}
          size={labelTextSize}
          // fontFamily='content'
        >
          {label}
        </TextContent>
      ) : null}
      <TouchableOpacity
        disabled={props.editable !== false}
        onPress={() => props.onPress && props.onPress()}>
        <TextInput
          {...props}
          ref={inputRef}
          numberOfLines={numberOfLines}
          onTouchStart={() => props.onPress && props.onPress()}
          placeholder={props.placeholder && renderText(props.placeholder)}
          value={props.value || textValue}
          placeholderTextColor={
            errorMessage || errorEnabled
              ? placeholderErrorColor || PLACEHOLDER_COLOR
              : PLACEHOLDER_COLOR
          }
          style={[
            styles.textInput,
            inputStyle,
            isActive && styles.textInputActive,
            hasValue && styles.textInputHasValue,
            (errorMessage || errorEnabled) && styles.textInputInvalid,
            size && {fontSize: FONT_SIZES[size]},
            // fontFamily && { fontFamily: FONT_FAMILIES[fontFamily] },
            iconLeft && styles.textInputWithIconLeft,
            (iconRight ||
              (showingErrorIcon && (errorMessage || errorEnabled))) &&
              styles.textInputWithIconRight,
            props.editable === false && disableInputStyle,
          ]}
        />
        {iconRight && isActive && (
          <SvgIcon style={styles.iconRight} {...iconRight} />
        )}
        {iconLeft && <SvgIcon style={styles.iconLeft} {...iconLeft} />}
        <TextError
          style={[styles.errorMessage, errorMessageStyle]}
          size="little-smaller"
          errorMessage={errorMessage}
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginTop: 5,
  },
  label: {
    minHeight: 21,
    marginBottom: 4,
    color: TEXT_DARK_COLOR,
  },
  textInput: {
    fontSize: FONT_SIZES.regular,
    color: TEXT_DARK_COLOR,
    backgroundColor: GRAY_COLOR,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: GRAY_COLOR,
    width: '100%',
    height: 40,
    paddingHorizontal: 12,
  },
  textInputActive: {
    borderColor: ACCENT_COLOR,
    borderRadius: 20,
    backgroundColor: WHITE_COLOR,
  },
  textInputHasValue: {
    borderColor: GRAY_COLOR,
    borderRadius: 20,
    backgroundColor: WHITE_COLOR,
  },
  textInputInvalid: {
    borderColor: DANGEROUS_COLOR,
    borderWidth: 1,
  },
  textInputWithIconLeft: {
    paddingLeft: 40,
  },
  textInputWithIconRight: {
    paddingRight: 40,
  },
  iconLeft: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconRight: {
    position: 'absolute',
    right: 0,
    top: 0,
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  errorIcon: {
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconButton: {
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  errorMessage: {
    marginTop: 10,
  },
});

export default Input;
