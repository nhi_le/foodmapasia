import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import _ from 'lodash';
import FieldInput from './FieldInput';
import {ICON_SEARCH} from '../../../resources/images';
import {
  BORDER_COLOR,
  BUTTON_GOOGLE_COLOR,
  GRAY_COLOR,
  ACCENT_COLOR,
} from '../../../resources/palette';

class SearchFieldInput extends Component {
  constructor(props) {
    super(props);
    this.icons = {
      clear: {},
      default: {},
    };

    this.state = {
      value: null,
      icon: this.clearIcon,
      isFocusing: false,
    };
  }

  componentDidMount() {
    const {value} = this.state;
    if (this.props.value != value) {
      this.setState({
        value: this.props.value,
      });
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (this.state.value !== nextProps.value) {
      this.setState({
        value: nextProps.value,
      });
    }
  }

  _onChangeText = (text) => {
    const {onChangeText = () => {}} = this.props;
    if (text != undefined || text != null) {
      this._onSearchQueryChanged(text);
    }
    this.setState({
      value: text,
    });
  };

  _onSearchQueryChanged = _.debounce((text) => {
    const {onSearchQueryChanged = () => {}} = this.props;
    onSearchQueryChanged(text);
  }, 1000);

  onFocus = () => {
    if (this.state.isFocusing) {
      return;
    }
    this.setState({isFocusing: true}, () => this.fieldInput.onFocus());
  };

  onBlur = () => {
    if (!this.state.isFocusing) {
      return;
    }
    this.setState({isFocusing: false}, () => this.fieldInput.onBlur());
  };

  render() {
    const {value, isFocusing, placeholder} = this.state;
    const icon = value ? this.icons.clear : this.icons.default;
    const iconLeft = {
      svgIcon: ICON_SEARCH,
      width: 20,
      height: 20,
      tintColor: ACCENT_COLOR,
    };
    return (
      <FieldInput
        ref={(ref) => (this.fieldInput = ref)}
        iconLeft={iconLeft}
        label={null}
        size={'normal'}
        {...this.props}
        onChangeText={(text) => this.props.onChangeText(text)}
        icon={icon}
        value={value}
        onFocus={this.onFocus}
        onBlur={this.onBlur}
        inputStyle={styles.input}
        placeholder={this.props.placeholder}
      />
    );
  }
}

const styles = StyleSheet.create({
  input: {
    borderRadius: 30,
    borderWidth: 1,
    backgroundColor: 'white',
    borderColor: ACCENT_COLOR,
  },
});

export default SearchFieldInput;
