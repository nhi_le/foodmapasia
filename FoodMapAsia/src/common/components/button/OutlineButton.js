import React from 'react';
import {StyleSheet} from 'react-native';
import Button from './Button';
import {ACCENT_COLOR, LIGHT_COLOR} from '@resources/palette';
import {
  TEXT_DARK_COLOR,
  GRAY_COLOR,
  DANGEROUS_COLOR,
  BLACK_COLOR,
  PLACEHOLDER_COLOR,
  TEXT_GRAY_LIGHT_COLOR,
} from '../../../resources/palette';

const OutlineButton = ({
  style,
  buttonStyle,
  textStyle,
  text,
  icon,
  iconStyle = false,
  textSize = 'medium-button',
  textWeight = 'semi-bold',
  type = 'default', // default, dangerous, info, disable
  onPress,
  multipleText,
  ...props
}) => {
  return (
    <Button
      {...props}
      style={style}
      buttonStyle={{
        ...styles.button,
        ...(type === 'dangerous' && styles.dangerousButton),
        ...(type === 'info' && styles.infoButton),
        ...(type === 'disable' && styles.disableButton),
        ...buttonStyle,
      }}
      textStyle={{
        ...styles.text,
        ...(type === 'dangerous' && styles.dangerousText),
        ...(type === 'info' && styles.infoText),
        ...(type === 'disable' && styles.disableText),
        ...textStyle,
      }}
      text={text}
      textSize={textSize}
      textWeight={textWeight}
      iconStyle={iconStyle}
      icon={icon}
      onPress={onPress}
      multipleText={multipleText}
    />
  );
};

const styles = StyleSheet.create({
  button: {
    borderColor: ACCENT_COLOR,
    backgroundColor: LIGHT_COLOR,
    borderWidth: 1,
  },
  text: {
    color: TEXT_DARK_COLOR,
  },
  dangerousButton: {
    borderColor: GRAY_COLOR,
  },
  dangerousText: {
    color: DANGEROUS_COLOR,
  },
  infoButton: {
    borderColor: GRAY_COLOR,
  },
  infoText: {
    color: BLACK_COLOR,
  },
  disableButton: {
    borderColor: GRAY_COLOR,
    backgroundColor: GRAY_COLOR,
  },
  disableText: {
    color: TEXT_GRAY_LIGHT_COLOR,
  },
});

export default OutlineButton;
