import React from 'react';
import {View, TouchableOpacity, StyleSheet, Image} from 'react-native';
import {LIGHT_COLOR, GRAY_COLOR} from '@resources/palette';
import TextContent from '../text/TextContent';
import {ACCENT_COLOR} from '../../../resources/palette';
import HorizontalView from '../layout/HorizontalView';
import SvgIcon from '../icon/SvgIcon';

const Button = ({
  style,
  buttonStyle,
  textStyle,
  text,
  multipleText,
  textSize = 'medium-button',
  textWeight = 'semi-bold',
  onPress,
  icon,
  iconStyle,
  iconLeft,
  iconLeftStyle,
  disabled = false,
}) => {
  return (
    <View style={[styles.container, style]}>
      <TouchableOpacity
        disabled={disabled}
        onPress={() => !disabled && onPress && onPress()}>
        <View
          style={[iconStyle ? styles.buttonIcon : styles.button, buttonStyle]}>
          <HorizontalView>
            {iconLeft && (
              <View style={{...styles.iconLeftStyle, ...iconLeftStyle}}>
                <SvgIcon
                  width={30}
                  height={30}
                  style={styles.iconLeft}
                  {...iconLeft}
                />
              </View>
            )}
            <TextContent
              style={[iconStyle ? styles.textIcon : styles.text, textStyle]}
              size={textSize}
              weight={textWeight}>
              {multipleText
                ? multipleText.map((item, index) => (
                    <TextContent key={index} style={item.style}>
                      {item.text}
                    </TextContent>
                  ))
                : text}
            </TextContent>
            {iconStyle && (
              <SvgIcon style={[styles.icon, iconStyle]} {...icon} />
            )}
          </HorizontalView>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    alignItems: 'center',
  },
  button: {
    backgroundColor: ACCENT_COLOR,
    borderRadius: 10,
    minHeight: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonIcon: {
    backgroundColor: ACCENT_COLOR,
    borderRadius: 10,
    minHeight: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingEnd: 10,
  },
  text: {
    color: LIGHT_COLOR,
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    textAlign: 'center',
    flex: 1,
  },
  textIcon: {
    color: LIGHT_COLOR,
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    flex: 1,
  },
  item: {
    textAlign: 'center',
  },
  icon: {
    marginLeft: 5,
    alignSelf: 'center',
    alignItems: 'center',
    resizeMode: 'contain',
  },
  iconLeft: {
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  iconLeftStyle: {
    height: '100%',
    aspectRatio: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Button;
