import React from 'react';
import {View, StyleSheet} from 'react-native';
import TextContent from './TextContent';
import {bookingStatusesData} from '../../../resources/enum/booking-statuses';

const StatusView = ({style, status, acceptedDate, isAccepted = false}) => {
  if (!isAccepted && !!acceptedDate && status.includes('Past')) {
    status = 'Pending6';
  } else {
    if (!status || !bookingStatusesData[status]) {
      return null;
    }

    if (status === 'Pending1' && !!acceptedDate) {
      status = isAccepted ? 'Pending5' : 'Pending6';
    }
  }

  return (
    <View
      style={[
        styles.statusContainer,
        {backgroundColor: bookingStatusesData[status].backgroundColor},
        style,
      ]}>
      <TextContent
        style={{
          color: bookingStatusesData[status].textColor,
          textAlign: 'center',
        }}
        size={'small'}
        weight={'semi-bold'}>
        {bookingStatusesData[status].text}
      </TextContent>
    </View>
  );
};

const styles = StyleSheet.create({
  statusContainer: {
    borderRadius: 5,
    paddingVertical: 5,
    paddingHorizontal: 7,
    maxWidth: 100,
  },
});

export default StatusView;
