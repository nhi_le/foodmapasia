import React from 'react';
import {Text, StyleSheet} from 'react-native';
import {DARK_COLOR} from '@resources/palette';
import {renderText} from '../StringHelper';

const FONT_SIZES = {
  'very-small': 10,
  small: 12,
  'little-smaller': 13,
  medium: 14,
  large: 16,
  'very-large': 18,
  'medium-button': 14,
  'medium-subtitle': 15,
  'small-title': 18,
  'medium-title': 20,
  'large-title': 21.328,
  'larger-title': 24,
  'largest-title': 32,
  code: 28,
};
const FONT_WEIGHTS = {
  normal: {},
  bold: {
    fontWeight: 'bold',
  },
  'semi-bold': {
    fontWeight: '500',
  },
};
const FONT_STYLE = {
  normal: {},
  italic: {
    fontStyle: 'italic',
  },
};

const TextContent = ({
  size = 'medium',
  weight = 'normal',
  fontStyle = 'normal',
  ...props
}) => {
  return (
    <Text
      {...props}
      style={[
        styles.text,
        size && {fontSize: FONT_SIZES[size]},
        weight && FONT_WEIGHTS[weight],

        fontStyle && FONT_STYLE[fontStyle],
        props.style,
      ]}>
      {typeof props.children === 'string' || typeof props.children === 'number'
        ? renderText(`${props.children}`)
        : props.children}
    </Text>
  );
};

const styles = StyleSheet.create({
  text: {
    color: DARK_COLOR,
  },
});

export default TextContent;
