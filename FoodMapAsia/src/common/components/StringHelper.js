import LanguageServices from '@resources/string/locale/LanguageServices';

export const renderText = (text) => {
  return LanguageServices.t(text, {defaultValue: text});
};
export const currencyConvert = (text, isUnit = true) => {
  try {
    let money = text.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
    return money + (isUnit ? 'đ' : '');
  } catch (errr) {
    return '';
  }
};
export const discountPercent = (beforePrice, AfterPrice) => {
  if (beforePrice.toString().localeCompare('0') === 0) {
    return '0%';
  }
  return (
    Math.abs(
      Math.round(
        ((parseFloat(AfterPrice) - parseFloat(beforePrice)) /
          parseFloat(beforePrice)) *
          100,
      ),
    ) + '%'
  );
};
export const isSale = (beforePrice, AfterPrice) => {
  if (
    ((parseFloat(AfterPrice) - parseFloat(beforePrice)) /
      parseFloat(beforePrice)) *
      100 <
    0
  ) {
    return true;
  } else {
    return false;
  }
};
export const decodeEntities = (encodedString) => {
  var translate_re = /&(nbsp|amp|quot|lt|gt);/g;
  var translate = {
    nbsp: ' ',
    amp: '&',
    quot: '"',
    lt: '<',
    gt: '>',
  };
  return encodedString
    .replace(translate_re, function (match, entity) {
      return translate[entity];
    })
    .replace(/&#(\d+);/gi, function (match, numStr) {
      var num = parseInt(numStr, 10);
      return String.fromCharCode(num);
    });
};
