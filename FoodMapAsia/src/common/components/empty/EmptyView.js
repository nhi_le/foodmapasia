import React from "react";
import { StyleSheet, View } from "react-native";
import SvgIcon from "../icon/SvgIcon";
import Title from "../text/Title";
import TextContent from "../text/TextContent";

export default EmptyView = ({
    icon,
    title,
    description
}) => {
    return (
        <View style={styles.container}>
            {icon &&
                <SvgIcon
                    style={styles.icon}
                    width={140}
                    height={140}
                    {...icon}
                />
            }
            <Title
                style={styles.title}
                size={'large'}
            >
                {title}
            </Title>
            <TextContent
                style={styles.description}
            >
                {description}
            </TextContent>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 34
    },
    icon: {

    },
    title: {
        marginTop: 15,
        textAlign: 'center'
    },
    description: {
        marginTop: 15,
        textAlign: 'center'
    }
});