import React from 'react';
import {StyleSheet, Dimensions, View} from 'react-native';
import {STATUS_BAR_HEIGHT} from '@resources/dimensions';
const windows = Dimensions.get('window');

const HeaderGradientTemplate = (props) => {
  return (
    <View
      {...props}
      style={{
        ...styles.container,
        ...props.style,
      }}>
      {props.children}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: (windows.height + STATUS_BAR_HEIGHT) / 4 - 16,
    width: '100%',
  },
});

export default HeaderGradientTemplate;
