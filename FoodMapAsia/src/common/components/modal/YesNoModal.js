import React from 'react';
import {View, StyleSheet} from 'react-native';
import Modal from 'react-native-modal';
import {ACCENT_COLOR, TEXT_GRAY_COLOR, WHITE_COLOR} from '@resources/palette';
import Title from '../text/Title';
import Button from '../button/Button';
import TransparentButton from '../button/TransparentButton';
import ListContent from '../list/ListContent';

export default YesNoModal = ({
  style,
  title,
  titleStyle,
  icon,
  iconPosition = 'title',
  description,
  listLocation,
  yesButton,
  noButton,
  onBackButtonPress,
  onBackdropPress,
  isVisible,
}) => {
  return (
    <View>
      <Modal
        isVisible={isVisible}
        onBackButtonPress={onBackButtonPress}
        onBackdropPress={onBackdropPress}>
        <View style={[styles.container, style]}>
          <Title size="large" style={styles.title}>
            {title}
          </Title>
          <View style={styles.line} />
          <ListContent
            {...listLocation}
            type="modelItem"
            style={styles.listView}
          />
          <View style={styles.buttonWrapper}>
            {noButton && (
              <TransparentButton
                style={styles.button}
                textStyle={{...styles.textStyle, ...styles.noButtonText}}
                buttonStyle={styles.buttonStyle}
                textWeight="bold"
                {...noButton}
              />
            )}
            {yesButton && (
              <Button
                style={styles.button}
                buttonStyle={styles.buttonStyle}
                textStyle={styles.textStyle}
                textWeight="bold"
                {...yesButton}
              />
            )}
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: WHITE_COLOR,
    borderRadius: 15,
    padding: 20,
  },
  icon: {
    width: 150,
    height: 130,
    marginTop: 30,
    marginBottom: 60,
    resizeMode: 'contain',
  },
  title: {
    color: ACCENT_COLOR,
    marginStart: 8,
    fontSize: 18,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  buttonWrapper: {
    marginTop: 16,
    alignItems: 'center',
  },
  button: {
    width: 130,
    marginHorizontal: 10,
  },
  buttonStyle: {
    minHeight: 40,
    justifyContent: 'center',
    width: 182,
    height: 43,
    borderRadius: 22,
    marginTop: 10,
    backgroundColor: ACCENT_COLOR,
  },
  textStyle: {
    color: WHITE_COLOR,
    fontSize: 16,
  },
  noButtonText: {
    color: TEXT_GRAY_COLOR,
  },
  line: {
    height: 1,
    width: '100%',
    backgroundColor: ACCENT_COLOR,
    marginTop: 10,
  },
  listView: {
    marginTop: 10,
    marginLeft: 70,
  },
});
