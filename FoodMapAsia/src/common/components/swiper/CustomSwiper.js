import React from 'react';
import {Image, View, StyleSheet, Dimensions} from 'react-native';
import {LIGHT_GRAY_COLOR_2} from '@resources/palette';
import {PINK_COLOR} from '@resources/palette';
import Swiper from 'react-native-swiper';

const renderItems = (images) => {
  return images.map((item, index) => {
    return (
      <View key={index}>
        <Image style={styles.image} {...item} resizeMode="cover" />
      </View>
    );
  });
};

const CustomSwiper = ({style, images}) => {
  return (
    <View
      style={{
        ...styles.container,
        ...style,
      }}>
      <Swiper
        style={styles.wrapper}
        showsButtons={false}
        dotColor={LIGHT_GRAY_COLOR_2}
        activeDotColor={PINK_COLOR}
        activeDotStyle={styles.activeDotStyle}>
        {renderItems(images)}
      </Swiper>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  wrapper: {
    marginHorizontal: 18,
  },
  image: {
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6,
    // width: Dimensions.get('window').width - 36,
    overflow: 'hidden',
  },
  activeDotStyle: {
    width: 19,
  },
});

export default CustomSwiper;
