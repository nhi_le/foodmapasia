'use strict';
import React, {Component} from 'react';
import {
  FlatList,
  RefreshControl,
  TouchableOpacity,
  FlatListProps,
  StyleSheet,
} from 'react-native';
import {ACCENT_COLOR} from '@resources/palette';
import EmptyView from '../empty/EmptyView';
import {
  NOTIFICATION_EMPTY_TITLE,
  NOTIFICATION_EMPTY_DESCRIPTION,
  MESSENGER_EMPTY_TITLE,
  MESSENGER_EMPTY_DESCRIPTION,
} from '../../../resources/string/strings';
import {
  ICON_NOTIFICATION_EMPTY,
  ICON_CONVERSATION_EMPTY,
} from '../../../resources/images';
import TagItem from './items/TagItem';
import OrderItem from './items/OrderItem';
import AddressItem from './items/AddressItem';
import NotificationItem from './items/NotificationItem';
import CategorieProductItem from './items/CategorieProductItem';
import CartPaymentItem from './items/CartPaymentItem';
import CategorieItem from './items/CategorieItem';
import FavoriteItem from './items/FavoriteItem';
import HomeItem from './items/HomeItem';
import HomeCategorie from './items/HomeCategorie';
import CartItem from './items/CartItem';
import ProductBottomSheetItem from './items/ProductBottomSheetItem';
import ModalItem from './items/ModalItem';
import RelatedItem from './items/RelatedItem';
import ListArticleItem from './items/ListArticleItem';
import ListArticleHightLight from './items/ListArticleHightLight';
import NewsCommentItem from './items/NewsCommentItem';
import NewsTagItem from './items/NewsTagItem';
import DetailOrderItem from './items/DetailOrderItem';
import CategoriesRoot from './items/CategoriesRoot';
import HomeChildItem from './items/HomeChildItem';
import ImageItem from './items/ImageItem';

export const ListContentTypes = {
  'account-category': TagItem,
  item: CategorieProductItem,
  order: OrderItem,
  address: AddressItem,
  notification: NotificationItem,
  cate: CategorieItem,
  favorite: FavoriteItem,
  home: HomeItem,
  relatedItem: RelatedItem,
  homeCategorie: HomeCategorie,
  homeChild: HomeChildItem,
  cart: CartItem,
  cartPayment: CartPaymentItem,
  'product-bottomsheet': ProductBottomSheetItem,
  modelItem: ModalItem,
  listArticle: ListArticleItem,
  listArticleHightLight: ListArticleHightLight,
  newsCommentItem: NewsCommentItem,
  newsTagItem: NewsTagItem,
  detailOrderItem: DetailOrderItem,
  categoriesRoot: CategoriesRoot,
  image: ImageItem,
};

export const ListEmptyViewStates = {
  messenger: {
    title: MESSENGER_EMPTY_TITLE,
    description: MESSENGER_EMPTY_DESCRIPTION,
    icon: {
      svgIcon: ICON_CONVERSATION_EMPTY,
    },
  },
  notification: {
    title: NOTIFICATION_EMPTY_TITLE,
    description: NOTIFICATION_EMPTY_DESCRIPTION,
    icon: {
      svgIcon: ICON_NOTIFICATION_EMPTY,
    },
  },
};

export default class ListContent extends Component<FlatListProps> {
  constructor() {
    super();
    this.state = {
      refreshing: false,
    };
  }

  onRefresh = async () => {
    if (!this.props.onRefresh) {
      return;
    }
    this.setState({refreshing: true});
    await this.props.onRefresh();
    this.setState({refreshing: false});
  };

  renderItem = (itemData) => {
    const {item, index} = itemData;
    const {
      type,
      onItemPress,
      data,
      onActionPress,
      onSelectedItem,
      isRemovable,
    } = this.props;
    const Control = ListContentTypes[type];
    return (
      <TouchableOpacity
        key={`list-content-${type}-${item}-${index}`}
        style={styles.itemContainer}
        onPress={() => onItemPress && onItemPress(item, index)}>
        <Control
          {...item}
          isFirst={index === 0}
          isLast={index === data.length - 1}
          onActionPress={onActionPress}
          onSelectedItem={onSelectedItem}
          isRemovable={isRemovable}
        />
      </TouchableOpacity>
    );
  };

  render() {
    const {
      type,
      inverted = false,
      flatListRef,
      isShowEmptyView = true,
      scrollEnabled = true,
    } = this.props;
    if (
      !this.props.isLoading &&
      (!this.props.data || this.props.data.length === 0) &&
      isShowEmptyView
    ) {
      return (
        <EmptyView
          style={StyleSheet.compose(styles.container, this.props.style)}
          {...ListEmptyViewStates[type]}
        />
      );
    }

    return (
      <FlatList
        ref={flatListRef}
        {...this.props}
        style={StyleSheet.compose(styles.container, this.props.style)}
        data={this.props.data}
        inverted={inverted}
        renderItem={this.renderItem}
        refreshing={this.state.refreshing}
        keyExtractor={(item, index) => `list-content-${type}-${item}-${index}`}
        scrollEnabled={scrollEnabled}
        refreshControl={
          this.props.onRefresh ? (
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.onRefresh}
              tintColor={ACCENT_COLOR}
              colors={[ACCENT_COLOR]}
            />
          ) : null
        }
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  itemContainer: {
    paddingVertical: 0,
  },
});
