/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import Image from '../../../../common/components/image/Image';
import {
  TEXT_GRAY_LIGHT_COLOR,
  BUTTON_LOGIN_COLOR,
  ACCENT_COLOR,
  TEXT_GREEN,
} from '../../../../resources/palette';
import TextContent from '../../text/TextContent';
import HorizontalView from '../../layout/HorizontalView';
import Title from '@common/components/text/Title';
import {currencyConvert} from '@common/components/StringHelper';
export default DetailOrderItem = ({
  style,
  oderItemName,
  oderCombo,
  productCode,
  unitPrice,
  total,
  delivery,
  totalMoney,
  ...props
}) => (
  <View style={{paddingHorizontal: 10}}>
    <Title size={'normal'} style={styles.parentTitle}>
      {oderItemName}
    </Title>
    <Title size={'normal'} style={styles.titleName}>
      {oderCombo}
    </Title>
    <View style={{paddingLeft: 10}}>
      <HorizontalView>
        <TextContent size={'small'} style={styles.address}>
          {productCode.title}
        </TextContent>
        <TextContent size={'small'} style={styles.address}>
          {productCode.value}
        </TextContent>
      </HorizontalView>

      <HorizontalView style={styles.HoriView}>
        <TextContent size={'small'} style={styles.address}>
          {unitPrice.title}
        </TextContent>
        <TextContent size={'small'} style={styles.address}>
          {currencyConvert(unitPrice.value)}
        </TextContent>
      </HorizontalView>

      <HorizontalView style={styles.HoriView}>
        <TextContent size={'small'} style={styles.address}>
          {total.title}
        </TextContent>
        <TextContent size={'small'} style={styles.address}>
          {currencyConvert(total.value)}
        </TextContent>
      </HorizontalView>

      <HorizontalView style={styles.HoriView}>
        <TextContent size={'small'} style={styles.address}>
          {delivery.title}
        </TextContent>
        <TextContent size={'small'} style={styles.address}>
          {currencyConvert(delivery.value)}
        </TextContent>
      </HorizontalView>

      <HorizontalView style={styles.HoriView}>
        <TextContent size={'small'} style={styles.address}>
          {totalMoney.title}
        </TextContent>
        <TextContent size={'small'} style={styles.address}>
          {currencyConvert(totalMoney.value)}
        </TextContent>
      </HorizontalView>
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerInfo: {
    flex: 1,
    padding: 12,
    marginBottom: 10,
  },
  titleName: {
    marginTop: 12,
    color: TEXT_GRAY_LIGHT_COLOR,
  },
  address: {
    color: TEXT_GRAY_LIGHT_COLOR,
    marginTop: 12,
  },
  rule: {
    padding: 10,
    borderBottomColor: '#E0E0E0',
    borderBottomWidth: 1,
  },
  parentTitle: {
    color: BUTTON_LOGIN_COLOR,
    marginTop: 12,
  },
  HoriView: {
    justifyContent: 'space-between',
  },
});
