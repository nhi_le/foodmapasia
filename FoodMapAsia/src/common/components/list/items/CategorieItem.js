import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {BUTTON_LOGIN_COLOR, GRAY_COLOR} from '@resources/palette';
import TextContent from '../../text/TextContent';
import SvgIcon from '../../icon/SvgIcon';

export default CategorieItem = ({
  title,
  isSelected,
  icon,
  onActionPress,
  items,
  ...props
}) => (
  <View style={styles.cateContainer}>
    <SvgIcon
      width={30}
      height={30}
      svgIcon={icon}
      style={styles.iconSelect}
      tintColor={isSelected ? BUTTON_LOGIN_COLOR : GRAY_COLOR}
    />
    <TextContent
      size={'small'}
      style={[
        styles.content,
        {color: isSelected ? BUTTON_LOGIN_COLOR : GRAY_COLOR},
      ]}>
      {title}
    </TextContent>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  cateContainer: {
    alignItems: 'center',
    paddingVertical: 10,
  },
  content: {
    fontSize: 10,
    textAlign: 'center',
    paddingHorizontal: 4,
  },
});
