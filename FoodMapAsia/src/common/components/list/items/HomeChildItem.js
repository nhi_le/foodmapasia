import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import Image from '@common/components/image/Image';
import {
  TEXT_GRAY_LIGHT_COLOR,
  BUTTON_LOGIN_COLOR,
  ACCENT_COLOR,
  RED_COLOR,
} from '@resources/palette';
import TextContent from '../../text/TextContent';
import HorizontalView from '../../layout/HorizontalView';
import {WIDTH_SCREEN} from '@resources/dimensions';
import {currencyConvert} from '@common/components/StringHelper';
import {renderText} from '@common/components/StringHelper';
import {TEXT_SOLD, TEXT_AVAILABLE} from '@resources/string/strings';

const HomeChildItem = ({
  style,
  itemName,
  image,
  priceAlfter,
  priceBefore,
  isSale,
  salePercent,
  variants,
  ...props
}) => {
  let isEmptyQuantity =
    variants &&
    variants.inventory_quantity &&
    parseInt(variants.inventory_quantity) === 0;
  return (
    <View style={styles.container}>
      <View style={styles.parentCard}>
        <View style={styles.imageContainer}>
          <Image
            style={styles.image}
            source={{
              uri:
                image.length > 0
                  ? image[0].src
                  : 'https://calories-info.com/site/assets/files/1182/persymona-hurma-hebanek.650x0.jpg', //default image placeholder
            }}
          />
        </View>
        <View {...props} style={styles.container}>
          <Text
            size="little-smaller"
            numberOfLines={1}
            style={styles.nameStyle}>
            {itemName}
          </Text>
          <HorizontalView style={styles.price}>
            <TextContent size="little-smaller" style={styles.codeContent}>
              {currencyConvert(priceAlfter)}
            </TextContent>
            {priceBefore.toString() !== '0' && (
              <HorizontalView>
                <TextContent style={styles.contentBefore}>
                  {currencyConvert(priceBefore)}
                </TextContent>
                {salePercent.toString().localeCompare('0%') === 0 ? (
                  <View />
                ) : (
                  <TextContent style={isSale ? styles.textDown : styles.textUp}>
                    {isSale ? '- ' + salePercent : salePercent}
                  </TextContent>
                )}
              </HorizontalView>
            )}
          </HorizontalView>
          <HorizontalView style={styles.horizontalViewAvailableStyle}>
            <TextContent
              size="little-smaller"
              style={
                isEmptyQuantity
                  ? styles.availableTextStyle
                  : styles.soldTextStyle
              }>
              {renderText(isEmptyQuantity ? TEXT_SOLD : TEXT_AVAILABLE)}
            </TextContent>
            <TextContent
              numberOfLines={1}
              size="little-smaller"
              style={styles.typeTextStyle}>
              Trái cây nhập khẩu
            </TextContent>
          </HorizontalView>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 6,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  code: {
    flex: 1,
  },
  codeContent: {
    flex: 1,
    color: BUTTON_LOGIN_COLOR,
    fontWeight: 'bold',
    fontSize: 14,
  },
  content: {
    color: TEXT_GRAY_LIGHT_COLOR,
    fontSize: 12,
    alignSelf: 'center',
  },
  contentBefore: {
    color: TEXT_GRAY_LIGHT_COLOR,
    fontSize: 12,
    textDecorationLine: 'line-through',
    marginHorizontal: 6,
  },
  nameStyle: {
    alignSelf: 'center',
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 13,
  },
  image: {
    width: '100%',
    height: 120,
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
  },
  imageContainer: {
    alignItems: 'center',
  },
  parentCard: {
    backgroundColor: 'white',
    borderRadius: 12,
    marginHorizontal: 6,
    width: WIDTH_SCREEN / 2 - 13,
  },

  price: {
    marginTop: 10,
    alignItems: 'center',
  },
  icon: {
    alignSelf: 'center',
    marginLeft: 10,
  },
  textUp: {
    fontSize: 12,
    color: BUTTON_LOGIN_COLOR,
    alignSelf: 'center',
  },
  textDown: {
    fontSize: 12,
    color: RED_COLOR,
    alignSelf: 'center',
  },
  horizontalViewAvailableStyle: {
    marginHorizontal: 4,
    marginTop: 10,
    alignItems: 'center',
  },
  availableTextStyle: {
    color: ACCENT_COLOR,
    fontSize: 14,
  },
  soldTextStyle: {
    color: RED_COLOR,
    fontSize: 14,
  },
  typeTextStyle: {
    flex: 1,
    marginStart: 8,
    fontSize: 12,
    color: TEXT_GRAY_LIGHT_COLOR,
  },
});

export default HomeChildItem;
