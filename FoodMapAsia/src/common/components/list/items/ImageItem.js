import React from 'react';
import {View, StyleSheet} from 'react-native';
import Image from '@common/components/image/Image';
import {WHITE_COLOR} from '@resources/palette';
const ImageItem = ({src, isChoose, ...props}) => {
  return (
    <View style={styles.container}>
      <Image
        style={isChoose ? styles.chooseStyle : styles.notChooseStyle}
        source={{uri: src}}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: WHITE_COLOR,
    paddingStart: 12,
  },
  notChooseStyle: {
    width: 85,
    height: 60,
    borderRadius: 10,
  },
  chooseStyle: {
    width: 85,
    height: 60,
    borderRadius: 10,
  },
});
export default ImageItem;
