import React from 'react';
import {View, StyleSheet} from 'react-native';
import {ACCENT_COLOR, COLOR_READ, COLOR_UNREAD} from '@resources/palette';
import TextContent from '@common/components/text/TextContent';
import Image from '@common/components/image/Image';
import Title from '@common/components/text/Title';
const ListArticleHightLight = ({
  id,
  title,
  image,
  published_at,
  content_preview,
  author,
  ...props
}) => {
  return (
    <View View style={styles.nextItems}>
      <Image style={styles.imagesNext} source={{uri: image}} />
      <View style={{flex: 1}}>
        <TextContent size="small" style={styles.textItemNext}>
          {published_at}{' '}
        </TextContent>
        <View style={styles.titleItemNext}>
          <Title size="large">{title}</Title>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  //firt
  firstItem: {
    height: 350,
  },
  ImageFirst: {
    width: '100%',
    height: 230,
    marginBottom: 10,
  },
  title: {
    color: ACCENT_COLOR,
    marginBottom: 10,
  },
  public: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  text: {
    color: COLOR_READ,
  },
  textUser: {
    color: COLOR_READ,
    fontWeight: 'bold',
  },
  textPreview: {
    flexShrink: 1,
    height: 40,
    color: COLOR_UNREAD,
  },
  //Next
  nextItems: {
    height: 70,
    flexDirection: 'row',
    marginBottom: 10,
  },
  imagesNext: {
    width: '30%',
    height: '100%',
    marginEnd: 10,
  },
  textItemNext: {
    color: COLOR_READ,
    marginTop: -3,
    flex: 0.2,
  },
  titleItemNext: {
    flex: 0.8,
    justifyContent: 'center',
  },
});
export default ListArticleHightLight;
