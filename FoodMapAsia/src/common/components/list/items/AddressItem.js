import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {TEXT_GRAY_LIGHT_COLOR, BUTTON_LOGIN_COLOR} from '@resources/palette';
import TextContent from '../../text/TextContent';
import HorizontalView from '../../layout/HorizontalView';
import SvgIcon from '../../icon/SvgIcon';
import {ICON_CANCEL, ICON_EDIT} from '@resources/images';
import {ACCENT_COLOR} from '@resources/palette';

export default AddressItem = ({
  style,
  code,
  isDefault,
  receiver,
  address,
  numberPhone,
  onActionPress,
  id,
  ...props
}) => (
  <View {...props} style={[styles.container]}>
    <HorizontalView>
      <HorizontalView style={styles.code}>
        <TextContent
          numberOfLines={1}
          style={
            isDefault === 'true'
              ? styles.codeDefaultContent
              : styles.codeContent
          }>
          {receiver}
        </TextContent>
        {isDefault === 'true' && (
          <TextContent style={styles.contentDefault}>
            {' (Mặc định)'}
          </TextContent>
        )}
      </HorizontalView>
      <TouchableOpacity
        style={styles.touchArea}
        onPress={() => {
          onActionPress && onActionPress('edit', id);
        }}>
        <SvgIcon
          height={16}
          width={16}
          style={styles.iconEditStyle}
          svgIcon={ICON_EDIT}
        />
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.touchArea}
        onPress={() => {
          onActionPress && onActionPress('cancel', id);
        }}>
        <SvgIcon height={14} width={14} svgIcon={ICON_CANCEL} />
      </TouchableOpacity>
    </HorizontalView>
    <HorizontalView style={styles.containerText}>
      <TextContent style={styles.content}>{'Người nhận: '}</TextContent>
      <TextContent style={styles.contentReceiver}>{receiver}</TextContent>
    </HorizontalView>
    <HorizontalView style={styles.containerText}>
      <TextContent style={styles.content}>{'Địa chỉ: '}</TextContent>
      <TextContent style={styles.content}>{address}</TextContent>
    </HorizontalView>
    <HorizontalView style={styles.containerText}>
      <TextContent style={styles.content}>{'Số điện thoại: '}</TextContent>
      <TextContent style={styles.content}>{numberPhone}</TextContent>
    </HorizontalView>
  </View>
);

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 12,
    marginBottom: 12,
  },
  code: {
    flex: 1,
    alignItems: 'center',
    alignContent: 'center',
  },
  codeDefaultContent: {
    flex: 1,
    color: BUTTON_LOGIN_COLOR,
    fontWeight: 'bold',
  },
  codeContent: {
    color: TEXT_GRAY_LIGHT_COLOR,
    fontWeight: 'bold',
    flex: 1,
  },
  content: {
    marginTop: 10,
    color: TEXT_GRAY_LIGHT_COLOR,
  },
  contentReceiver: {
    marginTop: 10,
    color: ACCENT_COLOR,
  },
  contentDefault: {
    color: TEXT_GRAY_LIGHT_COLOR,
    marginEnd: 8,
  },
  containerText: {
    marginLeft: 12,
    marginRight: 60,
  },
  touchArea: {
    width: 25,
    height: 30,
  },
  iconEditStyle: {
    marginRight: 8,
  },
});
