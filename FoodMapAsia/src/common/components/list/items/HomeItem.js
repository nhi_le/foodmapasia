import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import Image from '@common/components/image/Image';
import {
  TEXT_GRAY_LIGHT_COLOR,
  BUTTON_LOGIN_COLOR,
  ACCENT_COLOR,
  RED_COLOR,
  GRAY_COLOR,
  WHITE_COLOR,
} from '@resources/palette';
import TextContent from '../../text/TextContent';
import HorizontalView from '../../layout/HorizontalView';
import {WIDTH_SCREEN} from '@resources/dimensions';
import {currencyConvert} from '@common/components/StringHelper';
import {renderText} from '@common/components/StringHelper';
import {TEXT_DETAIL_MORE} from '@resources/string/strings';
import Button from '@common/components/button/Button';

var percent = 0;

const HomeItem = ({
  style,
  id,
  itemName,
  image,
  priceAlfter,
  priceBefore,
  isSale,
  salePercent,
  variants,
  onActionPress,
  ...props
}) => {
  percent = parseInt(salePercent.replace('%', ''));
  return (
    <View style={styles.container}>
      <View style={styles.parentCard}>
        <View style={styles.imageContainer}>
          <Image
            style={styles.image}
            source={{
              uri:
                image.length > 0
                  ? image[0].src
                  : 'https://calories-info.com/site/assets/files/1182/persymona-hurma-hebanek.650x0.jpg',
            }}
          />
        </View>
        <View>
          <HorizontalView style={styles.horizontalViewLinePercentStyle}>
            <View
              style={[
                styles.linePercentActiveStyle,
                {width: ((WIDTH_SCREEN / 2) * percent) / 100},
              ]}
            />
            <View style={styles.linePercentNoActiveStyle} />
          </HorizontalView>
          <View
            style={[
              styles.circlePercentViewStyle,
              {marginLeft: ((WIDTH_SCREEN / 2) * percent) / 100},
            ]}>
            <TextContent style={styles.textPercentStyle}>
              {salePercent}
            </TextContent>
          </View>
        </View>
        <View {...props} style={styles.container}>
          <Text
            size="little-smaller"
            numberOfLines={1}
            style={styles.nameStyle}>
            {itemName}
          </Text>
          <HorizontalView style={styles.price}>
            <TextContent size="little-smaller" style={styles.codeContent}>
              {currencyConvert(priceAlfter, false)}
            </TextContent>
            <TextContent style={styles.contentBefore}>đ</TextContent>
            <TextContent style={styles.codeContent}>
              / {variants[0].title}
            </TextContent>
          </HorizontalView>
        </View>
      </View>
      <Button
        text={renderText(TEXT_DETAIL_MORE)}
        textStyle={styles.saleTextBanner}
        style={styles.saleBanner}
        buttonStyle={styles.buttonDetailStyle}
        onPress={() => {
          onActionPress && onActionPress(id);
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 6,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  code: {
    flex: 1,
  },
  codeContent: {
    fontWeight: 'bold',
    fontSize: 14,
  },
  content: {
    color: TEXT_GRAY_LIGHT_COLOR,
    fontSize: 12,
    alignSelf: 'center',
  },
  contentBefore: {
    fontWeight: 'bold',
    fontSize: 14,
    textDecorationLine: 'underline',
  },
  nameStyle: {
    alignSelf: 'center',
    fontWeight: 'bold',
    color: ACCENT_COLOR,
    textAlign: 'center',
    fontSize: 16,
  },
  image: {
    width: '100%',
    height: 100,
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
  },
  imageContainer: {
    alignItems: 'center',
  },
  parentCard: {
    backgroundColor: 'white',
    borderRadius: 12,
    marginHorizontal: 6,
    height: 200,
    width: WIDTH_SCREEN / 2 - 13,
  },
  price: {
    marginVertical: 5,
    alignItems: 'center',
  },
  icon: {
    alignSelf: 'center',
    marginLeft: 10,
  },
  textUp: {
    fontSize: 12,
    color: BUTTON_LOGIN_COLOR,
    alignSelf: 'center',
  },
  textDown: {
    fontSize: 12,
    color: RED_COLOR,
    alignSelf: 'center',
  },
  multiPrice: {
    alignContent: 'flex-start',
  },
  multiPriceText: {
    fontSize: 12,
    fontWeight: 'bold',
    color: TEXT_GRAY_LIGHT_COLOR,
  },
  saleBanner: {
    position: 'relative',
    alignSelf: 'center',
    marginTop: -20,
    width: WIDTH_SCREEN / 2 - 50,
    height: 40,
  },
  saleTextBanner: {
    fontWeight: 'bold',
    fontSize: 14,
    color: 'white',
    alignSelf: 'center',
  },
  buttonDetailStyle: {
    borderRadius: 40,
  },
  priceContainerStyle: {
    marginTop: 2,
  },
  linePercentActiveStyle: {
    backgroundColor: ACCENT_COLOR,
  },
  linePercentNoActiveStyle: {
    backgroundColor: GRAY_COLOR,
  },
  horizontalViewLinePercentStyle: {
    height: 5,
    width: '100%',
  },
  circlePercentViewStyle: {
    height: 32,
    width: 32,
    borderRadius: 16,
    borderWidth: 2,
    borderColor: ACCENT_COLOR,
    justifyContent: 'center',
    position: 'relative',
    marginTop: -20,
    backgroundColor: WHITE_COLOR,
  },
  textPercentStyle: {
    fontSize: 12,
    textAlign: 'center',
    alignSelf: 'center',
    fontWeight: 'bold',
  },
});

export default HomeItem;
