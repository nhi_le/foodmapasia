import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {
  BORDER_COLOR,
  WHITE_COLOR,
  BUTTON_LOGIN_COLOR,
  COLOR_READ,
} from '../../../../resources/palette';
import TextContent from '../../text/TextContent';
import HorizontalView from '@common/components/layout/HorizontalView';

export default NewsCommentItem = ({
  style,
  author,
  created_at,
  content,
  ...props
}) => (
  <View style={styles.commentContainer}>
    <HorizontalView style = {styles.nameTimeRow}>
      <TextContent style={styles.author}>{author}</TextContent>
      <TextContent style={styles.time}>{created_at}</TextContent>
    </HorizontalView>
    <TextContent style={styles.content}>{content}</TextContent>
  </View>
);

const styles = StyleSheet.create({
  commentContainer: {
    marginHorizontal: 10,
    marginTop: 18,
  },
  author: {
    color: COLOR_READ,
    fontSize: 16,
    fontWeight: 'bold',
  },
  nameTimeRow:{
    justifyContent:'space-between',
    alignItems:'center'
  },
  time: {
    fontSize: 12,
    color: COLOR_READ,
    marginLeft: 20,
    fontStyle:"italic"
  },
  content: {
    marginHorizontal: 10,
    marginTop: 8,
    color: COLOR_READ,
    fontSize: 12,
  },
});
