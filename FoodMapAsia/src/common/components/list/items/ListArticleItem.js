import React from 'react';
import {View, StyleSheet} from 'react-native';
import {ACCENT_COLOR, COLOR_READ, COLOR_UNREAD} from '@resources/palette';
import TextContent from '@common/components/text/TextContent';
import Image from '@common/components/image/Image';
import Title from '@common/components/text/Title';
const ListArticleItem = ({
  id,
  title,
  image,
  published_at,
  content_preview,
  author,
  ...props
}) => {
  let text1 = 'Đăng bởi';
  let text3 = 'lúc';
  if (id == 0)
    return (
      <View style={styles.firstItem}>
        <Image style={styles.ImageFirst} source={{uri: image}} />
        <Title size="large" style={styles.title}>
          {title.toUpperCase()}
        </Title>
        <View style={styles.public}>
          <TextContent size="small" style={styles.text}>
            {text1}{' '}
          </TextContent>
          <TextContent size="small" style={styles.textUser}>
            {author}{' '}
          </TextContent>
          <TextContent size="small" style={styles.text}>
            {text3}{' '}
          </TextContent>
          <TextContent size="small" style={styles.text}>
            {published_at}{' '}
          </TextContent>
        </View>
        <View style={{flexDirection: 'row'}}>
          <TextContent size="large" style={styles.textPreview}>
            {content_preview[0].toUpperCase() +
              content_preview.slice(1).toLowerCase()}
          </TextContent>
        </View>
      </View>
    );
  else {
    return (
      <View View style={styles.nextItems}>
        <Image style={styles.imagesNext} source={{uri: image}} />
        <View style={{flex: 1}}>
          <TextContent size="small" style={styles.textItemNext}>
            {published_at}{' '}
          </TextContent>
          <View style={styles.titleItemNext}>
            <Title size="large">{title}</Title>
          </View>
        </View>
      </View>
    );
  }
};
const styles = StyleSheet.create({
  //firt
  firstItem: {
    height: 350,
  },
  ImageFirst: {
    width: '100%',
    height: 230,
    marginBottom: 10,
  },
  title: {
    color: ACCENT_COLOR,
    marginBottom: 10,
  },
  public: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  text: {
    color: COLOR_READ,
  },
  textUser: {
    color: COLOR_READ,
    fontWeight: 'bold',
  },
  textPreview: {
    flexShrink: 1,
    height: 40,
    color: COLOR_UNREAD,
  },
  //Next
  nextItems: {
    height: 70,
    flexDirection: 'row',
    marginBottom: 10,
  },
  imagesNext: {
    width: '30%',
    height: '100%',
    marginEnd: 10,
  },
  textItemNext: {
    color: COLOR_READ,
    marginTop: -3,
    flex: 0.2,
  },
  titleItemNext: {
    flex: 0.8,
    justifyContent: 'center',
  },
});
export default ListArticleItem;
