import React from 'react';
import {View, StyleSheet} from 'react-native';
import TextContent from '../../text/TextContent';
import SvgIcon from '../../icon/SvgIcon';
import {
  TEXT_GRAY_LIGHT_COLOR,
  BUTTON_LOGIN_COLOR,
  GRAY_COLOR,
} from '@resources/palette';

export default HomeCategorie = ({isSelected, title, icon, ...props}) => (
  <View style={styles.cateContainer}>
    <View style={[styles.redContainer, {backgroundColor: 'white'}]}>
      <SvgIcon
        width={48}
        height={48}
        svgIcon={icon}
        style={{alignSelf: 'center'}}
        tintColor={isSelected ? BUTTON_LOGIN_COLOR : GRAY_COLOR}
      />
    </View>
    <TextContent numberOfLines={2} size={'small'} style={styles.content}>
      {title}
    </TextContent>
  </View>
);

const styles = StyleSheet.create({
  cateContainer: {
    flex: 1,
    alignItems: 'center',
    marginTop: 20,
    marginHorizontal: 4,
    width: 70,
  },
  content: {
    marginTop: 5,
    color: TEXT_GRAY_LIGHT_COLOR,
    textAlign: 'center',
  },
  redContainer: {
    backgroundColor: BUTTON_LOGIN_COLOR,
    width: 48,
    height: 48,
    borderRadius: 30,
    alignItems: 'flex-start',
    justifyContent: 'center',
    alignSelf: 'center',
  },
});
