import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {
  TEXT_GRAY_LIGHT_COLOR,
  BUTTON_LOGIN_COLOR,
} from '../../../../resources/palette';
import TextContent from '../../text/TextContent';
import HorizontalView from '../../layout/HorizontalView';
import SvgIcon from '../../icon/SvgIcon';
import {ICON_RIGHT_ARROW} from '../../../../resources/images';
import {currencyConvert} from '@common/components/StringHelper';
export default OrderItem = ({
  style,
  code,
  dateOrder,
  total,
  status,
  delivery,
  onActionPress,
  ...props
}) => (
  <View {...props} style={[styles.container]}>
    <HorizontalView>
      <HorizontalView style={styles.code}>
        <TextContent style={styles.codeContent}>{'Mã đơn hàng: '}</TextContent>
        <TextContent style={[styles.codeContent, {fontWeight: 'bold'}]}>
          {code}
        </TextContent>
      </HorizontalView>
      <TouchableOpacity
        onPress={() => {
          onActionPress && onActionPress('hello', code);
        }}>
        <SvgIcon height={16} width={16} svgIcon={ICON_RIGHT_ARROW} />
      </TouchableOpacity>
    </HorizontalView>
    <HorizontalView>
      <TextContent style={styles.content}>{'Ngày đặt: '}</TextContent>
      <TextContent style={styles.content}>{dateOrder}</TextContent>
    </HorizontalView>
    <HorizontalView>
      <TextContent style={styles.content}>{'Tổng tiền: '}</TextContent>
      <TextContent style={styles.content}>{currencyConvert(total)}</TextContent>
    </HorizontalView>
    <HorizontalView>
      <TextContent style={styles.content}>
        {'Trạng thái thanh toán: '}
      </TextContent>
      <TextContent style={styles.content}>{status}</TextContent>
    </HorizontalView>
    <HorizontalView>
      <TextContent style={styles.content}>{'Vận chuyển: '}</TextContent>
      <TextContent style={styles.content}>{delivery}</TextContent>
    </HorizontalView>
  </View>
);

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 12,
    marginBottom: 10,
  },
  code: {
    flex: 1,
  },
  codeContent: {
    color: BUTTON_LOGIN_COLOR,
  },
  content: {
    marginTop: 10,
    color: TEXT_GRAY_LIGHT_COLOR,
  },
});
