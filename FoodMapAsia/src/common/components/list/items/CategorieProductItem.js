import React from 'react';
import {View, StyleSheet} from 'react-native';
import {
  TEXT_GRAY_LIGHT_COLOR,
  BUTTON_LOGIN_COLOR,
} from '../../../../resources/palette';
import TextContent from '../../text/TextContent';
import HorizontalView from '../../layout/HorizontalView';
import honestImg from '../../../../../assets/image/honest.png';
import {ICON_UP, ICON_DOWN} from '@resources/images';
import SvgIcon from '../../icon/SvgIcon';
import {currencyConvert} from '@common/components/StringHelper';
import Image from '../../image/Image';

const renderPrice = (variants) => {
  return variants.map((item, index) => {
    return (
      <HorizontalView style={styles.multiPrice}>
        <TextContent style={styles.content}>{item.title}</TextContent>
        <TextContent style={styles.content}>{': '}</TextContent>
        <TextContent style={styles.multiPrice}>
          {currencyConvert(item.price)}
        </TextContent>
      </HorizontalView>
    );
  });
};

export default CategorieProductItem = ({
  style,
  itemName,
  priceAfter,
  priceBefore,
  img,
  itemType,
  isSale,
  salePercent,
  variants,
  ...props
}) => (
  <View style={styles.parentCard}>
    <HorizontalView>
      <View style={styles.imageContainer}>
        <Image
          style={styles.image}
          source={img === '' ? honestImg : {uri: img}}
        />
        {salePercent.toString().localeCompare('0%') === 0 ? (
          <View />
        ) : (
          <View style={styles.saleBanner}>
            <TextContent style={styles.saleTextBanner}>
              {salePercent}
            </TextContent>
          </View>
        )}
      </View>

      <View {...props} style={styles.container}>
        <HorizontalView>
          <HorizontalView style={styles.code}>
            <TextContent numberOfLines={2} style={styles.itemName}>
              {itemName}
            </TextContent>
          </HorizontalView>
        </HorizontalView>
        <View style={styles.multiPriceContainer}>
          <HorizontalView style={styles.beforeAlterContainer}>
            <TextContent style={styles.priceAlterText}>
              {currencyConvert(priceAfter)}
            </TextContent>
          </HorizontalView>
          {priceBefore.toString() !== '0' && (
            <TextContent size={'small'} style={styles.priceBeforeText}>
              {currencyConvert(priceBefore)}
            </TextContent>
          )}
          <HorizontalView style={styles.multiPriceHori}>
            <View>{renderPrice(variants)}</View>
          </HorizontalView>
        </View>
        <HorizontalView style={styles.upDownPercent}>
          {salePercent.toString().localeCompare('0%') === 0 ? (
            <View />
          ) : (
            <HorizontalView>
              <SvgIcon
                style={styles.icon}
                width={8}
                height={8}
                svgIcon={isSale ? ICON_DOWN : ICON_UP}
              />
              <TextContent style={isSale ? styles.textDown : styles.textUp}>
                {salePercent}
              </TextContent>
            </HorizontalView>
          )}
        </HorizontalView>
      </View>
    </HorizontalView>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 8,
    justifyContent: 'center',
  },
  content: {
    color: TEXT_GRAY_LIGHT_COLOR,
    fontSize: 12,
    alignSelf: 'center',
  },
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
    borderRadius: 12,
  },
  imageContainer: {
    flex: 1,
  },
  parentCard: {
    backgroundColor: 'white',
    borderRadius: 12,
    marginHorizontal: 8,
    marginBottom: 8,
    height: 140,
    justifyContent: 'center',
  },
  price: {
    paddingRight: 15,
  },
  muiltiPrice: {
    fontSize: 12,
    fontWeight: 'bold',
    color: TEXT_GRAY_LIGHT_COLOR,
  },
  multiPriceContainer: {
    alignItems: 'flex-start',
    alignContent: 'flex-start',
  },
  saleBanner: {
    backgroundColor: BUTTON_LOGIN_COLOR,
    position: 'absolute',
    alignSelf: 'flex-end',
    width: 50,
    height: 20,
    borderTopRightRadius: 12,
    borderBottomLeftRadius: 12,
  },
  saleTextBanner: {
    fontWeight: 'bold',
    fontSize: 12,
    color: 'white',
    alignSelf: 'center',
  },
  itemName: {
    fontWeight: 'bold',
    fontSize: 12,
    marginRight: 7,
    alignSelf: 'flex-start',
  },
  priceAlterText: {
    color: BUTTON_LOGIN_COLOR,
    fontSize: 14,
    fontWeight: 'bold',
  },
  priceBeforeText: {
    color: TEXT_GRAY_LIGHT_COLOR,
    fontSize: 12,
    marginBottom: 4,
    textDecorationLine: 'line-through',
  },
  beforeAlterContainer: {
    alignItems: 'flex-start',
    marginTop: 2,
    marginBottom: 4,
  },
  icon: {
    alignSelf: 'center',
  },
  upDownPercent: {
    marginTop: 3,
  },
  multiPrice: {
    color: TEXT_GRAY_LIGHT_COLOR,
    fontSize: 12,
    fontWeight: 'bold',
  },
  multiPriceTitle: {
    color: TEXT_GRAY_LIGHT_COLOR,
    fontSize: 12,
  },
  textUp: {
    fontWeight: 'bold',
    fontSize: 12,
    color: BUTTON_LOGIN_COLOR,
    alignSelf: 'center',
  },
  textDown: {
    fontWeight: 'bold',
    fontSize: 12,
    color: '#049347',
    alignSelf: 'center',
  },
});
