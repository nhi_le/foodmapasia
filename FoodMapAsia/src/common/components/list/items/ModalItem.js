import React from 'react';
import {View, StyleSheet} from 'react-native';
import {
  TEXT_GRAY_LIGHT_COLOR,
  ACCENT_COLOR,
  COLOR_READ,
} from '@resources/palette';
import TextContent from '../../text/TextContent';
import HorizontalView from '../../layout/HorizontalView';
import SvgIcon from '../../icon/SvgIcon';
import {ICON_RADIO_CHECKED, ICON_RADIO_UNCHECKED} from '@resources/images';

const ModalItem = ({style, title, description, isSelected, type, ...props}) => {
  return (
    <View {...props} style={styles.container}>
      <SvgIcon
        width={18}
        height={18}
        svgIcon={isSelected ? ICON_RADIO_CHECKED : ICON_RADIO_UNCHECKED}
        style={styles.iconSelect}
      />
      <HorizontalView>
        <TextContent style={styles.TextContent}>{title}</TextContent>
        <TextContent>{description}</TextContent>
      </HorizontalView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
    flex: 1,
    flexDirection: 'row',
  },
  listItem: {
    flexDirection: 'row',
    flex: 1,
  },
  flexColumn: {
    marginBottom: 10,
    flexDirection: 'column',
    flex: 1,
  },
  flexRow: {
    marginBottom: 10,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  flatList: {
    marginTop: 10,
  },
  iconSelect: {
    marginEnd: 10,
    marginTop: 3,
  },
  TextContent: {
    fontSize: 16,
    color: COLOR_READ,
  },
  textPrice: {
    color: TEXT_GRAY_LIGHT_COLOR,
    fontWeight: 'bold',
  },
});
export default ModalItem;
