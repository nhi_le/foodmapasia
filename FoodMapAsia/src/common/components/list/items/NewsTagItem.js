import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {COLOR_READ, TAG_BACKGROUND_COLOR} from '../../../../resources/palette';
import TextContent from '../../text/TextContent';
import HorizontalView from '@common/components/layout/HorizontalView';

export default NewsTagItem = ({style, tag, ...props}) => (
  <View style={styles.tagItems}>
    <TextContent size="normal" style={styles.tagItemsText}>
      {tag}
    </TextContent>
  </View>
);

const styles = StyleSheet.create({
  tagItems: {
    margin: 5,
    backgroundColor: TAG_BACKGROUND_COLOR,
    alignSelf: 'center',
    borderRadius: 5,
  },
  tagItemsText: {
    color: COLOR_READ,
    fontSize: 14,
    textAlign: 'center',
    fontWeight: 'bold',
    padding: 5,
  },
});
