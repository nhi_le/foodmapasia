import React from 'react';
import {View, StyleSheet} from 'react-native';
import {BUTTON_LOGIN_COLOR, GRAY_COLOR} from '@resources/palette';
import TextContent from '../../text/TextContent';
import SvgIcon from '../../icon/SvgIcon';
import ListContent from '../ListContent';
import {
  ICON_UP_UNSELECTED,
  ICON_ARROW_DOWN_SELECTED,
} from '../../../../resources/images';
import HorizontalView from '../../layout/HorizontalView';
import {LIGHT_GRAY_COLOR} from '../../../../resources/palette';

export default CategorieItem = ({
  title,
  isSelected,
  icon,
  onActionPress,
  child_collection,
  ...props
}) => (
  <View style={styles.cateContainer}>
    <SvgIcon
      width={40}
      height={40}
      svgIcon={icon}
      style={styles.iconSelect}
      tintColor={isSelected ? BUTTON_LOGIN_COLOR : GRAY_COLOR}
    />
    <HorizontalView>
      <TextContent
        size={'small'}
        style={[
          styles.content,
          {color: isSelected ? BUTTON_LOGIN_COLOR : GRAY_COLOR},
        ]}>
        {title}
      </TextContent>
      <SvgIcon
        width={10}
        height={10}
        svgIcon={isSelected ? ICON_ARROW_DOWN_SELECTED : ICON_UP_UNSELECTED}
        style={styles.iconDropdown}
        tintColor={isSelected ? BUTTON_LOGIN_COLOR : GRAY_COLOR}
      />
    </HorizontalView>

    {isSelected && (
      <ListContent
        {...child_collection}
        onItemPress={onActionPress}
        type={'cate'}
        style={{marginTop: 10}}
      />
    )}
    <View style={styles.rule} />
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  cateContainer: {
    alignItems: 'center',
    marginVertical: 10,
    marginHorizontal:10
  
  },
  content: {
    textAlign: 'center',
    marginTop: 4,
    flex: 1,
    paddingLeft: 4,
  },
  rule: {
    height: 1,
    width: '85%',
    borderBottomColor: LIGHT_GRAY_COLOR,
    borderBottomWidth: 1,
    marginTop: 10,
  },

  iconDropdown: {
    width: 20,
    alignContent: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
  },
});
