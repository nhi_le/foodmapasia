/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import Image from '../../../../common/components/image/Image';
import {
  TEXT_GRAY_LIGHT_COLOR,
  BUTTON_LOGIN_COLOR,
  ACCENT_COLOR,
  TEXT_GREEN,
} from '../../../../resources/palette';
import TextContent from '../../text/TextContent';
import Input from '../../input/Input';
import HorizontalView from '../../layout/HorizontalView';
import SvgIcon from '../../icon/SvgIcon';
import {WIDTH_SCREEN} from '@resources/dimensions';
import {
  ICON_INCREASE,
  ICON_DECREASE,
  ICON_DOWN,
  ICON_CANCEL,
  ICON_UP,
} from '../../../../../src/resources/images';
import {currencyConvert} from '@common/components/StringHelper';

const renderPrice = (variants) => {
  return variants.map((item, index) => {
    return (
      <HorizontalView style={styles.multiPrice}>
        <TextContent style={styles.content}>{item.title}</TextContent>
        <TextContent style={styles.content}>{': '}</TextContent>
        <TextContent size={'small'} style={styles.contentBold}>
          {currencyConvert(item.price)}
        </TextContent>
      </HorizontalView>
    );
  });
};

export default FavoriteItem = ({
  style,
  id,
  itemName,
  image,
  count,
  isDiscount,
  discount,
  priceAfter,
  priceBefore,
  onActionPress,
  variants,
  ...props
}) => (
  <View style={styles.parentCard}>
    <HorizontalView>
      <View style={styles.imageContainer}>
        <Image style={styles.image} source={{uri: image}} />
        {discount && !discount.toString().localeCompare('0%') === 0 ? (
          <View style={styles.saleBanner}>
            <TextContent style={styles.saleTextBanner}>{discount}</TextContent>
          </View>
        ) : (
          <View />
        )}
      </View>

      <View {...props} style={[styles.container]}>
        <HorizontalView>
          <HorizontalView style={styles.code}>
            <View>
              <TextContent numberOfLines={2} style={styles.itemNameText}>
                {itemName}
              </TextContent>
            </View>
            <View style={{flex: 1}}>
              <TouchableOpacity
                style={{
                  marginRight: 12,
                  alignItems: 'flex-end',
                  height: 30,
                  width: 30,
                  alignSelf: 'flex-end',
                }}
                onPress={() => {
                  onActionPress && onActionPress('cancel', id);
                }}>
                <SvgIcon width={12} height={12} svgIcon={ICON_CANCEL} />
              </TouchableOpacity>
            </View>
          </HorizontalView>
        </HorizontalView>
        <View>
          <HorizontalView>
            <TextContent style={styles.codeContent}>
              {currencyConvert(priceAfter)}
            </TextContent>
            {priceBefore.toString() !== '0' && (
              <TextContent style={styles.contentBefore}>
                {currencyConvert(priceBefore)}
              </TextContent>
            )}
            {discount.localeCompare('0%') === 0 ? (
              <View />
            ) : (
              <HorizontalView>
                <SvgIcon
                  width={12}
                  height={12}
                  svgIcon={isDiscount ? ICON_DOWN : ICON_UP}
                  style={styles.promotion}
                />
                <TextContent
                  style={
                    isDiscount
                      ? styles.textContentDiscount
                      : styles.textContentUpcount
                  }>
                  {discount}
                </TextContent>
              </HorizontalView>
            )}
          </HorizontalView>
        </View>
        <View style={styles.bottomView}>
          <HorizontalView>
            <View style={styles.priceContainer}>{renderPrice(variants)}</View>
            <View style={styles.parentView}>
              <View style={styles.quantityContainer}>
                <HorizontalView>
                  <TouchableOpacity
                    style={styles.button}
                    onPress={() => {
                      onActionPress && onActionPress('decrease', id);
                    }}>
                    <SvgIcon
                      style={styles.button}
                      width={28}
                      height={28}
                      svgIcon={ICON_DECREASE}
                    />
                  </TouchableOpacity>
                  <View style={{alignSelf: 'center', marginBottom: 5}}>
                    <Input
                      keyboardType={'numeric'}
                      inputStyle={styles.amountContainer}
                      onChangeText={(text) => {
                        onActionPress(text, id);
                      }}>
                      {count}
                    </Input>
                  </View>
                  <TouchableOpacity
                    style={styles.button}
                    onPress={() => {
                      onActionPress && onActionPress('increase', id);
                    }}>
                    <SvgIcon
                      style={styles.button}
                      width={28}
                      height={28}
                      svgIcon={ICON_INCREASE}
                    />
                  </TouchableOpacity>
                </HorizontalView>
              </View>
            </View>
          </HorizontalView>
        </View>
      </View>
    </HorizontalView>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginLeft: 5,
    marginTop: 10,
    marginBottom: 10,
  },
  code: {
    flex: 1,
    justifyContent: 'space-between',
  },
  codeContent: {
    color: BUTTON_LOGIN_COLOR,
    fontWeight: 'bold',
    fontSize: 14,
  },
  content: {
    color: TEXT_GRAY_LIGHT_COLOR,
    fontSize: 12,
    alignSelf: 'center',
    padding: 1,
  },
  contentBold: {
    color: TEXT_GRAY_LIGHT_COLOR,
    fontSize: 12,
    fontWeight: 'bold',
  },
  contentBefore: {
    color: TEXT_GRAY_LIGHT_COLOR,
    marginLeft: 5,
    fontSize: 12,
    alignSelf: 'center',
    textDecorationLine: 'line-through',
  },
  amountItem: {
    color: TEXT_GRAY_LIGHT_COLOR,
    fontSize: 16,
    alignSelf: 'center',
  },
  image: {
    height: '100%',
    width: 120,
  },

  parentCard: {
    backgroundColor: 'white',
    marginVertical: 5,
    height: 140,
  },

  amountContainer: {
    textAlign: 'center',
    width: 50,
    borderRadius: 5,
    marginHorizontal: 2,
    borderColor: BUTTON_LOGIN_COLOR,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  priceContainer: {},
  parentView: {
    paddingLeft: 10,
    flexDirection: 'column',
    flex: 1,
  },
  button: {
    alignSelf: 'center',
    marginHorizontal: 2,
  },
  bottomView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  promotion: {
    marginLeft: 5,
    alignSelf: 'center',
  },
  saleBanner: {
    backgroundColor: BUTTON_LOGIN_COLOR,
    position: 'absolute',
    alignSelf: 'flex-end',
    width: 42,
    height: 18,
    borderTopRightRadius: 12,
    borderBottomLeftRadius: 12,
  },
  saleTextBanner: {
    fontWeight: 'bold',
    fontSize: 12,
    color: 'white',
    alignSelf: 'center',
  },
  imageContainer: {
    marginLeft: 12,
    marginTop: 12,
    marginBottom: 12,
  },
  textContentDiscount: {marginLeft: 3, color: TEXT_GREEN},
  textContentUpcount: {marginLeft: 3, color: ACCENT_COLOR},
  quantityContainer: {
    alignItems: 'flex-end',
    marginRight: 12,
    alignSelf: 'flex-end',
  },
  priceRow: {
    alignItems: 'center',
  },
  cancelButton: {
    marginRight: 12,
  },
  itemNameText: {
    fontWeight: 'bold',
    width: WIDTH_SCREEN / 2,
  },
});
