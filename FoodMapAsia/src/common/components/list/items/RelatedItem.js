import React from 'react';
import {View, StyleSheet} from 'react-native';
import Image from '../../../../common/components/image/Image';
import {
  TEXT_GRAY_LIGHT_COLOR,
  BUTTON_LOGIN_COLOR,
} from '../../../../resources/palette';
import {ICON_UP, ICON_DOWN} from '@resources/images';
import TextContent from '../../text/TextContent';
import HorizontalView from '../../layout/HorizontalView';
import SvgIcon from '../../icon/SvgIcon';
import {WIDTH_SCREEN} from '@resources/dimensions';
import {currencyConvert} from '@common/components/StringHelper';

const renderPrice = (variants) => {
  return variants.map((item, index) => {
    return (
      <HorizontalView style={styles.multiPrice}>
        <TextContent style={styles.content}>{item.title}</TextContent>
        <TextContent style={styles.content}>{': '}</TextContent>
        <TextContent style={styles.multiPriceText}>
          {currencyConvert(item.price)}
        </TextContent>
      </HorizontalView>
    );
  });
};

export default RelatedItem = ({
  style,
  itemName,
  image,
  priceAlfter,
  priceBefore,
  isSale,
  salePercent,
  itemType,
  variants,
  ...props
}) => (
  <View style={styles.parentCard}>
    <View style={styles.imageContainer}>
      <Image
        style={styles.image}
        source={{
          uri: !image.isEmpty()
            ? image
            : 'https://calories-info.com/site/assets/files/1182/persymona-hurma-hebanek.650x0.jpg',
        }}
      />
      {salePercent.toString().localeCompare('0%') === 0 ? (
        <View />
      ) : (
        <View style={styles.saleBanner}>
          <TextContent style={styles.saleTextBanner}>{salePercent}</TextContent>
        </View>
      )}
    </View>
    <View {...props} style={styles.container}>
      <HorizontalView>
        <HorizontalView style={styles.code}>
          <TextContent
            size="little-smaller"
            numberOfLines={1}
            style={{fontWeight: 'bold'}}>
            {itemName}
          </TextContent>
        </HorizontalView>
      </HorizontalView>
      <HorizontalView style={styles.price}>
        <TextContent size="little-smaller" style={styles.codeContent}>
          {currencyConvert(priceAlfter)}
        </TextContent>
      </HorizontalView>

      {priceBefore.toString() !== '0' && (
        <HorizontalView>
          <TextContent style={styles.contentBefore}>
            {currencyConvert(priceBefore)}
          </TextContent>
          {salePercent.toString().localeCompare('0%') === 0 ? (
            <View />
          ) : (
            <HorizontalView>
              <SvgIcon
                style={styles.icon}
                width={8}
                height={8}
                svgIcon={isSale ? ICON_DOWN : ICON_UP}
              />
              <TextContent style={isSale ? styles.textDown : styles.textUp}>
                {salePercent}
              </TextContent>
            </HorizontalView>
          )}
        </HorizontalView>
      )}

      <HorizontalView style={{marginTop: 2}}>
        <View>{renderPrice(variants)}</View>
      </HorizontalView>
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 6,
  },
  code: {
    flex: 1,
  },
  codeContent: {
    color: BUTTON_LOGIN_COLOR,
    fontWeight: 'bold',
    fontSize: 14,
  },
  content: {
    color: TEXT_GRAY_LIGHT_COLOR,
    fontSize: 12,
    alignSelf: 'center',
  },
  contentBefore: {
    color: TEXT_GRAY_LIGHT_COLOR,
    fontSize: 12,
    textDecorationLine: 'line-through',
  },
  image: {
    width: '100%',
    height: 150,
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
  },
  imageContainer: {
    alignItems: 'center',
  },
  parentCard: {
    backgroundColor: 'white',
    borderRadius: 12,
    marginVertical: 6,
    marginHorizontal: 6,
    height: 280,
    width: WIDTH_SCREEN / 2 - 20,
  },

  price: {
    marginTop: 5,
  },
  icon: {
    alignSelf: 'center',
    marginLeft: 10,
  },
  textUp: {
    fontWeight: 'bold',
    fontSize: 12,
    color: BUTTON_LOGIN_COLOR,
    alignSelf: 'center',
  },
  textDown: {
    fontWeight: 'bold',
    fontSize: 12,
    color: '#049347',
    alignSelf: 'center',
  },
  multiPrice: {
    alignContent: 'flex-start',
  },
  multiPriceText: {
    fontSize: 12,
    fontWeight: 'bold',
    color: TEXT_GRAY_LIGHT_COLOR,
  },
  saleBanner: {
    backgroundColor: BUTTON_LOGIN_COLOR,
    position: 'absolute',
    alignSelf: 'flex-end',
    width: 42,
    height: 18,
    borderTopRightRadius: 12,
    borderBottomLeftRadius: 12,
  },
  saleTextBanner: {
    fontWeight: 'bold',
    fontSize: 12,
    color: 'white',
    alignSelf: 'center',
    marginTop: 1,
  },
});
