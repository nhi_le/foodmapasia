import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {
  COLOR_READ,
  COLOR_UNREAD,
  LIGHT_GRAY_COLOR,
  ACCENT_COLOR,
  WHITE_COLOR,
} from '../../../../resources/palette';
const NotificationItem = ({
  style,
  title,
  type,
  date,
  content,
  isRead = false,
  ...props
}) => {
  return (
    <View {...props}>
      <View style={styles.container}>
        <View style={!isRead ? styles.pointUnRead : styles.viewRead} />
        <View style={styles.styleShowNotice}>
          <Text style={isRead ? styles.tittleIsReaded : styles.tittle}>
            {title}
          </Text>
          <Text
            style={[styles.date, {color: isRead ? COLOR_READ : COLOR_UNREAD}]}>
            {date}
          </Text>
          <Text
            style={[
              styles.content,
              {color: isRead ? COLOR_READ : COLOR_UNREAD},
            ]}>
            {content}
          </Text>
        </View>
      </View>
      <View style={styles.line} />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: WHITE_COLOR,
    padding: 12,
    flexDirection: 'row',
    marginStart: 5,
  },
  styleShowNotice: {
    marginStart: 10,
  },
  tittleIsReaded: {
    marginBottom: 4,
    fontWeight: 'bold',
    fontSize: 14,
    color: COLOR_READ,
  },
  tittle: {
    marginBottom: 4,
    fontWeight: 'bold',
    fontSize: 14,
    color: ACCENT_COLOR,
  },
  date: {
    marginBottom: 4,
    fontSize: 12,
  },
  content: {
    fontSize: 14,
  },
  pointUnRead: {
    marginTop: 4,
    height: 8,
    width: 8,
    borderRadius: 4,
    backgroundColor: 'red',
  },
  line: {
    backgroundColor: LIGHT_GRAY_COLOR,
    borderRadius: 1,
    height: 1,
    marginStart: 12,
    marginEnd: 12,
  },
  viewRead: {
    height: 8,
    width: 8,
  },
});
export default NotificationItem;
