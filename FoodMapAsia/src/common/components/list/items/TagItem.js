import React from 'react';
import {View, StyleSheet} from 'react-native';
import {WHITE_COLOR, BUTTON_LOGIN_COLOR} from '../../../../resources/palette';
import TextContent from '../../text/TextContent';

export default TagItem = ({
  style,
  label,
  tintColor = WHITE_COLOR,
  isSelected,
  ...props
}) => (
  <View
    {...props}
    style={[isSelected ? styles.containerSelected : styles.container, style]}>
    <TextContent style={isSelected ? styles.labelSelected : styles.label}>
      {label}
    </TextContent>
  </View>
);

const styles = StyleSheet.create({
  container: {
    padding: 6,
    marginHorizontal: 5,
  },
  containerSelected: {
    borderBottomColor: BUTTON_LOGIN_COLOR,
    borderBottomWidth: 2,
    padding: 6,
    marginHorizontal: 5,
  },
  label: {
    color: BUTTON_LOGIN_COLOR,
    opacity: 0.5,
  },
  labelSelected: {
    color: BUTTON_LOGIN_COLOR,
    fontWeight: 'bold',
  },
});
