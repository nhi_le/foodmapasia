/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import Image from '../../../../common/components/image/Image';
import {
  TEXT_GRAY_LIGHT_COLOR,
  BUTTON_LOGIN_COLOR,
  WHITE_COLOR,
} from '../../../../resources/palette';
import TextContent from '../../text/TextContent';
import Input from '../../input/Input';
import HorizontalView from '../../layout/HorizontalView';
import SvgIcon from '../../icon/SvgIcon';
import {
  ICON_INCREASE,
  ICON_DECREASE,
} from '../../../../../src/resources/images';
import {currencyConvert} from '@common/components/StringHelper';

export default ProductBottomSheetItem = ({
  style,
  id,
  itemName,
  image,
  count,
  isDiscount,
  discount,
  price,
  priceBefore,
  onActionPress,
  ...props
}) => {

  return (
    <View style={styles.parentCard}>
      <HorizontalView>
        <View style={styles.imageContainer}>
          <Image style={styles.image} source={{uri: image}} />
          <View style={styles.saleBanner}>
            <TextContent style={styles.saleTextBanner}>{discount}</TextContent>
          </View>
        </View>
        <View {...props} style={[styles.container]}>
          <TextContent numberOfLines={2} style={{fontWeight: 'bold'}}>
            {itemName}
          </TextContent>
          <HorizontalView style={{marginTop: 10}}>
            <View style={styles.priceContainer}>
              <TextContent size={'medium'} style={styles.codeContent}>
                {currencyConvert(price)}
              </TextContent>
            </View>

            <View style={styles.bottomView}>
              <HorizontalView>
                <TouchableOpacity
                  style={styles.button}
                  onPress={() => {
                    onActionPress && onActionPress('decrease', id);
                  }}>
                  <SvgIcon
                    style={styles.button}
                    width={28}
                    height={28}
                    svgIcon={ICON_DECREASE}
                  />
                </TouchableOpacity>
                <View style={{alignSelf: 'center', marginBottom: 5}}>
                  <Input
                    keyboardType={'numeric'}
                    inputStyle={styles.amountContainer}
                    onChangeText={(text) => {
                      onActionPress && onActionPress('inputValue', id, text);
                    }}>
                    {count}
                  </Input>
                </View>
                <TouchableOpacity
                  style={styles.button}
                  onPress={() => {
                    onActionPress && onActionPress('increase', id);
                  }}>
                  <SvgIcon
                    width={28}
                    height={28}
                    style={styles.button}
                    svgIcon={ICON_INCREASE}
                  />
                </TouchableOpacity>
              </HorizontalView>
            </View>
          </HorizontalView>
        </View>
      </HorizontalView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
  },
  code: {},
  codeContent: {
    color: BUTTON_LOGIN_COLOR,
    fontWeight: 'bold',
    fontSize: 14,
  },
  content: {
    color: TEXT_GRAY_LIGHT_COLOR,
    fontSize: 12,
    alignSelf: 'center',
  },
  contentBold: {
    color: TEXT_GRAY_LIGHT_COLOR,
    fontSize: 12,
    fontWeight: 'bold',
  },
  contentBefore: {
    color: TEXT_GRAY_LIGHT_COLOR,
    marginLeft: 5,
    fontSize: 12,
    textDecorationLine: 'line-through',
  },
  amountItem: {
    color: TEXT_GRAY_LIGHT_COLOR,
    fontSize: 16,
  },
  image: {
    height: 70,
    width: 70,
    borderRadius: 12,
  },

  parentCard: {
    backgroundColor: 'white',
    marginVertical: 10,
    height: 80,
  },

  amountContainer: {
    textAlign: 'center',
    width: 60,
    //  height: Platform.OS === 'ios' ? 30 : null,
    borderRadius: 5,
    marginHorizontal: 2,
    borderColor: BUTTON_LOGIN_COLOR,
    backgroundColor: WHITE_COLOR,
    alignItems: 'center',
  },
  priceContainer: {
    marginVertical: 10,
    flex: 1,
  },
  parentView: {
    paddingLeft: 5,
    flexDirection: 'column',
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
  },
  button: {
    marginHorizontal: 1,
    alignSelf: 'center',
  },
  bottomView: {
    justifyContent: 'flex-end',
  },
  promotion: {
    marginLeft: 5,
  },
  saleBanner: {
    backgroundColor: BUTTON_LOGIN_COLOR,
    position: 'absolute',
    alignSelf: 'flex-end',
    width: 42.19,
    height: 18.21,
    borderTopRightRadius: 12,
    borderBottomLeftRadius: 12,
  },
  saleTextBanner: {
    fontWeight: 'bold',
    fontSize: 12,
    color: 'white',
    alignSelf: 'center',
    margin: 2,
  },
});
