/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import Image from '../../../../common/components/image/Image';
import {
  TEXT_GRAY_LIGHT_COLOR,
  BUTTON_LOGIN_COLOR,
} from '../../../../resources/palette';
import TextContent from '../../text/TextContent';
import Input from '../../input/Input';
import HorizontalView from '../../layout/HorizontalView';
import SvgIcon from '../../icon/SvgIcon';
import {
  ICON_INCREASE,
  ICON_DECREASE,
  ICON_CANCEL,
} from '../../../../../src/resources/images';
import {currencyConvert} from '@common/components/StringHelper';
export default CartItem = ({
  style,
  id,
  itemName,
  image,
  count,
  isDiscount,
  discount,
  price,
  onActionPress,
  ...props
}) => (
  <View style={styles.parentCard}>
    <HorizontalView>
      <View style={styles.imageContainer}>
        <Image style={styles.image} source={{uri: image}} />
        {discount && discount.toString().localeCompare('0%') === 0 ? (
          <View></View>
        ) : (
          <View style={styles.saleBanner}>
            <TextContent style={styles.saleTextBanner}>{discount}</TextContent>
          </View>
        )}
      </View>

      <View {...props} style={[styles.container]}>
        <HorizontalView>
          <HorizontalView style={styles.code}>
            <View style={{flex: 10}}>
              <TextContent
                numberOfLines={2}
                style={{fontWeight: 'bold', fontSize: 14}}>
                {itemName}
              </TextContent>
            </View>
            <View style={{flex: 1}}>
              <TouchableOpacity
                style={{
                  height: 30,
                  width: 30,
                }}
                onPress={() => {
                  onActionPress && onActionPress('remove', id);
                }}>
                <SvgIcon width={12} height={12} svgIcon={ICON_CANCEL} />
              </TouchableOpacity>
            </View>
          </HorizontalView>
        </HorizontalView>
        <View>
          <View style={styles.priceContainer}>
            <HorizontalView>
              <TextContent size={'medium'} style={styles.codeContent}>
                {currencyConvert(price)}
              </TextContent>
            </HorizontalView>
          </View>
        </View>
        <View style={styles.bottomView}>
          <HorizontalView
            style={{
              alignSelf: 'flex-start',
              position: 'absolute',
              bottom: 0,
              alignItems: 'center',
              alignContent: 'center',
            }}>
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                onActionPress && onActionPress('decrease', id);
              }}>
              <SvgIcon
                style={styles.button}
                width={Platform.OS === 'ios' ? 25 : 33}
                height={Platform.OS === 'ios' ? 25 : 33}
                svgIcon={ICON_DECREASE}
              />
            </TouchableOpacity>
            <View style={{alignSelf: 'center'}}>
              <Input
                keyboardType={'numeric'}
                inputStyle={styles.amountContainer}
                onChangeText={(text) => {
                  onActionPress(text, id);
                }}>
                {count}
              </Input>
            </View>
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                onActionPress && onActionPress('increase', id);
              }}>
              <SvgIcon
                width={Platform.OS === 'ios' ? 25 : 33}
                height={Platform.OS === 'ios' ? 25 : 33}
                style={styles.button}
                svgIcon={ICON_INCREASE}
              />
            </TouchableOpacity>
          </HorizontalView>
        </View>
      </View>
    </HorizontalView>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginLeft: 15,
    marginTop: 8,
    marginBottom: 10,
  },
  code: {
    flex: 1,
  },
  codeContent: {
    color: BUTTON_LOGIN_COLOR,
    fontWeight: 'bold',
    fontSize: 16,
  },
  content: {
    color: TEXT_GRAY_LIGHT_COLOR,
    fontSize: 12,
    alignSelf: 'center',
  },
  contentBold: {
    color: TEXT_GRAY_LIGHT_COLOR,
    fontSize: 12,
    fontWeight: 'bold',
  },
  contentBefore: {
    color: TEXT_GRAY_LIGHT_COLOR,
    marginLeft: 5,
    fontSize: 12,
    textDecorationLine: 'line-through',
  },
  image: {
    height: 106,
    width: 106,
  },

  parentCard: {
    backgroundColor: 'white',
    marginVertical: 5,
    height: 130,
  },

  amountContainer: {
    textAlign: 'center',
    width: Platform.OS === 'ios' ? 50 : 60,
    height: Platform.OS === 'ios' ? 25 : 39,
    borderRadius: 5,
    marginHorizontal: 2,
    borderColor: BUTTON_LOGIN_COLOR,
    backgroundColor: 'white',
    alignItems: 'center',
    marginBottom: 5,
  },
  priceContainer: {
    marginTop: 3,
  },
  parentView: {
    paddingLeft: 10,
    flexDirection: 'column',
    flex: 1,
    justifyContent: 'flex-end',
  },
  button: {},
  bottomView: {
    flex: 1,
    alignContent: 'center',
    justifyContent: 'flex-end',
  },
  promotion: {
    marginLeft: 5,
  },
  saleBanner: {
    backgroundColor: BUTTON_LOGIN_COLOR,
    position: 'absolute',
    alignSelf: 'flex-end',
    width: 42,
    height: 18,
    borderTopRightRadius: 12,
    borderBottomLeftRadius: 12,
  },
  saleTextBanner: {
    fontWeight: 'bold',
    fontSize: 12,
    color: 'white',
    alignSelf: 'center',
  },
  imageContainer: {
    marginLeft: 12,
    marginTop: 10,
    marginBottom: 12,
  },
});
