import ImagePicker from 'react-native-image-picker';
import ImageCropPicker from 'react-native-image-crop-picker';
import ImageResizer from 'react-native-image-resizer';

const defaultOptions = {
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
  maxFiles: 10,
  mediaType: 'photo',
  maxWidth: 800,
  maxHeight: 800,
  quality: 80,
};

export const showImagePicker = async (options) => {
  return new Promise((resolve) => {
    ImagePicker.showImagePicker(
      {...defaultOptions, ...options, quality: undefined},
      (response) => {
        if (response.didCancel) {
          resolve(null);
          return;
        } else if (response.error) {
          resolve(null);
          return;
        } else if (response.customButton) {
          resolve(null);
          return;
        } else {
          const source = {uri: response.uri};
          resolve(source);
          return;
        }
      },
    );
  });
};

export const showImageCropPicker = async (options) => {
  let response = await ImageCropPicker.openPicker({
    ...defaultOptions,
    ...options,
  });

  if (response instanceof Array) {
    let resizedImages = await Promise.all(
      response.map((x) => resizeImage(x.path)),
    );
    return resizedImages;
  }
  if (response) {
    let resizeImage = await resizeImage(response.path);
    return resizeImage;
  }
  return null;
};

export const resizeImage = async (uri) => {
  try {
    let options = defaultOptions;
    let response = await ImageResizer.createResizedImage(
      uri,
      options.maxWidth,
      options.maxHeight,
      'JPEG',
      options.quality,
    );
    return {uri: response.uri};
  } catch (ex) {
    return null;
  }
};
