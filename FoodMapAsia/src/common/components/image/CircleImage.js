import React from 'react';
import { StyleSheet } from 'react-native';
import Image from './Image';

export default CircleImage = ({
    style,
    source,
    size = 50,
    ...props
}) => (
        <Image
            {...props}
            source={source}
            style={[
                styles.container,
                { width: size, height: size, borderRadius: size / 2 },
                style
            ]}
        />
    );

const styles = StyleSheet.create({
    container: {
    }
});