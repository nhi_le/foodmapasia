import React from 'react';
import {StyleSheet} from 'react-native';
import FastImage from 'react-native-fast-image';

export default Image = ({style, source, ...props}) => (
  <FastImage
    {...props}
    source={source}
    style={StyleSheet.compose(styles.container, style)}
  />
);

const styles = StyleSheet.create({
  container: {},
});
