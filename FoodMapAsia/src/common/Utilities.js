import {PERMISSIONS, RESULTS, check, request} from 'react-native-permissions';
import {Platform} from 'react-native';
import appOperations from '@redux/app/operations';

export const checkAndRequestPermission = async (dispatch) => {
  let permissions =
    Platform.OS === 'ios'
      ? [PERMISSIONS.IOS.CAMERA, PERMISSIONS.IOS.PHOTO_LIBRARY]
      : [PERMISSIONS.ANDROID.CAMERA, PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE];
  let permissionsAccepted = [];
  var permissionBlocked = 0;

  for (const permission of permissions) {
    await check(permission).then(async (result) => {
      switch (result) {
        case RESULTS.GRANTED:
          permissionsAccepted = [...permissionsAccepted, permission];
          break;
        case RESULTS.DENIED:
          await request(permission).then((requestResult) => {
            if (requestResult === RESULTS.GRANTED) {
              permissionsAccepted = [...permissionsAccepted, permission];
            }
          });
          break;
        case RESULTS.BLOCKED:
          permissionBlocked++;
          break;
      }
    });
  }

  if (permissionBlocked == 2 || permissionsAccepted.length < 2) {
    dispatch(appOperations.setPermissionModalVisible(true));
  }

  return permissionsAccepted;
};
