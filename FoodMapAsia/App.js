import React, {Component} from 'react';
import {createStore, applyMiddleware} from 'redux';
import reducers from './src/redux/index';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import {Platform} from 'react-native';
import ApplicationContainer from './src/ApplicationContainer';

const store = createStore(reducers, applyMiddleware(thunk));

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {ready: Platform.OS === 'ios'};
  }

  async componentDidMount() {
    if (Platform.OS !== 'ios') {
      setTimeout(() => {
        this.setState({ready: true});
      }, 1000);
    }
  }

  render() {
    const {ready} = this.state;
    return (
      <Provider store={store}>
        <ApplicationContainer ready={ready} />
      </Provider>
    );
  }
}

export default App;
